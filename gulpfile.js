"use strict";

var path = require("path");
var gulp = require("gulp");
var replace = require("gulp-string-replace");
var fs = require("fs");

var paths={
    dist:"dist"
}

gulp.task("change-html", function() {
  return gulp
    .src([path.join(paths.dist, "/index.html")]) // Any file globs are supported
    .pipe(replace(new RegExp("runtime", "g"), "workspaces/runtime"))
    .pipe(gulp.dest("."));
});
