export interface Badges {
    id: string;
    badgeTitle: string;
    iconUrl: string;
    description: string;
    progress: number;
    number_of_levels: string;
    current_level: number;
}
