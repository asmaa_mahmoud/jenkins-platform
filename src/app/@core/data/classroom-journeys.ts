export interface ClassroomJourneys {
    id: string;
    name: string;
    image: string;
    noOfAdventures: string;
    fromGrade: string;
    toGrade: string;
    XP: string;
    coins: string;
}
