export interface Classroom {
    id: string;
    name: string;
    image: string;
    noOfCourses: string;
    gradeName: string;
    classroomCode: string;
    numberOfStudents: string;
}
