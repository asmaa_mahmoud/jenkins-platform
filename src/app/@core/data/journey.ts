import { Observable } from 'rxjs';

export interface Journey {
  course_id?: string;
  name: string;
  image: string;
  views?: string;
  stars?: string;
  isLiked?: boolean;
  createdBy?: string;
  createDate?: string;
  likes?: string;
  id: string;
  fromGrade: string;
  toGrade: string;
  adventure_type?: string;
  noOfAdventures: string;
  coins: string;
  XP: string;
}



export abstract class UserData {
  abstract getJourneys(): Observable<Journey[]>;
}
