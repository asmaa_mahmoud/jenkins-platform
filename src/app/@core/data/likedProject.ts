export interface LikedProject {
    id: string;
    name: string;
    image: string;
    noOfLikes: number;
    noOfViews: number;
    createdBy: string;
    createdAt?: string;
    isLiked: boolean;
}
