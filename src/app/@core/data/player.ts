import { Observable } from 'rxjs';

export interface Player {
  id: number;
  name: string;
  image: string;
  noOfProjects: number;
  noOfLikes: number;
  badges: {
    id: number;
    name: string;
    iconurl: string;
  }[];
}

export abstract class UserData {
  abstract getPlayers(): Observable<Player[]>;
}
