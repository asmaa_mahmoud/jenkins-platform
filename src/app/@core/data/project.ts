export interface Project {
    id: string;
    name: string;
    image: string;
    isLiked: boolean;
    noOfLikes: number;
    noOfViews: number;
    creationDate: string;
    isPublic?: number;
}
