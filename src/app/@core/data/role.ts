export const Role = {
    'individualStudent': ['coding_ville_individual', 'coding_ville_individual-ca'],
    'Student': ['coding_ville_student', 'coding_ville_student-ca'],
    'Teacher': ['super_teacher'],
    'Admin': ['product_admin', 'admin'],
};
