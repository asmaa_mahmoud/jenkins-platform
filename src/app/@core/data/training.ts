export interface Training {
    id?: string;
    name?: string;
    image?: string;
    progress?: string;
    noOfAdventures?: string;
    noOfMissions?: string;
    fromGrade?: string;
    toGrade?: string;
    score?: Score;
    difficulty;
    totalCoins;
    totalXps;
}

interface Score {
    coins?: string;
    xp?: string;
}
