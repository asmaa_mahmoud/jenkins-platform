import { Role } from './role';

export interface User {
  username: any;
  id: string;
  name: string;
  image: string;
  role: string;
  token?: string;
  terms_agreement?: boolean;
}

