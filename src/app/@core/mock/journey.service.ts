import { Injectable } from '@angular/core';
import { of as observableOf, Observable } from 'rxjs';
import { Journey } from '../data/journey';

@Injectable({
  providedIn: 'root',
})
export class JourneyService {

  constructor() { }

  private journeys: Journey[] = [
    {
      name: 'First Journey First JoFirst JourneyurneyFirst JourneyFirst Journey', id: '1213',
      image: 'https://picsum.photos/500',
      fromGrade: '2', toGrade: '3', noOfAdventures: '33', coins: '212', XP: '142',
    },
    {
      name: 'Second Journey', id: '13213',
      image: 'https://picsum.photos/800',
      fromGrade: '3', toGrade: '4', noOfAdventures: '23', coins: '222', XP: '132',
    },
    {
      name: 'Third Journey', id: '4444',
      image: 'https://picsum.photos/200',
      fromGrade: '1', toGrade: '3', noOfAdventures: '3123123', coins: '225151', XP: '1123232',
    },
  ];


  getJourneys(): Observable<any> {
    return observableOf(this.journeys);
  }
}
