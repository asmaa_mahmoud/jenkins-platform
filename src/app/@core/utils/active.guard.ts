import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Role } from '../data/role';

@Injectable({ providedIn: 'root' })
export class ActiveGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const isActivated = this.authenticationService.isActivated();
        if (isActivated) {
            return true;
        }
        if (
          Role['Student'].includes(this.authenticationService.getUserRole()) ||
          Role['individualStudent'].includes(this.authenticationService.getUserRole())
        ) {
          this.router.navigate(['student/dashboard'], { queryParams: { returnUrl: state.url } });
        } else if (Role['Teacher'].includes(this.authenticationService.getUserRole())) {
          this.router.navigate(['teacher/dashboard'], { queryParams: { returnUrl: state.url } });
        } else if (Role['Admin'].includes(this.authenticationService.getUserRole()))
          this.router.navigate(['admin/dashboard'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
