import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { env } from '../env/env';
import { environment } from '../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticationService } from './authentication.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private pendingHTTPRequests$ = new Subject<void>();


  constructor(private _authService: AuthenticationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userData = localStorage.AuthorizationData;
    const helper = new JwtHelperService();
    const isOrg = JSON.stringify(env.isOrg);
    const clientName = JSON.stringify(env.clientName);
    const ifAvtivateApi = (req.url).includes(environment.apiUrlPrefix + 'user/activation');
    const isResendApi = (req.url).includes(environment.apiUrlPrefix + 'coding-ville/confirmation/mail');

    if (isResendApi) {
      const headers = new HttpHeaders({
        'client-status': `{"client": ${clientName}, "is-org": ${isOrg}}`,
      });
      const req1 = req.clone({ headers });
      return next.handle(req1);
    }

    if (!ifAvtivateApi && userData) {
      const token = localStorage.getItem('satellizer_token'); // you probably want to store it in localStorage or something
      if (token) {
        const isExpired = helper.isTokenExpired(token);
        if (isExpired) {
          this._authService.logout();
          this.pendingHTTPRequests$.next();
        }
      }

      if (!token) {
        return next.handle(req);
      }

      const headers = new HttpHeaders({
        'Authorization': `Bearer ${token}`,
        'client-status': `{"client": ${clientName}, "is-org": ${isOrg}}`,
      });

      const req1 = req.clone({ headers });

      return next.handle(req1);
    } else {
      const headers = new HttpHeaders({
        'client-status': `{"client": ${clientName}, "is-org": ${isOrg}}`,
      });
      const req1 = req.clone({ headers });
      return next.handle(req1);
    }

  }

}
