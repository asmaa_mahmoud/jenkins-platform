import { PublishService } from './../../coding-ville/project/publish.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../data/users';
import { GlobalService } from './global.service';
import { Role } from '../data/role';
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public currentUserSubject: BehaviorSubject<User>;
  public studentClassroomIDSubject: BehaviorSubject<string>;
  public currentUser: Observable<User>;
  public studentClassroomID: Observable<string>;
  public user;
  public rootUser;
  public userStatusSubject: BehaviorSubject<string>;
  public userStatus: Observable<string>;

  constructor(private _globalService: GlobalService, private router: Router, private publishService: PublishService) {
    this.observeData();
    this.observeClassroomData();
  }

  observeData() {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('AuthorizationData')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.userStatusSubject = new BehaviorSubject<string>(JSON.parse(localStorage.getItem('userStatus')));
    this.userStatus = this.userStatusSubject.asObservable();
  }

  observeClassroomData() {
    this.studentClassroomIDSubject = new BehaviorSubject<string>(JSON.parse(localStorage.getItem('classroomID')));
    this.studentClassroomID = this.studentClassroomIDSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public get studentClassroomValue() {
    return this.studentClassroomIDSubject.value;
  }

  public set currentUserValue(NewValue: User) {
    localStorage.setItem('AuthorizationData', JSON.stringify(NewValue));
    this.observeData();
  }

  public set studentClassroomValue(NewValue) {
    localStorage.setItem('classroomID', JSON.stringify(NewValue));
    this.observeData();
  }

  public set userStatusValue(NewValue) {
    localStorage.setItem('userStatus', JSON.stringify(NewValue));
    this.observeClassroomData();
  }

  login(username: string, password: string) {
    return this._globalService.post(`login`, { username, password })
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('AuthorizationData', JSON.stringify(user.Object.user));
        localStorage.setItem('userStatus', JSON.stringify(user.Object.status));
        this.userStatusSubject.next(user.Object.status);
        if (Role['Student'].includes(user.Object.user.role))
          localStorage.setItem('classroomID', JSON.stringify(user.Object.classroomId));
        localStorage.setItem('satellizer_token', user.Object.token.token);
        this.currentUserSubject.next(user.Object.user);
        this.studentClassroomIDSubject.next(user.Object.classroomId);
        this.publishService.makeConnection(user.Object.user.id);
        return user.Object.user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.clear();
    this.router.navigate(['auth/login']);
    this.currentUserSubject.next(null);
    this.userStatusSubject.next(null);
    this.publishService.cutConnection();
  }

  isAuthorized(allowedRoles: string[]): boolean {
    this.currentUser.subscribe(x => this.user = x);
    // check if the list of allowed roles is empty, if empty, authorize the user to access the page
    const currentRole = this.user.role;
    if (!currentRole) {
      return false;
    }

    // check if the user roles is in the list of allowed roles, return true if allowed and false if not allowed
    return allowedRoles.includes(currentRole);
  }

  getUserRole(): string {
    this.currentUser.subscribe(x => this.rootUser = x);
    if (this.rootUser && this.rootUser !== null) {
      return this.rootUser.role;
    }
    return '';
  }

  isActivated() {
    return this.userStatusSubject.value === 'active' ? true : false;
  }

  isTermsAgreed() {
    let TermsAgreed;
    this.currentUser.subscribe(x => TermsAgreed = x.terms_agreement);
    return TermsAgreed;
  }

}
