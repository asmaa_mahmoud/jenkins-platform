import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanActivateChild,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { Role } from '../data/role';

@Injectable({
  providedIn: 'root',
})
export class AuthorizationGuard implements CanActivate, CanActivateChild {
  private currentRole;
  constructor(
    private _authService: AuthenticationService,
    private router: Router,
  ) {
    this._authService.currentUser.subscribe(x => this.currentRole = x);
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    const allowedRoles = next.data.allowedRoles;
    const isAuthorized = this._authService.isAuthorized(allowedRoles);

    if (!isAuthorized) {
      if (Role['Student'].includes(this.currentRole) || Role['individualStudent'].includes(this.currentRole))
        this.router.navigate(['student/dashboard']);
      else if (Role['Teacher'].includes(this.currentRole))
        this.router.navigate(['teacher/dashboard']);
      else if (Role['Admin'].includes(this.currentRole))
        this.router.navigate(['admin/dashboard']);
    }

    return isAuthorized;
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    const allowedRoles = next.data.allowedRoles;
    const isAuthorized = this._authService.isAuthorized(allowedRoles);

    if (!isAuthorized) {
      // if not authorized, show access denied message
      if (Role['Student'].includes(this.currentRole) || Role['individualStudent'].includes(this.currentRole))
        this.router.navigate(['student/dashboard']);
      else if (Role['Teacher'].includes(this.currentRole))
        this.router.navigate(['teacher/dashboard']);
      else if (Role['Admin'].includes(this.currentRole))
        this.router.navigate(['admin/dashboard']);
    }

    return isAuthorized;
  }
}
