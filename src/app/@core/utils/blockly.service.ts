import { ChangeDetectorService } from './../../coding-ville/project/change-detector.service';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
declare var Blockly: any;
@Injectable({
    providedIn: 'root',
})
export class BlocklyProvider {
    initialized: BehaviorSubject<boolean> = new BehaviorSubject(false);
    levelsInitialized: BehaviorSubject<boolean> = new BehaviorSubject(false);
    instructions: string;
    constructor(private changeDetectorService: ChangeDetectorService) {
    }

    zoom(value) {
        Blockly.getMainWorkspace().setScale(
            Blockly.getMainWorkspace().scale + value,
        );
    }

    center() {
        Blockly.getMainWorkspace().setScale(Blockly.getMainWorkspace().options.zoomOptions.startScale);
        Blockly.getMainWorkspace().scrollCenter();
    }

    setInstructions(instructions) {
        this.instructions = instructions;
        if (this.initialized.getValue()) {
            this.setWorkspace();
        }
    }

    setWorkspace() {
        if (this.instructions) {
            Blockly.Xml.domToWorkspace(Blockly.getMainWorkspace(), Blockly.Xml.textToDom(this.instructions));
            Blockly.getMainWorkspace().scrollCenter();
        }
    }

    getJSCode() {
        return Blockly.JavaScript.workspaceToCode(Blockly.getMainWorkspace());
    }

    setWorkSpaceAsChanged() {
        if (this.initialized.getValue() && this.instructions) {
            this.changeDetectorService.setProjectAsChanged();
        }
    }

}



