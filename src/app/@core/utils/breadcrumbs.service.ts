import { Injectable, Output, EventEmitter } from '@angular/core';
import { Breadcrumb } from '../data/breadcrumb';

@Injectable({
  providedIn: 'root',
})
export class BreadcrumbsService {

  public breadcrumbs: Breadcrumb[];
  @Output() breadcrumbsChanged = new EventEmitter<Breadcrumb[]>();

  constructor() {

  }

  getBreadcrumbs(breadcrumbs: Breadcrumb[]) {
    this.breadcrumbs = breadcrumbs;
    this.breadcrumbsChanged.emit(this.breadcrumbs);
  }

  returnBreadcrumbs() {
    return this.breadcrumbs;
  }

}
