import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { NbToastrService, NbComponentStatus, NbToastrConfig } from '@nebular/theme';
import { Injectable, Injector } from '@angular/core';
import { BreadcrumbsService } from './breadcrumbs.service';
import { Breadcrumb } from './../data/breadcrumb';
import { ConnectionService } from './connection.service';
import { HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { GoogleAnalyticsService } from 'ngx-google-analytics';
import { Title } from '@angular/platform-browser';
import { SurveyService } from './survey.service';

declare let ga: Function; // Declare ga as a function


@Injectable({
  providedIn: 'root',
})

export class GlobalService {

  widthBreakPoint = new BehaviorSubject(null);

  private _breadcrumbsService: BreadcrumbsService;
  private _connectionService: ConnectionService;

  constructor(
    private injector: Injector,
    private toastrService: NbToastrService,
    private toastr: ToastrService,
    public breakpointObserver: BreakpointObserver,
    private _translateService: TranslateService,
    protected $gaService: GoogleAnalyticsService,
    private titleService: Title,
    private surveyService: SurveyService,
  ) {
    this.getWidth();
  }

  public get breadcrumbsService(): BreadcrumbsService {
    if (!this._breadcrumbsService) {
      this._breadcrumbsService = this.injector.get(BreadcrumbsService);
    }
    return this._breadcrumbsService;
  }

  public get connectionService(): ConnectionService {
    if (!this._connectionService) {
      this._connectionService = this.injector.get(ConnectionService);
    }
    return this._connectionService;
  }

  getBreadcrumbs(breadcrumbs: Breadcrumb[]) {
    return this.breadcrumbsService.getBreadcrumbs(breadcrumbs);
  }

  acceptTermsOfUse() {
    const url = `coding-ville/agreement`;
    return this.put(url);
  }

  // SECTION Connection Service

  post(url: string, body?: any) {
    return this.connectionService.post(url, body);
  }

  get(url: string, params?: HttpParams) {
    return params ? this.connectionService.get(url, params) : this.connectionService.get(url);
  }
  put(url: string, body?: any) {
    return this.connectionService.put(url, body);
  }
  delete(url: string) {
    return this.connectionService.delete(url);
  }


  // SECTION  Toaster

  showToast(status: NbComponentStatus, msg, title, customIcon?) {
    const config: Partial<NbToastrConfig> = {
      status: status,
      duration: 8000,
    };
    if (customIcon) {
      config.icon = customIcon;
    }
    this.toastrService.show(msg, title, config);
  }

  showNgxToast(message, title, type, config = {}) {
    this.toastr.show(message, title, config, type);
  }


  getWidth() {
    this.breakpointObserver
      .observe(['(min-width: 1199.98px)', '(min-width: 991.98px)', '(min-width: 767.98px)', '(min-width: 575.98px)'])
      .subscribe((state: BreakpointState) => {
        const bearkpoinst = state.breakpoints;
        if (bearkpoinst['(min-width: 1199.98px)']) {
          this.widthBreakPoint.next('xlg');
        } else if (bearkpoinst['(min-width: 991.98px)']) {
          this.widthBreakPoint.next('lg');
        } else if (bearkpoinst['(min-width: 767.98px)']) {
          this.widthBreakPoint.next('md');
        } else if (bearkpoinst['(min-width: 575.98px)']) {
          this.widthBreakPoint.next('sm');
        } else {
          this.widthBreakPoint.next('xsm');
        }
      });
  }

  showTranslatedToast(status: NbComponentStatus, msg: string, title: string, customIcon?) {
    const sub = this._translateService.get(title).subscribe(
      translation => {
        this.showToast(status, msg, translation, customIcon);
      });

    setTimeout(() => {
      sub.unsubscribe();
    }, 1000);
  }

  // create our event emitter to send our data to Google Analytics
  public eventEmitter(eventCategory: string,
    eventAction: string,
    eventLabel: string = null,
    eventValue: number = null, currentRole) {
    if (environment.production) {
      ga('send', 'event', {
        eventCategory: eventCategory,
        eventAction: eventAction,
        eventLabel: eventLabel,
        eventValue: eventValue,
      });

      ga('set', 'dimension2', currentRole);
      ga('send', 'pageview', {
        'dimension2': currentRole,
      });
    }
  }

  setTitle(title) {
    const sub = this._translateService.get(title).subscribe(
      translation => {
        this.titleService.setTitle(translation);
      });
    sub.unsubscribe();
  }

  // Section survey
  getSurveyScript(key) {
    return this.surveyService.showSurvey(key);
  }
}
