import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Role } from '../data/role';

@Injectable({ providedIn: 'root' })
export class NotAuthenticatedGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        if (!currentUser) {
            return true;
        } else {
            if (Role['Student'].includes(currentUser.role) || Role['individualStudent'].includes(currentUser.role)) {
                this.router.navigate(['student']);
            } else if (Role['Teacher'].includes(currentUser.role)) {
                this.router.navigate(['teacher']);
            } else if (Role['Admin'].includes(currentUser.role)) {
                this.router.navigate(['admin']);
            }
        }
    }
}
