import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService, NbIconLibraries } from '@nebular/theme';

import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DashboardService } from '../../../../coding-ville/accounts/student/dashboard/dashboard.service';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { Router } from '@angular/router';
import { Role } from '../../../../@core/data/role';

@Component({
  selector: 'ngx-accounts-sidebar-bottom',
  styleUrls: ['./accounts-sidebar-bottom.component.scss'],
  templateUrl: './accounts-sidebar-bottom.component.html',
})
export class AccountsSidebarBottomComponent implements OnInit, OnDestroy {

  @Input('isActivated') isActivated: boolean;

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;
  badges: any[];

  isTeacher: boolean;


  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
  ];

  currentTheme = 'default';
  currentUser;

  isAdmin = false;

  userMenu = [{ title: 'Profile' }, { title: 'Log out' }];

  constructor(
    private _iconLibraries: NbIconLibraries,
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private _dashboardService: DashboardService,
    private _authService: AuthenticationService,
    private router: Router,
    private breakpointService: NbMediaBreakpointsService,
  ) {
  }

  ngOnInit() {
    this.initFn();
    this.getUserData();
  }

  getUserData() {
    const currentUser = JSON.parse(localStorage.getItem('AuthorizationData'));
    this.isTeacher = currentUser.role.includes('teacher') ? true : false;
  }

  initFn() {
    this.currentTheme = this.themeService.currentTheme;

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);

    /***********  Get current user data ************/
    this._authService.currentUser.subscribe(x => this.currentUser = x);

    /****************** Get Badges ******************/
    if ((Role['Student'].includes(this.currentUser.role) || Role['individualStudent'].includes(this.currentUser.role)) && this.isActivated)
      this.getBadges();

    // check if user is admin
    this.isAdmin = Role['Admin'].includes(this.currentUser.role) ? true : false;

    // register icons pack
    this._iconLibraries.registerSvgPack('custom-icons', {
      'home': '<svg xmlns="http://www.w3.org/2000/svg" width="18.4" height="16.655" viewBox="0 0 18.4 16.655" fill="currentColor"><g transform="translate(-2.802 -3.345)"><path d="M19,9.3V5a1,1,0,0,0-1-1H17a1,1,0,0,0-1,1V6.6l-3.33-3a1.008,1.008,0,0,0-1.34,0L2.97,11.13A.5.5,0,0,0,3.3,12H5v7a1,1,0,0,0,1,1H9a1,1,0,0,0,1-1V14h4v5a1,1,0,0,0,1,1h3a1,1,0,0,0,1-1V12h1.7a.5.5,0,0,0,.33-.87ZM10,10a2,2,0,0,1,4,0Z"/></g></svg>',
      'info': '<svg xmlns="http://www.w3.org/2000/svg" width="7.708" height="18.495" viewBox="0 0 7.708 18.495"><path id="Intersection_3" data-name="Intersection 3" d="M28.689,32.54a2.279,2.279,0,0,1-.735-1.743,6.718,6.718,0,0,1,.054-.841q.057-.428.177-.968L29.1,25.56c.081-.329.15-.642.206-.932a4.407,4.407,0,0,0,.081-.807,1.293,1.293,0,0,0-.255-.914,1.39,1.39,0,0,0-.978-.257,2.415,2.415,0,0,0-.727.115c-.247.08-.461.153-.636.224l.241-1.056q.9-.389,1.721-.664a4.818,4.818,0,0,1,1.552-.278,2.857,2.857,0,0,1,2.035.676,2.309,2.309,0,0,1,.715,1.755c0,.149-.017.411-.049.787a5.534,5.534,0,0,1-.183,1.034l-.911,3.418c-.074.274-.14.589-.2.939a5.306,5.306,0,0,0-.087.8,1.188,1.188,0,0,0,.287.929,1.538,1.538,0,0,0,.994.247,2.726,2.726,0,0,0,.753-.124,4.08,4.08,0,0,0,.608-.216l-.244,1.055c-.732.306-1.313.538-1.75.7a4.361,4.361,0,0,1-1.517.241A2.912,2.912,0,0,1,28.689,32.54Zm2.1-14.126a2.131,2.131,0,0,1,0-3.046,2.113,2.113,0,0,1,1.54-.634,2.171,2.171,0,0,1,2.171,2.16,2.057,2.057,0,0,1-.637,1.521,2.106,2.106,0,0,1-1.534.627A2.128,2.128,0,0,1,30.788,18.414Z" transform="translate(-26.791 -14.733)" fill="currentColor"/></svg>',
      'teacher': '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="23.804" viewBox="0 0 24 23.804"><g transform="translate(0 -1.995)"><path d="M211.447,5.656H198.689a.768.768,0,0,0-.768.768v5.418l.555-.649a1.767,1.767,0,0,1,.98-.578V7.192h11.222V17.223H199.457V15.543l-.346.4a1.783,1.783,0,0,1-1.19.614v1.428a.768.768,0,0,0,.768.768h4.143l-2.105,9.557a.794.794,0,0,0,1.551.342l2.18-9.9h1.267l2.18,9.9a.794.794,0,1,0,1.551-.342l-2.1-9.557h4.094a.768.768,0,0,0,.768-.768V6.424A.768.768,0,0,0,211.447,5.656Z" transform="translate(-188.215 -3.481)" fill="currentColor"/><path d="M175.315,93.472a.3.3,0,0,0-.384-.167l-4.523,1.6a.993.993,0,0,0-1.36.139l-1.435,1.676-1.273-1.091c.008.09.027.178.027.27v2.368l.708.607a.993.993,0,0,0,1.4-.108l1.709-2,1.378-.806,3.627-2.122A.3.3,0,0,0,175.315,93.472Z" transform="translate(-158.182 -86.812)" fill="currentColor"/><path d="M5.391,105.776H4.538a1.989,1.989,0,0,0-1.591.8L.33,108.986a.993.993,0,0,0-.078,1.4l2.135,2.383a.993.993,0,1,0,1.479-1.325L2.395,109.8l2.09-1.87-1.217,1.654,1.191,1.329a1.781,1.781,0,0,1-1.919,2.871s.009,4.593.009,9.512a1.191,1.191,0,1,0,2.383,0v-7.1h.795v7.1a1.97,1.97,0,0,1-.248.943,1.177,1.177,0,0,0,.72.248A1.191,1.191,0,0,0,7.388,123.3c0-4.917,0-2.856,0-15.525A2,2,0,0,0,5.391,105.776Z" transform="translate(0 -98.691)" fill="currentColor"/><path d="M55.137,6.37a2.321,2.321,0,0,0,3.382-2.053A2.322,2.322,0,1,0,55.137,6.37Z" transform="translate(-51.234)" fill="currentColor"/></g></svg>',
      'individual': '<svg xmlns="http://www.w3.org/2000/svg" width="18.376" height="24" viewBox="0 0 18.376 24"><g transform="translate(-52.032 0)"><g transform="translate(52.032 0)"><g transform="translate(0 0)"><path d="M68.555,255.423a1.773,1.773,0,0,0-1.011-.863l-2.753-1.268-1.323-1.115-2.125,2.114h0l.917,6.131a.081.081,0,0,1-.015.06l-.959,1.312a.081.081,0,0,1-.131,0l-.959-1.312a.081.081,0,0,1-.015-.06l.917-6.131h0l-2.125-2.114-1.323,1.115-2.753,1.268a1.789,1.789,0,0,0-1.011.863s-2.993,7.125-1.369,7.125H69.925C71.548,262.548,68.555,255.423,68.555,255.423Z" transform="translate(-52.032 -238.548)" fill="currentColor"/><path d="M93.233,7.107a.154.154,0,0,0,.138-.171q-.046-.395-.092-.789L93.214,5.6a.154.154,0,0,0-.166-.135h-.119l0-1.849.062-.023.208-.075a.444.444,0,0,0,0-.835L85.857.027a.444.444,0,0,0-.3,0L78.21,2.681a.444.444,0,0,0,0,.835l2.715.981a6.641,6.641,0,0,0-.271,2.069c.048,4.4,2.6,7.023,4.965,7.023,2.336,0,4.917-2.624,4.965-7.023a6.705,6.705,0,0,0-.254-2.013L92.3,3.84l0,1.623h-.119a.153.153,0,0,0-.166.135l-.154,1.34a.154.154,0,0,0,.033.114.151.151,0,0,0,.1.056m-6.387,5.414c-2.062,0-3.484-2.415-3.771-4.83C81.582,5.459,84.812,5.355,85,6.739c.058.425-.431.881-.375,1.057.273.867,4.8-2.831,4.767-.105C89.369,9.615,87.584,12.521,85.619,12.521Z" transform="translate(-76.518 0)" fill="currentColor"/></g></g></g></svg>',
      'training': '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><g id="Bounding_Box"><rect id="Rectangle_1204" data-name="Rectangle 1204" width="24" height="24" fill="none"/><rect id="Rectangle_1205" data-name="Rectangle 1205" width="24" height="24" fill="none"/></g><g id="Flat" transform="translate(1 4.5)"><path id="Path_3406" data-name="Path 3406" d="M17.5,4.5A9.3,9.3,0,0,0,12,6,9.3,9.3,0,0,0,6.5,4.5a10.819,10.819,0,0,0-4.28.79A2.037,2.037,0,0,0,1,7.14V18.42a2,2,0,0,0,2.48,1.94A12.35,12.35,0,0,1,6.5,20a10.574,10.574,0,0,1,4.56.92,2.055,2.055,0,0,0,1.87,0A10.43,10.43,0,0,1,17.49,20a12.35,12.35,0,0,1,3.02.36,1.986,1.986,0,0,0,2.48-1.94V7.14a2.037,2.037,0,0,0-1.22-1.85A10.738,10.738,0,0,0,17.5,4.5ZM21,17.23a1.01,1.01,0,0,1-1.2.98,12.609,12.609,0,0,0-2.3-.2,12.045,12.045,0,0,0-5.5,1.5V8a12.045,12.045,0,0,1,5.5-1.5,12.687,12.687,0,0,1,2.7.28,1.014,1.014,0,0,1,.8.98Z" transform="translate(-1 -4.5)" fill="currentColor"/><path id="Path_3407" data-name="Path 3407" d="M13.98,11.01a.749.749,0,0,1-.23-1.46,13.3,13.3,0,0,1,5.36-.45.75.75,0,1,1-.17,1.49,11.82,11.82,0,0,0-4.73.39A2.079,2.079,0,0,1,13.98,11.01Z" transform="translate(-1 -4.5)" fill="currentColor"/><path id="Path_3408" data-name="Path 3408" d="M13.98,13.67a.749.749,0,0,1-.23-1.46,13.287,13.287,0,0,1,5.36-.45.75.75,0,1,1-.17,1.49,11.82,11.82,0,0,0-4.73.39A.969.969,0,0,1,13.98,13.67Z" transform="translate(-1 -4.5)" fill="currentColor"/><path id="Path_3409" data-name="Path 3409" d="M13.98,16.33a.749.749,0,0,1-.23-1.46,13.287,13.287,0,0,1,5.36-.45.75.75,0,1,1-.17,1.49,11.82,11.82,0,0,0-4.73.39A.969.969,0,0,1,13.98,16.33Z" transform="translate(-1 -4.5)" fill="currentColor"/></g></svg>',
      'classroom': '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path id="Path_3399" data-name="Path 3399" d="M0,0H24V24H0Z" fill="none"/><path id="Path_3400" data-name="Path 3400" d="M4,11.5v4a1.5,1.5,0,0,0,3,0v-4a1.5,1.5,0,0,0-3,0Zm6,0v4a1.5,1.5,0,0,0,3,0v-4a1.5,1.5,0,0,0-3,0ZM3.5,22h16a1.5,1.5,0,0,0,0-3H3.5a1.5,1.5,0,0,0,0,3ZM16,11.5v4a1.5,1.5,0,0,0,3,0v-4a1.5,1.5,0,0,0-3,0ZM10.57,1.49,2.67,5.65A1.245,1.245,0,0,0,3.25,8H19.76a1.245,1.245,0,0,0,.57-2.35l-7.9-4.16a1.976,1.976,0,0,0-1.86,0Z" fill="currentColor"/></svg>',
      'projects': '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path id="Path_3404" data-name="Path 3404" d="M0,0H24V24H0Z" fill="none"/><path id="Path_3405" data-name="Path 3405" d="M20.5,11H19V7a2.006,2.006,0,0,0-2-2H13V3.5a2.5,2.5,0,0,0-5,0V5H4A2,2,0,0,0,2.01,7v3.8H3.5a2.7,2.7,0,0,1,0,5.4H2V20a2.006,2.006,0,0,0,2,2H7.8V20.5a2.7,2.7,0,1,1,5.4,0V22H17a2.006,2.006,0,0,0,2-2V16h1.5a2.5,2.5,0,0,0,0-5Z" fill="currentColor"/></svg>',
      'dashboard': '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path id="Path_3421" data-name="Path 3421" d="M0,0H24V24H0Z" fill="none"/><path id="Path_3422" data-name="Path 3422" d="M19,3H5A2.006,2.006,0,0,0,3,5V19a2.006,2.006,0,0,0,2,2H19a2.006,2.006,0,0,0,2-2V5A2.006,2.006,0,0,0,19,3ZM8,17a1,1,0,0,1-1-1V11a1,1,0,0,1,2,0v5A1,1,0,0,1,8,17Zm4,0a1,1,0,0,1-1-1V8a1,1,0,0,1,2,0v8A1,1,0,0,1,12,17Zm4,0a1,1,0,0,1-1-1V14a1,1,0,0,1,2,0v2A1,1,0,0,1,16,17Z" fill="currentColor"/></svg>',
      'journeys': '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path id="Path_3412" data-name="Path 3412" d="M0,0H24V24H0Z" fill="none"/><path id="Path_3413" data-name="Path 3413" d="M11.15,3.4,7.43,9.48A1,1,0,0,0,8.28,11h7.43a1,1,0,0,0,.85-1.52L12.85,3.4a.993.993,0,0,0-1.7,0Z" fill="currentColor"/><circle id="Ellipse_421" data-name="Ellipse 421" cx="4.5" cy="4.5" r="4.5" transform="translate(13 13)" fill="currentColor"/><path id="Path_3414" data-name="Path 3414" d="M4,21.5h6a1,1,0,0,0,1-1v-6a1,1,0,0,0-1-1H4a1,1,0,0,0-1,1v6A1,1,0,0,0,4,21.5Z" fill="currentColor"/></svg>',
      'badges': '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path id="Path_3652" data-name="Path 3652" d="M0,0H24V24H0Z" fill="none"/><path id="Path_3653" data-name="Path 3653" d="M12,1,3,5v6c0,5.55,3.84,10.74,9,12,5.16-1.26,9-6.45,9-12V5ZM10,17,6,13l1.41-1.41L10,14.17l6.59-6.59L18,9Z" fill="currentColor"/></svg>',
    });
  }


  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }

  goToEditProfile() {
    if (Role['Student'].includes(this.currentUser.role) || Role['individualStudent'].includes(this.currentUser.role)) {
      if (this.isActivated) this.router.navigate(['student/edit-profile']);
    } else if (Role['Teacher'].includes(this.currentUser.role)) {
      if (this.isActivated) this.router.navigate(['teacher/edit-profile']);
    } else if (Role['Admin'].includes(this.currentUser.role)) {
      this.router.navigate(['admin/edit-profile']);
    }
    document.querySelectorAll('nb-menu .menu-item a.active')[0] ?
      document.querySelectorAll('nb-menu .menu-item a.active')[0].classList.remove('active') : null;
  }

  goToOnBoardVideo() {
    if (Role['Student'].includes(this.currentUser.role) || Role['individualStudent'].includes(this.currentUser.role)) {
      if (this.isActivated) this.router.navigate(['student/on-board']);
    } else {
      if (this.isActivated) this.router.navigate(['teacher/on-board']);
    }
    document.querySelectorAll('nb-menu .menu-item a.active')[0] ?
      document.querySelectorAll('nb-menu .menu-item a.active')[0].classList.remove('active') : null;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  getBadges() {
    this._dashboardService.getBadges().subscribe((data: any) => {
      this.badges = data.Object;
    });
  }

  logout() {
    this._authService.logout();
    this.router.navigate(['/auth/login']);
  }



  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
