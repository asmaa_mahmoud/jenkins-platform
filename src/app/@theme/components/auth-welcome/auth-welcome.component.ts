import { env } from './../../../@core/env/env';
import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { AuthService } from '../../../coding-ville/auth/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../../@core/utils/authentication.service';
import { Role } from '../../../@core/data/role';

@Component({
  selector: 'ngx-auth-welcome',
  styleUrls: ['./auth-welcome.component.scss'],
  templateUrl: './auth-welcome.component.html',
})
export class AuthWelcomeComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  isOrg = env.isOrg;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          const userRole = data.role;
          if (Role['Student'].includes(userRole) || Role['individualStudent'].includes(userRole)) {
            this.router.navigate(['student/dashboard']);
          } else if (Role['Teacher'].includes(userRole)) {
            this.router.navigate(['teacher/dashboard']);
          } else if (Role['Admin'].includes(userRole)) {
            this.router.navigate(['admin/dashboard']);
          } else {
            this.router.navigate(['auth']);
          }
        },
        error => {
          console.log(error);
          this.error = error;
          this.loading = false;
        },
      );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
