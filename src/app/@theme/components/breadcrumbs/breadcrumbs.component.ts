import { Component, OnDestroy, OnInit, OnChanges } from '@angular/core';

import { Subject } from 'rxjs';
import { Breadcrumb } from '../../../@core/data/breadcrumb';
import { BreadcrumbsService } from './../../../@core/utils/breadcrumbs.service';

@Component({
  selector: 'ngx-breadcrumbs',
  styleUrls: ['./breadcrumbs.component.scss'],
  templateUrl: './breadcrumbs.component.html',
})
export class BreadcrumbsComponent implements OnInit, OnDestroy, OnChanges {

  breadcrumbs: Breadcrumb[];

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  constructor(private breadcrumbsService: BreadcrumbsService) {
  }

  ngOnInit() {
    this.breadcrumbs = this.breadcrumbsService.returnBreadcrumbs();
    this.breadcrumbsService.breadcrumbsChanged.subscribe(
      (breadcrumbs: Breadcrumb[]) => {
        this.breadcrumbs = breadcrumbs;
      },
    );
  }

  ngOnChanges() {
    this.breadcrumbs = this.breadcrumbsService.returnBreadcrumbs();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
