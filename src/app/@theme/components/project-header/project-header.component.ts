import { NbDialogService } from '@nebular/theme';
import { ChangeDetectorService } from './../../../coding-ville/project/change-detector.service';
import { ProjectSpaceService } from './../../../coding-ville/project/project-space/project-space.service';
import { Location } from '@angular/common';
import { AuthenticationService } from './../../../@core/utils/authentication.service';
import { GlobalService } from './../../../@core/utils/global.service';
import { PublishService } from '../../../coding-ville/project/publish.service';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { ProjectService } from '../../../coding-ville/project/project.service';
import { Router, ActivatedRoute } from '@angular/router';
import { env } from '../../../@core/env/env';

@Component({
  selector: 'ngx-project-header',
  styleUrls: ['./project-header.component.scss'],
  templateUrl: './project-header.component.html',
})
export class ProjectHeaderComponent implements OnInit, OnDestroy {
  @ViewChild('finishedModal', { static: false }) finishedModal;
  @ViewChild('saveModal', { static: false }) saveModal;
  @ViewChild('publishModal', { static: false }) publishModal;


  private destroy$: Subject<void> = new Subject<void>();
  currentUser;

  isOrg = env.isOrg;
  userMenu = [{ title: 'Profile' }, { title: 'Log out' }];
  projectName: string;
  projectId: number;
  notification;

  // isPublish;
  isTeacher;
  isAnswerChecking;
  isLoading = false;

  isJourny;
  userXp;
  timer;
  finalTimeInSec;

  private subscriptions = new Subscription();

  interval: NodeJS.Timer;
  isProjectPublished;
  breakPoint: any;
  isNavExpanded = false;
  // Change Detector for save
  hasChanges: boolean;
  constructor(
    private projectService: ProjectService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private publishService: PublishService,
    private _globalService: GlobalService,
    private _authService: AuthenticationService,
    private _location: Location,
    private _projectSpaceService: ProjectSpaceService,
    private _dialogService: NbDialogService,
    private _changeDetectorService: ChangeDetectorService,
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getBreakPoint();
    this.subscribeChangeDetector();
  }

  getBreakPoint() {
    this._globalService.widthBreakPoint.subscribe(
      res => {
        this.breakPoint = res;
        if (this.isNavExpanded) {
          this.toggleNav();
        }
      });
  }

  subscribeChangeDetector() {
    const url = this._location.path();
    if (!url.includes('create')) {
      const sub = this._changeDetectorService.projectHasChangesForSave.subscribe(value => {
        this.hasChanges = value;
      });
      this.subscriptions.add(sub);
    } else {
      this.hasChanges = true;
    }
  }

  getData() {

    /***********  Get current user data ************/
    this.getUserData();

    const sub = this.projectService.projectName.subscribe(name => this.projectName = name);
    this.subscriptions.add(sub);

    if (!this.projectName) {
      this.projectService.updateSelectedProject('My Awesome Game');
    }

    this.activatedRoute.params.subscribe(params => {
      if (params.id) { this.projectId = params.id; }
      if (params.studentId) { this.isAnswerChecking = true; }
    });

    const sub2 = this.publishService.getNotification().subscribe(
      data => {
        if (data) {
          this.notification = data;
        }
      });

    this.subscriptions.add(sub2);

  }

  getUserData() {
    this.currentUser = JSON.parse(localStorage.getItem('AuthorizationData'));
    this.isTeacher = this.currentUser.role.includes('teacher');
    this.checkUrl();
  }

  checkUrl() {

    if (this.router.url.includes('journey')) {
      this.isJourny = true;
      if (!this.isTeacher) {
        this.toggleTimer(true);
        this.projectService.getUserScore().subscribe(res => {
          this.userXp = res.Object.xp;
        });
      }
    } else {
      this.isJourny = false;

      const sub = this.publishService.isProjectPublished.subscribe(
        res => {
          this.isProjectPublished = res;
          if (res == null || (this.router.url.includes('create') && res !== 0)) {
            this.publishService.isProjectPublished.next(0);
          }

        });

      this.subscriptions.add(sub);
    }
  }



  save(asACopy, modalRef) {
    let projectId;
    if (asACopy) {
      projectId = null;
      this.projectName += '_Copy';
    } else {
      projectId = this.projectId;
    }
    this.isLoading = true;
    this.projectService.saveProject(this.projectName, projectId).subscribe(
      data => {
        if (data) {
          this.isLoading = false;
          this._changeDetectorService.setNoChangesForSave();
          if (modalRef) {
            modalRef.close();
            this.goToPreviousPage();
          } else {

            if (asACopy) {
              this._globalService.showTranslatedToast('info', null, 'project-copied', 'copy-outline');
            } else {
              this._globalService.showTranslatedToast('success', null, 'project-saved-successfully');
            }
            this.projectId = data.Object.id;

            if (this.router.url.includes('remix')) {
              this.publishService.isProjectPublished.next(0);
            }
            this._location.go(`/project-game/${this.projectId}`);


          }


        }
      }, error => {
        this.isLoading = false;
      });
  }


  togglePublishView() {
    this.publishService.togglePublishModal(this.projectId);
  }

  logout() {
    this._authService.logout();
    this.router.navigate(['/auth/login']);
  }


  toggleTimer(status) {
    if (status) {
      this.timer = {
        hr: 0,
        min: 0,
        sec: 0,
      };
      this.interval = setInterval(
        () => {
          const sec = this.timer.sec + 1;
          this.timer.sec = sec === 61 ? 1 : sec;

          const min = sec === 61 ? this.timer.min + 1 : this.timer.min;
          this.timer.min = min === 61 ? this.timer.min = 1 : min;


          this.timer.hr = min === 61 ? this.timer.hr + 1 : this.timer.hr;

        }
        , 1000);
    } else {
      if (this.interval) {
        clearInterval(this.interval);
        this.finalTimeInSec = this.timer ? this.timer.hr * 3600 + this.timer.min * 60 + this.timer.sec : 0;
      } else {
        this.finalTimeInSec = 0;
      }
    }
  }






  submitProjectAnswer() {
    this.toggleTimer(false);

    const data = {
      time_taken: this.finalTimeInSec,
      success_percentage: null,
    };

    this.activatedRoute.params.subscribe(
      params => {

        if (params.adventureId) {
          data['adventure_id'] = params.adventureId;
        }
        if (params.courseId) {
          data['course_id'] = params.courseId;
        }
        if (params.jourenyId) {
          data['journey_id'] = params.jourenyId;
        }
        if (params.missionId) {
          data['project_id'] = params.missionId;
        }

      });

    this.isLoading = true;
    this.projectService.submitProjectAnswer(data).subscribe(
      res => {
        if (res.Object) {
          this.isLoading = false;
          if (!this.isTeacher) {
            this.userXp = res.Object.score.xp;
          }
          const modalData = {
            xp: res.Object.xpgained_xps,
            progress_after: res.Object.progress_after,
          };
          this._projectSpaceService.toggleFinishedModal(modalData);

          const sub = this._projectSpaceService.finishModalStatus.subscribe(
            status => {
              if (!status) {
                this.toggleTimer(true);
              }
            });

          this.subscriptions.add(sub);
        }

      }, error => {
        this.isLoading = false;
      });
  }

  showModelAnswerWarning() {
    this._dialogService.open(this.publishModal, {
      context: {
        title: 'Warning',
      },
    });
  }

  toggleModelAnswer(value, modalRef) {
    this.projectService.toggleModelAnswer.next(value);
    modalRef.close();
  }

  toggleNav() {
    const navStatus = !this.isNavExpanded;
    // this.isNavExpanded = !this.isNavExpanded;

    if (navStatus) {
      const lvl = this.breakPoint === 'md' ? 2 : 3;
      this._projectSpaceService.expandNavBar.next(lvl);
    } else {
      this.isNavExpanded = false;
    }

    setTimeout(() => {
      if (navStatus) {
        this.isNavExpanded = true;
      } else {
        this._projectSpaceService.expandNavBar.next(null);
      }
    }, 150);
  }


  goBack() {

    const saveHasChanges = this._changeDetectorService.projectHasChangesForSave.getValue();

    if (saveHasChanges) {
      this.toggleSaveModal();
      return;

    } else {
      this.goToPreviousPage();
    }

  }

  goHome() {
    const saveHasChanges = this._changeDetectorService.projectHasChangesForSave.getValue();

    if (saveHasChanges) {
      this.toggleSaveModal();
      return;

    } else {
      this.router.navigate(['/']);
    }

  }

  goToPreviousPage() {

    const url = this._location.path();
    let backPage = '';
    if (url.includes('pre-journey')) {
      if (this.isTeacher) {
        backPage = '';
      } else {
        backPage = '';
      }


    } else if (url.includes('journey')) {

      let courseId;
      let jourenyId;
      let studentId;

      this.activatedRoute.params.subscribe(
        res => {
          courseId = res['courseId'];
          jourenyId = res['jourenyId'];
          studentId = res['studentId'];

          if (this.isTeacher) {

            if (this.isAnswerChecking) {
              backPage = `/teacher/classroom`;

              // ----------- TODO  get the classroom code

              // backPage = `/teacher/classroom/1323/course/${courseId}/journey/${jourenyId}/progress`;

            } else if (courseId) {
              backPage = `/teacher/journey/${jourenyId}/course/${courseId}/adventure`;
            } else {
              backPage = `/teacher/journeys/${jourenyId}/adventure`;
            }
          } else {
            backPage = `/student/journey/${jourenyId}/course/${courseId}/adventure`;
          }
        });

      // Default project
    } else if (url.includes('create')) {
      if (this.isTeacher) {
        backPage = '/teacher/projects';
      } else {
        backPage = '/student/projects';
      }

    } else if (url.includes('remix')) {
      if (this.isTeacher) {
        backPage = `/teacher/discover/${this.projectId}/discover-project`;
      } else {
        backPage = `/student/discover/${this.projectId}/discover-project`;
      }

    } else {
      if (this.isTeacher) {
        backPage = `/teacher/projects/${this.projectId}/preview`;
      } else {
        backPage = `/student/projects/${this.projectId}/preview`;
      }
    }



    this.router.navigate([backPage]);
  }

  projectNameChanged(event) {
    this._changeDetectorService.projectHasChangesForSave.next(true);
  }

  projectNameFocusOut(event) {
    let value = event.target.value;
    value = value.trim();
    const name = value ? value : this.projectService.projectName.getValue();
    event.target.value = name;
    if (this.projectService.projectName.getValue() !== name) {
      this.projectService.projectName.next(name);
      this._changeDetectorService.projectHasChangesForSave.next(true);
    }
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.subscriptions.unsubscribe();
    this._changeDetectorService.setNoChangesForSave();
    this._projectSpaceService.expandNavBar.next(null);
  }

  toggleSaveModal() {
    this._dialogService.open(this.saveModal, {
      context: {
        title: 'Save Project',
      },
    });
  }

}
