import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { env } from '../../../@core/env/env';

@Component({
  selector: 'ngx-visitor-header',
  styleUrls: ['./visitor-header.component.scss'],
  templateUrl: './visitor-header.component.html',
})
export class VisitorHeaderComponent implements OnInit, OnDestroy {

  isOrg = env.isOrg;

  private destroy$: Subject<void> = new Subject<void>();

  constructor() { }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
