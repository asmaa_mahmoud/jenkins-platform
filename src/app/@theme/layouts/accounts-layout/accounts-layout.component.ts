import { AuthenticationService } from './../../../@core/utils/authentication.service';
import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { AgreementModalComponent } from '../../../coding-ville/accounts/shared/agreement-modal/agreement-modal.component';
import { Role } from '../../../@core/data/role';

@Component({
  selector: 'ngx-accounts-layout',
  styleUrls: ['./accounts-layout.component.scss'],
  templateUrl: './accounts-layout.component.html',
})
export class AccountsLayoutComponent implements OnInit {
  isActivated: boolean;
  isTermsAgreed: boolean;
  userRole;
  termsagreementDialogRef: NbDialogRef<AgreementModalComponent>;
  constructor(private authService: AuthenticationService, private dialogService: NbDialogService) {
    this.isActivated = this.authService.isActivated();
    this.isTermsAgreed = this.authService.isTermsAgreed();
    this.authService.currentUser.subscribe(x => this.userRole = x.role);
  }

  ngOnInit() {
    this.isAcceptTermsAgreement();
  }

  isAcceptTermsAgreement() {
    const closeOnBackdropClick = false;
    if (!this.isTermsAgreed && !Role['Admin'].includes(this.userRole)) {
      this.termsagreementDialogRef = this.dialogService.open(AgreementModalComponent, { closeOnBackdropClick },
      );
    }
  }

}

