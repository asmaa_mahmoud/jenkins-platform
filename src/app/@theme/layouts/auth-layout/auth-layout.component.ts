import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { env } from './../../../@core/env/env';

@Component({
  selector: 'ngx-auth-layout',
  styleUrls: ['./auth-layout.component.scss'],
  templateUrl: './auth-layout.component.html',
})
export class AuthLayoutComponent {

  isOrg = env.isOrg;

  constructor(private router: Router) {}

  goToCodingVille() {
    window.location.href = 'http://codingville.ca';
  }
}

