import { ProjectSpaceService } from './../../../coding-ville/project/project-space/project-space.service';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './../../../@core/utils/authentication.service';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { AgreementModalComponent } from '../../../coding-ville/accounts/shared/agreement-modal/agreement-modal.component';
@Component({
  selector: 'ngx-project-layout',
  styleUrls: ['./project-layout.component.scss'],
  templateUrl: './project-layout.component.html',
})

export class ProjectLayoutComponent implements OnInit {

  oldLvl;
  expandLvl;
  isTermsAgreed: boolean;
  termsagreementDialogRef: NbDialogRef<AgreementModalComponent>;

  constructor(
    private projectSpaceService: ProjectSpaceService,
    private authService: AuthenticationService,
    private dialogService: NbDialogService,
  ) {
    this.isTermsAgreed = this.authService.isTermsAgreed();
  }

  ngOnInit() {
    this.getBreakPoint();
    this.isAcceptTermsAgreement();
  }


  getBreakPoint() {
    this.projectSpaceService.expandNavBar.subscribe(
      res => {
        this.oldLvl = this.expandLvl;
        this.expandLvl = res;
      });
  }

  isAcceptTermsAgreement() {
    const closeOnBackdropClick = false;
    if (!this.isTermsAgreed) {
      this.termsagreementDialogRef = this.dialogService.open(AgreementModalComponent, { closeOnBackdropClick },
      );
    }
  }

}

