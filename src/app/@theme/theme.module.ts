import { TranslatorModule } from './../@shared/translator/translator.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSidebarModule,
  NbUserModule,
  NbContextMenuModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbThemeModule,
  NbCardModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NbSecurityModule } from '@nebular/security';

import {
  FooterComponent,
  HeaderComponent,
  ProjectHeaderComponent,
  LayoutDirectionSwitcherComponent,
  SearchInputComponent,
  SwitcherComponent,
  AccountsSidebarTopComponent,
  AccountsSidebarBottomComponent,
  AccountsHeaderComponent,
  VisitorHeaderComponent,
  AuthWelcomeComponent,
  BreadcrumbsComponent,
} from './components';
import {
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  ThousandSuffixesPipe,
} from './pipes';
import {
  OneColumnLayoutComponent,
  ThreeColumnsLayoutComponent,
  TwoColumnsLayoutComponent,
  ProjectLayoutComponent,
  AccountsLayoutComponent,
  AuthLayoutComponent,
  VisitorLayoutComponent,
  PolicyLayoutComponent,
} from './layouts';
import { DEFAULT_THEME } from './styles/theme.default';
// import { COSMIC_THEME } from './styles/theme.cosmic';
// import { CORPORATE_THEME } from './styles/theme.corporate';
// import { DARK_THEME } from './styles/theme.dark';

const NB_MODULES = [
  NbLayoutModule,
  NbMenuModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbContextMenuModule,
  NbSecurityModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbEvaIconsModule,
];
const COMPONENTS = [
  BreadcrumbsComponent,
  AccountsSidebarTopComponent,
  AccountsSidebarBottomComponent,
  AccountsHeaderComponent,
  VisitorHeaderComponent,
  AuthWelcomeComponent,
  SwitcherComponent,
  LayoutDirectionSwitcherComponent,
  HeaderComponent,
  ProjectHeaderComponent,
  FooterComponent,
  SearchInputComponent,
  OneColumnLayoutComponent,
  ThreeColumnsLayoutComponent,
  TwoColumnsLayoutComponent,
  ProjectLayoutComponent,
  AccountsLayoutComponent,
  AuthLayoutComponent,
  VisitorLayoutComponent,
  PolicyLayoutComponent,
];
const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  ThousandSuffixesPipe,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ...NB_MODULES,
    TranslatorModule,
    NbCardModule,
  ],
  exports: [CommonModule, ...PIPES, ...COMPONENTS],
  declarations: [...COMPONENTS, ...PIPES, PolicyLayoutComponent],
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeModule,
      providers: [
        ...NbThemeModule.forRoot(
          {
            name: 'default',
          },
          // [ DEFAULT_THEME, COSMIC_THEME, CORPORATE_THEME, DARK_THEME ],
          [DEFAULT_THEME],
        ).providers,
      ],
    };
  }
}
