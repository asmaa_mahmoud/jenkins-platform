import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from './@core/utils/auth.guard';
import { AuthorizationGuard } from './@core/utils/authorization.guard';
import { NotAuthenticatedGuard } from './@core/utils/notAuthenticated.guard';
import { Role } from './@core/data/role';
import { LtiComponent } from './coding-ville/auth/lti/lti.component';
import { ActiveGuard } from './@core/utils/active.guard';
import { ActivateUserComponent } from './coding-ville/activate-user/activate-user.component';

const routes: Routes = [
  {
    path: 'lti',
    component: LtiComponent,
  },
  {
    path: 'activate/:hash',
    component: ActivateUserComponent,
  },
  {
    path: 'student',
    loadChildren: () => import('./coding-ville/accounts/student/student.module')
      .then(m => m.StudentModule),
    canActivate: [AuthGuard, AuthorizationGuard],
    data: {
      allowedRoles: Role['Student'].concat(Role['individualStudent']),
    },
  },
  {
    path: 'project-game',
    loadChildren: () => import('./coding-ville/project/project.module')
      .then(m => m.ProjectModule),
    canActivate: [ActiveGuard],
  },
  {
    path: 'teacher',
    loadChildren: () => import('./coding-ville/accounts/teacher/teacher.module')
      .then(m => m.TeacherModule),
    canActivate: [AuthGuard, AuthorizationGuard],
    data: {
      allowedRoles: Role['Teacher'],
    },
  },

  {
    path: 'admin',
    loadChildren: () => import('./coding-ville/accounts/admin/admin.module')
      .then(m => m.AdminModule),
    canActivate: [AuthGuard, AuthorizationGuard],
    data: {
      allowedRoles: Role['Admin'],
    },
  },
  {
    path: 'visitor',
    loadChildren: () => import('./coding-ville/visitor/visitor.module')
      .then(m => m.VisitorModule),
  },



  {
    path: 'auth',
    canActivate: [NotAuthenticatedGuard],
    loadChildren: () => import('./coding-ville/auth/auth.module')
      .then(m => m.AuthModule),
    data: {
      allowedRoles: ['teacher', 'super_teacher', 'student', 'Admin'],
    },
  },

  {
    path: 'docs',
    loadChildren: () => import('./coding-ville/policies/policies.module')
      .then(m => m.PoliciesModule),
  },
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
    canActivate: [NotAuthenticatedGuard],
  },
  { path: '**', redirectTo: 'auth' },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
