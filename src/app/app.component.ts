/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { TranslateService } from '@ngx-translate/core';
import { PublishService } from './coding-ville/project/publish.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from './@core/utils/authentication.service';
import { Role } from './@core/data/role';
import { GlobalService } from './@core/utils/global.service';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

declare let ga: Function;

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {
  currentUser: any;
  currentRole: any;
  isTermsAgreed: boolean;


  constructor(
    private analytics: AnalyticsService,
    private publishService: PublishService,
    private translate: TranslateService,
    private _authService: AuthenticationService,
    private router: Router,
    private globalServics: GlobalService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.translate.setDefaultLang('en');
    this._authService.currentUser.subscribe(x => this.currentUser = x);
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use('en');
    if (this.currentUser && environment.production) {
      if (Role['Teacher'].includes(this.currentUser.role))
        this.currentRole = 'teacher';
      else if (Role['Student'].includes(this.currentUser.role))
        this.currentRole = 'student';
      else if (Role['individualStudent'].includes(this.currentUser.role))
        this.currentRole = 'individual';
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          ga('set', 'page', event.urlAfterRedirects);
          ga('set', 'dimension2', this.currentRole);
          ga('send', 'pageview', {
            'dimension2': this.currentRole,
          });
        }
      });
    }
  }


  ngOnInit() {
    const currentUser = JSON.parse(localStorage.getItem('AuthorizationData'));
    this.analytics.trackPageViews();
    currentUser ? this.publishService.makeConnection(currentUser.id) : null;

    this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      })
      .filter((route) => route.outlet === 'primary')
      .mergeMap((route) => route.data)
      .subscribe((event) => {
        this.globalServics.setTitle(event['title'] || 'CODINGVILLE');
      });
  }
}
