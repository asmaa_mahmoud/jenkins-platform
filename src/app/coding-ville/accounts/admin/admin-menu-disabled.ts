import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS_DISABLED: NbMenuItem[] = [
  {
    title: 'dashboard',
    icon: 'bar-chart-outline',
    link: '',
    home: true,
  },
  {
    title: 'teachers',
    icon: 'map',
    link: '',
  },
  {
    title: 'individuals',
    icon: 'flag',
    link: '',
  },
];
