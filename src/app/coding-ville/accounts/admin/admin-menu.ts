import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'dashboard',
    icon: 'bar-chart-outline',
    link: '/admin/dashboard',
    home: true,
  },
  {
    title: 'teachers',
    icon: { icon: 'teacher', pack: 'custom-icons' },
    link: '/admin/teachers',
    pathMatch: 'prefix',
  },
  {
    title: 'students',
    icon: { icon: 'individual', pack: 'custom-icons' },
    link: '/admin/students',
    pathMatch: 'prefix',
  },
  {
    title: 'individuals',
    icon: { icon: 'individual', pack: 'custom-icons' },
    link: '/admin/individuals',
    pathMatch: 'prefix',
  },
];
