import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IndividualsComponent } from './individuals/individuals.component';
import { TeachersComponent } from './teachers/teachers.component';
import { AdventureComponent } from '../shared/adventure/adventure.component';
import { EditProfileComponent } from '../shared/edit-profile/edit-profile.component';
import { StudentProfileComponent } from '../shared/profile/student-profile.component';
import { ProjectPreviewComponent } from '../shared/project-preview/project-preview.component';
import { AdminEchartsBarComponent } from './dashboard/charts/echarts-bar.component';
import { StudentProgressComponent } from './teachers/classrooms/student-progress/student-progress.component';
import { ClassroomsComponent } from './teachers/classrooms/classrooms.component';
import { ClassroomTabsComponent } from './teachers/classrooms/classroom-tabs/classroom-tabs.component';
import { ClassroomJourneysComponent } from './teachers/classrooms/classroom-journeys/classroom-journeys.component';
import { ClassroomMiniJourneysComponent } from './teachers/classrooms/classroom-mini-journeys/classroom-mini-journeys.component';
import { ClassroomStudentsComponent } from './teachers/classrooms/classroom-students/classroom-students.component';
import { ClassroomProjectsComponent } from './teachers/classrooms/classroom-projects/classroom-projects.component';
import { StudentsComponent } from './students/students.component';


const routes: Routes = [{
  path: '',
  component: AdminComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: 'dashboard',
      component: DashboardComponent,
    }, {
      path: 'students',
      component: StudentsComponent,
    },
    {
      path: 'individuals',
      component: IndividualsComponent,
    },
    {
      path: 'teachers',
      component: TeachersComponent,
    },
    {
      path: 'teachers/classrooms/:teacher_id',
      component: ClassroomsComponent,
    },
    {
      path: 'teachers/classrooms/:teacher_id/:classroom_id/course/:course_id/:journey_type/:journey_id/progress',
      component: StudentProgressComponent,
    },
    {
      path: 'teachers/classrooms/:teacher_id/:classroom_id',
      component: ClassroomTabsComponent,
    },
    {
      path: 'journeys/:journey_id/course/:course_id/:type',
      component: AdventureComponent,
    },
    {
      path: 'journeys/:journey_id/:type',
      component: AdventureComponent,
    },
    {
      path: ':type/:project_id/:discover-project',
      component: ProjectPreviewComponent,
    },
    // ****** preview default projects of journey ******//
    {
      path: ':type/:project_id/:journey-type/adventure/:adventureID/course/:courseID',
      component: ProjectPreviewComponent,
    },
    {
      path: ':type/:project_id/:journey-type/:journeyID/adventure/:adventureID',
      component: ProjectPreviewComponent,
    },
    // ****** preview default projects of pre-journey ******//
    {
      path: ':type/:project_id/:journey-type/:activityID/course/:courseID',
      component: ProjectPreviewComponent,
    },
    {
      path: ':type/:project_id/:journey-type/:activityID',
      component: ProjectPreviewComponent,
    },
    {
      path: 'profile/:student_id',
      component: StudentProfileComponent,
    },
    {
      path: 'edit-profile',
      component: EditProfileComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class AdminRoutingModule {
}
export const Components =
  [
    AdminComponent,
    DashboardComponent,
    AdminEchartsBarComponent,
    StudentProgressComponent,
    DashboardComponent,
    IndividualsComponent,
    TeachersComponent,
    ClassroomsComponent,
    ClassroomTabsComponent,
    ClassroomJourneysComponent,
    ClassroomMiniJourneysComponent,
    ClassroomProjectsComponent,
    ClassroomStudentsComponent,
    StudentProgressComponent,
  ];
