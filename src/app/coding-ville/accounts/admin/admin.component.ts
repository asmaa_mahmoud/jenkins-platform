import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../@core/utils/authentication.service';
import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './admin-menu';
import { MENU_ITEMS_DISABLED } from './admin-menu-disabled';

@Component({
  selector: 'ngx-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})

export class AdminComponent implements OnInit {

  isActivated: boolean;
  menu: any;

  constructor(private authService: AuthenticationService, private router: Router, private translateService: TranslateService) {
    this.isActivated = this.authService.isActivated();
  }

  ngOnInit() {
    if (!this.isActivated) this.router.navigate(['/admin/dashboard']);
    this.getMenu();
  }

  getMenu() {
    this.menu = this.isActivated ? MENU_ITEMS : MENU_ITEMS_DISABLED;
    this.menu.map(item => {
      this.translateService.get(item.title).subscribe(res => {
        item.title = res;
      });
      // if (!this.isActivated) item.link = '';
    });
  }

}
