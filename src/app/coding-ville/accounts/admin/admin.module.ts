
import { TranslatorModule } from '../../../@shared/translator/translator.module';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NgModule } from '@angular/core';
import { NbMenuModule, NbCardModule, NbSelectModule, NbTabsetModule, NbIconModule, NbInputModule, NbButtonModule } from '@nebular/theme';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../../@theme/theme.module';
import { AdminRoutingModule, Components } from './admin-routing.module';
import { ClipboardModule } from 'ngx-clipboard';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { AgreementModalComponent } from '../shared/agreement-modal/agreement-modal.component';
import { StudentsComponent } from './students/students.component';

@NgModule({
  imports: [
    CommonModule,
    NgxEchartsModule,
    NgxChartsModule,
    TranslateModule,
    ThemeModule,
    NbMenuModule,
    FormsModule,
    SharedModule,
    AdminRoutingModule,
    NbCardModule,
    NbSelectModule,
    NbTabsetModule,
    NbEvaIconsModule,
    NbIconModule,
    NbInputModule,
    NbButtonModule,
    ClipboardModule,
    TranslatorModule,
  ],

  declarations: [
    ...Components,
    StudentsComponent,
  ],
  entryComponents: [
    AgreementModalComponent,
  ],
})
export class AdminModule { }


