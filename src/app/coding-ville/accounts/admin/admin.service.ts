import { Injectable } from '@angular/core';
import { GlobalService } from '../../../@core/utils/global.service';

@Injectable({
  providedIn: 'root',
})
export class AdminService {

  constructor(private _globalService: GlobalService) { }

}
