import { AfterViewInit, Component, OnDestroy, Input, OnChanges } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'ngx-admin-echarts-bar',
  template: `
    <div echarts [options]="options" [initOpts]="initOpts" class="echart"></div>
  `,
  styleUrls: ['./echarts.component.scss'],
})
export class AdminEchartsBarComponent implements AfterViewInit, OnDestroy, OnChanges {

  @Input('chartData') chartData: any;
  @Input('numberOf') numberOf: string;
  options: any = {};
  initOpts: any = {};
  themeSubscription: any;
  xAxisData: any = [];
  yAxisData: any = ['0', '20%', '40%', '60%', '80%', '100%'];
  seriesData: any = [];

  constructor(private theme: NbThemeService) {
  }

  ngAfterViewInit() {
    this.updateChart();
  }

  ngOnChanges() {
    this.updateChart();
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

  updateChart() {
    this.xAxisData = [];
    this.seriesData = [];
    if (this.chartData) {
      Object.keys(this.chartData).forEach(key => {
        const data = key.length > 10 ? key.slice(0, 10) + '...' : key;
        this.xAxisData.push(data);
        this.seriesData.push(parseInt(this.chartData[key], 10));
      });
    }

    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;

      echarts.bg = '#fff';
      echarts.textColor = '#707070';
      echarts.axisLineColor = '#707070';
      echarts.barColor = '#8ae25a';

      this.options = {
        backgroundColor: echarts.bg,
        color: [echarts.barColor],
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow',
          },
        },
        grid: {
          left: '20px',
          right: '10px',
          bottom: '10px',
          containLabel: true,
        },
        xAxis: [
          {
            name: 'Provincies',
            nameLocation: 'center',
            nameGap: 50,
            type: 'category',
            data: this.xAxisData,
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            axisLabel: {
              interval: 0,
              rotate: 40,
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            name: `Number of ${this.numberOf}`,
            nameLocation: 'center',
            nameGap: 25,
            nameRotate: 90,
            minInterval: 1,
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            splitLine: {
              lineStyle: {
                color: echarts.splitLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        series: [
          {
            name: `Number of ${this.numberOf}`,
            type: 'bar',
            barWidth: '60%',
            // data: [3, 15, 20, 38, 40, 35, 28],
            data: this.seriesData,
            itemStyle: {
              barBorderRadius: [50, 50, 0, 0],
            },
          },
        ],
      };
    });
  }

}
