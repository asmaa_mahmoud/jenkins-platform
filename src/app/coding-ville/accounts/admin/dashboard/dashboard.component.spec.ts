import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AuthenticationService } from './../../../../@core/utils/authentication.service';
import { GlobalService } from './../../../../@core/utils/global.service';
import { AdminDashboardService } from './dashboard.service';
import { DashboardComponent } from './dashboard.component';
import { ThousandSuffixesPipe } from '../../../../@theme/pipes';
import { TranslatePipe } from '@ngx-translate/core';
describe('AdminDashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  beforeEach(() => {
    const authenticationServiceStub = () => ({ isActivated: () => ({}) });
    const globalServiceStub = () => ({ getBreadcrumbs: breadcrumbs => ({}) });
    const adminDashboardServiceStub = () => ({
      getGeneralData: () => ({ subscribe: () => ({}) }),
      getClassroomsPerProvince: () => ({ subscribe: () => ({}) }),
      getStudentsPerProvince: () => ({ subscribe: () => ({}) }),
    });
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [DashboardComponent, ThousandSuffixesPipe, TranslatePipe],
      providers: [
        {
          provide: AuthenticationService,
          useFactory: authenticationServiceStub,
        },
        { provide: GlobalService, useFactory: globalServiceStub },
        {
          provide: AdminDashboardService,
          useFactory: adminDashboardServiceStub,
        },
      ],
    });
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });
  it('selectedFilter defaults to: Students', () => {
    expect(component.selectedFilter).toEqual('Students');
  });
  it('dataLoaded defaults to: false', () => {
    expect(component.dataLoaded).toEqual(false);
  });
  it('generalDataLoaded defaults to: false', () => {
    expect(component.generalDataLoaded).toEqual(false);
  });
  describe('ngOnInit', () => {
    it('makes expected calls', () => {
      const globalServiceStub: GlobalService = fixture.debugElement.injector.get(
        GlobalService,
      );
      spyOn(component, 'changeChart').and.callThrough();
      spyOn(component, 'getGeneralData').and.callThrough();
      spyOn(globalServiceStub, 'getBreadcrumbs').and.callThrough();
      component.ngOnInit();
      expect(component.changeChart).toHaveBeenCalled();
      expect(component.getGeneralData).toHaveBeenCalled();
      expect(globalServiceStub.getBreadcrumbs).toHaveBeenCalled();
    });
  });
  describe('changeChart', () => {
    it('#getStudentsPerProvince to have been called', () => {
      component.selectedFilter = 'Students';
      spyOn(component, 'getStudentsPerProvince').and.callThrough();
      component.changeChart();
      expect(component.getStudentsPerProvince).toHaveBeenCalled();
    });
    it('#getClassroomsPerProvince to have been called', () => {
      component.selectedFilter = 'Classrooms';
      spyOn(component, 'getClassroomsPerProvince').and.callThrough();
      component.changeChart();
      expect(component.getClassroomsPerProvince).toHaveBeenCalled();
    });
  });
  describe('getGeneralData', () => {
    it('makes expected calls', () => {
      const adminDashboardServiceStub: AdminDashboardService = fixture.debugElement.injector.get(
        AdminDashboardService,
      );
      spyOn(adminDashboardServiceStub, 'getGeneralData').and.callThrough();
      component.getGeneralData();
      expect(adminDashboardServiceStub.getGeneralData).toHaveBeenCalled();
    });
  });
  describe('getClassroomsPerProvince', () => {
    it('makes expected calls', () => {
      const adminDashboardServiceStub: AdminDashboardService = fixture.debugElement.injector.get(
        AdminDashboardService,
      );
      spyOn(
        adminDashboardServiceStub,
        'getClassroomsPerProvince',
      ).and.callThrough();
      component.getClassroomsPerProvince();
      expect(
        adminDashboardServiceStub.getClassroomsPerProvince,
      ).toHaveBeenCalled();
    });
  });
  describe('getStudentsPerProvince', () => {
    it('makes expected calls', () => {
      const adminDashboardServiceStub: AdminDashboardService = fixture.debugElement.injector.get(
        AdminDashboardService,
      );
      spyOn(
        adminDashboardServiceStub,
        'getStudentsPerProvince',
      ).and.callThrough();
      component.getStudentsPerProvince();
      expect(
        adminDashboardServiceStub.getStudentsPerProvince,
      ).toHaveBeenCalled();
    });
  });
});
