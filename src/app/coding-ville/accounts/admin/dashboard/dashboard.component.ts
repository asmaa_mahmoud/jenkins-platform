import { AuthenticationService } from './../../../../@core/utils/authentication.service';
import { GlobalService } from './../../../../@core/utils/global.service';
import { Component, OnInit } from '@angular/core';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { AdminDashboardService } from './dashboard.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  generalData: any;
  classrooms: any;
  chartData: any = null;
  selectedFilter = 'Students';
  isActivated: boolean;
  dataLoaded = false;
  generalDataLoaded = false;
  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'DASHBOARD',
      link: '',
    },
  ];

  constructor(
    private globalService: GlobalService,
    private _dashboardService: AdminDashboardService,
    private authService: AuthenticationService,
  ) {
    this.isActivated = this.authService.isActivated();
  }

  ngOnInit() {
    this.globalService.getBreadcrumbs(this.breadcrumbs);
    this.changeChart();
    this.getGeneralData();
  }

  changeChart() {
    if (this.selectedFilter === 'Students') this.getStudentsPerProvince();
    if (this.selectedFilter === 'Classrooms') this.getClassroomsPerProvince();
  }

  getGeneralData() {
    this._dashboardService.getGeneralData().subscribe(res => {
      this.generalData = {
        classrooms: res.Object.countClassrooms || 0,
        teachers: res.Object.countTeachers || 0,
        students: res.Object.countStudents || 0,
        projects: res.Object.countProjects || 0,
      };
      this.generalDataLoaded = true;
    }, err => {
      console.log(err);
      this.generalData = {
        classrooms: null,
        teachers: null,
        students: null,
        projects: null,
      };
      this.generalDataLoaded = false;
    });
  }

  getClassroomsPerProvince() {
    this._dashboardService.getClassroomsPerProvince().subscribe(res => {
      this.chartData = res.Object;
      this.dataLoaded = true;
    }, err => {
      console.log(err);
      this.chartData = null;
      this.dataLoaded = true;
    });
  }

  getStudentsPerProvince() {
    this._dashboardService.getStudentsPerProvince().subscribe(res => {
      this.chartData = res.Object;
      this.dataLoaded = true;
    }, err => {
      console.log(err);
      this.chartData = null;
      this.dataLoaded = true;
    });
  }

}
