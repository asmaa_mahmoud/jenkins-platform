import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { GlobalService } from '../../../../@core/utils/global.service';

@Injectable({
  providedIn: 'root',
})
export class AdminDashboardService {

  constructor(private _globalService: GlobalService) { }

  getGeneralData() {
    const url = 'coding-ville/admin/dashboard/summary_data';
    return this._globalService.get(url);
  }

  getClassroomsPerProvince() {
    const url: string = `coding-ville/admin/dashboard/classrooms_per_province`;
    return this._globalService.get(url);
  }

  getStudentsPerProvince() {
    const url: string = `coding-ville/admin/dashboard/students_per_province`;
    return this._globalService.get(url);
  }

}
