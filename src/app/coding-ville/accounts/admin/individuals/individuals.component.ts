import { Component, OnInit } from '@angular/core';
import { IndividualsService } from './individuals.service';
import { GlobalService } from '../../../../@core/utils/global.service';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'ngx-ndividuals',
  templateUrl: './individuals.component.html',
  styleUrls: ['./individuals.component.scss'],
})
export class IndividualsComponent implements OnInit {

  tableLengths = [10, 25, 50, 100];
  tableLength = '10';
  individualsCount: string;
  start: number = 0;
  individuals = [];
  journeys = [];
  currentPage: number = 1;
  searchValue: string = '';
  maxShown = '10';
  ages = [];
  selectedAge = 'all';
  dataLoaded: boolean = false;
  shareCode: string;
  csvData: any;
  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Individuals',
      link: '',
    },
  ];

  constructor(private _individualService: IndividualsService, private _globalService: GlobalService) {
    this._globalService.breadcrumbsService.getBreadcrumbs(this.breadcrumbs);
  }

  ngOnInit() {
    this.getAllAges();
    this.getIndividuals();
  }

  getAllAges() {
    for (let i = 5; i < 17; i++) {
      this.ages.push(`${i}`);
    }
  }

  getIndividuals() {
    this.dataLoaded = false;
    this._individualService.getIndividualsData(this.start.toString(), this.tableLength, this.searchValue, this.selectedAge)
      .subscribe((data: any) => {
        this.individualsCount = data.Object.count;
        this.individuals = data.Object.students;
        this.journeys = data.Object.journeys;
        this.dataLoaded = true;
      });
  }

  prepareDataToCsv() {
    this._individualService.getIndividualsData(0, 'all', '', 'all')
      .subscribe((data: any) => {
        this.csvData = data.Object.students;
        this.csvData.filter((el) => {
          delete el.id;
          delete el.journeys;
          return el;
        });
        for (let i = 0; i < this.csvData.length; i++) {
          for (let j = 0; j < this.journeys.length; j++) {
            this.csvData[i][this.journeys[j].name] =
              this.individuals[i].journeys[j]['completedMissions'] + '/' + this.individuals[i].journeys[j]['totalMissions'];
          }
        }
      });
  }

  changePagination(num: number) {
    this.currentPage = num;
    this.start = (num * Number(this.tableLength) - Number(this.tableLength));
    this.getIndividuals();
  }

  search(event: string) {
    this.searchValue = event;
    this.start = 0;
    this.getIndividuals();
  }

  onChangeLength() {
    this.start = 0;
    this.maxShown = this.tableLength;
    if (this.tableLength === 'all')
      this.maxShown = this.individualsCount;
    this.getIndividuals();
  }

  onFilterByAge() {
    this.start = 0;
    this.getIndividuals();
  }

  onExportCSV() {
    this.prepareDataToCsv();
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'individuals',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.csvData);
  }

}
