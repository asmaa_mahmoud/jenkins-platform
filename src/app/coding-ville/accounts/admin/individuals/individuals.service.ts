import { Injectable } from '@angular/core';
import { GlobalService } from '../../../../@core/utils/global.service';
import { HttpParams } from '@angular/common/http';
import { CustomEncoder } from '../../../../@core/utils/custom-encoder';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class IndividualsService {

  constructor(private _globalService: GlobalService) { }

  getIndividualsData(start, limit, search, age) {
    const url: string = `coding-ville/admin/individual/data`;
    const params = new HttpParams({ encoder: new CustomEncoder() })
      .set('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('age', age);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }



}
