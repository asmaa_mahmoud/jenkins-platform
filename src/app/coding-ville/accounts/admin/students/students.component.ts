import { Component, OnInit } from '@angular/core';
import { ExportToCsv } from 'export-to-csv';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../@core/utils/global.service';
import { StudentsService } from './students.service';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'ngx-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss'],
})
export class StudentsComponent implements OnInit {

  tableLengths = [10, 25, 50, 100];
  tableLength = '10';
  studentsCount: string;
  start: number = 0;
  students = [];
  journeys = [];
  currentPage: number = 1;
  searchValue: string = '';
  maxShown = '10';
  schools = [];
  provinces = [];
  cities = [];
  selectedAge = 'all';
  dataLoaded: boolean = false;
  shareCode: string;
  csvData: any;
  filterBy: string = '';
  filterValue: any = '';
  selectedSchool: string;
  selectedProvince: string;
  selectedCity: string;

  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Students',
      link: '',
    },
  ];

  constructor(private _studentsService: StudentsService,
    private _authService: AuthService,
    private _globalService: GlobalService) {
    this._globalService.breadcrumbsService.getBreadcrumbs(this.breadcrumbs);
  }

  ngOnInit() {
    this.getAllSchools();
    this.getAllProvinces();
    this.getStudents();
    this.getAllCities();
  }

  getAllSchools() {
    this._studentsService.getAllSchools().subscribe(data => {
      this.schools = data.Object;
    });
  }

  getAllProvinces() {
    this._authService.getProvinces(39).subscribe(data => {
      this.provinces = data.Object;
    });
  }

  getAllCities() {
    this._studentsService.getAllCities().subscribe(data => {
      this.cities = data.Object;
    });
  }

  getStudents() {
    this.dataLoaded = false;
    this._studentsService.
      getStudentsData(this.start.toString(), this.tableLength, this.searchValue, this.filterBy, this.filterValue)
      .subscribe((data: any) => {
        this.studentsCount = data.Object.count;
        this.students = data.Object.students_data;
        this.dataLoaded = true;
      });
  }

  prepareDataToCsv() {
    this._studentsService.getStudentsData(0, 'all', '', this.filterBy, this.filterValue)
      .subscribe((data: any) => {
        this.csvData = data.Object.students;
        this.csvData.filter((el) => {
          delete el.id;
          delete el.journeys;
          return el;
        });
        for (let i = 0; i < this.csvData.length; i++) {
          for (let j = 0; j < this.journeys.length; j++) {
            this.csvData[i][this.journeys[j].name] =
              this.students[i].journeys[j]['completedMissions'] + '/' + this.students[i].journeys[j]['totalMissions'];
          }
        }
      });
  }

  changePagination(num: number) {
    this.currentPage = num;
    this.start = (num * Number(this.tableLength) - Number(this.tableLength));
    this.getStudents();
  }

  search(event: string) {
    this.searchValue = event;
    this.start = 0;
    this.getStudents();
  }

  onChangeLength() {
    this.start = 0;
    this.maxShown = this.tableLength;
    if (this.tableLength === 'all')
      this.maxShown = this.studentsCount;
    this.getStudents();
  }

  onFilterBySchool() {
    this.start = 0;
    this.filterBy = 'school';
    this.filterValue = this.selectedSchool;
    if (this.selectedSchool === 'all') {
      this.filterBy = '';
      this.filterValue = '';
    }
    this.selectedProvince = '';
    this.selectedCity = '';
    this.getStudents();
  }

  onFilterByProvince() {
    this.start = 0;
    this.filterBy = 'province';
    this.filterValue = this.selectedProvince;
    if (this.selectedProvince === 'all') {
      this.filterBy = '';
      this.filterValue = '';
    }
    this.selectedSchool = '';
    this.selectedCity = '';
    this.getStudents();
  }

  onFilterByCity() {
    this.start = 0;
    this.filterBy = 'city';
    this.filterValue = this.selectedCity;
    if (this.selectedCity === 'all') {
      this.filterBy = '';
      this.filterValue = '';
    }
    this.selectedSchool = '';
    this.selectedProvince = '';
    this.getStudents();
  }

  onExportCSV() {
    this.prepareDataToCsv();
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Students',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.csvData);
  }

}
