import { Injectable } from '@angular/core';
import { GlobalService } from '../../../../@core/utils/global.service';
import { HttpParams } from '@angular/common/http';
import { CustomEncoder } from '../../../../@core/utils/custom-encoder';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class StudentsService {

    constructor(private _globalService: GlobalService) { }

    getStudentsData(start, limit, search, filterBy, filterValue) {
        const url: string = `coding-ville/admin/students/all`;
        const params = new HttpParams({ encoder: new CustomEncoder() })
            .set('start', start)
            .append('limit', limit)
            .append('search', search)
            .append('filter_by', filterBy)
            .append('filter_value', filterValue);
        return this._globalService.get(url, params).map(
            (data: any) => {
                return data;
            },
        ).catch(
            (error: any) => {
                console.log(error.message);
                this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
                return Observable.throw('Something went wrong!');
            },
        );
    }

    getAllSchools() {
        const url: string = `coding-ville/admin/get/schools`;
        return this._globalService.get(url).map(
            (data: any) => {
                return data;
            },
        ).catch(
            (error: any) => {
                console.log(error.message);
                this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
                return Observable.throw('Something went wrong!');
            },
        );
    }

    getAllCities() {
        const url: string = `coding-ville/admin/get/cities`;
        return this._globalService.get(url).map(
            (data: any) => {
                return data;
            },
        ).catch(
            (error: any) => {
                console.log(error.message);
                this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
                return Observable.throw('Something went wrong!');
            },
        );
    }



}
