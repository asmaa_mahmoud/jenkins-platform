import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { ClassroomsService } from '../classrooms.service';
import { Classroom } from '../../../../../../@core/data/classroom';
import { ClassroomJourneys } from '../../../../../../@core/data/classroom-journeys';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-classroom-journeys',
  templateUrl: './classroom-journeys.component.html',
  styleUrls: ['./classroom-journeys.component.scss'],
})
export class ClassroomJourneysComponent implements OnInit, OnChanges {

  @Input() classroom_id: string;
  @Input() teacher_id: string;

  noOfCardsPerPage: any[] = [3, 6, 9, 12];
  classrooms: Classroom[];
  cardsPerPage = '3';
  currentPage: number = 1;
  start: number = 0;
  searchValue: string = '';
  classroomsCount: number;
  journeysCount: number;
  journeys: ClassroomJourneys[];
  selectedFilter: string = null;

  constructor(
    private _classroomService: ClassroomsService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getJourneys();
  }

  ngOnChanges() {
    this.getJourneys();
  }

  onChangeFilter() {
    this.start = 0;
    this.getJourneys();
  }

  getJourneys() {
    const difficulty_id = this.selectedFilter === 'all' ? null : this.selectedFilter;
    this._classroomService.getClassroomJourneys(
      String(this.start), String(this.cardsPerPage), this.searchValue, this.classroom_id, this.teacher_id, difficulty_id,
    ).subscribe(data => {
      this.journeysCount = data.Object.count;
      this.journeys = data.Object.objects;
    });
  }

  search(event) {
    this.searchValue = event;
    this.start = 0;
    this.getJourneys();
  }

  onChangeLength() {
    this.start = 0;
    this.getJourneys();
  }

  changePagination(num) {
    this.start = num * +this.cardsPerPage - +this.cardsPerPage;
    this.getJourneys();
  }

  goToJourneyProgress(journey: any) {
    this.router.navigate([`admin/teachers/classrooms/${this.teacher_id}/${this.classroom_id}/course/${journey.course_id}/journey/${journey.id}/progress`]);
  }

}
