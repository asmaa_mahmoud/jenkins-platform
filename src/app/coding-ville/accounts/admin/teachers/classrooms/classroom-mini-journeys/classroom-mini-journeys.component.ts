import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Classroom } from '../../../../../../@core/data/classroom';
import { ClassroomsService } from '../classrooms.service';
import { ClassroomJourneys } from '../../../../../../@core/data/classroom-journeys';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-classroom-mini-journeys',
  templateUrl: './classroom-mini-journeys.component.html',
  styleUrls: ['./classroom-mini-journeys.component.scss'],
})
export class ClassroomMiniJourneysComponent implements OnInit, OnChanges {

  @Input() classroom_id: number;
  @Input() teacher_id: number;

  noOfCardsPerPage = [3, 6, 9, 12];
  classrooms: Classroom[];
  cardsPerPage = '3';
  currentPage: number = 1;
  start: number = 0;
  searchValue: string = '';
  classroomsCount: number;
  journeysCount: number;
  journeys: ClassroomJourneys[];
  course_id: string;
  classroomName;
  selectedFilter: string = null;


  constructor(
    private _classroomService: ClassroomsService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getPreJourneys();
  }

  ngOnChanges() {
    this.getPreJourneys();
  }

  onChangeFilter() {
    this.start = 0;
    this.getPreJourneys();
  }

  getPreJourneys() {
    const difficulty_id = this.selectedFilter === 'all' ? null : this.selectedFilter;
    this._classroomService.getClassroomPreJourneys(
      String(this.start), String(this.cardsPerPage), this.searchValue, String(this.classroom_id), String(this.teacher_id), difficulty_id,
    ).subscribe(data => {
      this.journeysCount = data.Object.count;
      this.journeys = data.Object.objects;
      this.course_id = data.Object.course_id;
    });
  }

  search(event) {
    this.searchValue = event;
    this.start = 0;
    this.getPreJourneys();
  }

  onChangeLength() {
    this.start = 0;
    this.getPreJourneys();
  }

  changePagination(num) {
    this.start = num * +this.cardsPerPage - +this.cardsPerPage;
    this.getPreJourneys();
  }

  goToPreJourneyProgress(journey: any) {
    this.router.navigate([`admin/teachers/classrooms/${this.teacher_id}/${this.classroom_id}/course/${this.course_id}/prejourney/${journey.id}/progress`]);
  }

}
