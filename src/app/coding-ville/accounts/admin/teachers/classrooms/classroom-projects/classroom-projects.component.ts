import { Router } from '@angular/router';
import { ClassroomsService } from '../classrooms.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Project } from '../../../../../../@core/data/project';

@Component({
  selector: 'ngx-classroom-projects',
  templateUrl: './classroom-projects.component.html',
  styleUrls: ['./classroom-projects.component.scss'],
})
export class ClassroomProjectsComponent implements OnInit {

  @Output() likeProject = new EventEmitter<number>();

  @Input() classroom_id: number;
  @Input() teacher_id: number;

  projects: Project[];
  students: any[];
  projectsCount: number;
  selectedStudent: string = null;
  classroomName: string;
  start: any = 0;
  cardsPerPage: any = 6;
  searchValue: string = '';
  studentID: any;


  constructor(
    private _classroomService: ClassroomsService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getClassroomStudents();
    this.getClassroomProjects();
  }

  getClassroomProjects() {
    const studentFilter = this.studentID === 'all' ? null : this.studentID;
    this._classroomService.getClassroomProjects(
      this.start, this.cardsPerPage, this.searchValue, String(this.classroom_id), String(this.teacher_id), studentFilter,
    ).subscribe(res => {
      this.projectsCount = res.Object.count;
      this.projects = res.Object.objects;
    },
      error => {
        console.log('error:', error);
        this.projects = [];
      });
  }

  toggleLikeProject(index: number) {
    this.likeProject.emit(index);
  }

  getClassroomStudents() {
    this._classroomService.getClassroomStudents(
      this.start, 'all', this.searchValue, String(this.classroom_id), String(this.teacher_id), null,
    ).subscribe((data: any) => {
      this.students = data.Object.objects.map(student => ({ id: student.id, name: `${student.fname} ${student.lname}` }));
    }, err => console.log(err));
  }

  updateProjects(data) {
    this.start = data.start;
    this.cardsPerPage = data.limit;
    this.searchValue = data.search;
    this.studentID = data.student_id;
    this.getClassroomProjects();
  }

  openProject(project) {
    this.router.navigate([`teacher/classroom/${project.id}/preview-project`],
      { queryParams: { classroomName: this.classroomName, classroomID: this.classroom_id } });
  }

}
