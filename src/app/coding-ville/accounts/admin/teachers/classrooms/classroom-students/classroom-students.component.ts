import { ActivatedRoute, Router } from '@angular/router';
import { ClassroomsService } from '../classrooms.service';
import { Component, OnInit, Input } from '@angular/core';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'ngx-classroom-students',
  templateUrl: './classroom-students.component.html',
  styleUrls: ['./classroom-students.component.scss'],
})
export class ClassroomStudentsComponent implements OnInit {

  @Input() classroom_id: string;
  @Input() teacher_id: string;

  tableLengths = [10, 25, 50, 100];
  tableLength = '10';
  studentsCount: string;
  start: number = 0;
  students = [];
  ages = [];
  currentPage: number = 1;
  searchValue: string = '';
  maxShown = '10';
  csvData: any;

  journeysCount: number;

  selectedFilter: string = null;

  constructor(private _classroomService: ClassroomsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getAllAges();
    this.getClassroomStudents();
  }

  getAllAges() {
    for (let i = 5; i < 17; i++) {
      this.ages.push(`${i}`);
    }
  }

  getClassroomStudents() {
    const age = this.selectedFilter === 'all' ? null : this.selectedFilter;
    this._classroomService.getClassroomStudents(
      this.start.toString(), this.tableLength, this.searchValue, this.classroom_id, this.teacher_id, age,
    ).subscribe((data: any) => {
      this.studentsCount = data.Object.count;
      this.students = data.Object.objects;
    },
    );
  }

  changePagination(num: number) {
    this.currentPage = num;
    this.start = (num * Number(this.tableLength) - Number(this.tableLength));
    this.getClassroomStudents();
  }

  search(event: string) {
    this.searchValue = event;
    this.start = 0;
    this.getClassroomStudents();
  }

  onChangeLength() {
    this.start = 0;
    this.maxShown = this.tableLength;
    if (this.tableLength === 'all')
      this.maxShown = this.studentsCount;
    this.getClassroomStudents();
  }

  onFilterByAge() {
    this.start = 0;
    this.getClassroomStudents();
  }

  onExportCSV() {
    const age = this.selectedFilter === 'all' ? null : this.selectedFilter;
    this._classroomService.getClassroomStudents('0', 'all', '', this.classroom_id, this.teacher_id, age)
      .subscribe((data: any) => {
        this.csvData = data.Object.objects;
        for (let i = 0; i < this.csvData.length; i++) {
          this.csvData[i]['Progress'] = this.csvData[i].completedJourneysAndPrejourneys + '/' + this.csvData[i].totalJourneysAndPrejourneys;
        }

        this.csvData.filter((el) => {
          delete el.id;
          delete el.completedJourneysAndPrejourneys;
          delete el.totalJourneysAndPrejourneys;
          return el;
        });

        const options = {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalSeparator: '.',
          showLabels: true,
          showTitle: true,
          title: 'Students',
          useTextFile: false,
          useBom: true,
          useKeysAsHeaders: true,
        };

        const csvExporter = new ExportToCsv(options);
        csvExporter.generateCsv(this.csvData);
      });
  }

}
