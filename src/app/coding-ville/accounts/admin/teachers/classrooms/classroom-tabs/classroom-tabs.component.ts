import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Breadcrumb } from '../../../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../../../@core/utils/global.service';
import { ClassroomsService } from '../classrooms.service';

@Component({
  selector: 'ngx-classroom-tabs',
  templateUrl: './classroom-tabs.component.html',
  styleUrls: ['./classroom-tabs.component.scss'],
})
export class ClassroomTabsComponent implements OnInit {

  classroomName: string = '';
  classroom_id: number;
  teacher_id: number;

  private breadcrumbs: Breadcrumb[];

  public type: string;
  constructor(
    private _globalService: GlobalService,
    private _classroomService: ClassroomsService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getIDs();
    this.getClassroomName();
  }

  setBreadcrumbs(name: string) {
    this.breadcrumbs = [
      {
        name: 'TEACHERS',
        link: 'teachers',
      },
      {
        name: 'CLASSROOMS',
        link: 'teachers/classrooms/' + this.teacher_id,
      },
      {
        name: name,
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(this.breadcrumbs);
  }

  toggleLikeProject(index: number) {
    return;
  }

  getIDs() {
    this.route.params.subscribe((params: Params) => {
      this.classroom_id = params['classroom_id'];
      this.teacher_id = params['teacher_id'];
    });
  }

  getClassroomName() {
    this._classroomService.getClassroomData(this.teacher_id, this.classroom_id).subscribe(data => {
      this.classroomName = data.Object.Name;
      this.setBreadcrumbs(this.classroomName);
      this._globalService.getBreadcrumbs(this.breadcrumbs);
    });
  }

}
