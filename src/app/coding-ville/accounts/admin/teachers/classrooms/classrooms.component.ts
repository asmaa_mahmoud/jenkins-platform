import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Breadcrumb } from '../../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { ClassroomsService } from './classrooms.service';
import { Classroom } from '../../../../../@core/data/classroom';

@Component({
  selector: 'ngx-classrooms',
  templateUrl: './classrooms.component.html',
  styleUrls: ['./classrooms.component.scss'],
})
export class ClassroomsComponent implements OnInit {
  noOfCardsPerPage: any[] = [6, 9, 12];
  classrooms: Classroom[];
  cardPerPage = 6;
  start: number = 0;
  currentPage = 1;
  searchValue = '';
  classroomsCount;
  teacher_id;
  dataLoaded: boolean = false;
  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Teachers',
      link: 'teachers',
    },
    {
      name: 'CLASSROOMS',
      link: '',
    },
  ];

  constructor(
    private _globalService: GlobalService,
    private _classroomService: ClassroomsService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getTeacherId();
    this._globalService.getBreadcrumbs(this.breadcrumbs);
    this.getClassrooms();
  }

  getTeacherId() {
    this.teacher_id = this.route.snapshot.paramMap.get('teacher_id');
  }

  getClassrooms() {
    this._classroomService.getTeachersClassrooms(
      this.start.toString(), this.cardPerPage.toString(), this.searchValue, this.teacher_id).subscribe(
        (res: any) => {
          this.classroomsCount = res.Object.count;
          this.classrooms = res.Object.objects;
          this.dataLoaded = true;
        });
  }

  search(event) {
    this.searchValue = event;
    this.getClassrooms();
  }

  openJourney(classroom) {
    this.router.navigate(['admin/teachers/classrooms', this.teacher_id, classroom.id]);
  }

  changePagination(num) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.getClassrooms();
  }

  changePageLimit() {
    this.start = 0;
    this.getClassrooms();
  }

}
