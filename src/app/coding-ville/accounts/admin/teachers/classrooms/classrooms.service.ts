import { Observable } from 'rxjs';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CustomEncoder } from '../../../../../@core/utils/custom-encoder';

@Injectable({
  providedIn: 'root',
})
export class ClassroomsService {

  constructor(private _globalService: GlobalService) { }


  // ===============================//
  // ------START CLASSROOM------//
  // ===============================//

  getTeachersClassrooms(start, limit, search, teacher_id) {
    const url: string = `coding-ville/admin/teacher/classroom/all`;
    const params = new HttpParams({ encoder: new CustomEncoder() })
      .set('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('teacher_id', teacher_id);
    return this._globalService.get(url, params);
  }

  // ===============================//
  // ------END CLASSROOM------//
  // ===============================//

  getGrades() {
    const url: string = `school/grade`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------START JOURNEYS------//
  // ===============================//

  getClassroomJourneys(start: string, limit: string, search: string, classroom_id: string, teacher_id: string, difficulty_id: string) {
    const url: string = `coding-ville/admin/teacher/classroom/journeys/all`;
    let params = new HttpParams()
      .set('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('classroom_id', classroom_id)
      .append('teacher_id', teacher_id);
    if (difficulty_id) params = params.append('difficulty_id', difficulty_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getJourneyAdventures(journey_id: string) {
    const url: string = `coding-ville/journey/${journey_id}/adventure`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END JOURNEYS------//
  // ===============================//

  // ===============================//
  // ------START PRE JOURNEYS------//
  // ===============================//

  getClassroomPreJourneys(start: string, limit: string, search: string, classroom_id: string, teacher_id: string, difficulty_id: string) {
    const url: string = `coding-ville/admin/teacher/classroom/activities/all`;
    let params = new HttpParams()
      .set('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('classroom_id', classroom_id)
      .append('teacher_id', teacher_id);
    if (difficulty_id) params = params.append('difficulty_id', difficulty_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getPreJourneyMissions(journey_id: string) {
    const url: string = `coding-ville/journey/${journey_id}/adventure`;
    return this._globalService.get(url);
  }

  // ===============================//
  // ------END JOURNEYS------//
  // ===============================//



  // ===============================//
  // ------START STUDENTS------//
  // ===============================//

  getClassroomStudents(start: string, limit: string, search: string, classroom_id: string, teacher_id: string, age: string) {
    const url: string = `coding-ville/admin/teacher/classroom/students/all`;
    let params = new HttpParams()
      .set('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('classroom_id', classroom_id)
      .append('teacher_id', teacher_id);
    if (age) params = params.append('age', age);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END STUDENTS------//
  // ===============================//

  // ===============================//
  // ------START PROJECTS------//
  // ===============================//

  getClassroomProjects(start, limit, search, classroom_id, teacher_id, student_id) {
    const url: string = 'coding-ville/admin/teacher/classroom/projects/all';
    let params = new HttpParams()
      .set('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('classroom_id', classroom_id)
      .append('teacher_id', teacher_id);
    if (student_id) params = params.append('student_id', student_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllStudentOfClassroom(classroom_id) {
    const url: string = `school/students/${classroom_id}`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllClassrooms() {
    const url = `coding-ville/teacher/classroom/classroom/all`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END PROJECTS------//
  // ===============================//



  // ===============================//
  // ------START STUDENT PROGRESS------//
  // ===============================//

  getJourneyProgress(start, limit, search, teacher_id, adventure_id, course_id) {
    const url = 'coding-ville/admin/teacher/classroom/journeys/data';
    const params = new HttpParams()
      .set('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('teacher_id', teacher_id)
      .append('adventure_id', adventure_id)
      .append('course_id', course_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


  getPreJourneyProgress(start, limit, search, teacher_id, activity_id, course_id) {
    const url = 'coding-ville/admin/teacher/classroom/activities/data';
    const params = new HttpParams()
      .set('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('teacher_id', teacher_id)
      .append('activity_id', activity_id)
      .append('course_id', course_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


  getStudentJourneyProgress(teacher_id, course_id, student_id) {
    const url = 'coding-ville/admin/teacher/classroom/journeys/student';
    const params = new HttpParams()
      .set('teacher_id', teacher_id)
      .append('course_id', course_id)
      .append('student_id', student_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


  getStudentPreJourneyProgress(teacher_id, course_id, student_id) {
    const url = 'coding-ville/admin/teacher/classroom/activities/student';
    const params = new HttpParams()
      .set('teacher_id', teacher_id)
      .append('course_id', course_id)
      .append('student_id', student_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


  getClassroomData(teacher_id, classroom_id) {
    const url = 'coding-ville/admin/teacher/classroom/data';
    const params = new HttpParams()
      .set('teacher_id', teacher_id)
      .append('classroom_id', classroom_id);
    return this._globalService.get(url, params);
  }

  // ===============================//
  // ------END  STUDENT PROGRESS------//
  // ===============================//



}
