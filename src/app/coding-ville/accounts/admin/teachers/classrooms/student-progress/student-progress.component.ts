import { ActivatedRoute, Params } from '@angular/router';
import { ClassroomsService } from '../classrooms.service';
import { GlobalService } from './../../../../../../@core/utils/global.service';
import { Breadcrumb } from './../../../../../../@core/data/breadcrumb';
import { Component, OnInit } from '@angular/core';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'ngx-student-progress',
  templateUrl: './student-progress.component.html',
  styleUrls: ['./student-progress.component.scss'],
})
export class StudentProgressComponent implements OnInit {
  teacher_id: number;
  classroom_id: number;
  journey_id: number;
  course_id: number;
  journey_type: string;
  studentsCount: number;
  classroom: any;
  breadcrumbs: Breadcrumb[];
  missions: any;
  students: any;
  adventures: any;
  initialAdventure: string = '1';
  dataLoaded: boolean = false;
  selectedAdventure: any;
  csvData: any;
  isPrintCSV = true;

  constructor(
    private _globalService: GlobalService,
    private _classroomService: ClassroomsService,
    private route: ActivatedRoute,
  ) { }


  ngOnInit() {
    this.getIDs();
    this.implementAllInOrder();
  }

  updateData(data: any) {
    this.getData(data.start, data.limit, data.search, data.selectedAdventure);
  }

  implementAllInOrder() {
    this._classroomService.getClassroomData(this.teacher_id, this.classroom_id).subscribe(data => {
      this.classroom = data.Object;
      this.getBreadcrumbs();
    });
    if (this.journey_type === 'journey') {
      this.getAdventures().subscribe(
        (res: any) => {
          this.adventures = res.Object.Adventure;
          this.initialAdventure = this.adventures[0].id.toString();
          this.getData(0, '10', '', this.initialAdventure);
        }, (error: any) => {
          console.log(error);
        },
      );
    } else if (this.journey_type === 'prejourney') {
      this.getData(0, 10, '', 1);
    }
  }

  getAdventures() {
    return this._classroomService.getJourneyAdventures(this.journey_id.toString());
  }

  getBreadcrumbs() {
    this.breadcrumbs = [
      {
        name: 'TEACHERS',
        link: 'teachers',
      },
      {
        name: 'CLASSROOMS',
        link: 'teachers/classrooms',
      },
      {
        name: this.classroom.Name,
        link: `teachers/classrooms/${this.teacher_id}/${this.classroom.id}`,
      },
      {
        name: 'PROGRESS',
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(this.breadcrumbs);
  }

  /**************************************/
  /********** HANDLE COUNT HERE *********/
  /**************************************/
  getData(start, limit, search, selectedAdventure) {
    this.selectedAdventure = selectedAdventure;
    if (this.journey_type === 'journey') {
      this._classroomService.getJourneyProgress(
        start,
        limit,
        search,
        this.teacher_id,
        selectedAdventure,
        this.course_id).subscribe((data: any) => {
          this.studentsCount = data.Object.count || 10;
          this.missions = data.Object.objects.missions;
          this.students = data.Object.objects.students;
          this.dataLoaded = true;
        });
    } else if (this.journey_type === 'prejourney') {
      this._classroomService.getPreJourneyProgress(
        start,
        limit,
        search,
        this.teacher_id,
        this.journey_id,
        this.course_id).subscribe((data: any) => {
          this.studentsCount = data.Object.count || 10;
          this.missions = data.Object.objects.missions;
          this.students = data.Object.objects.students;
          this.dataLoaded = true;
        });

    }
  }

  getIDs() {
    this.route.params.subscribe((params: Params) => {
      this.teacher_id = params['teacher_id'];
      this.classroom_id = params['classroom_id'];
      this.journey_id = params['journey_id'];
      this.course_id = params['course_id'];
      this.journey_type = params['journey_type'];
    });
  }

  prepareDataToCsv(missions, students) {
    for (let i = 0; i < students.length; i++) {
      for (let j = 0; j < missions.length; j++) {
        const success = students[i].tasks[j]['success'] ? 'success' : 'failed';
        students[i][missions[j].name] =
          'trials:' + String(students[i].tasks[j]['noOfTrials']) + '-' + success;
      }
    }
    this.csvData = students;
    this.csvData.filter((el) => {
      delete el.id;
      delete el.profile_image;
      delete el.tasks;
      return el;
    });

    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Students',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.csvData);
  }


  onExportCSV() {
    if (this.journey_type === 'journey') {
      this._classroomService.getJourneyProgress(
        0,
        'all',
        '',
        this.teacher_id,
        this.selectedAdventure,
        this.course_id).subscribe((data: any) => {
          this.prepareDataToCsv(data.Object.objects.missions, data.Object.objects.students);
        });
    } else if (this.journey_type === 'prejourney') {
      this._classroomService.getPreJourneyProgress(
        0,
        'all',
        '',
        this.teacher_id,
        this.journey_id,
        this.course_id).subscribe((data: any) => {
          this.prepareDataToCsv(data.Object.objects.missions, data.Object.objects.students);
        });

    }

  }

}
