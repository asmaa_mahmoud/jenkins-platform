import { GlobalService } from './../../../../@core/utils/global.service';
import { Breadcrumb } from './../../../../@core/data/breadcrumb';
import { TeachersService } from './teachers.service';
import { Component, OnInit } from '@angular/core';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'ngx-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss'],
})
export class TeachersComponent implements OnInit {

  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Teachers',
      link: '',
    },
  ];

  tableLengths = [10, 25, 50, 100];
  tableLength = '10';
  teachersCount: string;
  start: number = 0;
  teachers = [];
  currentPage: number = 1;
  searchValue: string = '';
  maxShown = '10';
  provincies = [];
  selectedProvince = null;
  dataLoaded: boolean = false;
  csvData: any;
  activities: any[];

  shareCode: string;

  constructor(private _globalService: GlobalService, private teachersService: TeachersService) {
    this._globalService.breadcrumbsService.getBreadcrumbs(this.breadcrumbs);
  }

  ngOnInit() {
    this.getAllProvincies();
    this.getTeachers();
  }

  getAllProvincies() {
    this.teachersService.getAllProvincies().subscribe(res => {
      this.provincies = res.Object;
    });
  }

  getTeachers() {
    this.dataLoaded = false;
    const province_id = this.selectedProvince === 'all' ? null : this.selectedProvince;
    this.teachersService.getTeachersData(this.start, this.tableLength, this.searchValue, province_id).subscribe(res => {
      this.teachersCount = res.Object.count;
      this.teachers = res.Object.objects;
      this.dataLoaded = true;
    });
  }

  changePagination(num: number) {
    this.currentPage = num;
    this.start = (num * Number(this.tableLength) - Number(this.tableLength));
    this.getTeachers();
  }

  search(event: string) {
    this.searchValue = event;
    this.start = 0;
    this.getTeachers();
  }

  onChangeLength() {
    this.start = 0;
    this.maxShown = this.tableLength;
    if (this.tableLength === 'all')
      this.maxShown = this.teachersCount;
    this.getTeachers();
  }

  onFilterByProvince() {
    this.start = 0;
    this.getTeachers();
  }

  onExportCSV() {
    const province_id = this.selectedProvince === 'all' ? null : this.selectedProvince;
    this.teachersService.getTeachersData(0, 'all', '', province_id)
      .subscribe((data: any) => {
        this.csvData = data.Object.objects;
        for (let i = 0; i < this.csvData.length; i++) {
          this.csvData[i]['schoolName'] = this.csvData[i].school['name'];
          this.csvData[i]['province'] = this.csvData[i].school['province'];
          this.csvData[i]['city'] = this.csvData[i].school['city'];
        }

        this.csvData.filter((el) => {
          delete el.id;
          delete el.school;
          return el;
        });

        const options = {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalSeparator: '.',
          showLabels: true,
          showTitle: true,
          title: 'Teachers',
          useTextFile: false,
          useBom: true,
          useKeysAsHeaders: true,
        };

        const csvExporter = new ExportToCsv(options);
        csvExporter.generateCsv(this.csvData);
      });
  }

}
