import { Injectable } from '@angular/core';
import { GlobalService } from '../../../../@core/utils/global.service';
import { HttpParams } from '@angular/common/http';
import { CustomEncoder } from '../../../../@core/utils/custom-encoder';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TeachersService {

  constructor(private _globalService: GlobalService) { }

  getAllProvincies(country_id = 39) {
    const url = `states?country_id=${country_id}`;
    return this._globalService.get(url);
  }

  getTeachersData(start, limit, search, province_id) {
    const url: string = `coding-ville/admin/teacher/all`;
    let params = new HttpParams({ encoder: new CustomEncoder() })
      .set('start', start)
      .append('limit', limit)
      .append('search', search);
    if (province_id) params = params.append('province_id', province_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }
}
