import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { Journey } from '../../../../@core/data/journey';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../@core/utils/global.service';
import { JourneysService } from '../../student/journey/journeys.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
import { TutorialVideoComponent } from './tutorial-video/tutorial-video.component';

@Component({
  selector: 'ngx-adventure',
  templateUrl: './adventure.component.html',
  styleUrls: ['./adventure.component.scss'],
})
export class AdventureComponent implements OnInit {

  showTutorial = true;

  classroom_id: string;
  role: string;

  currentUserRole: string;

  adventures: Journey[];
  missions: any[];
  journeysCards = [];
  showProject = false;
  journeyName: string;
  activeAdventure: any;
  selectedAdventureType: string;
  selectedAdventureId: any;
  dataLoaded: boolean = false;
  journeyId: any;
  courseID: any;
  classroomID: any;
  type: string;
  currentURL: string;
  noClassroom: boolean = true;
  classroomName;
  projectMissions: any;
  backInfoData: any;
  isOpened: boolean;
  journeyVideoUrl: string;
  tutorialVideoDialogRef: NbDialogRef<TutorialVideoComponent>;


  private breadcrumbs: Breadcrumb[] = [];
  constructor(
    private _journeyService: JourneysService,
    private _globalService: GlobalService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private dialogService: NbDialogService,
    private _authService: AuthenticationService,
  ) {
    this.router.events.subscribe(x => {
      this.currentURL = this.location.path();
    });
    this.route.snapshot.paramMap.get('classroom_id') ? this.classroom_id = this.route.snapshot.paramMap.get('classroom_id') :
      this._authService.studentClassroomID ? this._authService.studentClassroomID.subscribe(x => this.classroom_id = x) :
        this.noClassroom = true;
    this.route.snapshot.paramMap.get('classroom_id') ? this.role = 'teacher' :
      this._authService.studentClassroomID ? this.role = 'student' :
        this.role = 'teacher';
    if (this.noClassroom) {
      this.route.queryParams
        .filter(params => params.classroomName)
        .subscribe(params => {
          this.classroomName = params.classroomName;
        });
    }
  }

  ngOnInit() {
    this.journeyId = this.route.snapshot.paramMap.get('journey_id');
    this.courseID = this.route.snapshot.paramMap.get('course_id');
    this.classroomID = this.route.snapshot.paramMap.get('classroom_id');
    this.type = this.route.snapshot.paramMap.get('type');
    this.backInfoData = JSON.parse(localStorage.getItem('backInfo'));
    if (this.type === 'adventure') {
      this.getJourneyAdventures();
    } else if (this.type === 'missions' && this.courseID) {
      this.getPreJourneyMissions();
    } else if (this.type === 'missions' && !this.courseID) {
      this.getPreJourneyMissionsWithNoCourse();
    }
  }

  setBackInfoForWorkspace() {
    localStorage.setItem('backURL', this.currentURL);
  }

  initBreadcrumb() {
    if (this.classroomID) {
      this.breadcrumbs = [
        {
          name: 'Classrooms',
          link: 'classroom',
        },
        {
          name: this.classroomName,
          link: 'classroom/' + this.classroomID,
        },
        {
          name: this.journeyName,
          link: '',
        },
      ];
    } else {
      this.breadcrumbs = [
        {
          name: 'Journeys',
          link: 'journeys',
        },
        {
          name: this.journeyName,
          link: '',
        },
      ];
    }

    this._globalService.getBreadcrumbs(this.breadcrumbs);
  }

  getJourneyAdventures() {
    this._journeyService.getJourneyAdventure(this.journeyId, this.courseID).subscribe((data: any) => {
      this.journeyName = data.Object.JourneyName;
      this.isOpened = data.Object.isOpened;
      this.journeyVideoUrl = data.Object.journeyVideoUrl;
      this.adventures = data.Object.Adventure;
      this.initBreadcrumb();
      this.checkTutorialVideo();

      if (this.backInfoData && this.journeyId === this.backInfoData['journeyID']) {
        this.selectedAdventureId = this.backInfoData['adventureID'];
        this.selectedAdventureType = this.backInfoData['adventureType'];
      } else {
        this.selectedAdventureId = this.adventures[0].id;
        this.selectedAdventureType = this.adventures[0].adventure_type;
      }
      this.courseID ? this.getAdventureMissions(this.selectedAdventureId, this.selectedAdventureType)
        : this.getAdventureMissionsWithoutCourseID(this.journeyId, this.selectedAdventureId, this.selectedAdventureType);
    }, (error: string) => {
      console.log(error);
    },
    );
  }

  checkTutorialVideo() {
    if (!this.isOpened && this.journeyVideoUrl) {
      this.videoSeen();
      this.tutorialVideoDialogRef = this.dialogService.open(TutorialVideoComponent, {
        context: {
          videoUrl: this.journeyVideoUrl,
        },
      });
    }
  }

  videoSeen() {
    this._journeyService.videoSeen(this.journeyId).subscribe(data => { });
  }

  structureProjectMissions(missions) {
    const beginner = [];
    const intermediate = [];
    const advanced = [];
    beginner.push(...missions.filter(mission => mission.type === 'project' && mission.difficulty === 'Beginner'));
    beginner[0] && (missions.indexOf(beginner[0]) - 1) !== -1 && missions[missions.indexOf(beginner[0]) - 1].type === 'lesson' ?
      beginner.unshift(missions[missions.indexOf(beginner[0]) - 1]) : null;
    intermediate.push(...missions.filter(mission => mission.type === 'project' && mission.difficulty === 'Intermediate'));
    intermediate[0] && (missions.indexOf(intermediate[0]) - 1) !== -1 && missions[missions.indexOf(intermediate[0]) - 1].type === 'lesson' ?
      intermediate.unshift(missions[missions.indexOf(intermediate[0]) - 1]) : null;
    advanced.push(...missions.filter(mission => mission.type === 'project' && mission.difficulty === 'Advanced'));
    advanced[0] && (missions.indexOf(advanced[0]) - 1) !== -1 && missions[missions.indexOf(advanced[0]) - 1].type === 'lesson' ?
      advanced.unshift(missions[missions.indexOf(advanced[0]) - 1]) : null;
    this.projectMissions = {
      beginner: beginner,
      intermediate: intermediate,
      advanced: advanced,
    };
  }

  getAdventureMissions(adventureID, adventureType) {
    this.dataLoaded = false;
    this.selectedAdventureType = adventureType;
    this.selectedAdventureId = adventureID;
    this._journeyService.getAdventureMissions(adventureID, this.courseID).subscribe((data: any) => {
      this.convertMissionTypesNames(data.Object);
      this.dataLoaded = true;
      this.selectedAdventureType === 'project' ? this.structureProjectMissions(this.missions) : null;
    }, (error: string) => {
      console.log(error);
    },
    );
  }

  getAdventureMissionsWithoutCourseID(journeyId, adventureID, adventureType) {
    this.dataLoaded = false;
    this.selectedAdventureId = adventureID;
    this.selectedAdventureType = adventureType;
    this._journeyService.getAdventureMissionsWithoutCourseID(journeyId, adventureID).subscribe((data: any) => {
      this.convertMissionTypesNames(data.Object);
      this.dataLoaded = true;
      this.selectedAdventureType === 'project' ? this.structureProjectMissions(this.missions) : null;
    }, (error: string) => {
      console.log(error);
    },
    );
  }

  checkForCourseID(adventureID, adventureType) {
    this.selectedAdventureType = adventureType;
    this.courseID ? this.getAdventureMissions(adventureID, adventureType)
      : this.getAdventureMissionsWithoutCourseID(this.journeyId, adventureID, adventureType);
  }

  getPreJourneyMissions() {
    this._journeyService.getPreJourneyMissions(this.classroom_id, this.journeyId).subscribe((data: any) => {
      this.journeyName = data.Object.name;
      this.convertMissionTypesNames(data.Object.missions);
      this.initBreadcrumb();
      this.dataLoaded = true;
    }, (error: string) => {
      console.log(error, 'preJourney');
    },
    );
  }

  getPreJourneyMissionsWithNoCourse() {
    this._journeyService.getPreJourneyMissionsWithNoCourse(this.journeyId).subscribe((data: any) => {
      this.journeyName = data.Object.name;
      this.convertMissionTypesNames(data.Object.missions);
      this.initBreadcrumb();
      this.dataLoaded = true;
    });
  }

  convertMissionTypesNames(missions) {
    missions.forEach(element => {
      if (element.type === 'mission') element.type = 'blocks';
      else if (element.type === 'html_mission') element.type = 'lesson';
      else if (element.type === 'mission_editor' || element.type === 'editor_mission') element.type = 'webeditor';
    });
    this.missions = missions;
  }

  setBackInfo() {
    const backInfo = {
      adventureID: this.selectedAdventureId,
      adventureType: this.selectedAdventureType,
      journeyID: this.journeyId,
    };

    localStorage.setItem('backInfo', JSON.stringify(backInfo));
  }

  goToMission(mission) {
    if (!mission.isLocked) {
      this.setBackInfoForWorkspace();
      this.setBackInfo();
      if (mission.type === 'project') {
        this.goToProjectMission(mission);
      } else {
        if (this.type === 'adventure' && this.courseID) {
          window.location.href =
            'workspaces/journey/' + mission.type + '/' + this.journeyId + '/' +
            this.selectedAdventureId + '/' + mission.id + '/' + this.courseID;
        } else if (this.type === 'adventure' && !this.courseID) {
          window.location.href =
            'workspaces/journey/' + mission.type + '/' + this.journeyId + '/' +
            this.selectedAdventureId + '/' + mission.id;
        } else if (this.type === 'missions' && this.courseID) {
          window.location.href =
            'workspaces/prejourney/' + mission.type + '/' + this.journeyId + '/' + mission.id + '/' + this.courseID;
        } else if (this.type === 'missions' && !this.courseID) {
          window.location.href =
            'workspaces/prejourney/' + mission.type + '/' + this.journeyId + '/' + mission.id;
        }
      }
    }
  }

  goToProjectMission(mission) {
    if (!mission.isLocked) {
      if (mission.type_id === 1 || mission.type_id === 2) {
        if (this.type === 'adventure' && this.courseID) {
          this.router.navigate([`project-game/journey/${this.selectedAdventureId}/${mission.id}/${this.journeyId}/${this.courseID}`]);
        } else if (this.type === 'adventure' && !this.courseID) {
          this.router.navigate([`project-game/journey/${this.selectedAdventureId}/${mission.id}/${this.journeyId}`]);
        } else if (this.type === 'missions' && this.courseID) {
          this.router.navigate([`project-game/pre-journey/${mission.id}/${this.journeyId}/${this.courseID}`]);
        } else if (this.type === 'missions' && !this.courseID) {
          this.router.navigate([`project-game/pre-journey/${mission.id}/${this.journeyId}`]);
        }
      } else if (mission.type_id === 3) {
        if (this.type === 'adventure' && this.courseID) {
          window.location.href =
            'workspaces/journey/project/app/' + this.journeyId + '/' + this.selectedAdventureId + '/' + mission.id + '/' + this.courseID;
        } else if (this.type === 'adventure' && !this.courseID) {
          window.location.href =
            'workspaces/journey/project/app/' + this.journeyId + '/' + this.selectedAdventureId + '/' + mission.id;
        } else if (this.type === 'missions' && this.courseID) {
          window.location.href =
            'workspaces/prejourney/project/app/' + this.journeyId + '/' + mission.id + '/' + this.courseID;
        } else if (this.type === 'missions' && !this.courseID) {
          'workspaces/prejourney/project/app/' + this.journeyId + '/' + mission.id;
        }
      }
    }
  }

}
