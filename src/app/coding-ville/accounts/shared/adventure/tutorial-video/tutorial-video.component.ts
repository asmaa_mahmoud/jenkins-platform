import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { JourneysService } from '../../../student/journey/journeys.service';

@Component({
  selector: 'ngx-tutorial-video',
  templateUrl: './tutorial-video.component.html',
  styleUrls: ['./tutorial-video.component.scss'],
})
export class TutorialVideoComponent implements OnInit {
  @Input() videoUrl: string;

  constructor(protected ref: NbDialogRef<TutorialVideoComponent>, private journeyService: JourneysService) { }

  ngOnInit() {
  }

  closeTutorialVideo() {
    this.ref.close();
  }



}
