import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../../@core/utils/global.service';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { User } from '../../../../@core/data/users';
import { SurveyModalComponent } from '../survey-modal/survey-modal.component';

@Component({
  selector: 'ngx-agreement-modal',
  templateUrl: './agreement-modal.component.html',
  styleUrls: ['./agreement-modal.component.scss'],
})
export class AgreementModalComponent implements OnInit {

  private userData: User;


  constructor(private globalService: GlobalService,
    private _dialogService: NbDialogService,
    private authService: AuthenticationService,
    protected ref: NbDialogRef<AgreementModalComponent>) { }

  ngOnInit() {
    this.userData = this.authService.currentUserValue;
  }

  acceptTerms() {
    const status = this.authService.isActivated();
    this.globalService.acceptTermsOfUse().subscribe(data => {
      this.userData['terms_agreement'] = true;
      localStorage.setItem('AuthorizationData', JSON.stringify(this.userData));
      if (this.userData['hasLogged'] === 0 && status) {
        // this._dialogService.open(SurveyModalComponent, {
        //   context: {
        //     scriptKey: 'initial',
        //   },
        // });
      }
      this.close();
    });
  }

  close() {
    this.ref.close();
  }

}
