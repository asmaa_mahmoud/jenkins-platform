import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-badge-card',
  templateUrl: './badge-card.component.html',
  styleUrls: ['./badge-card.component.scss'],
})
export class BadgeCardComponent implements OnInit {

  constructor() { }

  @Input() badgeData: any;

  ngOnInit() {
  }

}
