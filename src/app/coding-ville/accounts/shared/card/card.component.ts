import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { Role } from '../../../../@core/data/role';

@Component({
  selector: 'ngx-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() cardData: any;

  @Input() canLike: boolean = true;

  @Input() isDelete: boolean;
  @Input() isShare: boolean;
  @Input() isEdit: boolean;
  @Input() isUnassign: boolean;
  @Input() isMakePublic;
  @Input() isClassroom: boolean;
  @Input() progressPercentage: number;


  @Input() btn1: string;
  @Input() btn2: string;


  @Output() deleteClickedEvent = new EventEmitter<void>();
  @Output() editClickedEvent = new EventEmitter<void>();
  @Output() unassignClickedEvent = new EventEmitter<void>();
  @Output() makePublicBtnClicked = new EventEmitter<void>();
  @Output() shareClickedEvent = new EventEmitter<void>();
  @Output() likeBtnClicked = new EventEmitter<void>();
  @Output() btn1Clicked = new EventEmitter<void>();
  @Output() btn2Clicked = new EventEmitter<void>();

  isMenuOpen = false;
  currentUser: any;

  constructor(private router: Router, private _authService: AuthenticationService) { }

  ngOnInit() {
  }


  startShare(e) {
    e.stopPropagation();
    this.shareClickedEvent.next();
  }

  toggleLike(event, cardData) {
    if (this.canLike) {
      event.stopPropagation();
      cardData.isLiked = !cardData.isLiked;
      this.likeBtnClicked.next();
    }
  }

  edit() {
    this.editClickedEvent.next();
  }

  delete() {
    this.deleteClickedEvent.next();
  }

  unassign() {
    this.unassignClickedEvent.next();
  }

  makePublic() {
    this.makePublicBtnClicked.next();
  }

  fireBtn1() {
    this.btn1Clicked.next();
  }

  fireBtn2() {
    this.btn2Clicked.next();
  }

  goToStudentProfile(studentId, event) {
    event.stopPropagation();
    this._authService.currentUser.subscribe(x => this.currentUser = x);
    if (Role['Teacher'].includes(this.currentUser.role)) {
      this.router.navigate(['teacher/profile/', studentId]);
    } else if (Role['Student'].includes(this.currentUser.role) || Role['individualStudent'].includes(this.currentUser.role)) {
      this.router.navigate(['student/profile/', studentId]);
    }
  }
}
