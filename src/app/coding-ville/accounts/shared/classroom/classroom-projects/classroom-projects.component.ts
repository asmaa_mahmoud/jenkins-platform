import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from '../../../../../@core/data/project';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ShareModalComponent } from '../../../shared/share-modal/share-modal.component';

@Component({
  selector: 'ngx-shared-classroom-projects',
  templateUrl: './classroom-projects.component.html',
  styleUrls: ['./classroom-projects.component.scss'],
})
export class SharedClassroomProjectsComponent implements OnInit {

  @Output() emitLikeProject = new EventEmitter<number>();
  @Output() updateData = new EventEmitter<any>();

  @Input() projects: Project[];
  @Input() projectsCount: number;
  @Input() students: any[];
  @Input() classroomName: string;
  @Input() classroom_id: number;

  cardsPerPage = [6, 9, 12];
  cardPerPage: number = 6;
  currentPage: number = 1;
  start: number = 1;
  limit: number = 3;
  searchValue: string = '';
  selectedStudent: string = null;

  constructor(
    private router: Router,
    private dialogService: NbDialogService,
  ) { }

  ngOnInit() {}

  toggleLikeProject(index: number) {
    this.emitLikeProject.emit(index);
  }

  openProject(project) {
    this.router.navigate([`teacher/classroom/${project.id}/preview-project`],
    { queryParams: { classroomName: this.classroomName, classroomID: this.classroom_id } });
  }

  search(event) {
    this.searchValue = event;
    this.start = 0;
    this.updateData.emit({start: this.start, limit: this.cardPerPage, search: this.searchValue, student_id: this.selectedStudent});
  }

  changeStudent() {
    this.start = 0;
    this.updateData.emit({start: this.start, limit: this.cardPerPage, search: this.searchValue, student_id: this.selectedStudent});
  }

  changeItemPerPage() {
    this.start = 0;
    this.updateData.emit({start: this.start, limit: this.cardPerPage, search: this.searchValue, student_id: this.selectedStudent});
  }

  changePagination(num) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.updateData.emit({start: this.start, limit: this.cardPerPage, search: this.searchValue, student_id: this.selectedStudent});
  }

  share(project) {
    if (project.isPublic) {
      const data = {
        name: project.name,
        createdBy: project.createdBy,
        image: project.image,
        link: window.location.host + `/visitor/discover/${project.hashId}/discover-project`,
      };

      this.dialogService.open(ShareModalComponent, {
        context: {
          data: data,
        },
      });
    }

  }

}
