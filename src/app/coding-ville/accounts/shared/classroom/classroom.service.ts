import { Observable } from 'rxjs';
import { Classroom } from '../../../../@core/data/classroom';
import { GlobalService } from '../../../../@core/utils/global.service';
import { HttpParams } from '@angular/common/http';

export class ClassroomsService {


  constructor(private _globalService: GlobalService) { }

  // ===============================//
  // ------START CLASSROOM------//
  // ===============================//
  countClassrooms() {
    const url = 'school/teacher/classrooms/count';
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getClassrooms(start, limit, search): Observable<Classroom[]> {
    const url: string = `coding-ville/teacher/classroom/classroom/all`;
    const params = new HttpParams().set('start', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END CLASSROOM------//
  // ===============================//

  getGrades() {
    const url: string = `school/grade`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------START JOURNEYS------//
  // ===============================//

  getJourneysCount(classroom_id: string, search) {
    const url: string = 'coding-ville/teacher/classroom/journeys/count';
    const params = new HttpParams().set('classroom_id', classroom_id).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllJourneys(classroom_id: string, start: string, limit: string, search: string) {
    const url: string = `coding-ville/teacher/classroom/journeys/all`;
    const params = new HttpParams().set('classroom_id', classroom_id)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getJourneyAdventures(journey_id: string) {
    const url: string = `coding-ville/journey/${journey_id}/adventure`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END JOURNEYS------//
  // ===============================//

  // ===============================//
  // ------START PRE JOURNEYS------//
  // ===============================//

  getPreJourneysCount(classroom_id: string, search) {
    const url: string = 'coding-ville/teacher/classroom/activities/count';
    const params = new HttpParams().set('classroom_id', classroom_id).append('search', search);
    return this._globalService.get(url, params);
  }

  getAllPreJourneys(classroom_id: string, start: string, limit: string, search: string) {
    const url: string = `coding-ville/teacher/classroom/activities/all`;
    const params = new HttpParams().set('classroom_id', classroom_id)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getPreJourneyMissions(journey_id: string) {
    const url: string = `coding-ville/journey/${journey_id}/adventure`;
    return this._globalService.get(url);
  }

  // ===============================//
  // ------END JOURNEYS------//
  // ===============================//



  // ===============================//
  // ------START STUDENTS------//
  // ===============================//

  getStudentsCount(classroom_id: string) {
    const url: string = `school/classrooms/${classroom_id}/count`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getClassroomStudents(classroom_id: string, start: string, limit: string, search: string) {
    const url: string = `school/students/${classroom_id}/index`;
    const params = new HttpParams().set('start_from', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END STUDENTS------//
  // ===============================//

  // ===============================//
  // ------START PROJECTS------//
  // ===============================//

  countProjectsOfClassroom(classroom_id, search) {
    const url: string = `school/${classroom_id}/project/count`;
    const params = new HttpParams().set('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getProjectsOfClassroom(classroom_id, start, limit, search, student_id) {
    const url: string = `coding-ville/classroom/${classroom_id}/project`;
    const params = new HttpParams().set('classroom_id', classroom_id)
      .append('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('student_id', student_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllStudentOfClassroom(classroom_id) {
    const url: string = `school/students/${classroom_id}`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllClassrooms() {
    const url = `coding-ville/teacher/classroom/classroom/all`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END PROJECTS------//
  // ===============================//



  // ===============================//
  // ------START STUDENT PROGRESS------//
  // ===============================//
  getStudentJourneyProgress(adventureID, courseID, start, limit, search) {
    const url = 'coding-ville/teacher/classroom/journeys/students';
    const params = new HttpParams()
      .set('adventure_id', adventureID)
      .append('course_id', courseID)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


  getStudentPreJourneyProgress(activity_id, courseID, start, limit, search) {
    const url = 'coding-ville/teacher/classroom/activities/students';
    const params = new HttpParams()
      .set('activity_id', activity_id)
      .append('course_id', courseID)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


  getClassroomData(classroomID) {
    const url = `school/classroom/${classroomID}`;
    return this._globalService.get(url);
  }

  // ===============================//
  // ------END  STUDENT PROGRESS------//
  // ===============================//



}
