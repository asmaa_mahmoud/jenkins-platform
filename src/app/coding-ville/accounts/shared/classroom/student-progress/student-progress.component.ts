import { Router } from '@angular/router';
import { Breadcrumb } from './../../../../../@core/data/breadcrumb';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-shared-student-progress',
  templateUrl: './student-progress.component.html',
  styleUrls: ['./student-progress.component.scss'],
})
export class SharedStudentProgressComponent implements OnInit, OnChanges {

  @Output() updateData = new EventEmitter<any>();
  @Output() exportCsv = new EventEmitter<any>();

  @Input() breadcrumbs: Breadcrumb[];
  @Input() missions: any;
  @Input() students: any;
  @Input() studentsCount: number;
  @Input() adventures: any;
  @Input() initialAdventure: string;
  @Input() classroom_id: number;
  @Input() journey_id: number;
  @Input() course_id: number;
  @Input() journey_type: string;
  @Input() isPrintCSV: boolean;

  tableLengths = [10, 25, 50, 100];
  tableLength = '10';
  start: number = 0;
  searchValue: string = '';
  currentAdventure: string = '1';
  currentURL: string;
  currentPage: number = 1;

  constructor(private location: Location, private router: Router) {
    this.router.events.subscribe(x => {
      this.currentURL = this.location.path();
    });
  }


  ngOnInit() { }

  ngOnChanges() {
    this.currentAdventure = this.initialAdventure;
  }

  goToMission(mission, student_id) {
    this.setBackInfoForWorkspace();
    const mission_type = mission.type === 'mission_editor' ? 'webeditor' : 'blocks';
    if (this.journey_type === 'journey') {
      if (mission.type === 'project') {
        if (mission.type_id === 1 || mission.type_id === 2) {
          this.router.navigate([`project-game/journey/${this.currentAdventure}/${mission.id}/${this.journey_id}/${this.course_id}/${student_id}`]);
        } else if (mission.type_id === 3) {
          window.location.href =
            `workspaces/journey/${mission_type}/app/${this.journey_id}/${this.currentAdventure}/${mission.id}/${this.course_id}/${student_id}`;
        }
      } else {
        window.location.href =
          `workspaces/journey/${mission_type}/${this.journey_id}/${this.currentAdventure}/${mission.id}/${this.course_id}/${student_id}`;
      }
    } else if (this.journey_type === 'prejourney') {
      if (mission.type === 'project') {
        if (mission.type_id === 1 || mission.type_id === 2) {
          this.router.navigate([`project-game/prejourney/${mission.id}/${this.journey_id}/${this.course_id}/${student_id}`]);
        } else if (mission.type_id === 3) {
          window.location.href =
            `workspaces/prejourney/${mission_type}/app/${this.journey_id}/${mission.id}/${this.course_id}/${student_id}`;
        }
      } else {
        window.location.href =
          `workspaces/prejourney/${mission_type}/${this.journey_id}/${mission.id}/${this.course_id}/${student_id}`;
      }
    }
  }

  onChangeAdventure() {
    this.start = 0;
    this.updateData.emit(
      { start: this.start, limit: this.tableLength, search: this.searchValue, selectedAdventure: this.currentAdventure });
  }

  onChangeLength() {
    this.start = 0;
    this.updateData.emit(
      { start: this.start, limit: this.tableLength, search: this.searchValue, selectedAdventure: this.currentAdventure });
  }

  onSearch(value: string) {
    this.searchValue = value;
    this.start = 0;
    this.updateData.emit(
      { start: this.start, limit: this.tableLength, search: this.searchValue, selectedAdventure: this.currentAdventure });
  }

  changePagination(num: number) {
    this.currentPage = num;
    this.start = num * +this.tableLength - +this.tableLength;
    this.updateData.emit(
      { start: this.start, limit: this.tableLength, search: this.searchValue, selectedAdventure: this.currentAdventure });
  }

  setBackInfoForWorkspace() {
    localStorage.setItem('backURL', this.currentURL);
  }

  onExportCSV() {
    this.exportCsv.emit();
  }

}
