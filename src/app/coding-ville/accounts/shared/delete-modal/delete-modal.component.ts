import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { ClassRoomService } from '../../teacher/classroom/class-room.service';
import { GlobalService } from '../../../../@core/utils/global.service';
import { ProjectsService } from '../projects/projects.service';

@Component({
  selector: 'ngx-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss'],
})
export class DeleteModalComponent {

  @Output() dataUpdated = new EventEmitter<void>();
  @Input() type: string;
  @Input() title: string;
  @Input() content: string;
  @Input() deleted_id: number;

  service;

  forced = 1;

  constructor(protected ref: NbDialogRef<DeleteModalComponent>,
    private _classroomService: ClassRoomService,
    private _globalService: GlobalService,
    private _projectService: ProjectsService) { }

  close() {
    this.ref.close();
  }

  deleteAction() {
    this.service = this.type === 'classroom' ? this._classroomService.deleteClassroom(this.deleted_id, String(this.forced)) :
      this.type === 'classroomStudent' ? this._classroomService.deleteStudent(this.deleted_id) :
        this.title === 'project' ? this._projectService.deleteProject(this.deleted_id) :
          this._classroomService.deleteClassroom(this.deleted_id, String(this.forced));
    this.service.subscribe(
      (data: any) => {
        this.dataUpdated.emit();
        this._globalService.showTranslatedToast('success', null , 'deleted-successfully');
        this.ref.close();
      },
      (error: any) => {
        console.log(error);
        this.ref.close();
      },
    );
  }

}
