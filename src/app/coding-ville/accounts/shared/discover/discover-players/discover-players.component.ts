import { Component, OnInit } from '@angular/core';
import { Player } from '../../../../../@core/data/player';
import { Router } from '@angular/router';
import { DiscoverService } from '../discover.service';
import { Role } from '../../../../../@core/data/role';
import { AuthenticationService } from '../../../../../@core/utils/authentication.service';

@Component({
  selector: 'ngx-discover-players',
  templateUrl: './discover-players.component.html',
  styleUrls: ['./discover-players.component.scss'],
})
export class DiscoverPlayersComponent implements OnInit {
  players: Player[];
  playersCount = 1;
  noOfCardsPerPage: any[] = [6, 9, 12];
  cardPerPage = 6;
  currentPage = 1;
  start: number = 0;
  dataLoaded: boolean = false;
  currentRole: any;

  private searchValue = '';
  constructor(private _discoverService: DiscoverService,
    private router: Router,
    private _authService: AuthenticationService) {
    this._authService.currentUser.subscribe(x => this.currentRole = x.role);
  }

  ngOnInit() {
    this.getPlayers();
    this.countPlayers();
  }

  changePagination(num: number) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.getPlayers();
  }

  goToUserProfile(student_id) {
    if (Role['Teacher'].includes(this.currentRole)) {
      this.router.navigate(['teacher/profile', student_id]);
    } else if (Role['Student'].includes(this.currentRole) || Role['individualStudent'].includes(this.currentRole)) {
      this.router.navigate(['student/profile', student_id]);
    }
  }

  changeItemPerPage() {
    this.start = 0;
    this.getPlayers();
  }

  search(value: string) {
    this.searchValue = value;
    this.start = 0;
    this.getPlayers();
  }


  getPlayers() {
    this._discoverService.getTopPlayers(
      this.start.toString(), this.cardPerPage.toString(), this.searchValue,
    ).subscribe(
      res => {
        if (res.status_code === 200) {
          this.players = res.Object;
          this.dataLoaded = true;
        }
      },
    );
  }

  countPlayers() {
    this._discoverService.countTopPlayers(this.searchValue).subscribe(
      res => {
        if (res.status_code === 200) {
          this.playersCount = res.Object.Count;
        }
      });
  }



}
