import { Subscription } from 'rxjs';
import { LikedProject } from '../../../../../@core/data/likedProject';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DiscoverService } from '../discover.service';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ShareModalComponent } from '../../share-modal/share-modal.component';
import { AuthenticationService } from '../../../../../@core/utils/authentication.service';
import { Role } from '../../../../../@core/data/role';
import { ProjectsService } from '../../projects/projects.service';
@Component({
  selector: 'ngx-discover-projects',
  templateUrl: './discover-projects.component.html',
  styleUrls: ['./discover-projects.component.scss'],
})
export class DiscoverProjectsComponent implements OnInit, OnDestroy {
  cardPerPage = 6;
  projects: LikedProject[] = [];
  noOfCardsPerPage: any[] = [6, 9, 12];
  currentPage = 1;
  start: number = 0;
  projectsCount = 1;
  searchValue = '';
  dataLoaded: boolean = false;
  currentUser;
  currentRole;
  shareLink;

  subscriptions = new Subscription();

  constructor(private dialogService: NbDialogService,
    private _projectService: ProjectsService,
    private _discoverService: DiscoverService,
    private _authService: AuthenticationService,
    private router: Router) {
    this._authService.currentUser.subscribe(x => { this.currentUser = x, this.currentRole = x.role; });
  }

  ngOnInit() {
    this.getProjectsCount();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  changePagination(num: number) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.getProjectsList();
  }

  openJourneyAdventure(journey) {
    this.router.navigate(['/student/adventure']);
  }

  getProjectsCount() {
    this._discoverService.countTopProjects(this.searchValue).subscribe(
      res => {
        this.projectsCount = res.Object.Count;
        this.getProjectsList();
      });
  }

  changeItemPerPage() {
    this.start = 0;
    this.getProjectsCount();
  }

  search(value: string) {
    this.searchValue = value;
    this.start = 0;
    this.getProjectsList();
  }

  getProjectsList() {
    this._discoverService.getTopProjects(
      this.start.toString(), this.cardPerPage.toString(), this.searchValue,
    ).subscribe(
      res => {
        if (res.status_code === 200) {
          this.projects = res.Object.filter((el) => {
            return delete el.createdAt;
          });
          this.dataLoaded = true;
        }
      },
    );
  }

  toggleLikeOnCardUI(index) {
    if (this.projects[index].isLiked) this.projects[index].noOfLikes++;
    else this.projects[index].noOfLikes--;
  }

  toggleLikeProject(index) {
    this.toggleLikeOnCardUI(index);
    const sub = this._discoverService
      .toggleLikeProject(this.projects[index].id)
      .subscribe(res => { },
        error => {
          console.log('error:', error);
          this.projects[index].isLiked = !this.projects[index].isLiked;
          this.toggleLikeOnCardUI(index);
        }, () => { });
    this.subscriptions.add(sub);
  }

  share(project) {
    if (project.isPublic) {
      const data = {
        name: project.name,
        createdBy: project.createdBy,
        image: project.icon_url,
        link: window.location.host + `/visitor/discover/${project.hashId}/discover-project`,
      };
      this.dialogService.open(ShareModalComponent, {
        context: {
          data: data,
        },
      });
    }
  }

  openProjectOverview(projectId) {
    if (Role['Teacher'].includes(this.currentUser.role)) {
      this.router.navigate([`teacher/discover/${projectId}/discover-project`]);
    } else if (Role['Student'].includes(this.currentUser.role) || Role['individualStudent'].includes(this.currentUser.role)) {
      this.router.navigate([`student/discover/${projectId}/discover-project`]);
    }
  }

}
