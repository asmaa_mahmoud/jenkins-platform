import { Component, OnInit } from '@angular/core';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss'],
})
export class DiscoverComponent implements OnInit {

  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Discover',
      link: '',
    },
  ];
  constructor(private _globalService: GlobalService) { }

  ngOnInit() {
    this._globalService.getBreadcrumbs(this.breadcrumbs);
  }

}
