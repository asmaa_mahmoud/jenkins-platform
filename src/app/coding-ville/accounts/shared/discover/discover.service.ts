import { Injectable } from '@angular/core';
import { GlobalService } from '../../../../@core/utils/global.service';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DiscoverService {

  constructor(private _globalService: GlobalService) { }

  countTopPlayers(search) {
    const url: string = `coding-ville/projects/users/top/count`;
    const params = new HttpParams().append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error);
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getTopPlayers(startFrom = '1', limit = '3', search = '') {
    const url: string = `coding-ville/projects/users/top`;
    const params = new HttpParams().set('start_from', startFrom).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error);
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  countTopProjects(search) {
    const url: string = `coding-ville/projects/all/top/count`;
    const params = new HttpParams().append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error);
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getTopProjects(startFrom = '1', limit = '3', search = '') {
    const url: string = `coding-ville/projects/all/top`;
    const params = new HttpParams().set('start_from', startFrom).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error);
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  public toggleLikeProject(projectID) {
    const url: string = `coding-ville/projects/${projectID}/toggleLikeState`;
    return this._globalService.put(url, {}).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error);
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


}
