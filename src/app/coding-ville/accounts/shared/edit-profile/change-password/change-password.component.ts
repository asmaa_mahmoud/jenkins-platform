import { GlobalService } from './../../../../../@core/utils/global.service';
import { AuthenticationService } from './../../../../../@core/utils/authentication.service';
import { Component, OnInit } from '@angular/core';
import { EditProfileService } from '../edit-profile.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'ngx-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  loading = false;
  submitted: any;
  current_password: any;
  new_password: any;
  retype_password: any;
  passwordsMatch: boolean;
  retypePasswordMsg: string;

  constructor(
    private _globalService: GlobalService,
    private formBuilder: FormBuilder,
    private _editProfile: EditProfileService,
    private _authService: AuthenticationService,
  ) { }

  ngOnInit() {
    this.setBreadcrumbs();
    this.changePasswordForm = this.formBuilder.group({
      current_password: ['', [Validators.required, Validators.maxLength(24), Validators.minLength(6)]],
      new_password: ['', [Validators.required, Validators.maxLength(24), Validators.minLength(6)]],
      retype_password: ['', [Validators.required, Validators.maxLength(24), Validators.minLength(6)]],
    });
  }


  setBreadcrumbs() {
    const breadcrumbs = [
      {
        name: 'Edit Profile',
        link: '',
      },
      {
        name: 'Change Password',
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(breadcrumbs);
  }

  // convenience getter for easy access to form fields
  get f() { return this.changePasswordForm.controls; }

  onSubmit() {
    if (this.changePasswordForm.invalid || this.f.new_password.value !== this.f.retype_password.value) {
      return;
    }
    this.loading = true;
    const body = {
      old: this.f.current_password.value,
      new: this.f.new_password.value,
    };
    this._editProfile.changePassword(body).subscribe(
      (data: any) => {
        this.loading = false;
        this._authService.logout();
      }, (error: any) => {
        console.log(error);
        this.loading = false;
      },
    );
  }

  validateRetype() {
    if (!this.f.retype_password.value || !this.f.new_password.value) {
      this.retypePasswordMsg = '';
    } else if (this.f.retype_password.value !== this.f.new_password.value) {
      this.retypePasswordMsg = 'passwords must match';
      this.passwordsMatch = false;
    } else {
      this.retypePasswordMsg = 'passwords match';
      this.passwordsMatch = true;
    }
  }

}
