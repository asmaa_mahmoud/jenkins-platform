import { env } from './../../../../@core/env/env';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'ngx-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {

  tabs = [];
  isOrg = env.isOrg;
  isTeacher;

  constructor(private _translateService: TranslateService) { }

  ngOnInit() {
    this.setTabs();
  }


  setTabs() {
    const currentUser = JSON.parse(localStorage.getItem('AuthorizationData'));
    this.isTeacher = currentUser.role.includes('teacher');

    this.set_tabs_translations('profile_details', ['./profile-details']);
    this.set_tabs_translations('change_password', ['./change-password']);

    if (this.isOrg && this.isTeacher) {
      this.set_tabs_translations('plans', ['./plans']);
      this.set_tabs_translations('purchase_history', ['./purchase-history']);
    }

  }


  set_tabs_translations(title, route) {

    const sub = this._translateService.get(title).subscribe(
      translation => {
        this.tabs.push({
          title: translation,
          route: route,
        });
      });

    setTimeout(() => {
      sub.unsubscribe();
    }, 1000);
  }


}
