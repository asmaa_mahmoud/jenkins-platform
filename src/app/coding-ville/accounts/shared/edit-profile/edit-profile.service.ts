import { Injectable } from '@angular/core';
import { GlobalService } from '../../../../@core/utils/global.service';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class EditProfileService {

  constructor(private _globalService: GlobalService) { }

  updateProfileData(body) {
    const url: string = `user/basicinfo`;
    return this._globalService.post(url, body).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', 'Done', 'updated-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  changePassword(body) {
    const url: string = `user/password`;
    return this._globalService.post(url, body).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'updated-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        console.log(error);
        return Observable.throw('Something went wrong!');
      },
    );
  }

  uploadProfileImage(body) {
    const url: string = `user/image`;
    return this._globalService.post(url, body).map((data: any) => {
      this._globalService.showTranslatedToast('success', 'Done', 'updated-successfully');
      return data;
    },
    ).catch(
      (error: any) => {
        console.log(error);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


}
