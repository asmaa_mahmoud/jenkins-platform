import { GlobalService } from './../../../../../@core/utils/global.service';
import { Component, OnInit } from '@angular/core';
import { EditProfileService } from '../edit-profile.service';
import { User } from '../../../../../@core/data/users';
import { AuthenticationService } from '../../../../../@core/utils/authentication.service';
@Component({
  selector: 'ngx-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss'],
})
export class PersonalInfoComponent implements OnInit {
  loading = false;
  fullname_pattern = '[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ ]+';

  user = {
    full_name: '',
    username: '',
    email: '',
  };
  dataNotChanged = true;
  private userData: User;
  constructor(private _editProfile: EditProfileService,
    private authenticationService: AuthenticationService,
    private _globalService: GlobalService,
  ) { }

  ngOnInit() {
    this.userData = this.authenticationService.currentUserValue;
    this.updateLocalObject();
    this.setBreadcrumbs();
  }
  setBreadcrumbs() {
    const breadcrumbs = [
      {
        name: 'Edit Profile',
        link: '',
      },
      {
        name: 'Personal Info',
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(breadcrumbs);
  }
  updateChanged() {
    this.dataNotChanged =
      this.userData['full_name'] === this.user.full_name &&
      this.userData['username'] === this.user.username &&
      this.userData['email'] === this.user.email;
  }

  onSubmit() {
    this.loading = true;
    this.updateStorageObject();
    this._editProfile.updateProfileData(this.user).subscribe(
      response => {
        // update local storage if successful
        this.authenticationService.currentUserValue = this.userData;
        localStorage.setItem('AuthorizationData', JSON.stringify(this.userData));
        this.updateChanged();
        this.loading = false;
      }, error => {
        console.log(error);
        this.loading = false;
        this.updateChanged();
      },
    );
  }

  updateStorageObject() {
    this.userData['full_name'] = this.user.full_name;
    this.userData['username'] = this.user.username;
    this.userData['email'] = this.user.email;
  }

  updateLocalObject() {
    this.user = {
      full_name: this.userData['full_name'],
      username: this.userData['username'],
      email: this.userData['email'],
    };
  }

}
