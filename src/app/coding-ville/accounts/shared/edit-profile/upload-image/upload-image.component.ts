import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { EditProfileService } from '../edit-profile.service';
import { AuthenticationService } from '../../../../../@core/utils/authentication.service';

@Component({
  selector: 'ngx-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss'],
})
export class UploadImageComponent implements OnInit {
  @ViewChild('uploadImage', { static: false }) uploadImage: ElementRef;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  currentImageUrl = '';
  showPreview = false;
  private userData;
  updatedImage;

  constructor(private _editProfileService: EditProfileService, private _authService: AuthenticationService) { }

  ngOnInit() {
    this._authService.currentUser.subscribe(x => this.userData = x);
    this.currentImageUrl = this.userData['profile_image_url'] ? this.userData['profile_image_url']
      : 'assets/images/no-image.png';
  }


  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.showPreview = true;
    this.uploadProfileImage();
  }

  uploadProfileImage() {
    if (this.imageChangedEvent.target.files && this.imageChangedEvent.target.files[0]) {
      const reader = new FileReader();
      reader.onload = () => {
        this._editProfileService.uploadProfileImage(this.imageChangedEvent.target.files[0]).subscribe((data: any) => {
          this.updatedImage = data.Object.image;
          this.updateStorageObject();
        });
      };
      reader.readAsDataURL(this.imageChangedEvent.target.files[0]);
    }
  }


  updateStorageObject() {
    this.userData['profile_image_url'] = this.updatedImage;
    this.currentImageUrl = this.updatedImage;
    localStorage.setItem('AuthorizationData', JSON.stringify(this.userData));
  }

}
