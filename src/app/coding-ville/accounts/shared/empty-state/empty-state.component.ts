import { Component, OnInit, Input } from '@angular/core';
import { GlobalService } from './../../../../@core/utils/global.service';
import { AuthenticationService } from './../../../../@core/utils/authentication.service';
import { env } from '../../../../@core/env/env';

@Component({
  selector: 'ngx-empty-state',
  templateUrl: './empty-state.component.html',
  styleUrls: ['./empty-state.component.scss'],
})
export class EmptyStateComponent implements OnInit {

  @Input() type: any;
  isOrg: boolean = env.isOrg;

  constructor(private globalService: GlobalService) { }

  ngOnInit() {
  }

  onClickResend() {
    const username = JSON.parse(localStorage.getItem('AuthorizationData')).username;
    const url = `coding-ville/confirmation/mail/${username}`;
    this.globalService.get(url).subscribe(
      (res: any) => {
        console.log(res);
        this.globalService.showTranslatedToast('success', null, 'sent-successfully');
      }, (error: any) => {
        console.log(error);
        this.globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
      },
    );
  }

}
