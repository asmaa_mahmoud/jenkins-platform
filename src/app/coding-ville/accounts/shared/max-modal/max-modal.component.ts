import { Component, Output, EventEmitter, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { ClassRoomService } from '../../teacher/classroom/class-room.service';
import { GlobalService } from '../../../../@core/utils/global.service';
import { ProjectsService } from '../projects/projects.service';

@Component({
  selector: 'ngx-max-modal',
  templateUrl: './max-modal.component.html',
  styleUrls: ['./max-modal.component.scss'],
})
export class MaxModalComponent {
  @Input() type: string;
  @Input() number: string;
  @Input() price: number;


  constructor(
    protected ref: NbDialogRef<DeleteModalComponent>,
  ) { }

  close() {
    this.ref.close();
  }


}
