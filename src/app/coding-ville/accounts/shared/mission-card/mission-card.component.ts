import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-mission-card',
  templateUrl: './mission-card.component.html',
  styleUrls: ['./mission-card.component.scss'],
})
export class MissionCardComponent implements OnInit {
  @Input('missionData') mission;
  @Input('adventureType') adventureType;
  @Input('difficulty') difficulty: string;

  constructor() { }

  ngOnInit() {
  }

}
