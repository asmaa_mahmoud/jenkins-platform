import { JourneysService } from './../../student/journey/journeys.service';
import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { Role } from '../../../../@core/data/role';

@Component({
  selector: 'ngx-mission-control',
  templateUrl: './mission-control.component.html',
  styleUrls: ['./mission-control.component.scss'],
})
export class MissionControlComponent implements OnInit, OnChanges {

  @Input('journey') journey: string;
  @Input('adventures') adventures: any;
  @Input('role') role: string;
  @Input('selectedAdventureId') selectedAdventureId: string;
  activeAdventure: any;
  adventuresShown = [];
  lastAdventureID: number;
  slectedIndex = 1;
  currentStart = 0;
  currentEnd = 6;
  currentRole;

  @Output() getMissionsClicked = new EventEmitter();


  constructor(private _authService: AuthenticationService) {
    this._authService.currentUser.subscribe(x => this.currentRole = x.role);

  }

  ngOnInit() {
    let index = 1;
    this.adventures.forEach(function (element) {
      element.index = index;
      index++;
    });
    this.activeAdventure = this.adventures.find(adv => Number(adv.id) === Number(this.selectedAdventureId)) || this.adventures[0];
    this.slectedIndex = this.adventures.indexOf(this.activeAdventure) + 1;
    this.lastAdventureID = this.adventures[this.adventures.length - 1].id;
    // this.adventuresShown = this.adventures.slice(this.currentStart, this.currentEnd);
    this.adventuresShown = this.adventures;
    // this.adventuresShown.unshift(this.adventures[0]);
    // this.adventuresShown.unshift(this.adventures[0]);
  }
  ngOnChanges() {
    // console.log(this.selectedAdventureId);
    // this.activeAdventure = this.adventures.find(adv => adv.id === this.selectedAdventureId) || this.adventures[0];
    // this.slectedIndex = this.selectedAdventureId || 0;
  }

  isStudent() {
    if (Role['Student'].includes(this.currentRole) || Role['individualStudent'].includes(this.currentRole))
      return true;
    return false;
  }

  onViewAdventure(adventure: any) {
    this.activeAdventure = adventure;
    this.slectedIndex = adventure.index;
    this.getMissionsClicked.emit(adventure);
  }

  adventureGoLeft() {
    if (this.currentStart >= 0) {
      this.currentStart--;
      this.currentEnd--;
      this.adventuresShown = this.adventures.slice(this.currentStart, this.currentEnd);
    }
  }

  adventureGoRight() {
    if (this.currentEnd < this.adventures.length) {
      this.currentEnd++;
      this.currentStart++;
      this.adventuresShown = this.adventures.slice(this.currentStart, this.currentEnd);
    }
  }

}
