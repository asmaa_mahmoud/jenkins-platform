import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'ngx-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit, OnChanges {

  @Input() arrayLength: number;
  @Input() currentPage: number;
  @Input() maxShown: number;
  @Output() pageChange = new EventEmitter<any>();


  totalPages: number;

  constructor() { }

  ngOnInit() {
    this.calculateTotalPages(this.maxShown);
  }

  ngOnChanges() {
    this.calculateTotalPages(this.maxShown);
  }

  calculateTotalPages(maxShown) {

    let pages = this.arrayLength / Number(maxShown);
    const remainder = pages % 1;

    if (remainder > 0) { pages = pages - remainder + 1; }
    this.totalPages = pages;

    if (this.currentPage > this.totalPages) {
      this.currentPage = this.totalPages;
    }
    this.currentPage = Number(this.currentPage);
  }

  changePage(num, forward) {
    let newPage = this.currentPage;
    newPage = forward ? newPage + num : newPage - num;

    if (newPage > this.totalPages || newPage < 1) {
      return;
    }
    this.currentPage = newPage;
    this.pageChange.emit(this.currentPage);
  }

}
