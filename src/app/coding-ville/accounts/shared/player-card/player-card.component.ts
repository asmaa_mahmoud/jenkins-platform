import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Player } from '../../../../@core/data/player';
@Component({
  selector: 'ngx-player-card',
  templateUrl: './player-card.component.html',
  styleUrls: ['./player-card.component.scss'],
})
export class PlayerCardComponent implements OnInit {
  @Input() playerData: Player;

  constructor(public sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

}
