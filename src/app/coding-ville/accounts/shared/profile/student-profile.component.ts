import { ShareModalComponent } from '../share-modal/share-modal.component';
import { DashboardService } from '../../student/dashboard/dashboard.service';
import { NbDialogService } from '@nebular/theme';
import { GlobalService } from '../../../../@core/utils/global.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { Subscription } from 'rxjs';
import { StudentProfileService } from './student-profile.service';
import { ActivatedRoute } from '@angular/router';
import { Project } from '../../../../@core/data/project';
import { ProjectsService } from '../projects/projects.service';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { Role } from '../../../../@core/data/role';

@Component({
    selector: 'ngx-student-profile',
    templateUrl: './student-profile.component.html',
    styleUrls: ['./student-profile.component.scss'],
})

export class StudentProfileComponent implements OnInit, OnDestroy {

    cardsPerPage = [6, 9, 12];
    cardPerPage = 6;
    dataLoaded: boolean = false;
    playerDataLoaded: boolean = false;
    private subscriptions = new Subscription();
    private breadcrumbs: Breadcrumb[] = [];

    generalData;
    projects: Project[] = [];
    studentData: any;
    projectsCount;
    currentPage = 1;
    badgesShown = [];
    allBadges = [];
    lastShownBadge: number;
    searchValue: string = '';
    studentId: any;
    currentRole: any;

    constructor(
        private globalService: GlobalService,
        private _dashboardService: DashboardService,
        private _studentProfileService: StudentProfileService,
        private _projectService: ProjectsService,
        private dialogService: NbDialogService,
        private _authService: AuthenticationService,
        private route: ActivatedRoute,
    ) {
        this._authService.currentUser.subscribe(x => this.currentRole = x.role);
    }

    ngOnInit() {
        this.studentId = this.route.snapshot.paramMap.get('student_id');
        this.checkRole();

        this.getProjectsCount();
        this.getProjectsList();
        this.getPlayerData();
    }

    checkRole() {
        if (Role['Student'].includes(this.currentRole) || Role['individualStudent'].includes(this.currentRole)) {
            this.breadcrumbs = [
                {
                    name: 'Discover',
                    link: 'discover',
                },
                {
                    name: 'Profile',
                    link: '',
                },
            ];
        } else if (Role['Teacher'].includes(this.currentRole)) {
            this.breadcrumbs = [
                {
                    name: 'Discover',
                    link: 'discover',
                },
                {
                    name: 'Profile',
                    link: '',
                },
            ];
        }
        this.globalService.getBreadcrumbs(this.breadcrumbs);
    }

    getProjectsCount() {
        const sub = this._projectService.getProjectsCount(this.studentId, '').subscribe(
            res => {
                if (res.status_code === 200) {
                    this.projectsCount = res.Object.Count;
                    this.getProjectsList();
                }
            });
        this.subscriptions.add(sub);
    }

    getProjectsList() {
        const sub = this._projectService
            .getProjects(this.studentId, ((this.currentPage - 1) * (this.cardPerPage + 1)).toString(), this.cardPerPage.toString(), '')
            .subscribe(
                res => {
                    this.projects = res.Object.filter((el) => {
                        return delete el.createdBy;
                    });
                    this.dataLoaded = true;
                });
        this.subscriptions.add(sub);
    }

    getPlayerData() {
        this._studentProfileService.getPlayerData(this.studentId).subscribe((data: any) => {
            this.studentData = data.Object;
            this.allBadges = this.studentData.badges;
            this.badgesShown = this.allBadges.slice(0, 5);
            this.lastShownBadge = this.badgesShown.length;
            this.playerDataLoaded = true;
        });
    }

    changePagination(num) {
        if (this.currentPage !== num) {
            this.currentPage = num;
            this.getProjectsList();
        }
    }

    changePageLimit() {
        this.currentPage = 1;
        this.getProjectsList();
    }

    search(event) {
        this.searchValue = event;
        this.getProjectsList();
    }

    // --------------------- SECTION  Badges------------------

    badgesGoLeft() {
        if (this.lastShownBadge > 5) {
            this.lastShownBadge--;
            if (this.allBadges.length - this.lastShownBadge > 1) {
                let prevBadge;
                this.badgesShown.pop();
                prevBadge = this.allBadges[this.lastShownBadge - 5];
                this.badgesShown.unshift(prevBadge);
            }
        }
    }

    badgesGoRight() {
        if (this.lastShownBadge < this.allBadges.length) {
            this.lastShownBadge++;
            if (this.lastShownBadge < this.allBadges.length) {
                this.badgesShown.shift();
                const nextBadge = this.allBadges[this.lastShownBadge - 1];
                this.badgesShown.push(nextBadge);
            }
        }
    }

    // --------------------- SECTION  Card Events ------------------


    share(project) {
        if (project.isPublic) {
            this._projectService.getProjectLink(project.id).subscribe((response: any) => {
                const projectData = response.Object;
                const data = {
                    name: project.name,
                    createdBy: project.createdBy,
                    image: projectData.image,
                    link: window.location.host + `/visitor/discover/${projectData.projectUrl}/discover-project`,
                };

                this.dialogService.open(ShareModalComponent, {
                    context: {
                        data: data,
                    },
                });
            });
        }
    }

    toggleLikeProject(project) {
        this._projectService
            .toggleLikeProject(project.id)
            .subscribe(res => {
                this.getProjectsList();
            },
                error => {
                    console.log('error:', error);
                    project.isLiked = !project.isLiked;
                });
    }

    // --------------------- SECTION  Destory------------------
    ngOnDestroy() {
        this.subscriptions.unsubscribe();
    }

}
