import { Injectable } from '@angular/core';
import { GlobalService } from '../../../../@core/utils/global.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StudentProfileService {

  constructor(private _globalService: GlobalService) { }

  getPlayerData(player_id) {
    const url = `coding-ville/projects/users/${player_id}`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        console.log(error.message);
        return Observable.throw('Something went wrong!');
      },
    );
  }
}
