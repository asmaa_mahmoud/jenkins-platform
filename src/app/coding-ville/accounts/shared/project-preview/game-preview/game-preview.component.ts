import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import gamebuilder from '../../../../../../assets/js/gamebuilder/Game.js';
import { env } from '../../../../../@core/env/env';

@Component({
  selector: 'ngx-project-game-preview',
  templateUrl: './game-preview.component.html',
  styleUrls: ['./game-preview.component.scss'],
})
export class GamePreviewComponent implements OnInit, OnDestroy {

  isOrg = env.isOrg;
  game: any = null;
  isRunning: boolean = false;
  @Input() data: any;
  constructor() {
  }

  runProject() {
    // event.target.disabled= true;
    if (this.game) {
      this.game.destroy(true);
      this.game = null;
    }
    setTimeout(() => {
      this.data.config['isOrg'] = this.isOrg;
      this.game = new gamebuilder(this.data.userCode, this.data.config, 'main');
      this.isRunning = true;
    }, 500);
  }

  stopProject(event) {
    this.isRunning = false;
    // Array of scenes keys as string
    const scenesKeys = Object.keys(this.game.scene.keys);
    scenesKeys.forEach(key => {
      if (this.game.scene.isActive(key)) {
        this.game.scene.pause(key);
        console.log(this.game.scene.isPaused(key));
        console.log(key + ' is Paused');
        return;
      }
    });
  }

  ngOnInit() {
    this.runProject();
  }

  ngOnDestroy() {
    if (this.game) {
      this.game.destroy(true);
      this.game = null;
    }
  }

}
