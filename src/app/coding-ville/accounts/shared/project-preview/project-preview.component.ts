import { PublishService } from './../../../project/publish.service';
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../@core/utils/global.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { ShareModalComponent } from '../share-modal/share-modal.component';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { Role } from '../../../../@core/data/role';
import { ProjectsService } from '../projects/projects.service';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';

@Component({
  selector: 'ngx-project-preview',
  templateUrl: './project-preview.component.html',
  styleUrls: ['./project-preview.component.scss'],
})
export class ProjectPreviewComponent implements OnInit, OnDestroy {

  @ViewChild('iframe', { static: false }) iframe: ElementRef;
  @Output() shareClickEvent = new EventEmitter<void>();

  // Params Variables
  subscriptions = new Subscription();
  stateType: any;
  classroomName: any;
  classroomID: any;
  journeyType: any;
  journeyID: any;
  adventureID: any;
  courseID: any;
  activityID: any;
  currentURL;

  // Build APK Variables
  building = true;
  finishedBuilding = false;
  newApk: string;
  oldApk: string;

  // Project Data Variables
  isLiked: any;
  projectID: any;
  type: any;
  projectData: any;
  isMenuOpen: any;
  dataLoaded: boolean = false;
  isMyProject: boolean = false;
  creatorID: any;
  taskID: any;
  isGame: boolean = false;
  isDefaultProject: boolean = false;
  deleteDialogRef: NbDialogRef<DeleteModalComponent>;


  // Current User/Role Variables
  currentUserID: any;
  currentRole: any;
  currentUsername: any;
  isTeacher: boolean;
  noUser: boolean = false;

  // Render code to iframe Vars
  code;
  html;
  css;
  js;
  source;
  dataToProject = { userCode: '', config: { gameName: '', userName: '', builder: {}, levels: [] } };


  private breadcrumbs: Breadcrumb[] = [];
  constructor(
    private globalService: GlobalService,
    private _projectService: ProjectsService,
    private activatedRoute: ActivatedRoute,
    private dialogService: NbDialogService,
    private _authService: AuthenticationService,
    private router: Router,
    private publishService: PublishService,
    private location: Location,
  ) {
    if (this._authService.currentUserValue)
      this._authService.currentUser.subscribe(x => {
        this.currentUserID = x.id,
          this.currentRole = x.role,
          this.currentUsername = x.username;
      });
    else {
      this.currentUserID = null;
      this.currentRole = null;
    }

    this.router.events.subscribe(x => {
      this.currentURL = this.location.path();
    });

    this.activatedRoute.params.subscribe((params: Params) => {
      this.projectID = params['project_id'];
      this.stateType = params['type'];
    });

    // Get Query params from URL
    this.activatedRoute.queryParams
      .filter(params => params.classroomName)
      .subscribe(params => {
        this.classroomName = params.classroomName;
        this.classroomID = params.classroomID;
      });
  }

  ngOnInit() {
    this.globalService.getBreadcrumbs(this.breadcrumbs);
    this.getProjectID();
    this.getNotifications();
  }

  getNotifications() {
    const sub = this.publishService.getNotification().subscribe(
      (data: any) => {
        if (data && data.apk_link !== this.newApk && Number(this.projectID) === Number(data.project_id)) {
          this.newApk = data.apk_link;
          this.building = false;
          this.finishedBuilding = true;
        }
      }, (error: any) => {
        console.log(error);
        this.building = false;
      },
    );

    this.subscriptions.add(sub);
  }


  initBreadcrumb() {
    if (this.isMyProject) {
      this.breadcrumbs = [
        {
          name: 'projects',
          link: 'projects',
        },
        {
          name: this.projectData.name,
          link: '',
        },
      ];
    } else if (!this.isMyProject) {
      if (Role['Teacher'].includes(this.currentRole)) {
        if (this.stateType === 'classroom') {
          this.breadcrumbs = [
            {
              name: 'Classrooms',
              link: 'classroom',
            },
            {
              name: this.classroomName,
              link: 'classroom/' + this.classroomID,
            },
            {
              name: this.projectData.name,
              link: '',
            },
          ];
        } else {
          this.breadcrumbs = [
            {
              name: 'Discover',
              link: 'discover',
            },
            {
              name: this.projectData.name,
              link: '',
            },
          ];
        }

      } else {
        this.breadcrumbs = [
          {
            name: 'Discover',
            link: 'discover',
          },
          {
            name: this.projectData.name,
            link: '',
          },
        ];
      }
    }
    this.globalService.getBreadcrumbs(this.breadcrumbs);
  }

  getProjectData() {
    this._projectService.getProjectDataByID(this.projectID).subscribe((data: any) => {
      this.projectData = data.Object;
      this.projectData.buildStatus ? this.building = this.projectData.buildStatus.id === 2 : this.building = false;
      this.type = this.projectData.type_id;
      this.isLiked = this.projectData.isLiked;
      this.creatorID = this.projectData.owner.creatorId;
      this.oldApk = this.projectData.apk_url;
      this.checkUser();
      this.initBreadcrumb();
      setTimeout(() => {
        this.type === 3 ? this.renderCode() : null;
      }, 50);
      this.type !== 3 ? this.customizeProjectData() : null;
      this.dataLoaded = true;
    });
  }

  getJourneyProjectDataByCourseID() {
    this._projectService.getJourneyProjectDataByCourseID(this.projectID, this.adventureID, this.courseID)
      .subscribe(data => {
        this.projectData = data.Object;
        this.taskID = this.projectData.task_id || null;
        this.projectData.buildStatus ? this.building = this.projectData.buildStatus.id === 2 : this.building = false;
        this.type = this.projectData.type_id;
        this.isLiked = this.projectData.isLiked;
        this.oldApk = this.projectData.apk_url;
        this.isGame = this.projectData.type_id === 3 ? false : true;
        this.checkUser();
        this.initBreadcrumb();
        setTimeout(() => {
          this.type === 3 ? this.renderCode() : null;
        }, 50);
        this.type !== 3 ? this.customizeProjectData() : null;
        this.dataLoaded = true;
      });
  }

  getActivityProjectDataByCourseID() {
    this._projectService.getActivityProjectDataByCourseID(this.projectID, this.activityID, this.courseID)
      .subscribe(data => {
        this.projectData = data.Object;
        this.taskID = this.projectData.task_id || null;
        this.projectData.buildStatus ? this.building = this.projectData.buildStatus.id === 2 : this.building = false;
        this.type = this.projectData.type_id;
        this.isLiked = this.projectData.isLiked;
        this.oldApk = this.projectData.apk_url;
        this.isGame = this.projectData.type_id === 3 ? false : true;
        this.checkUser();
        this.initBreadcrumb();
        setTimeout(() => {
          this.type === 3 ? this.renderCode() : null;
        }, 50);
        this.type !== 3 ? this.customizeProjectData() : null;
        this.dataLoaded = true;
      });
  }

  getJourneyProjectData() {
    this._projectService.getJourneyProjectData(this.projectID, this.adventureID, this.journeyID)
      .subscribe(data => {
        this.projectData = data.Object;
        this.taskID = this.projectData.task_id || null;
        this.projectData.buildStatus ? this.building = this.projectData.buildStatus.id === 2 : this.building = false;
        this.type = this.projectData.type_id;
        this.oldApk = this.projectData.apk_url;
        this.isGame = this.projectData.type_id === 3 ? false : true;
        this.checkUser();
        this.initBreadcrumb();
        setTimeout(() => {
          this.type === 3 ? this.renderCode() : null;
        }, 50);
        this.type !== 3 ? this.customizeProjectData() : null;
        this.dataLoaded = true;
      });
  }

  getActivityProjectData() {
    this._projectService.getActivityProjectData(this.projectID, this.activityID)
      .subscribe(data => {
        this.projectData = data.Object;
        this.taskID = this.projectData.task_id || null;
        this.projectData.buildStatus ? this.building = this.projectData.buildStatus.id === 2 : this.building = false;
        this.type = this.projectData.type_id;
        this.isLiked = this.projectData.isLiked;
        this.oldApk = this.projectData.apk_url;
        this.isGame = this.projectData.type_id === 3 ? false : true;
        this.checkUser();
        this.initBreadcrumb();
        setTimeout(() => {
          this.type === 3 ? this.renderCode() : null;
        }, 50);
        this.type !== 3 ? this.customizeProjectData() : null;
        this.dataLoaded = true;
      });
  }

  customizeProjectData() {
    this.dataToProject.userCode = this.projectData.logic && this.projectData.logic.length > 0 ? this.projectData.logic[0].js_code : null;
    this.dataToProject.config.gameName = this.projectData.name;
    this.dataToProject.config.userName = this.projectData.owner ? this.projectData.owner.createdBy : this.currentUsername;
    const levels = this.projectData.levels ? this.projectData.levels.map(level => JSON.parse(level)) : null;
    this.dataToProject.config.builder = {
      cell_size: 86,
      width: levels && levels.length > 0 ? levels[0].dimensions.cols : null,
      height: levels && levels.length > 0 ? levels[0].dimensions.rows : null,
    };
    this.dataToProject.config.levels = levels ? levels.map(level => ({ ...level, objects: level.items })) : null;
  }

  checkUser() {
    if (this._authService.currentUserValue) {
      this.noUser = false;
      if (this.creatorID === this.currentUserID)
        this.isMyProject = true;
      else
        this.isMyProject = false;
    } else {
      this.noUser = true;
    }
  }

  renderCode() {
    this.code = this.projectData.userLogic || this.projectData.logic;
    this.code.forEach(item => {
      item['language'] === 'js' ? this.js = item['code'] :
        item['language'] === 'html' ? this.html = item['code'] :
          item['language'] === 'css' ? this.css = item['code'] : null;
    });

    this.source = `<!DOCTYPE html><html><head><style> ${this.css} </style></head><body> ${this.html} <script> ${this.js} </script><script>'use strict';var element=document.querySelector('form');element.addEventListener('submit',function(e){e.preventDefault()});</script></body></html>`;
    const iframe = document.getElementById('project_iframe') as HTMLIFrameElement;
    const iframe_doc = iframe.contentDocument;
    iframe_doc.open();
    iframe_doc.write(this.source);
    iframe_doc.close();
  }

  getProjectID() {
    if (this.stateType === 'journeys') {
      this.isDefaultProject = true;
      this.isMyProject = true;
      this.activatedRoute.params.subscribe((params: Params) => {
        this.journeyType = params['journey-type'];
        this.journeyID = params['journeyID'];
        this.adventureID = params['adventureID'];
        this.courseID = params['courseID'];
        this.activityID = params['activityID'];
      });

      if (this.journeyType === 'journey') {
        if (this.courseID !== undefined)
          this.getJourneyProjectDataByCourseID();
        else
          this.getJourneyProjectData();
      } else if (this.journeyType === 'pre-journey') {
        if (this.courseID !== undefined)
          this.getActivityProjectDataByCourseID();
        else
          this.getActivityProjectData();
      }
    } else {
      this.isDefaultProject = false;
      this.getProjectData();
    }
  }

  toggleLike() {
    this.projectData.isLiked = !this.projectData.isLiked;
    this._projectService.toggleLikeProject(this.projectData.id)
      .subscribe(res => {
        this.projectData.noOfLikes = res.Object.Likes;
      }, error => {
        this.projectData.isLiked = !this.projectData.isLiked;
      });
  }

  togglePublic() {
    this._projectService.togglePublic(this.projectData.id).subscribe(res => {
      this.projectData.isPublic = res.Object.public;
    });
  }

  share(project) {
    if (project.isPublic) {
      const data = {
        name: project.name,
        createdBy: project.createdBy,
        image: project.icon_url,
        link: window.location.host + `/visitor/discover/${project.hashId}/discover-project`,
      };

      this.dialogService.open(ShareModalComponent, {
        context: {
          data: data,
        },
      });
    }
  }

  setBackInfoForWorkspace() {
    localStorage.setItem('backURL', this.currentURL);
  }

  goToRemixedProject(projectID, projectTypeID) {
    if (projectTypeID === 1 || projectTypeID === 2) {
      this.router.navigate([`project-game/remix/${projectID}`]);
    } else if (projectTypeID === 3) {
      this.setBackInfoForWorkspace();
      window.location.href = `workspaces/app/project/remix/${projectID}`;
    }
  }

  goToEditProject(projectID, projectTypeID) {
    if (this.isDefaultProject) {
      this.goToProjectMission();
    } else {
      if (projectTypeID === 1 || projectTypeID === 2) {
        this.router.navigate([`project-game/${projectID}`]);
      } else if (projectTypeID === 3) {
        this.setBackInfoForWorkspace();
        window.location.href = `workspaces/app/project/${projectID}`;
      }
    }
  }

  goToProjectMission() {
    if (this.isGame) {
      if (this.stateType === 'journeys' && this.courseID) {
        this.router.navigate([`project-game/journey/${this.adventureID}/${this.projectData.id}/${this.journeyID}/${this.courseID}`]);
      } else if (this.stateType === 'journeys' && !this.courseID) {
        this.router.navigate([`project-game/journey/${this.adventureID}/${this.projectData.id}/${this.journeyID}`]);
      } else if (this.stateType === 'pre-journey' && this.courseID) {
        this.router.navigate([`project-game/pre-journey/${this.projectData.id}/${this.journeyID}/${this.courseID}`]);
      } else if (this.stateType === 'pre-journey' && !this.courseID) {
        this.router.navigate([`project-game/pre-journey/${this.projectData.id}/${this.journeyID}`]);
      }
    } else if (!this.isGame) {
      if (this.stateType === 'journeys' && this.courseID) {
        window.location.href =
          'workspaces/journey/project/app/' + this.journeyID + '/' + this.adventureID + '/' + this.projectData.id + '/' + this.courseID;
      } else if (this.stateType === 'journeys' && !this.courseID) {
        window.location.href =
          'workspaces/journey/project/app/' + this.journeyID + '/' + this.adventureID + '/' + this.projectData.id;
      } else if (this.stateType === 'pre-journey' && this.courseID) {
        window.location.href =
          'workspaces/prejourney/project/app/' + this.journeyID + '/' + this.projectData.id + '/' + this.courseID;
      } else if (this.stateType === 'pre-journey' && !this.courseID) {
        'workspaces/prejourney/project/app/' + this.journeyID + '/' + this.projectData.id;
      }
    }
  }

  goToViewProject(projectID, projectTypeID) {
    if (projectTypeID === 1 || projectTypeID === 2) {
      this.router.navigate([`project-game/${projectID}`]);
    } else if (projectTypeID === 3) {
      this.setBackInfoForWorkspace();
      window.location.href = `workspaces/app/project/${projectID}`;
    }
  }

  deleteProject(projectID) {
    this.deleteDialogRef = this.dialogService.open(DeleteModalComponent, {
      context: {
        title: 'project',
        content: 'project',
        deleted_id: projectID,
      },
    });
    const sub = this.deleteDialogRef.componentRef.instance.dataUpdated.subscribe(() => {
      this.backAfterDelete();
    });
    this.deleteDialogRef.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }

  backAfterDelete() {
    if (this.isMyProject) {
      if (Role['Teacher'].includes(this.currentRole))
        this.router.navigate(['teacher/projects']);
      else
        this.router.navigate(['student/projects']);
    } else if (!this.isMyProject) {
      if (Role['Teacher'].includes(this.currentRole)) {
        if (this.stateType === 'classroom') {
          this.router.navigate([`teacher/classroom/${this.classroomID}`]);
        } else {
          this.router.navigate(['teacher/discover']);
        }
      } else {
        this.router.navigate(['student/discover']);
      }
    }
  }

  onBuildApk() {
    this.building = true;
    const userID = JSON.parse(localStorage.getItem('AuthorizationData')).id;
    if (this.projectData.type_id === 3) {
      const code = {
        html: '',
        css: '',
        js: '',
      };
      this.projectData.logic.forEach(ele => {
        code[ele.language] = ele.code;
      });
      const data = [{
        'index.html': code.html,
        'style.css': code.css,
        'script.js': code.js,
      }];
      if (this.isDefaultProject) {
        this.publishService.publishDefault(this.projectData.type_id, data).subscribe(
          res => console.log(res),
          err => {
            this.building = false;
            console.log(err);
          },
        );
      } else {
        this.publishService.publish(this.projectID, userID, this.projectData.type_id, data);
      }
    } else {
      if (this.isDefaultProject) {
        const data = {
          code: this.publishService.prepareDataForSave(
            this.projectData.userLevels,
            this.projectData.userLogic,
            this.currentUsername,
            this.projectData.name,
          ),
          parameters: {
            adventure_id: this.adventureID,
            journey_id: this.journeyID,
            course_id: this.courseID,
            task_id: this.taskID,
          },
        };
        this.publishService.publishDefault(this.projectData.type_id, data).subscribe(
          res => {},
          err => {
            this.building = false;
            console.log(err);
          },
        );
      } else {
        const data = this.publishService.prepareDataForSave(
          this.projectData.levels,
          this.projectData.logic,
          this.projectData.owner.createdBy,
          this.projectData.name,
        );
        this.publishService.publishFromPreview(this.projectID, userID, this.projectData.type_id, data).subscribe(
          res => console.log(res),
          err => {
            this.building = false;
            console.log(err);
          },
        );
      }
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
