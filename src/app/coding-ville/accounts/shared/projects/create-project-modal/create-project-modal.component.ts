import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-create-project-modal',
  templateUrl: './create-project-modal.component.html',
  styleUrls: ['./create-project-modal.component.scss'],
})
export class CreateProjectModalComponent implements OnInit {
  currentURL: any;

  constructor(private router: Router,
    protected ref: NbDialogRef<CreateProjectModalComponent>,
    private location: Location,
  ) {
    this.router.events.subscribe(x => {
      this.currentURL = this.location.path();
    });
  }

  ngOnInit() {
  }

  createApp() {
    window.location.href = 'workspaces/app/project';
  }

  createGame() {
    this.router.navigate(['project-game/create']);
  }

  close() {
    this.ref.close();
  }



}
