import { Project } from './../../../../@core/data/project';
import { ProjectsService } from './projects.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { ShareModalComponent } from '../../shared/share-modal/share-modal.component';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { GlobalService } from './../../../../@core/utils/global.service';
import { DeleteModalComponent } from '../../shared/delete-modal/delete-modal.component';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { CreateProjectModalComponent } from './create-project-modal/create-project-modal.component';
import { Role } from '../../../../@core/data/role';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
})
export class ProjectsComponent implements OnInit {

  projects: Project[];
  cardPerPage = 6;
  currentPage = 1;
  start: number = 0;
  studentID;
  noOfCardsPerPage: any[] = [6, 9, 12];
  projectsCount: number;
  private searchValue = '';
  subscriptions = new Subscription();
  dataLoaded: boolean = false;
  deleteDialogRef: NbDialogRef<DeleteModalComponent>;
  currentRole: any;
  shareLink;
  currentURL;

  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'PROJECTS',
      link: '',
    },
  ];

  constructor(
    private dialogService: NbDialogService,
    private globalService: GlobalService,
    private _projectService: ProjectsService,
    private _authService: AuthenticationService,
    private location: Location,
    private router: Router,
  ) {
    this._authService.currentUser.subscribe(x => { this.studentID = x.id, this.currentRole = x.role; });
    this.router.events.subscribe(x => {
      this.currentURL = this.location.path();
    });
  }

  ngOnInit() {
    this.globalService.getBreadcrumbs(this.breadcrumbs);
    this.getProjectsCount();
  }

  search(value) {
    this.searchValue = value;
    this.start = 0;
    this.getProjectsCount();
  }

  changeItemPerPage() {
    this.start = 0;
    this.getProjectsCount();
  }

  getProjectsCount() {
    this._projectService.getProjectsCount(this.studentID, this.searchValue).subscribe(
      res => {
        if (res.status_code === 200) {
          this.projectsCount = res.Object.Count;
          this.getProjectsList();
        }
      });
  }

  getProjectsList() {
    this._projectService.getProjects(this.studentID, String(this.start), String(this.cardPerPage), this.searchValue)
      .subscribe(
        res => {
          this.projects = res.Object.filter((el) => {
            return delete el.createdBy;
          });
          this.dataLoaded = true;
        });
  }

  toggleLikeOnCardUI(index) {
    if (this.projects[index].isLiked) this.projects[index].noOfLikes++;
    else this.projects[index].noOfLikes--;
  }

  toggleLikeProject(index) {
    this.toggleLikeOnCardUI(index);
    const sub = this._projectService
      .toggleLikeProject(this.projects[index].id)
      .subscribe(res => { },
        error => {
          console.log('error:', error);
          this.projects[index].isLiked = !this.projects[index].isLiked;
          this.toggleLikeOnCardUI(index);
        }, () => { });
    this.subscriptions.add(sub);
  }

  togglePublic(index) {
    const sub = this._projectService
      .togglePublic(this.projects[index].id)
      .subscribe(res => {
        this.projects[index].isPublic = res.Object.public;
      },
        error => {
          console.log('error:', error);
        }, () => { });
    this.subscriptions.add(sub);
  }

  changePagination(num) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.getProjectsCount();
  }

  share(project) {
    const data = {
      name: project.name,
      createdBy: 'Me',
      image: project.icon_url,
      link: window.location.host + `/visitor/discover/${project.hashId}/discover-project`,
    };

    this.dialogService.open(ShareModalComponent, {
      context: {
        data: data,
      },
    });
  }

  deleteProject(projectId) {
    this.deleteDialogRef = this.dialogService.open(DeleteModalComponent, {
      context: {
        title: 'project',
        content: 'project',
        deleted_id: projectId,
      },
    });
    const sub = this.deleteDialogRef.componentRef.instance.dataUpdated.subscribe(() => {
      this.getProjectsCount();
      if (this.start >= this.projectsCount - 1) {
        this.start = 0;
        this.getProjectsCount();
      } else {
        this.getProjectsCount();
      }
    });
    this.deleteDialogRef.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }

  openProject(projectId) {
    if (Role['Student'].includes(this.currentRole) || Role['individualStudent'].includes(this.currentRole))
      this.router.navigate([`student/projects/${projectId}/preview`]);
    else if (Role['Teacher'].includes(this.currentRole))
      this.router.navigate([`teacher/projects/${projectId}/preview`]);
  }

  createProject() {
    this.setBackInfoForWorkspace();
    this.dialogService.open(CreateProjectModalComponent, {});
  }

  setBackInfoForWorkspace() {
    localStorage.setItem('backURL', this.currentURL);
  }

}
