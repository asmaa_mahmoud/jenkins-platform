import { GlobalService } from './../../../../@core/utils/global.service';
import { Project } from './../../../../@core/data/project';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProjectsService {

  constructor(private _globalService: GlobalService) { }

  deleteProject(project_id) {
    const url = `coding-ville/projects/${project_id}`;
    return this._globalService.delete(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getProjects(studentID, startFrom = '1', limit = '3', search = '') {
    const url: string = `coding-ville/users/${studentID}/projects`;
    const params = new HttpParams().set('start_from', startFrom).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getProjectsCount(studentID, search) {
    const url: string = `coding-ville/users/${studentID}/projects/count`;
    const params = new HttpParams().set('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  toggleLikeProject(projectID) {
    const url: string = `coding-ville/projects/${projectID}/toggleLikeState`;
    return this._globalService.put(url, {}).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  public togglePublic(projectID) {
    const url: string = `coding-ville/projects/${projectID}/togglePublicState`;
    return this._globalService.put(url, {}).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  public getProjectDataByID(projectID) {
    const url: string = `coding-ville/projects/${projectID}/data`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getProjectDataByHashID(projectID) {
    const url: string = `coding-ville/projects/hash/${projectID}`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getJourneyProjectDataByCourseID(projectID, adventureID, courseID) {
    const url: string = `coding-ville/default_projects/data`;
    const params = new HttpParams().set('project_id', projectID).append('adventure_id', adventureID).append('course_id', courseID);
    return this._globalService.post(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getActivityProjectDataByCourseID(projectID, activityID, courseID) {
    const url: string = `coding-ville/default_projects/data`;
    const params = new HttpParams().set('project_id', projectID).append('activity_id', activityID).append('course_id', courseID);
    return this._globalService.post(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getJourneyProjectData(projectID, adventureID, journeyID) {
    const url: string = `coding-ville/default_projects/data`;
    const params = new HttpParams().set('project_id', projectID).append('adventure_id', adventureID).append('journey_id', journeyID);
    return this._globalService.post(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getActivityProjectData(projectID, activityID) {
    const url: string = `coding-ville/default_projects/data`;
    const params = new HttpParams().set('project_id', projectID).append('activity_id', activityID);
    return this._globalService.post(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  public getProjectLink(projectID) {
    const url: string = `coding-ville/projects/link/${projectID}`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }
}
