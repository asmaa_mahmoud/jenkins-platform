import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {

  @Output() search = new EventEmitter<string>();
  private searchDelay;
  private delayTime = 1000;
  searchValue: string = '';

  constructor() { }

  ngOnInit() {
  }

  onSearchHandler() {
    this.cancelEmit();
    this.emitIt();
  }

  emitIt() {
    this.searchDelay = setTimeout(() => { this.search.emit(this.searchValue); }, this.delayTime);
  }

  cancelEmit() {
    clearTimeout(this.searchDelay);
  }

}
