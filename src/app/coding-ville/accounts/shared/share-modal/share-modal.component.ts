import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-share-modal',
  templateUrl: './share-modal.component.html',
  styleUrls: ['./share-modal.component.scss'],
})
export class ShareModalComponent implements OnInit {

  @Input() data;
  constructor(protected ref: NbDialogRef<ShareModalComponent>) { }

  ngOnInit() {
  }
  close() {
    this.ref.close();
  }

}




