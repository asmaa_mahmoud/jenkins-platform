import { MaxModalComponent } from './max-modal/max-modal.component';
import { TranslatorModule } from './../../../@shared/translator/translator.module';
import { RouterModule } from '@angular/router';
import { ThemeModule } from './../../../@theme/theme.module';
import { PaginationComponent } from './pagination/pagination.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbInputModule, NbCardModule, NbButtonModule, NbSearchModule,
  NbProgressBarModule, NbRadioModule, NbIconModule, NbSelectModule, NbTabsetModule, NbToastrModule, NbRouteTabsetModule,
} from '@nebular/theme';

import { SearchComponent } from './search/search.component';
import { CardComponent } from './card/card.component';
import { BadgeCardComponent } from './badge-card/badge-card.component';
import { PlayerCardComponent } from './player-card/player-card.component';
import { MissionCardComponent } from './mission-card/mission-card.component';
import { MissionControlComponent } from './mission-control/mission-control.component';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ContainerComponent } from './container/container.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmptyStateComponent } from './empty-state/empty-state.component';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { DiscoverComponent } from './discover/discover.component';
import { DiscoverPlayersComponent } from './discover/discover-players/discover-players.component';
import { DiscoverProjectsComponent } from './discover/discover-projects/discover-projects.component';
import { DiscoverTabsComponent } from './discover/discover-tabs/discover-tabs.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { PersonalInfoComponent } from './edit-profile/personal-info/personal-info.component';
import { ChangePasswordComponent } from './edit-profile/change-password/change-password.component';
import { UploadImageComponent } from './edit-profile/upload-image/upload-image.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ProjectPreviewComponent } from './project-preview/project-preview.component';
import { StudentProfileComponent } from './profile/student-profile.component';
import { ClipboardModule } from 'ngx-clipboard';
import { ShareModalComponent } from './share-modal/share-modal.component';
import { AdventureComponent } from './adventure/adventure.component';
import { GamePreviewComponent } from './project-preview/game-preview/game-preview.component';
import { ProjectsComponent } from './projects/projects.component';
import { CreateProjectModalComponent } from './projects/create-project-modal/create-project-modal.component';
import { SharedClassroomProjectsComponent } from './classroom/classroom-projects/classroom-projects.component';
import { SharedStudentProgressComponent } from './classroom/student-progress/student-progress.component';
import { AgreementModalComponent } from './agreement-modal/agreement-modal.component';
import { TutorialVideoComponent } from './adventure/tutorial-video/tutorial-video.component';
import { JourneyTutorialComponent } from './journey-tutorial/journey-tutorial.component';
import { SurveyModalComponent } from './survey-modal/survey-modal.component';

@NgModule({
  imports: [
    CommonModule,
    NbSearchModule,
    FormsModule,
    ReactiveFormsModule,
    NbCardModule,
    NbInputModule,
    NbButtonModule,
    NbProgressBarModule,
    NbRadioModule,
    NbEvaIconsModule,
    NbIconModule,
    NbSelectModule,
    NbTabsetModule,
    ThemeModule,
    RouterModule,
    NbTabsetModule,
    ImageCropperModule,
    NgCircleProgressModule.forRoot(),
    ClipboardModule,
    NbToastrModule,
    TranslatorModule,
    NbRouteTabsetModule,
  ],
  declarations: [CardComponent, SearchComponent, PaginationComponent, BadgeCardComponent,
    PlayerCardComponent, MissionCardComponent, MissionControlComponent, ContainerComponent,
    EmptyStateComponent, DeleteModalComponent, ShareModalComponent, EditProfileComponent, PersonalInfoComponent,
    ChangePasswordComponent, UploadImageComponent, ProjectPreviewComponent,
    DiscoverComponent, DiscoverPlayersComponent, DiscoverProjectsComponent, AdventureComponent,
    DiscoverTabsComponent, StudentProfileComponent, GamePreviewComponent, ProjectsComponent, CreateProjectModalComponent,
    SharedClassroomProjectsComponent, SharedStudentProgressComponent, AgreementModalComponent, JourneyTutorialComponent,
    TutorialVideoComponent, MaxModalComponent, SurveyModalComponent,
  ],
  exports: [CardComponent, SearchComponent, PaginationComponent, BadgeCardComponent,
    PlayerCardComponent, MissionCardComponent, MissionControlComponent, ContainerComponent,
    EmptyStateComponent, DeleteModalComponent, ShareModalComponent, EditProfileComponent, PersonalInfoComponent,
    ChangePasswordComponent, UploadImageComponent, ProjectPreviewComponent, AdventureComponent,
    DiscoverComponent, StudentProfileComponent, ProjectsComponent, CreateProjectModalComponent,
    SharedClassroomProjectsComponent, SharedStudentProgressComponent, AgreementModalComponent, JourneyTutorialComponent,
    SurveyModalComponent, MaxModalComponent,
  ],
  entryComponents: [
    DeleteModalComponent,
    ShareModalComponent,
    CreateProjectModalComponent,
    MaxModalComponent,
    AgreementModalComponent,
    TutorialVideoComponent,
    SurveyModalComponent,
  ],
})
export class SharedModule { }
