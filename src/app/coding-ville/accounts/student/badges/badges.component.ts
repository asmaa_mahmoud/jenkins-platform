import { Component, OnInit } from '@angular/core';
import { BadgesService } from './badges.service';
import { Badges } from '../../../../@core/data/badges';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-badges',
  templateUrl: './badges.component.html',
  styleUrls: ['./badges.component.scss'],
})
export class BadgesComponent implements OnInit {

  badges: Badges[];
  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Badges',
      link: '',
    },
  ];

  constructor(
    private badgesService: BadgesService,
    private globalService: GlobalService,
  ) { }

  ngOnInit() {
    this.globalService.getBreadcrumbs(this.breadcrumbs);
    this.getAllBadges();
  }

  getAllBadges() {
    this.badgesService.getBadges().subscribe((data: any) => {
      this.badges = data.Object;
    });
  }

}
