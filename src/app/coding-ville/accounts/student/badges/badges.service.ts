import { Injectable } from '@angular/core';
import { Badges } from '../../../../@core/data/badges';
import { GlobalService } from '../../../../@core/utils/global.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BadgesService {

  constructor(private globalService: GlobalService) { }

  getBadges(): Observable<Badges[]> {
    const url = 'coding-ville/all/badges';
    return this.globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        this.globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        console.log(error.message);
        return Observable.throw('Something went wrong!');
      },
    );
  }
}
