import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AuthenticationService } from './../../../../@core/utils/authentication.service';
import { GlobalService } from './../../../../@core/utils/global.service';
import { DashboardService } from './dashboard.service';
import { DashboardComponent } from './dashboard.component';
import { ThousandSuffixesPipe } from '../../../../@theme/pipes';
import { TranslatePipe } from '@ngx-translate/core';
describe('AdminDashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  beforeEach(() => {
    const authenticationServiceStub = () => ({ isActivated: () => ({}) });
    const globalServiceStub = () => ({ getBreadcrumbs: breadcrumbs => ({}) });
    const adminDashboardServiceStub = () => ({
      getGeneralData: () => ({ subscribe: () => ({}) }),
      getClassroomsPerProvince: () => ({ subscribe: () => ({}) }),
      getStudentsPerProvince: () => ({ subscribe: () => ({}) }),
    });
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [DashboardComponent, ThousandSuffixesPipe, TranslatePipe],
      providers: [
        {
          provide: AuthenticationService,
          useFactory: authenticationServiceStub,
        },
        { provide: GlobalService, useFactory: globalServiceStub },
        {
          provide: DashboardService,
          useFactory: adminDashboardServiceStub,
        },
      ],
    });
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });
  describe('ngOnInit', () => {
  });
});
