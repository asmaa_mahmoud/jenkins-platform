import { ShareModalComponent } from '../../shared/share-modal/share-modal.component';
import { NbDialogService } from '@nebular/theme';
import { LikedProject } from '../../../../@core/data/likedProject';
import { DashboardService } from './dashboard.service';
import { GlobalService } from './../../../../@core/utils/global.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { Role } from '../../../../@core/data/role';
import { Router } from '@angular/router';
import { ProjectsService } from '../../shared/projects/projects.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {

  currentUser;
  cardPerPage: number = 6;
  cardsPerPage = [6, 9, 12];
  generalData;
  likedProjects: LikedProject[] = [];
  likedProjectsCount;
  dataLoaded: boolean = false;
  currentPage = 1;
  start: number = 0;
  badgesShown = [];
  allBadges = [];
  lastShownBadge: number;
  searchValue: string = '';
  currentURL;
  isActivated: boolean;


  private subscriptions = new Subscription();

  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Dashboard',
      link: '',
    },
  ];

  constructor(
    private globalService: GlobalService,
    private _dashboardService: DashboardService,
    private _projectService: ProjectsService,
    private dialogService: NbDialogService,
    private _authService: AuthenticationService,
    private router: Router,
  ) {
    this._authService.currentUser.subscribe(x => this.currentUser = x);
    this.isActivated = this._authService.isActivated();
  }

  ngOnInit() {
    this.globalService.getBreadcrumbs(this.breadcrumbs);
    if (this.isActivated) {
      this.countLikedProjects();
      this.getLikedProjects();
      this.getGeneralData();
      this.getBadges();
    }
  }

  countLikedProjects() {
    this._dashboardService.getLikedProjectsCount().subscribe(res => {
      this.likedProjectsCount = res.Object.Count;
    });
  }

  getLikedProjects() {
    this._dashboardService.getLikedProjects
      (this.start.toString(), this.cardPerPage.toString(), this.searchValue).subscribe(res => {
        if (res && res.Object) {
          this.likedProjects = res.Object;
          this.likedProjects = this.likedProjects.filter((el) => {
            return delete el.createdAt;
          });
          this.likedProjects = this.likedProjects.map((el) => {
            const project = Object.assign({}, el);
            project.isLiked = true;
            return project;
          });
          this.dataLoaded = true;
        }
      });
  }

  getGeneralData() {
    this._dashboardService.getGenralData().subscribe(res => {
      this.generalData = res.Object;
    });
  }

  changePagination(num) {
    this.currentPage = num;
    this.start = num * this.cardPerPage - num;
    this.getLikedProjects();
  }

  changePageLimit() {
    this.start = 0;
    this.getLikedProjects();
  }
  // --------------------- SECTION  Badges------------------

  getBadges() {
    this._dashboardService.getBadges().subscribe(res => {
      if (res) {
        this.allBadges = res.Object;
        this.badgesShown = res.Object.slice(0, 5);
        this.lastShownBadge = this.badgesShown.length;
      }
    }, error => { }, () => { });
  }


  badgesGoLeft() {
    if (this.lastShownBadge > 5) {
      this.lastShownBadge--;
      if (this.allBadges.length - this.lastShownBadge > 1) {
        let prevBadge;
        this.badgesShown.pop();
        prevBadge = this.allBadges[this.lastShownBadge - 5];
        this.badgesShown.unshift(prevBadge);
      }
    }
  }

  badgesGoRight() {
    if (this.lastShownBadge < this.allBadges.length) {
      this.lastShownBadge++;
      if (this.lastShownBadge < this.allBadges.length) {
        this.badgesShown.shift();
        const nextBadge = this.allBadges[this.lastShownBadge - 1];
        this.badgesShown.push(nextBadge);
      }
    }
  }

  // --------------------- SECTION  Card Events ------------------

  share(project) {
    if (project.isPublic) {
      const data = {
        name: project.name,
        createdBy: project.createdBy,
        image: project.icon_url,
        link: window.location.host + `/visitor/discover/${project.hashId}/discover-project`,
      };

      this.dialogService.open(ShareModalComponent, {
        context: {
          data: data,
        },
      });
    }
  }


  toggleLikeProject(project) {
    this._projectService
      .toggleLikeProject(project.id)
      .subscribe(res => {
        this.getLikedProjects();
      },
        error => {
          console.log('error:', error);
          project.isLiked = !project.isLiked;
        });
  }

  openProjectOverview(projectId) {
    if (Role['Teacher'].includes(this.currentUser.role)) {
      this.router.navigate([`teacher/discover/${projectId}/discover-project`]);
    } else if (Role['Student'].includes(this.currentUser.role) || Role['individualStudent'].includes(this.currentUser.role)) {
      this.router.navigate([`student/discover/${projectId}/discover-project`]);
    }
  }
  // --------------------- SECTION  Destory------------------

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
