import { ConnectionService } from './../../../../@core/utils/connection.service';
import { LikedProject } from '../../../../@core/data/likedProject';
import { GlobalService } from './../../../../@core/utils/global.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {

  constructor(private _globalService: GlobalService, private _connectionService: ConnectionService) { }

  getLikedProjectsCount() {
    const url = 'coding-ville/projects/liked/count';
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      }).catch((error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      });
  }

  getLikedProjects(start, limit, search) {
    const url: string = `coding-ville/projects/liked`;
    const params = new HttpParams().set('start_from', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getGenralData() {
    const url = 'coding-ville/dashboard/student/data';
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      }).catch((error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      });
  }

  getBadges() {
    const url = 'coding-ville/badges';
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      }).catch((error: any) => {
         this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      });
  }
}
