
import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../../../@core/utils/global.service';
import { Breadcrumb } from './../../../../@core/data/breadcrumb';

@Component({
  selector: 'ngx-journey',
  templateUrl: './journey.component.html',
  styleUrls: ['./journey.component.scss'],
})

export class JourneyComponent implements OnInit {

  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Journeys',
      link: '',
    },
  ];

  constructor(private _globalService: GlobalService) { }

  ngOnInit() {
    this._globalService.getBreadcrumbs(this.breadcrumbs);
  }

}
