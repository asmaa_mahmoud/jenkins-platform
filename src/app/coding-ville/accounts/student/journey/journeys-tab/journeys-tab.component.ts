import { AuthenticationService } from './../../../../../@core/utils/authentication.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { Journey } from '../../../../../@core/data/journey';
import { Router } from '@angular/router';
import { JourneysService } from '../journeys.service';
import { Role } from '../../../../../@core/data/role';


@Component({
  selector: 'ngx-journeys-tab',
  templateUrl: './journeys-tab.component.html',
  styleUrls: ['./journeys-tab.component.scss'],
})
export class JourneysTabComponent implements OnInit {

  journeys: any;
  journeysLoaded = false;
  journeysLength: number;
  @Input() classroom_id;
  classroomName;
  @Output() changePage = new EventEmitter();
  @Output() changePagePerSelect = new EventEmitter();

  cardPerPage = 6;
  currentPage = 1;
  start: number = 0;
  searchValue: string = '';
  noOfCardsPerPage = [6, 9, 12];
  currentRole;
  isUserIndividual: boolean = false;

  constructor(
    private _journeyService: JourneysService,
    private router: Router,
    private _authService: AuthenticationService,
  ) {
    this._authService.studentClassroomID.subscribe(x => this.classroom_id = x);
    this._authService.currentUser.subscribe(x => this.currentRole = x.role);
  }

  ngOnInit() {
    this.isIndividual();
  }

  isIndividual() {
    if (Role['Student'].includes(this.currentRole)) {
      this.countJourneys();
      this.isUserIndividual = false;
    } else if (Role['individualStudent'].includes(this.currentRole)) {
      this.countIndividualJourneys();
      this.isUserIndividual = true;
    }
  }

  countJourneys() {
    this._journeyService.getJourneysCount(this.classroom_id, this.searchValue).subscribe((data: any) => {
      this.journeysLength = data.Object;
      this.getJourneys();
    });
  }

  countIndividualJourneys() {
    this._journeyService.countIndividualJourneys().subscribe((data: any) => {
      this.journeysLength = data.Object.count;
      this.getAllindividualJourneys();
    });
  }

  getJourneys() {
    this._journeyService.getClassroomJourneys(
      this.classroom_id, this.start.toString(), this.cardPerPage.toString(), this.searchValue,
    ).subscribe((data: any) => {
      this.classroomName = data.Object.classroomName;
      this.journeys = data.Object.journeys;
      this.journeysLoaded = true;
    });
  }

  getAllindividualJourneys() {
    this._journeyService.getAllIndividualJourneys(this.start.toString(), this.cardPerPage.toString(), this.searchValue)
      .subscribe((data: any) => {
        this.journeys = data.Object;
        this.journeysLoaded = true;
      });
  }

  openJourneyAdventure(journey: Journey) {
    if (this.isUserIndividual) {
      this.router.navigate([`student/journeys/${journey.id}/adventure`]);
    } else {
      this.router.navigate([`student/journeys/${journey.id}/course/${journey.course_id}/adventure`]);
    }
  }

  onChangePagination(num: number) {
    this.start = num * this.cardPerPage - num;
    if (this.isUserIndividual) {
      this.countIndividualJourneys();
    } else {
      this.countJourneys();
    }
  }

  onChangePageLimit() {
    this.start = 0;
    if (this.isUserIndividual) {
      this.countIndividualJourneys();
    } else {
      this.countJourneys();
    }
  }

  search(event: string) {
    this.searchValue = event;
    this.start = 0;
    if (this.isUserIndividual) {
      this.countIndividualJourneys();
    } else {
      this.countJourneys();
    }
  }

}
