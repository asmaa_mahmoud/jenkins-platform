import { GlobalService } from '../../../../@core/utils/global.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { HttpParams } from '@angular/common/http';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { CustomEncoder } from '../../../../@core/utils/custom-encoder';

@Injectable({
  providedIn: 'root',
})
export class JourneysService {

  constructor(private _globalService: GlobalService) { }

  countIndividualJourneys() {
    const url = 'coding-ville/journeys/count';
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllIndividualJourneys(start, limit, search) {
    const url: string = `coding-ville/journeys/all`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('start', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getJourneysCount(classroom_id, search) {
    const url: string = `school/${classroom_id}/journey/count`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('search', search);
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        console.log(error.message);
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getClassroomJourneys(classroom_id: string, start: string = '1', limit: string = '3', search: string = '') {
    const url: string = `coding-ville/classroom/${classroom_id}/journeys`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('start', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getClassroomPreJourneys(classroom_id: string, start: string = '1', limit: string = '3', search: string = '') {
    const url: string = `coding-ville/classroom/${classroom_id}/prejourneys`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('start', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getJourneyAdventure(journeyID, courseID?) {
    const url: string = `coding-ville/journey/${journeyID}/adventure`;
    const params = courseID ? new HttpParams().set('course_id', courseID) : null;
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAdventureMissions(adventureID: string, courseID) {
    const url: string = `coding-ville/student/courses/${courseID}/${adventureID}/data`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAdventureMissionsWithoutCourseID(journeyId, adventureId) {
    const url: string = `coding-ville/journeys/missions`;
    const params = new HttpParams().set('journey_id', journeyId).append('adventure_id', adventureId);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getPreJourneyMissions(classroom_id, journey_id) {
    const url: string = `coding-ville/activities/missions`;
    const params = new HttpParams().set('classroom_id', classroom_id)
      .append('activity_id', journey_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getPreJourneyMissionsWithNoCourse(journeyID) {
    const url: string = `coding-ville/activities/missions`;
    const params = new HttpParams().set('activity_id', journeyID);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  videoSeen(journeyID) {
    const url: string = `coding-ville/journey/${journeyID}/video/save`;
    return this._globalService.post(url);
  }

}
