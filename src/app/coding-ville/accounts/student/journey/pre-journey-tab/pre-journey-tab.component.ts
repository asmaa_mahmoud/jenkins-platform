import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { Journey } from '../../../../../@core/data/journey';
import { Router } from '@angular/router';
import { ClassRoomService } from '../../../teacher/classroom/class-room.service';
import { AuthenticationService } from '../../../../../@core/utils/authentication.service';
import { Role } from '../../../../../@core/data/role';
import { JourneysService } from '../../../teacher/journeys/journeys.service';

@Component({
  selector: 'ngx-pre-journey-tab',
  templateUrl: './pre-journey-tab.component.html',
  styleUrls: ['./pre-journey-tab.component.scss'],
})
export class PreJourneyTabComponent implements OnInit {

  cardPerPage: number = 6;
  currentPage = 1;
  classroomID;
  start: number = 0;
  classroomName: string;
  noOfCardsPerPage = [6, 9, 12];
  searchValue: string = '';
  preJourneyData: any[];
  preJourneysLoaded: boolean = false;
  preJourneysLength;
  courseID;
  currentRole;
  isUserIndividual: boolean = false;

  constructor(private router: Router,
    private _classroomService: ClassRoomService,
    private _individualService: JourneysService,
    private _authService: AuthenticationService) {
    this._authService.studentClassroomID.subscribe(x => this.classroomID = x);
    this._authService.currentUser.subscribe(x => this.currentRole = x.role);
  }

  ngOnInit() {
    this.isIndividual();
  }

  isIndividual() {
    if (Role['Student'].includes(this.currentRole)) {
      this.countPreJourneys();
      this.isUserIndividual = false;
    } else if (Role['individualStudent'].includes(this.currentRole)) {
      this.countIndividualPreJourneys();
      this.isUserIndividual = true;
    }
  }

  countPreJourneys() {
    this._classroomService.getPreJourneysCount(this.classroomID, this.searchValue).subscribe((data: any) => {
      this.preJourneysLength = data.Object.count;
      this.getAllPreJourneys();
    });
  }

  countIndividualPreJourneys() {
    this._individualService.countPreJourneys(this.searchValue).subscribe((data: any) => {
      this.preJourneysLength = data.Object.count;
      this.getAllindividualPreJourneys();
    });
  }

  getAllPreJourneys() {
    this._classroomService.getAllPreJourneys(this.classroomID,
      this.start.toString(), this.cardPerPage.toString(), this.searchValue)
      .subscribe((data: any) => {
        this.classroomName = data.Object.classroomName;
        this.courseID = data.Object.course_id;
        this.preJourneyData = data.Object.journeys;
        this.preJourneysLoaded = true;
      });
  }

  getAllindividualPreJourneys() {
    this._individualService.getAllPreJourneys(this.start.toString(), this.cardPerPage.toString(), this.searchValue)
      .subscribe((data: any) => {
        this.preJourneyData = data.Object;
        this.preJourneysLoaded = true;
      });
  }

  openPreJourneyAdventure(journey: Journey) {
    if (this.isUserIndividual) {
      this.router.navigate([`student/journeys/${journey.id}/missions`]);
    } else {
      this.router.navigate([`student/journeys/${journey.id}/course/${this.courseID}/missions`]);
    }
  }

  onChangePagination(num: number) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    if (this.isUserIndividual) {
      this.countIndividualPreJourneys();
    } else {
      this.countPreJourneys();
    }
  }

  onChangePageLimit() {
    this.start = 0;
    if (this.isUserIndividual) {
      this.countIndividualPreJourneys();
    } else {
      this.countPreJourneys();
    }
  }

  search(event: string) {
    this.searchValue = event;
    this.start = 0;
    if (this.isUserIndividual) {
      this.countIndividualPreJourneys();
    } else {
      this.countPreJourneys();
    }
  }

}
