import { Component, OnInit } from '@angular/core';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-onboard',
  templateUrl: './onboard.component.html',
  styleUrls: ['./onboard.component.scss'],
})
export class OnboardComponent implements OnInit {

  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Get Started',
      link: '',
    },
  ];

  constructor(
    private globalService: GlobalService,
  ) { }

  ngOnInit() {
    this.globalService.getBreadcrumbs(this.breadcrumbs);

  }

}
