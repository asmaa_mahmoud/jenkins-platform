import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS_DISABLED: NbMenuItem[] = [
  {
    title: 'dashboard',
    icon: 'bar-chart-outline',
    link: '',
    home: true,
  },
  {
    title: 'journey',
    icon: 'map',
    link: '',
  },
  {
    title: 'projects',
    icon: 'flag',
    link: '',
  },
  {
    title: 'badges',
    icon: 'award',
    link: '',
  },
  {
    title: 'discover',
    icon: 'compass',
    link: '',
  },
];
