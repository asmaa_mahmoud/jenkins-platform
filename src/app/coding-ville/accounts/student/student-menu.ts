import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'dashboard',
    icon: { icon: 'dashboard', pack: 'custom-icons' },
    link: '/student/dashboard',
    home: true,
  },
  {
    title: 'journey',
    icon: { icon: 'journeys', pack: 'custom-icons' },
    link: '/student/journeys',
    pathMatch: 'prefix',
  },
  {
    title: 'projects',
    icon: { icon: 'projects', pack: 'custom-icons' },
    link: '/student/projects',
    pathMatch: 'prefix',
  },
  {
    title: 'badges',
    icon: { icon: 'badges', pack: 'custom-icons' },
    link: '/student/badges',
    pathMatch: 'prefix',
  },
  {
    title: 'discover',
    icon: 'compass',
    link: '/student/discover',
    pathMatch: 'prefix',
  },
];
