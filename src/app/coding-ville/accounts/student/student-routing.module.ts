import { ChangePasswordComponent } from './../shared/edit-profile/change-password/change-password.component';
import { PersonalInfoComponent } from './../shared/edit-profile/personal-info/personal-info.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { StudentComponent } from './student.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { JourneyComponent } from './journey/journey.component';
import { AdventureComponent } from '../shared/adventure/adventure.component';
import { BadgesComponent } from './badges/badges.component';
import { DiscoverComponent } from '../shared/discover/discover.component';
import { EditProfileComponent } from '../shared/edit-profile/edit-profile.component';
import { StudentProfileComponent } from '../shared/profile/student-profile.component';
import { PreJourneyTabComponent } from './journey/pre-journey-tab/pre-journey-tab.component';
import { JourneysTabComponent } from './journey/journeys-tab/journeys-tab.component';
import { ProjectPreviewComponent } from '../shared/project-preview/project-preview.component';
import { ProjectsComponent } from '../shared/projects/projects.component';
import { OnboardComponent } from './onboard/onboard.component';


const routes: Routes = [{
  path: '',
  component: StudentComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: 'dashboard',
      component: DashboardComponent,
      data: { title: 'dashboard' },
    },
    {
      path: 'journeys',
      component: JourneyComponent,
      data: { title: 'journey' },

    },
    {
      path: 'journeys/:journey_id/course/:course_id/:type',
      component: AdventureComponent,
      data: { title: 'journey' },
    },
    {
      path: 'journeys/:journey_id/:type',
      component: AdventureComponent,
      data: { title: 'journey' },
    },
    {
      path: 'badges',
      component: BadgesComponent,
      data: { title: 'badges' },
    },
    {
      path: 'discover',
      component: DiscoverComponent,
      data: { title: 'discover' },
    },
    {
      path: ':type/:project_id/:discover-project',
      component: ProjectPreviewComponent,
    },
    // ****** preview default projects of journey ******//
    {
      path: ':type/:project_id/:journey-type/adventure/:adventureID/course/:courseID',
      component: ProjectPreviewComponent,
    },
    {
      path: ':type/:project_id/:journey-type/:journeyID/adventure/:adventureID',
      component: ProjectPreviewComponent,
    },
    // ****** preview default projects of pre-journey ******//
    {
      path: ':type/:project_id/:journey-type/:activityID/course/:courseID',
      component: ProjectPreviewComponent,
    },
    {
      path: ':type/:project_id/:journey-type/:activityID',
      component: ProjectPreviewComponent,
    },
    {
      path: 'projects',
      component: ProjectsComponent,
      data: { title: 'Projects' },
    },
    {
      path: 'profile/:student_id',
      component: StudentProfileComponent,
    },
    {
      path: 'edit-profile',
      component: EditProfileComponent,
      data: { title: 'edit-profile' },
      children: [
        {
          path: '',
          redirectTo: 'profile-details',
          pathMatch: 'full',
        },
        {
          path: 'profile-details',
          component: PersonalInfoComponent,
        },
        {
          path: 'change-password',
          component: ChangePasswordComponent,
        },
      ],
    },
    {
      path: 'on-board',
      component: OnboardComponent,
      data: { title: 'on-board' },
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class StudentRoutingModule {
}
export const Components =
  [
    DashboardComponent,
    JourneyComponent,
    StudentComponent,
    BadgesComponent,
    PreJourneyTabComponent,
    JourneysTabComponent,
    OnboardComponent,
  ];
