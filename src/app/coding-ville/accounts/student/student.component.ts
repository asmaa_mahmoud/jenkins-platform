import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../../@core/utils/authentication.service';
import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './student-menu';
import { MENU_ITEMS_DISABLED } from './student-menu-disabled';

@Component({
  selector: 'ngx-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss'],
})

export class StudentComponent implements OnInit {

  isActivated: boolean;
  menu: any;

  constructor(private authService: AuthenticationService, private router: Router, private translateService: TranslateService) {
    this.isActivated = this.authService.isActivated();
  }

  ngOnInit() {
    if (!this.isActivated) this.router.navigate(['/student/dashboard']);
    this.getMenu();
  }

  getMenu() {
    this.menu = this.isActivated ? MENU_ITEMS : MENU_ITEMS_DISABLED;
    this.menu.map(item => {
      this.translateService.get(item.title).subscribe(res => {
        item.title = res;
      });
      // if (!this.isActivated) item.link = '';
    });
  }

}
