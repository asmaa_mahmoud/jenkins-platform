import { TranslatorModule } from './../../../@shared/translator/translator.module';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { DashboardService } from './dashboard/dashboard.service';
import { JourneysService } from './journey/journeys.service';
import { NgModule } from '@angular/core';
import { NbMenuModule, NbCardModule, NbSelectModule, NbTabsetModule, NbIconModule, NbDialogService, NbInputModule, NbButtonModule } from '@nebular/theme';

import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../../@theme/theme.module';
import { StudentRoutingModule, Components } from './student-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ClipboardModule } from 'ngx-clipboard';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbMenuModule,
    FormsModule,
    SharedModule,
    StudentRoutingModule,
    NbCardModule,
    NbSelectModule,
    NbTabsetModule,
    NbEvaIconsModule,
    NbIconModule,
    NbInputModule,
    NbButtonModule,
    ClipboardModule,
    TranslatorModule,
  ],

  declarations: [
    ...Components,
  ],
  providers: [JourneysService, DashboardService, NbDialogService],
})
export class StudentModule { }


