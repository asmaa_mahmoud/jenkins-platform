import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Breadcrumb } from '../../../../../@core/data/breadcrumb';
import { ClassRoomService } from '../class-room.service';
import { Journey } from '../../../../../@core/data/journey';
import { GlobalService } from '../../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-add-journey',
  templateUrl: './add-journey.component.html',
  styleUrls: ['./add-journey.component.scss'],
})
export class AddJourneyComponent implements OnInit {
  noOfCardsPerPage: any[] = [6, 9, 12];
  cardsPerPage: number = 6;
  currentPage: number = 1;
  start: number = 0;
  journeys: Journey[];
  classroom_id: string;
  searchValue: string = '';
  journeysCount: number;
  allData: any;
  private breadcrumbs: Breadcrumb[];
  loadingId: any;


  constructor(private route: ActivatedRoute,
    private _classroomService: ClassRoomService,
    private router: Router,
    private globalService: GlobalService) { }

  ngOnInit() {
    this.getClassroomId();
    this.getUnassignedJourneysCount();
  }

  getUnassignedJourneysCount() {
    this._classroomService.getUnassignedJourneysCount(this.classroom_id, this.searchValue).subscribe((data: any) => {
      this.journeysCount = data.Object.count;
      this.getUnassignedJourneys();
    });
  }

  getUnassignedJourneys() {
    this._classroomService.getUnassignedJourneys(
      this.classroom_id, this.start.toString(), this.cardsPerPage.toString(), this.searchValue,
    ).subscribe(data => {
      this.allData = data.Object;
      this.journeys = data.Object.journeys;
      this.initBreadcrumb();
      this.loadingId = null;
    });
  }

  initBreadcrumb() {
    this.breadcrumbs = [
      {
        name: 'CLASSROOMs',
        link: 'classroom',
      },
      {
        name: this.allData.classroomName,
        link: 'classroom/' + this.classroom_id,
      },
      {
        name: 'Add Journey',
        link: '',
      },
    ];
    this.globalService.getBreadcrumbs(this.breadcrumbs);
  }

  getClassroomId() {
    this.route.params.subscribe((params: Params) => {
      this.classroom_id = params['classroom_id'];
    });
  }

  changePagination(num: number) {
    this.start = num * this.cardsPerPage - this.cardsPerPage;
    this.getUnassignedJourneysCount();
  }

  changeItemPerPage() {
    this.start = 0;
    this.getUnassignedJourneysCount();
  }

  search(event: string) {
    this.searchValue = event;
    this.start = 0;
    this.getUnassignedJourneysCount();
  }

  assignJourney(id) {
    this.loadingId = id;
    this._classroomService.assignJourney(id, this.classroom_id).subscribe(
      Response => {
        this.getUnassignedJourneysCount();

      },
    );
  }

  openJourneyMissions(journey) {
    this.router.navigate([`teacher/classroom/${this.classroom_id}/journey/${journey.id}/adventure`],
      { queryParams: { classroomName: this.allData.classroomName } });
  }

}
