import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Breadcrumb } from '../../../../../@core/data/breadcrumb';
import { ClassRoomService } from '../class-room.service';
import { Journey } from '../../../../../@core/data/journey';
import { GlobalService } from '../../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-add-pre-journey',
  templateUrl: './add-pre-journey.component.html',
  styleUrls: ['./add-pre-journey.component.scss'],
})
export class AddPreJourneyComponent implements OnInit {
  noOfCardsPerPage: any[] = [6, 9, 12];
  cardsPerPage: number = 6;
  currentPage: number = 1;
  start: number = 0;
  journeys: Journey[];
  classroom_id: string;
  searchValue: string = '';
  journeysCount: number;
  allData: any;
  private breadcrumbs: Breadcrumb[];


  constructor(private route: ActivatedRoute,
    private _classroomService: ClassRoomService,
    private router: Router,
    private globalService: GlobalService) { }

  ngOnInit() {
    this.getClassroomId();
    this.getUnassignedPreJourneysCount();
  }

  getUnassignedPreJourneysCount() {
    this._classroomService.getUnassignedPreJourneysCount(this.classroom_id).subscribe((data: any) => {
      this.journeysCount = data.Object.count;
      this.getUnassignedPreJourneys();
    });
  }

  getUnassignedPreJourneys() {
    this._classroomService.getUnassignedPreJourneys(
      this.classroom_id, this.start.toString(), this.cardsPerPage.toString(), this.searchValue,
    ).subscribe(data => {
      this.allData = data.Object;
      this.journeys = data.Object.journeys;
      this.initBreadcrumb();
    });
  }

  initBreadcrumb() {
    this.breadcrumbs = [
      {
        name: 'CLASSROOM',
        link: 'classroom',
      },
      {
        name: this.allData.classroomName,
        link: 'classroom/' + this.classroom_id,
      },
      {
        name: 'Add PreJourney',
        link: '',
      },
    ];
    this.globalService.getBreadcrumbs(this.breadcrumbs);
  }

  getClassroomId() {
    this.route.params.subscribe((params: Params) => {
      this.classroom_id = params['classroom_id'];
    });
  }

  changePagination(num: number) {
    this.start = num * this.cardsPerPage - this.cardsPerPage;
    this.getUnassignedPreJourneysCount();
  }

  changeItemPerPage() {
    this.start = 0;
    this.getUnassignedPreJourneysCount();
  }

  search(value: string) {
    this.searchValue = value;
    this.start = 0;
    this.getUnassignedPreJourneysCount();
  }

  assignPreJourney(journey) {
    this._classroomService.assignPreJourney(journey.id, this.classroom_id).subscribe((data: any) => {
      this.getUnassignedPreJourneysCount();
    });
  }

  openJourneyMissions(journey) {
    this.router.navigate([`teacher/classroom/${this.classroom_id}/journey/${journey.id}/missions`],
      { queryParams: { classroomName: this.allData.classroomName } });
  }

}
