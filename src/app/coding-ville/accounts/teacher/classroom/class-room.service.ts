import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Classroom } from '../../../../@core/data/classroom';
import { ClassroomJourneys } from '../../../../@core/data/classroom-journeys';
import { GlobalService } from '../../../../@core/utils/global.service';
import { Project } from '../../../../@core/data/project';
import { HttpParams } from '@angular/common/http';
import { CustomEncoder } from '../../../../@core/utils/custom-encoder';

@Injectable({
  providedIn: 'root',
})
export class ClassRoomService {


  constructor(private _globalService: GlobalService) { }

  // ===============================//
  // ------START CLASSROOM------//
  // ===============================//
  countClassrooms() {
    const url = 'school/teacher/classrooms/count';
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getClassrooms(start, limit, search): Observable<Classroom[]> {
    const url: string = `coding-ville/teacher/classroom/classroom/all`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('start', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  editClassroom(classroom: FormData) {
    const url: string = `coding-ville/teacher/classroom/classroom/edit`;
    return this._globalService.post(url, classroom).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'edited-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  createClassroom(classroom: FormData) {
    const url: string = `coding-ville/teacher/classroom/classroom/create`;
    return this._globalService.post(url, classroom).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'created-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  deleteClassroom(classroom_id: number, forced: string) {
    const url: string = `coding-ville/teacher/classroom/classroom/del`;
    return this._globalService.post(url, { classroom_id: classroom_id, force: forced }).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END CLASSROOM------//
  // ===============================//

  getGrades() {
    const url: string = `school/grade`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------START JOURNEYS------//
  // ===============================//

  getJourneysCount(classroom_id: string, search) {
    const url: string = 'coding-ville/teacher/classroom/journeys/count';
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('classroom_id', classroom_id).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllJourneys(classroom_id: string, start: string, limit: string, search: string) {
    const url: string = `coding-ville/teacher/classroom/journeys/all`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('classroom_id', classroom_id)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getJourneyAdventures(journey_id: string) {
    const url: string = `coding-ville/journey/${journey_id}/adventure`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getUnassignedJourneysCount(classroom_id: string, search) {
    const url: string = 'coding-ville/teacher/classroom/journeys/uascount';
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('classroom_id', classroom_id).append('search', search);
    return this._globalService.get(url, params);
  }

  getUnassignedJourneys(classroom_id: string, start: string, limit: string, search: string) {
    const url: string = `coding-ville/teacher/classroom/journeys/uasall`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('classroom_id', classroom_id)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  assignJourney(journeyID, classroomID) {
    const url: string = `coding-ville/teacher/classroom/journeys/assign`;
    const params = new HttpParams().set('journey_id', journeyID).append('classroom_id', classroomID);
    return this._globalService.post(url, params).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'assigned-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  unassignJourney(journeyID, classroomID) {
    const url: string = `coding-ville/teacher/classroom/journeys/unassign`;
    const params = new HttpParams().set('journey_id', journeyID).append('classroom_id', classroomID);
    return this._globalService.post(url, params).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'unassigned-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END JOURNEYS------//
  // ===============================//

  // ===============================//
  // ------START PRE JOURNEYS------//
  // ===============================//

  getPreJourneysCount(classroom_id: string, search) {
    const url: string = 'coding-ville/teacher/classroom/activities/count';
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('classroom_id', classroom_id).append('search', search);
    return this._globalService.get(url, params);
  }

  getAllPreJourneys(classroom_id: string, start: string, limit: string, search: string) {
    const url: string = `coding-ville/teacher/classroom/activities/all`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('classroom_id', classroom_id)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getPreJourneyMissions(journey_id: string) {
    const url: string = `coding-ville/journey/${journey_id}/adventure`;
    return this._globalService.get(url);
  }

  getUnassignedPreJourneysCount(classroom_id: string) {
    const url: string = 'coding-ville/teacher/classroom/activities/uascount';
    const params = new HttpParams().set('classroom_id', classroom_id);
    return this._globalService.get(url, params);
  }

  getUnassignedPreJourneys(classroom_id: string, start: string, limit: string, search: string) {
    const url: string = `coding-ville/teacher/classroom/activities/uasall`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('classroom_id', classroom_id)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  assignPreJourney(preJourneyID, classroomID) {
    const url: string = `coding-ville/teacher/classroom/activities/assign`;
    const params = new HttpParams().set('activity_id', preJourneyID).append('classroom_id', classroomID);
    return this._globalService.post(url, params).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'assigned-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  unassignPreJourney(journeyID, classroomID) {
    const url: string = `coding-ville/teacher/classroom/activities/unassign`;
    const params = new HttpParams().set('activity_id', journeyID).append('classroom_id', classroomID);
    return this._globalService.post(url, params).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'unassigned-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END JOURNEYS------//
  // ===============================//



  // ===============================//
  // ------START STUDENTS------//
  // ===============================//

  getStudentsCount(classroom_id: string) {
    const url: string = `school/classrooms/${classroom_id}/count`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getClassroomStudents(classroom_id: string, start: string, limit: string, search: string) {
    const url: string = `school/students/${classroom_id}/index`;
    const params = new HttpParams({ encoder: new CustomEncoder() })
      .set('start_from', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getShareCode(classroom_id: string) {
    const url: string = `school/classroom/${classroom_id}/code`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  uploadCSV(file: any, classroom_id: string) {
    const url: string = `coding-ville/teacher/classroom/uploadcsv/${classroom_id}`;
    const formdata = new FormData();
    formdata.append('file', file);
    return this._globalService.post(url, formdata).map(
      (data: any) => {
        if (data.Object.totalSuccess === data.Object.total) {
          this._globalService.showTranslatedToast('success', null, 'created-successfully');
        } else {
          this._globalService.showTranslatedToast('danger', data.Object.errors[0].errors, 'something-went-wrong');
        }
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  addStudent(classroom_id: string, name: string, username: string, password: string) {
    const url: string = `school/classroom/student`;
    return this._globalService.post(url, { id: classroom_id, name: name, username: username, password: password }).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'added-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  editStudent(student_id: string, username: string, password: string, classroom_id: string) {
    const url: string = `school/update/student/${student_id}`;
    return this._globalService.post(
      url,
      { username: username, password: password, classroom: classroom_id },
    ).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'edited-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  deleteStudent(student_id: number) {
    const url: string = `school/student/delete`;
    return this._globalService.post(url, { students: [student_id] }).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END STUDENTS------//
  // ===============================//

  // ===============================//
  // ------START PROJECTS------//
  // ===============================//

  countProjectsOfClassroom(classroom_id, search) {
    const url: string = `school/${classroom_id}/project/count`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getProjectsOfClassroom(classroom_id, start, limit, search, student_id) {
    const url: string = `coding-ville/classroom/${classroom_id}/project`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('classroom_id', classroom_id)
      .append('start', start)
      .append('limit', limit)
      .append('search', search)
      .append('student_id', student_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllStudentOfClassroom(classroom_id) {
    const url: string = `school/students/${classroom_id}`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  likeProject(project_id: number, isLiked: boolean) {
    let url = `coding-ville/projects/${project_id}/`;
    isLiked ? url += 'like' : url += 'dislike';
    return this._globalService.put(url, {}).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllClassrooms() {
    const url = `coding-ville/teacher/classroom/classroom/all`;
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  // ===============================//
  // ------END PROJECTS------//
  // ===============================//



  // ===============================//
  // ------START STUDENT PROGRESS------//
  // ===============================//
  getStudentJourneyProgress(adventureID, courseID, start, limit, search) {
    const url = 'coding-ville/teacher/classroom/journeys/students';
    const params = new HttpParams({ encoder: new CustomEncoder() })
      .set('adventure_id', adventureID)
      .append('course_id', courseID)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


  getStudentPreJourneyProgress(activity_id, courseID, start, limit, search) {
    const url = 'coding-ville/teacher/classroom/activities/students';
    const params = new HttpParams({ encoder: new CustomEncoder() })
      .set('activity_id', activity_id)
      .append('course_id', courseID)
      .append('start', start)
      .append('limit', limit)
      .append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }


  getClassroomData(classroomID) {
    const url = `school/classroom/${classroomID}`;
    return this._globalService.get(url);
  }



  // ===============================//
  // ------END  STUDENT PROGRESS------//
  // ===============================//



}
