import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ClassRoomDataService {

  allClassrooms: any = [];
  selectedJourney: any = null;
  selectedAdventure: any = null;

  constructor() { }

}
