import { ClassRoomDataService } from './../classroom-data.service';
import { Component, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';
import { ClassRoomService } from '../class-room.service';
import { Classroom } from '../../../../../@core/data/classroom';
import { ClassroomJourneys } from '../../../../../@core/data/classroom-journeys';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'ngx-classroom-journeys',
  templateUrl: './classroom-journeys.component.html',
  styleUrls: ['./classroom-journeys.component.scss'],
})
export class ClassroomJourneysComponent implements OnInit, OnChanges {

  @Output() fetchedClassroomName = new EventEmitter<string>();

  noOfCardsPerPage: any[] = [6, 9, 12];
  classrooms: Classroom[];
  cardsPerPage: number = 6;
  currentPage: number = 1;
  start: number = 0;
  searchValue: string = '';
  classroomsCount: number;
  journeysCount: number;
  journeys: ClassroomJourneys[];
  classroom_id: string;
  classroomName;

  constructor(
    private _classroomService: ClassRoomService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _classroomDataService: ClassRoomDataService,
  ) { }

  ngOnInit() {
    this.getClassroomId();
    this.getJourneysCount();
  }

  ngOnChanges() {
    this.getJourneysCount();
  }

  getJourneysCount() {
    this._classroomService.getJourneysCount(this.classroom_id, this.searchValue).subscribe((data: any) => {
      this.journeysCount = data.Object.count;
      this.getJourneys();
    });
  }

  getJourneys() {
    this._classroomService.getAllJourneys(
      this.classroom_id, (this.start).toString(), this.cardsPerPage.toString(), this.searchValue,
    ).subscribe(data => {
      this.journeys = data.Object.journeys;
      this.classroomName = data.Object.classroomName;
      this.fetchedClassroomName.emit(data.Object.classroomName);
    });
  }

  search(event) {
    this.searchValue = event;
    this.start = 0;
    this.getJourneysCount();
  }

  getClassroomId() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.classroom_id = params['classroom_id'];
    });
  }

  changePagination(num) {
    this.start = num * this.cardsPerPage - this.cardsPerPage;
    this.getJourneysCount();
  }

  unassignJourney(journey) {
    this._classroomService.unassignJourney(journey.id, this.classroom_id).subscribe((data: any) => {
      this.getJourneysCount();
    });
  }

  goToJourneyProgress(journey: any) {
    this.router.navigate([`teacher/classroom/${this.classroom_id}/course/${journey.course_id}/journey/${journey.id}/progress`]);
  }

  addJourneyToClassroom() {
    this.router.navigate(['teacher/classroom/add', this.classroom_id]);
  }

  openJourney(journey) {
    this.router.navigate([`teacher/classroom/${this.classroom_id}/journey/${journey.id}/course/${journey.course_id}/adventure`],
      { queryParams: { classroomName: this.classroomName } });
  }

}
