import { Component, OnInit } from '@angular/core';
import { Classroom } from '../../../../../@core/data/classroom';
import { ClassRoomService } from '../class-room.service';
import { ClassroomJourneys } from '../../../../../@core/data/classroom-journeys';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ClassRoomDataService } from '../classroom-data.service';

@Component({
  selector: 'ngx-classroom-mini-journeys',
  templateUrl: './classroom-mini-journeys.component.html',
  styleUrls: ['./classroom-mini-journeys.component.scss'],
})
export class ClassroomMiniJourneysComponent implements OnInit {
  noOfCardsPerPage: any[] = [6, 9, 12];
  classrooms: Classroom[];
  cardsPerPage: number = 6;
  currentPage: number = 1;
  start: number = 0;
  searchValue: string = '';
  classroomsCount: number;
  journeysCount: number;
  journeys: ClassroomJourneys[];
  classroom_id: string;
  course_id: string;
  classroomName;


  constructor(private _classroomService: ClassRoomService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _classroomDataService: ClassRoomDataService,
  ) { }

  ngOnInit() {
    this.getClassroomId();
    this.getPreJourneysCount();
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnChanges() {
    this.getPreJourneysCount();
  }

  getPreJourneysCount() {
    this._classroomService.getPreJourneysCount(this.classroom_id, this.searchValue).subscribe((data: any) => {
      this.journeysCount = data.Object.count;
      this.getPreJourneys();
    });
  }

  getPreJourneys() {
    this._classroomService.getAllPreJourneys(
      this.classroom_id, (this.start).toString(), this.cardsPerPage.toString(), this.searchValue,
    ).subscribe(data => {
      this.journeys = data.Object.journeys;
      this.course_id = data.Object.course_id;
    });
  }

  search(event) {
    this.searchValue = event;
    this.start = 0;
    this.getPreJourneysCount();
  }

  getClassroomId() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.classroom_id = params['classroom_id'];
      this.getClassroomName();
    });
  }

  getClassroomName() {
    this._classroomService.getClassroomData(this.classroom_id).subscribe(data => {
      this.classroomName = data.Object.Name;
    });
  }

  changePagination(num) {
    this.start = num * this.cardsPerPage - this.cardsPerPage;
    this.getPreJourneysCount();
  }

  unassignPrejourney(journey) {
    this._classroomService.unassignPreJourney(journey.id, this.classroom_id).subscribe((data: any) => {
      this.getPreJourneysCount();
    });
  }

  goToPreJourneyProgress(journey: any) {
    this.router.navigate([`teacher/classroom/${this.classroom_id}/course/${this.course_id}/prejourney/${journey.id}/progress`]);
  }

  addPreJourneyToClassroom() {
    this.router.navigate(['teacher/classroom/add-pre', this.classroom_id]);
  }

  openPreJourneyMissions(journey) {
    this.router.navigate([`teacher/classroom/${this.classroom_id}/journey/${journey.id}/course/${this.course_id}/missions`],
      { queryParams: { classroomName: this.classroomName } });
  }

}
