import { Component, OnInit } from '@angular/core';
import { ClassRoomService } from '../class-room.service';
import { Project } from '../../../../../@core/data/project';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ShareModalComponent } from '../../../shared/share-modal/share-modal.component';

@Component({
  selector: 'ngx-classroom-projects',
  templateUrl: './classroom-projects.component.html',
  styleUrls: ['./classroom-projects.component.scss'],
})
export class ClassroomProjectsComponent implements OnInit {

  projects: Project[];
  students: any[];
  cardsPerPage = [6, 9, 12];
  cardPerPage: number = 6;
  currentPage: number = 1;
  start: number = 1;
  limit: number = 3;
  searchValue: string = '';
  classroom_id: number;
  countOfProjects: number;
  selectedStudent: string = 'all';
  classroomName: string;

  constructor(private _classroomService: ClassRoomService,
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: NbDialogService,
  ) { }

  ngOnInit() {
    this.getClassroomId();
    this.getStudents();
    this.countProjectsOfClassroom();
  }

  countProjectsOfClassroom() {
    this._classroomService.countProjectsOfClassroom(this.classroom_id, this.searchValue).subscribe(res => {
      this.countOfProjects = res.Object;
      this.getProjectsOfClassroom();
    });
  }

  getProjectsOfClassroom() {
    this._classroomService.getProjectsOfClassroom(
      this.classroom_id,
      this.start,
      this.cardPerPage.toString(),
      this.searchValue,
      this.selectedStudent,
    )
      .subscribe(res => {
        this.classroomName = res.Object.classroomName;
        this.projects = res.Object.Projects;
      },
        error => {
          console.log('error:', error);
          this.projects = [];
        });
  }

  search(event) {
    this.searchValue = event;
    this.start = 0;
    this.countProjectsOfClassroom();
  }

  getStudents() {
    this._classroomService.getAllStudentOfClassroom(this.classroom_id).subscribe((data: any) => {
      this.students = data.Object;
      // this.selectedStudent = data.Object.length > 0 ? data.Object[0].id : null;
    });
  }

  openProject(project) {
    this.router.navigate([`teacher/classroom/${project.id}/preview-project`],
      { queryParams: { classroomName: this.classroomName, classroomID: this.classroom_id } });
  }

  getClassroomId() {
    this.route.params.subscribe((params: Params) => {
      this.classroom_id = params['classroom_id'];
    });
  }

  changeStudent() {
    this.countProjectsOfClassroom();
  }

  share(project) {
    if (project.isPublic) {
      const data = {
        name: project.name,
        createdBy: project.createdBy,
        image: project.image,
        link: window.location.host + `/visitor/discover/${project.hashId}/discover-project`,
      };

      this.dialogService.open(ShareModalComponent, {
        context: {
          data: data,
        },
      });
    }

  }

  toggleLikeProject(index: number) {
    const id = parseInt(this.projects[index].id, 10);
    const isLiked = this.projects[index].isLiked;
    this._classroomService.likeProject(id, isLiked).subscribe(
      (res: any) => {
        isLiked ? this.projects[index].noOfLikes = +this.projects[index].noOfLikes + 1 :
          this.projects[index].noOfLikes = +this.projects[index].noOfLikes - 1;
      }, (err: any) => {
        console.log(err);
      },
    );
  }

  changeItemPerPage() {
    this.start = 0;
    this.countProjectsOfClassroom();
  }

  changePagination(num) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.countProjectsOfClassroom();
  }

}
