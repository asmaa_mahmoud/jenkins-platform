import { DashboardService } from './../../dashboard/dashboard.service';
import { MaxModalComponent } from './../../../shared/max-modal/max-modal.component';
import { env } from './../../../../../@core/env/env';
import { ActivatedRoute, Params } from '@angular/router';
import { ClassRoomService } from './../class-room.service';
import { StudentModalComponent } from './student-modal/student-modal.component';
import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { DeleteModalComponent } from '../../../shared/delete-modal/delete-modal.component';

@Component({
  selector: 'ngx-classroom-students',
  templateUrl: './classroom-students.component.html',
  styleUrls: ['./classroom-students.component.scss'],
})
export class ClassroomStudentsComponent implements OnInit {

  tableLengths = [10, 25, 50, 100];
  tableLength = '10';
  studentsCount: string;
  start: number = 0;
  students = [];
  currentPage: string = '1';
  searchValue: string = '';
  maxShown = '10';

  journeysCount: number;
  classroom_id: string;

  shareCode: string;




  deleteDialogRef: NbDialogRef<DeleteModalComponent>;
  updateDialogRef: NbDialogRef<StudentModalComponent>;


  // ------------ SECTION ORG ------------
  isOrg = env.isOrg;
  numberOfStudents: number;
  maxStudentNumber: string;
  pricePerStudent: number;
  maxDialogRef: NbDialogRef<MaxModalComponent>;
  // ----------------

  constructor(
    private dialogService: NbDialogService,
    private _classroomService: ClassRoomService,
    private route: ActivatedRoute,
    private _dashboardService: DashboardService,
  ) { }

  ngOnInit() {
    this.getClassroomId();
    this.getStudentsCount();
    this.getShareCode();

    if (this.isOrg) {
      this.getMaxStudentsAndClasses();
      this.getGeneralData();
    }

  }


  getStudentsCount() {
    this._classroomService.getStudentsCount(this.classroom_id).subscribe((data: any) => {
      this.studentsCount = data.Object;
      this.getClassroomStudents();
    }, (error: any) => {
      console.log(error);
    });
  }

  getClassroomStudents() {
    this._classroomService.getClassroomStudents(
      this.classroom_id, this.start.toString(), this.tableLength, this.searchValue,
    ).subscribe((data: any) => {
      this.students = data.Object;
    },
    );
  }

  getShareCode() {
    this._classroomService.getShareCode(this.classroom_id).subscribe((data: any) => {
      this.shareCode = data.Object;
    });
  }

  deleteStudent(student_id: number) {
    this.deleteDialogRef = this.dialogService.open(DeleteModalComponent, {
      context: {
        type: 'classroomStudent',
        title: 'student',
        content: 'student',
        deleted_id: student_id,
      },
    });
    const sub = this.deleteDialogRef.componentRef.instance.dataUpdated.subscribe(() => {
      this.getStudentsCount();
    });
    this.deleteDialogRef.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }

  getClassroomId() {
    this.route.params.subscribe((params: Params) => {
      this.classroom_id = params['classroom_id'];
    });
  }

  changePagination(num: number) {
    this.start = (num * Number(this.tableLength) - Number(this.tableLength));
    this.getStudentsCount();
  }

  search(event: string) {
    this.searchValue = event;
    this.start = 0;
    this.getStudentsCount();
  }

  onChangeLength() {
    this.start = 0;
    this.maxShown = this.tableLength;
    if (this.tableLength === 'all')
      this.maxShown = this.studentsCount;
    this.getStudentsCount();
  }


  // ----------------- SECTION  .ORG -----------------

  getMaxStudentsAndClasses() {
    this._dashboardService.getMaxStudentsAndClasses().subscribe(
      res => {
        const students = res.Object.find(x => x.code && x.code === 'students');
        this.maxStudentNumber = students.value;

        const price = res.Object.find(x => x.code && x.code === 'student_price');
        this.pricePerStudent = price.value;
      });
  }

  getGeneralData() {
    this._dashboardService.getGeneralData().subscribe(
      res => {
        this.numberOfStudents = res.Object.noOfStudents;
      });
  }




  // ----------------- SECTION  Adding a student -----------------


  addStudent() {


    if (this.isOrg && !this.maxStudentNumber) {
      return;
    }


    if (this.isOrg && this.numberOfStudents.toString() === this.maxStudentNumber) {
      this.openUpgradeModal();
    } else {
      this.openAddStudentModal();
    }
  }


  openAddStudentModal() {
    this.updateDialogRef = this.dialogService.open(StudentModalComponent, {
      context: {
        modalType: 'add',
        shareCode: this.shareCode,
        classroom_id: this.classroom_id,
      },
    });
    const sub = this.updateDialogRef.componentRef.instance.updateStudents.subscribe(() => {
      this.getStudentsCount();
    });
    this.updateDialogRef.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }

  openUpgradeModal() {
    this.maxDialogRef = this.dialogService.open(MaxModalComponent, {
      context: {
        type: 'student',
        number: this.maxStudentNumber,
        price: this.pricePerStudent,
      },
    });
  }
  // ----------------

  // ----------------- SECTION  Edit a student -----------------


  openEditStudentModal(student: any) {
    this.updateDialogRef = this.dialogService.open(StudentModalComponent, {
      context: {
        modalType: 'edit',
        modalData: student,
        shareCode: this.shareCode,
        classroom_id: this.classroom_id,
      },
    });
    const sub = this.updateDialogRef.componentRef.instance.updateStudents.subscribe(() => {
      this.getStudentsCount();
    });
    this.updateDialogRef.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }


}
