import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ClassRoomService } from './../../../class-room.service';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'ngx-student-modal-add',
  templateUrl: './student-modal-add.component.html',
  styleUrls: ['./student-modal-add.component.scss'],
})
export class StudentModalAddComponent implements OnInit, OnChanges {

  @Input('modalType') modalType: string;
  @Input('modalData') modalData: any;
  // @Input('classrooms') classrooms: any;
  @Input('classroom_id') classroom_id: string;

  classrooms: any;

  @Output() statusChanged = new EventEmitter<string>();
  @Output() closeModal = new EventEmitter<void>();
  @Output() studentsUpdated = new EventEmitter<void>();

  addStudentForm: FormGroup;
  editStudentForm: FormGroup;

  loading = false;

  selectedGrade: string = '';
  selectedClassroom: number;
  grades = [];
  editStudent_id: number;

  retypePasswordMsg_add: string;
  retypePasswordMsg_edit: string;
  passwordsMatch: boolean;
  username_pattern = '^[a-zA-Z0-9\u0620-\u064A_\\$@\\-]*$';
  fullname_pattern = '[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ ]+';


  add_name = new FormControl(null, [Validators.required, Validators.minLength(2), Validators.pattern(this.fullname_pattern)]);
  add_username = new FormControl(null, [Validators.required, Validators.minLength(2), Validators.pattern(this.username_pattern)]);
  add_password = new FormControl(null, [Validators.required, Validators.maxLength(24), Validators.minLength(6)]);
  add_repeat_password = new FormControl(null, [Validators.required]);

  edit_username = new FormControl(null, [Validators.required, Validators.minLength(2), Validators.pattern(this.username_pattern)]);
  edit_password = new FormControl(null, [Validators.maxLength(24), Validators.minLength(6)]);
  edit_repeat_password = new FormControl(null);
  edit_classroom = new FormControl(this.selectedClassroom, Validators.required);

  constructor(private _classroomService: ClassRoomService, private fb: FormBuilder) {
    this.addStudentForm = this.fb.group({
      add_name: this.add_name,
      add_username: this.add_username,
      add_password: this.add_password,
      add_repeat_password: this.add_repeat_password,
    });
    this.editStudentForm = this.fb.group({
      edit_username: this.edit_username,
      edit_password: this.edit_password,
      edit_classroom: this.edit_classroom,
      edit_repeat_password: this.edit_repeat_password,
    });
  }

  ngOnInit() {
    this.retypePasswordMsg_add = null;
    this.retypePasswordMsg_edit = null;
    this.getGrades();
    this.fillFields();
    this.getAllClassrooms();
  }

  ngOnChanges() {
    this.edit_classroom.setValue(this.selectedClassroom);
  }

  getAllClassrooms() {
    this._classroomService.getAllClassrooms().subscribe(
      (data: any) => {
        this.classrooms = data.Object;
        this.selectedClassroom = parseInt(this.classroom_id, 10);
        this.edit_classroom.setValue(this.selectedClassroom);
      },
    );
  }

  switchClassroom() {
    // why doesn't it detect the change right away!
    setTimeout(() => { this.edit_classroom.setValue(this.selectedClassroom); }, 100);

  }


  submitAddStudent() {
    if (this.addStudentForm.status === 'VALID' && this.add_password.value === this.add_repeat_password.value) {
      this.loading = true;
      this._classroomService.addStudent(
        this.classroom_id, this.add_name.value, this.add_username.value, this.add_password.value,
      ).subscribe((data: any) => {
        this.loading = false;
        this.studentsUpdated.emit();
        this.closeModal.emit();
      }, (error: any) => {
        console.log(error);
        this.loading = false;
      });
    }
  }

  submitEditStudent() {
    if (this.editStudentForm.status === 'VALID' && this.edit_password.value === this.edit_repeat_password.value) {
      this.loading = true;
      this._classroomService.editStudent(
        this.editStudent_id.toString(), this.edit_username.value, this.edit_password.value, this.edit_classroom.value,
      ).subscribe((data: any) => {
        this.loading = false;
        this.studentsUpdated.emit();
        this.closeModal.emit();
      }, (error: any) => {
        console.log(error);
        this.loading = false;
      });
    }
  }

  getGrades() {
    this._classroomService.getGrades().subscribe(
      (data: any) => {
        this.grades = data.Object;
      },
      (error: any) => {
        console.log(error);
      },
    );
  }

  changeStatus(status: string) {
    this.statusChanged.emit(status);
  }

  fillFields() {
    if (this.modalData) {
      this.editStudent_id = this.modalData.id;
      this.edit_username.setValue(this.modalData.name);
    }
  }

  close() {
    this.closeModal.emit();
  }

  validateAddRetype() {
    if (!this.add_repeat_password.value || !this.add_password.value) {
      this.retypePasswordMsg_add = '';
    } else if (this.add_repeat_password.value !== this.add_password.value) {
      this.retypePasswordMsg_add = 'passwords must match';
      this.passwordsMatch = false;
    } else {
      this.retypePasswordMsg_add = 'passwords match';
      this.passwordsMatch = true;
    }
  }

  validateEditRetype() {
    if (!this.edit_repeat_password.value || !this.edit_password.value) {
      this.retypePasswordMsg_edit = '';
    } else if (this.edit_repeat_password.value !== this.edit_password.value) {
      this.retypePasswordMsg_edit = 'passwords must match';
      this.passwordsMatch = false;
    } else {
      this.retypePasswordMsg_edit = 'passwords match';
      this.passwordsMatch = true;
    }
  }

}
