import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { NbDialogRef, NbPopoverDirective } from '@nebular/theme';

@Component({
  selector: 'ngx-student-modal-cards',
  templateUrl: './student-modal-cards.component.html',
  styleUrls: ['./student-modal-cards.component.scss'],
})
export class StudentModalCardsComponent implements OnInit {

  @ViewChild(NbPopoverDirective, { static: false }) popover: NbPopoverDirective;
  @ViewChild('copyBtn', { static: false }) copyBtn: ElementRef;

  @Input('modalType') modalType: string;
  @Input('shareCode') shareCode: string;

  @Output() statusChanged = new EventEmitter<string>();

  shareCodeMessage: string = 'Click to copy code';

  constructor() {}


  ngOnInit() {

  }

  changeStatus(status: string) {
    this.statusChanged.emit(status);
  }

  shareCodeHint() {
    this.shareCodeMessage = 'Click to copy code';
    this.popover.show();
  }

  copyShareCode() {
    const copyText = this.copyBtn.nativeElement.textContent;
    const $temp = document.createElement('input');
    document.body.append($temp);
    $temp.value = copyText;
    $temp.select();
    $temp.setSelectionRange(0, 99999);
    document.execCommand('copy');
    $temp.remove();
    this.shareCodeMessage = 'Copied to clipboard!';
    this.popover.show();
  }

  hidePopover() {
    this.popover.hide();
  }

}
