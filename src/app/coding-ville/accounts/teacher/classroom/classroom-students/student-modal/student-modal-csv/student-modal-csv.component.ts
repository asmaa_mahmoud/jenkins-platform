import { ClassRoomService } from './../../../class-room.service';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { GlobalService } from '../../../../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-student-modal-csv',
  templateUrl: './student-modal-csv.component.html',
  styleUrls: ['./student-modal-csv.component.scss'],
})
export class StudentModalCSVComponent implements OnInit {

  @ViewChild('mobileInstructions', { static: false }) mobileInstructions: ElementRef;

  @Input('modalType') modalType: string;
  @Input('classroom_id') classroom_id: string;

  @Output() statusChanged = new EventEmitter<string>();
  @Output() studentsUpdated = new EventEmitter<void>();
  @Output() closeModal = new EventEmitter<void>();

  uploadCSVForm: FormGroup;
  fileName: string | ArrayBuffer;

  loading = false;

  uploadedFile = new FormControl(null, Validators.required);

  constructor(private _classroomService: ClassRoomService, private fb: FormBuilder, private _globalService: GlobalService) {
    this.uploadCSVForm = this.fb.group({
      'uploadedFile': this.uploadedFile,
    });
  }

  ngOnInit() {

  }

  uploadFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.uploadCSVForm.get('uploadedFile').setValue(event.target.files[0]);
      this.fileName = event.target.files[0].name;
    }
  }

  submitStudents() {
    if (this.uploadCSVForm.status === 'VALID') {
      this.loading = true;
      this._classroomService.uploadCSV(this.uploadedFile.value, this.classroom_id).subscribe((data: any) => {
        if (data.Object.totalSuccess === data.Object.total) {
          this.studentsUpdated.emit();
          this.loading = false;
        }
        this.close();
      }, (error: any) => {
        this.close();
        this.loading = false;
        console.log(error);
      });
    }
  }

  changeStatus(status: string) {
    this.statusChanged.emit(status);
  }

  toggleEllipsis() {
    this.mobileInstructions.nativeElement.classList.toggle('ellipsis');
  }

  close() {
    this.closeModal.emit();
  }


}
