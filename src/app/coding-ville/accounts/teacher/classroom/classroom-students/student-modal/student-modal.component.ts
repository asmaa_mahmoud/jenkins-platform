import { ClassRoomService } from './../../class-room.service';
import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-student-modal',
  templateUrl: './student-modal.component.html',
  styleUrls: ['./student-modal.component.scss'],
})
export class StudentModalComponent implements OnInit, OnChanges {

  @Input() modalType: string;
  @Input() modalData: any;
  @Input() shareCode: string;
  @Input() classroom_id: string;

  @Output() updateStudents = new EventEmitter<void>();

  modalStatus: string = 'cards';

  allClassrooms: any;

  modalTitle: string;

  constructor(protected ref: NbDialogRef<StudentModalComponent>, private _classroomService: ClassRoomService) { }

  ngOnInit() {
    this.modalStatus = this.modalType === 'edit' ? 'edit' : 'cards';
    this.changeModalTitle();
  }

  ngOnChanges() {
    this.changeModalTitle();
  }


  changeStatus(status: string) {
    this.modalStatus = status;
    this.changeModalTitle();
  }

  studentsUpdated() {
    this.updateStudents.emit();
  }

  changeModalTitle() {
    this.modalTitle = this.modalStatus === 'cards' ? 'ADD STUDENT' :
      this.modalStatus === 'csv' ? 'Upload a CSV File' :
        this.modalStatus === 'add' ? 'ADD STUDENT' :
          this.modalStatus === 'edit' ? 'EDIT STUDENT' :
            'ADD STUDENT';
  }

  close() {
    this.ref.close();
  }

}
