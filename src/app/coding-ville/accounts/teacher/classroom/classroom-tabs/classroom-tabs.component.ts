import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Breadcrumb } from '../../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-classroom-tabs',
  templateUrl: './classroom-tabs.component.html',
  styleUrls: ['./classroom-tabs.component.scss'],
})
export class ClassroomTabsComponent implements OnInit {

  classroomName: string = '';

  private breadcrumbs: Breadcrumb[];

  public type: string;
  constructor(private _globalService: GlobalService) { }

  ngOnInit() {
    this._globalService.getBreadcrumbs(this.breadcrumbs);
  }

  onFetchClassroomName(name: string) {
    this.breadcrumbs = [
      {
        name: 'CLASSROOMS',
        link: '/teacher/classroom',
      },
      {
        name: name,
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(this.breadcrumbs);
  }

}
