import { DashboardService } from './../dashboard/dashboard.service';
import { MaxModalComponent } from './../../shared/max-modal/max-modal.component';
import { env } from './../../../../@core/env/env';
import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { Router } from '@angular/router';

import { CreateClassroomComponent } from './create-classroom/create-classroom.component';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../@core/utils/global.service';
import { DeleteModalComponent } from '../../shared/delete-modal/delete-modal.component';
import { ClassRoomService } from './class-room.service';
import { Classroom } from '../../../../@core/data/classroom';

@Component({
  selector: 'ngx-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.scss'],
})
export class ClassroomComponent implements OnInit {
  noOfCardsPerPage: any[] = [6, 9, 12];
  classrooms: Classroom[];
  cardPerPage = 6;
  start: number = 0;
  currentPage = 1;
  searchValue = '';
  classroomsCount;
  dataLoaded: boolean = false;
  createDialogRef: NbDialogRef<CreateClassroomComponent>;
  deleteDialogRef: NbDialogRef<DeleteModalComponent>;
  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'CLASSROOMS',
      link: '',
    },
  ];

  // ------------ SECTION ORG ------------
  isOrg = env.isOrg;
  maxClassrooms: string;
  maxDialogRef: NbDialogRef<MaxModalComponent>;
  // ----------------

  constructor(private dialogService: NbDialogService,
    private _globalService: GlobalService,
    private _classroomService: ClassRoomService,
    private router: Router,
    private _dashboardService: DashboardService,
  ) { }

  ngOnInit() {
    this._globalService.getBreadcrumbs(this.breadcrumbs);
    this.getClassrooms();
    this.countClassroom();

    if (this.isOrg) {
      this.getMaxClassrooms();
    }
  }

  getMaxClassrooms() {
    this._dashboardService.getMaxStudentsAndClasses().subscribe(
      res => {
        const classRooms = res.Object.find(x => x.code === 'classrooms');
        this.maxClassrooms = classRooms.value;
        console.log('max class', this.maxClassrooms);

      });
  }

  countClassroom() {
    this._classroomService.countClassrooms().subscribe(res => {
      this.classroomsCount = res.Object.count;
    });
  }

  getClassrooms() {
    this.dataLoaded = false;
    this._classroomService.getClassrooms(
      this.start.toString(), this.cardPerPage.toString(), this.searchValue).subscribe(
        (data: any) => {
          this.classrooms = data.Object;
          this.dataLoaded = true;
        });
  }

  search(event) {
    this.searchValue = event;
    this.getClassrooms();
  }


  openJourney(classroomId) {
    this.router.navigate(['teacher/classroom', classroomId]);
  }

  editClassroom(classroom: Classroom) {
    this.createDialogRef = this.dialogService.open(CreateClassroomComponent, {
      context: {
        modalType: 'edit',
        modalData: classroom,
      },
    });
    const sub = this.createDialogRef.componentRef.instance.classroomsUpdated.subscribe(() => {
      this.getClassrooms();
    });
    this.createDialogRef.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }

  deleteClassroom(classroom_id: number) {
    this.deleteDialogRef = this.dialogService.open(DeleteModalComponent, {
      context: {
        type: 'classroom',
        title: 'classroom',
        content: 'classroom',
        deleted_id: classroom_id,
      },
    });
    const sub = this.deleteDialogRef.componentRef.instance.dataUpdated.subscribe(() => {
      if (this.classrooms.length === 1 && this.start > 0) {
        this.start -= 6;
      }
      this.getClassrooms();
      this.countClassroom();
    });
    this.deleteDialogRef.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }

  changePagination(num) {
    this.currentPage = num;
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.getClassrooms();
  }



  changePageLimit() {
    this.start = 0;
    this.getClassrooms();
  }

  // ----------------- SECTION  Adding a classroom -----------------

  addClassroom() {
    if (this.classroomsCount === undefined || (this.isOrg && !this.maxClassrooms)) {
      return;
    }


    if (this.isOrg && this.classroomsCount.toString() === this.maxClassrooms) {
      this.openUpgradeModal();
    } else {
      this.openCreateClassroomModal();
    }
  }

  openCreateClassroomModal() {
    this.createDialogRef = this.dialogService.open(CreateClassroomComponent, {
      context: {
        modalType: 'create',
      },
    });
    const sub = this.createDialogRef.componentRef.instance.classroomsUpdated.subscribe(() => {
      this.getClassrooms();
      this.countClassroom();
    });
    this.createDialogRef.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }

  openUpgradeModal() {
    this.maxDialogRef = this.dialogService.open(MaxModalComponent, {
      context: {
        type: 'classroom',
        number: this.maxClassrooms,
      },
    });
  }
  // ----------------


}
