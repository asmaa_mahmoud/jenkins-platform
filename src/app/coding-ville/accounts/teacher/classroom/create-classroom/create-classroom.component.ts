import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ClassRoomService } from '../class-room.service';

@Component({
  selector: 'ngx-create-classroom',
  templateUrl: './create-classroom.component.html',
  styleUrls: ['./create-classroom.component.scss'],
})
export class CreateClassroomComponent implements OnInit {
  @Output() classroomsUpdated = new EventEmitter<void>();
  @Input() modalType: string;
  @Input() modalData: any;
  grades: any[];
  selectedGrade: string;
  createClassroomForm: FormGroup;
  classroomImageSrc: string | ArrayBuffer;
  name: string;

  forced = 1;

  loading = false;

  classroomName = new FormControl(null, Validators.required);
  classroomGrade = new FormControl();
  classroomImage = new FormControl();


  constructor(protected ref: NbDialogRef<CreateClassroomComponent>,
    private _classroomService: ClassRoomService,
    private fb: FormBuilder) {
    this.createClassroomForm = this.fb.group({
      'classroomName': this.classroomName,
      'classroomGrade': this.classroomGrade,
      'classroomImage': this.classroomImage,
    });
  }


  ngOnInit() {
    this.grades = [];
    this.getGrades();
    if (this.modalData) {
      console.log(this.modalData);
      this.setEditValues();
    }
  }

  getGrades() {
    this._classroomService.getGrades().subscribe(
      (data: any) => {
        this.grades = data.Object;
      },
      (error: any) => {
        console.log(error);
      },
    );
  }

  onChangeGrade() {
    this.classroomGrade.setValue(this.selectedGrade);
  }

  setEditValues() {
    this.classroomName.setValue(this.modalData.name);
    console.log(this.modalData);
    this.classroomGrade.setValue(this.modalData.gradeName);
    this.selectedGrade = this.modalData.gradeName;
    this.classroomImage.setValue(this.modalData.image);
    this.classroomImageSrc = this.modalData.image;
  }

  clearValues() {
    this.classroomName.setValue(null);
    this.classroomGrade.setValue(null);
    this.selectedGrade = null;
    this.classroomImage.setValue(null);
    this.classroomImageSrc = null;
  }

  uploadFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = () => {
        this.createClassroomForm.get('classroomImage').setValue(event.target.files[0]);
        this.classroomImageSrc = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  close() {
    this.ref.close();
  }

  submitClassroom() {
    if (this.modalType === 'edit')
      this.editClassroom();
    else
      this.createClassroom();
  }

  createClassroom() {
    if (this.createClassroomForm.invalid) {
      return;
    }
    this.loading = true;
    const formData = new FormData();
    formData.append('name', this.createClassroomForm.get('classroomName').value);
    formData.append('grade', this.createClassroomForm.get('classroomGrade').value);
    formData.append('image', this.createClassroomForm.get('classroomImage').value);
    this._classroomService.createClassroom(formData).subscribe(
      (data: any) => {
        this.classroomsUpdated.emit();
        this.clearValues();
        this.ref.close();
        this.loading = false;
      },
      (error: any) => {
        console.log(error);
        this.clearValues();
        this.ref.close();
        this.loading = false;
      },
    );
  }

  editClassroom() {
    if (this.createClassroomForm.invalid) {
      return;
    }
    this.loading = true;
    const formData = new FormData();
    formData.append('classroom_id', this.modalData.id);
    formData.append('name', this.createClassroomForm.get('classroomName').value);
    formData.append('grade', this.createClassroomForm.get('classroomGrade').value);
    formData.append('image', this.createClassroomForm.get('classroomImage').value);
    formData.append('force', String(this.forced));
    this._classroomService.editClassroom(formData).subscribe(
      (data: any) => {
        this.classroomsUpdated.emit();
        this.clearValues();
        this.ref.close();
        this.loading = false;
      },
      (error: any) => {
        console.log(error);
        this.clearValues();
        this.ref.close();
        this.loading = false;
      },
    );
  }

}
