import { Router, ActivatedRoute, Params } from '@angular/router';
import { ClassRoomDataService } from './../classroom-data.service';
import { ClassRoomService } from './../class-room.service';
import { GlobalService } from './../../../../../@core/utils/global.service';
import { Breadcrumb } from './../../../../../@core/data/breadcrumb';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { NbDialogService } from '@nebular/theme';
import { SurveyModalComponent } from '../../../shared/survey-modal/survey-modal.component';

@Component({
  selector: 'ngx-student-progress',
  templateUrl: './student-progress.component.html',
  styleUrls: ['./student-progress.component.scss'],
})
export class StudentProgressComponent implements OnInit {
  classroom_id: number;
  journey_id: number;
  course_id: number;
  journey_type: string;
  studentsCount: number;
  classroom: any;
  journeyName: string;
  private breadcrumbs: Breadcrumb[];
  missions: any;
  students: any;
  tableLengths = [10, 25, 50, 100];
  tableLength = '10';
  start: number = 0;
  searchValue: string = '';
  selectedJourney: any;
  adventures: any;
  currentAdventure: string = '1';
  currentURL: string;
  dataLoaded: boolean = false;

  constructor(
    private _globalService: GlobalService,
    private _classroomService: ClassRoomService,
    private _classroomDataService: ClassRoomDataService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private _dialogService: NbDialogService,
  ) {
    this.router.events.subscribe(x => {
      this.currentURL = this.location.path();
    });
  }


  ngOnInit() {
    this.getIDs();
    this.getStudentsCount();
    this.implementAllInOrder();
  }

  goToMission(mission, student_id) {
    this.setBackInfoForWorkspace();
    const mission_type = mission.type === 'mission_editor' ? 'webeditor' : 'blocks';
    if (this.journey_type === 'journey') {
      if (mission.type === 'project') {
        if (mission.type_id === 1 || mission.type_id === 2) {
          this.router.navigate([`project-game/journey/${this.currentAdventure}/${mission.id}/${this.journey_id}/${this.course_id}/${student_id}`]);
        } else if (mission.type_id === 3) {
          window.location.href =
            `workspaces/journey/${mission_type}/app/${this.journey_id}/${this.currentAdventure}/${mission.id}/${this.course_id}/${student_id}`;
        }
      } else {
        window.location.href =
          `workspaces/journey/${mission_type}/${this.journey_id}/${this.currentAdventure}/${mission.id}/${this.course_id}/${student_id}`;
      }
    } else if (this.journey_type === 'prejourney') {
      if (mission.type === 'project') {
        if (mission.type_id === 1 || mission.type_id === 2) {
          this.router.navigate([`project-game/prejourney/${mission.id}/${this.journey_id}/${this.course_id}/${student_id}`]);
        } else if (mission.type_id === 3) {
          window.location.href =
            `workspaces/prejourney/${mission_type}/app/${this.journey_id}/${mission.id}/${this.course_id}/${student_id}`;
        }
      } else {
        window.location.href =
          `workspaces/prejourney/${mission_type}/${this.journey_id}/${mission.id}/${this.course_id}/${student_id}`;
      }
    }
  }

  getStudentsCount() {
    this._classroomService.getStudentsCount(this.classroom_id.toString()).subscribe((data: any) => {
      this.studentsCount = data.Object;
    }, (error: any) => {
      console.log(error);
    });
  }

  implementAllInOrder() {
    this._classroomService.getClassroomData(this.classroom_id).subscribe(data => {
      this.classroom = data.Object;
      this.getBreadcrumbs();
    });
    if (this.journey_type === 'journey') {
      this.getAdventures().subscribe(
        (res: any) => {
          this.adventures = res.Object.Adventure;
          this.currentAdventure = this.adventures[0].id.toString();
          this.getData();
        }, (error: any) => {
          console.log(error);
        },
      );
    } else if (this.journey_type === 'prejourney') {
      this.getData();
    }
  }

  getAdventures() {
    return this._classroomService.getJourneyAdventures(this.journey_id.toString());
  }

  getBreadcrumbs() {
    this.breadcrumbs = [
      {
        name: 'CLASSROOM',
        link: 'classroom',
      },
      {
        name: this.classroom.Name,
        link: `classroom/${this.classroom.id}`,
      },
      {
        name: 'PROGRESS',
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(this.breadcrumbs);
  }

  getData() {
    if (this.journey_type === 'journey') {
      this._classroomService.getStudentJourneyProgress(
        this.currentAdventure,
        this.course_id,
        this.start,
        this.tableLength,
        this.searchValue).subscribe((data: any) => {
          // if (data.Object.open_survey) {
          //   this.showSurvey();
          // }
          this.missions = data.Object.missions;
          this.students = data.Object.students;
          this.dataLoaded = true;
        });
    } else if (this.journey_type === 'prejourney') {
      this._classroomService.getStudentPreJourneyProgress(
        this.journey_id,
        this.course_id,
        this.start,
        this.tableLength,
        this.searchValue).subscribe((data: any) => {
          this.dataLoaded = true;
          this.missions = data.Object.missions;
          this.students = data.Object.students;
        });

    }
  }

  showSurvey() {
    this._dialogService.open(SurveyModalComponent, {
      context: {
        scriptKey: 'teacher_progess',
      },
    });
  }

  onChangeAdventure() {
    this.getData();
  }

  onChangeLength() {
    this.start = 0;
    this.getData();
  }

  onSearch(value: string) {
    this.searchValue = value;
    this.start = 0;
    this.getData();
  }

  changePagination(num: number) {
    this.start = num * +this.tableLength - +this.tableLength;
    this.getData();
  }

  getIDs() {
    this.route.params.subscribe((params: Params) => {
      this.classroom_id = params['classroom_id'];
      this.journey_id = params['journey_id'];
      this.course_id = params['course_id'];
      this.journey_type = params['journey_type'];
    });
  }

  setBackInfoForWorkspace() {
    localStorage.setItem('backURL', this.currentURL);
  }

}
