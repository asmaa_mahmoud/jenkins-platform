import { AfterViewInit, Component, OnDestroy, Input, OnChanges } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'ngx-echarts-bar',
  template: `
    <div echarts [options]="options" [initOpts]="initOpts" class="echart"></div>
  `,
  styleUrls: ['./echarts.component.scss'],
})
export class EchartsBarComponent implements AfterViewInit, OnDestroy, OnChanges {

  @Input('classroomScore') classroomScore: any;
  options: any = {};
  initOpts: any = {};
  themeSubscription: any;
  xAxisData: any = [];
  yAxisData: any = ['0', '20%', '40%', '60%', '80%', '100%'];
  seriesData: any = [];

  constructor(private theme: NbThemeService) {
  }

  ngAfterViewInit() {
    this.updateChart();
  }

  ngOnChanges() {
    this.updateChart();
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

  updateChart() {
    this.xAxisData = [];
    this.seriesData = [];
    if (this.classroomScore) {
      for (const score of this.classroomScore) {
        this.xAxisData.push(`${score.from} - ${score.to}`);
        this.seriesData.push(parseInt(score.noOfStudents, 10));
      }
    }
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;

      echarts.bg = '#fcfcfc';
      echarts.textColor = '#707070';
      echarts.axisLineColor = '#707070';
      echarts.barColor = '#8ae25a';

      this.options = {
        backgroundColor: echarts.bg,
        color: [echarts.barColor],
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow',
          },
        },
        grid: {
          left: '50px',
          right: '8%',
          bottom: '10%',
          containLabel: true,
        },
        xAxis: [
          {
            name: 'Score Range',
            nameLocation: 'center',
            nameGap: 25,
            type: 'category',
            // data: ['1', '2', '3', '4', '5', '6', '7'],
            data: this.xAxisData,
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            name: 'Number of students',
            nameLocation: 'center',
            nameGap: 25,
            nameRotate: 90,
            minInterval: 1,
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            splitLine: {
              lineStyle: {
                color: echarts.splitLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        series: [
          {
            name: 'Number of students',
            type: 'bar',
            barWidth: '60%',
            // data: [3, 15, 20, 38, 40, 35, 28],
            data: this.seriesData,
            itemStyle: {
              barBorderRadius: [50, 50, 0, 0],
            },
          },
        ],
      };
    });
  }

}
