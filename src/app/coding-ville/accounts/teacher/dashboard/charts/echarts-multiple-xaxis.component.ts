import { Component, AfterViewInit, OnDestroy, Input, OnChanges } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'ngx-echarts-multiple-xaxis',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
  styleUrls: ['./echarts.component.scss'],
})
export class EchartsMultipleXaxisComponent implements AfterViewInit, OnDestroy, OnChanges {

  @Input('studentActivity') studentActivity: any;
  @Input('from_time') from_time: any;
  @Input('to_time') to_time: any;
  options: any = {};
  themeSubscription: any;
  xAxisData: any;
  studentsData: any;
  missionsData: any;

  constructor(private theme: NbThemeService) {
  }

  ngAfterViewInit() {
    // console.log(this.studentActivity);
    this.updateChart();
  }

  ngOnChanges() {
    // console.log(this.studentActivity);
    this.updateChart();
  }

  updateChart() {

    this.xAxisData = [];
    this.studentsData = [];
    this.missionsData = [];
    if (this.studentActivity) {
      // console.log(this.studentActivity);
      this.xAxisData = [];
      this.studentsData = [];
      this.missionsData = [];
      for (const score of this.studentActivity) {
        this.xAxisData.push(score.date);
        this.studentsData.push(score.no_students_open_mission_page);
        this.missionsData.push(score.no_missions_solved);
      }
    }

    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;

      echarts.bg = '#fcfcfc';
      echarts.textColor = '#707070';
      echarts.axisLineColor = '#707070';

      this.options = {
        backgroundColor: echarts.bg,
        // color: [colors.success, colors.info],
        color: ['#8ae25a', '#1b7fdd'],
        tooltip: {
          trigger: 'none',
          axisPointer: {
            type: 'cross',
          },
        },
        // initOpts: {
        //   height: '350px',
        // },
        legend: {
          data: ['Number of students', 'Completed missions'],
          textStyle: {
            color: echarts.textColor,
          },
        },
        grid: {
          top: 80,
          bottom: 50,
        },
        xAxis: [
          {
            type: 'category',
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              onZero: false,
              lineStyle: {
                color: colors.info,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
            axisPointer: {
              label: {
                formatter: params => {
                  return (
                    'Completed missions  ' + params.value + (params.seriesData.length ? '：' + params.seriesData[0].data : '')
                  );
                },
              },
            },
            data: this.xAxisData,
          },
          {
            type: 'category',
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              onZero: false,
              lineStyle: {
                color: colors.success,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
            axisPointer: {
              label: {
                formatter: params => {
                  return (
                    'Students  ' + params.value + (params.seriesData.length ? '：' + params.seriesData[0].data : '')
                  );
                },
              },
            },
            data: this.xAxisData,
          },
        ],
        yAxis: [
          {
            type: 'value',
            minInterval: 1,
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            splitLine: {
              lineStyle: {
                color: echarts.splitLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        series: [
          {
            name: 'Number of students',
            type: 'line',
            xAxisIndex: 1,
            smooth: true,
            data: this.studentsData,
          },
          {
            name: 'Completed missions',
            type: 'line',
            smooth: true,
            data: this.missionsData,
          },
        ],
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
