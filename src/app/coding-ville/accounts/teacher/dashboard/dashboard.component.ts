import { env } from './../../../../@core/env/env';
import { AuthenticationService } from './../../../../@core/utils/authentication.service';
import { GlobalService } from './../../../../@core/utils/global.service';
import { Component, OnInit } from '@angular/core';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { DashboardService } from './dashboard.service';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
import { AgreementModalComponent } from '../../shared/agreement-modal/agreement-modal.component';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  generalData: any;
  topTenStudents: any[];
  classrooms: any;
  recentActivities: any;
  classroomScore: any = null;
  studentActivity: any;
  foundStudentActivity = false;
  selectedClassrooms = {
    studentActivity: null,
    classroomScore: null,
    topTenStudents: null,
    recentActivities: null,
  };
  from_time: string;
  to_time: string;
  isActivated: boolean;

  userPlan;

  isOrg = env.isOrg;
  termsAgreement: boolean;

  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'DASHBOARD',
      link: '',
    },
  ];

  constructor(
    private globalService: GlobalService,
    private _dashboardService: DashboardService,
    private authService: AuthenticationService,

  ) {
    this.authService.currentUser.subscribe(x => this.termsAgreement = x.terms_agreement);
    this.isActivated = this.authService.isActivated();
  }

  ngOnInit() {
    this.setFromTo();
    this.globalService.getBreadcrumbs(this.breadcrumbs);
    if (this.isActivated) {
      this.getAllClassrooms();
      this.getGeneralData();

      if (this.isOrg) {
        this.getMaxStudentsAndClasses();
      }
    }
  }


  getMaxStudentsAndClasses() {
    this._dashboardService.getMaxStudentsAndClasses().subscribe(
      res => {
        const plan = {
          students: 0,
          classrooms: 0,
        };
        res.Object.forEach(x => {
          if (x.code && x.code === 'students') {
            plan.students = x.value;
          } else if (x.code && x.code === 'classrooms') {
            plan.classrooms = x.value;
          }
        });
        this.userPlan = plan;
      });
  }


  setFromTo() {
    const period: number = 7;
    const currentDate = new Date();
    const oldDate = new Date(currentDate.getTime() - period * 24 * 60 * 60 * 1000);
    this.from_time = `${oldDate.getFullYear()}-${oldDate.getMonth() + 1}-${oldDate.getDate()}`;
    this.to_time = `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`;
  }

  changeClassroom(data: string) {
    data === 'studentActivity' ? this.getStudentActivity() :
      data === 'topTenStudents' ? this.getTopStudents() :
        data === 'recentActivity' ? this.getRecentActivities() :
          data === 'classroomScore' ? this.getClassroomScore() :
            null;
  }

  getAllClassrooms() {
    this._dashboardService.getAllClassrooms().subscribe(res => {
      this.classrooms = res.Object;
      if (this.classrooms && this.classrooms.length > 0) {
        this.selectedClassrooms.studentActivity = this.classrooms[0].id;
        this.selectedClassrooms.classroomScore = this.classrooms[0].id;
        this.selectedClassrooms.topTenStudents = this.classrooms[0].id;
        this.selectedClassrooms.recentActivities = this.classrooms[0].id;
      }
      this.getTopStudents();
      this.getRecentActivities();
      this.getClassroomScore();
      this.getStudentActivity();
    });
  }

  getGeneralData() {
    this._dashboardService.getGeneralData().subscribe(res => {
      this.generalData = res.Object;
    });
  }

  getTopStudents() {
    this._dashboardService.getTopTenStudent(this.selectedClassrooms.topTenStudents).subscribe(res => {
      this.topTenStudents = res.Object;
    });

  }

  getRecentActivities() {
    this._dashboardService.getRecentActivities(this.selectedClassrooms.recentActivities).subscribe(res => {
      this.recentActivities = res.Object;
    });
  }

  getClassroomScore() {
    this._dashboardService.getClassroomScore(this.selectedClassrooms.classroomScore).subscribe(res => {
      this.classroomScore = res.Object;
    }, err => {
      console.log(err);
      this.classroomScore = null;
    });
  }

  getStudentActivity() {
    this._dashboardService.getStudentActivity(
      this.selectedClassrooms.studentActivity.toString(), this.from_time, this.to_time,
    ).subscribe(res => {
      this.studentActivity = res.Object;
      this.foundStudentActivity = this.studentActivity.filter(el => el.no_students_open_mission_page > 0).length > 0;
    });
  }

}
