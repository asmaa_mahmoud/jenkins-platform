import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { GlobalService } from '../../../../@core/utils/global.service';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {

  constructor(private _globalService: GlobalService) { }

  getGeneralData() {
    const url = `school/teacher/dashboard/general/journeys`;
    return this._globalService.get(url);
  }

  getStudentActivity(classroom_id: string, from_time: string, to_time: string) {
    const url: string = `school/teacher/dashboard/missions`;
    const params = new HttpParams().set('classroom_id', classroom_id).append('from_time', from_time).append('to_time', to_time);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getTopTenStudent(classroomId: number) {
    const url: string = `school/teacher/dashboard/top_10`;
    const params = new HttpParams().set('classroom_id', classroomId.toString());
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getClassroomScore(classroomId: number) {
    const url: string = `school/teacher/dashboard/classroomscorerange`;
    const params = new HttpParams().set('classroom_id', classroomId.toString());
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getRecentActivities(classroomId) {
    const url: string = `school/teacher/dashboard/activity`;
    const params = new HttpParams().set('classroom_id', classroomId);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllClassrooms() {
    const url = `school/teacher/classrooms`;
    return this._globalService.get(url);
  }
  getAllClassroomsWithFlagJourney(journey_id) {
    const url = `school/teacher/classrooms/${journey_id}`;
    return this._globalService.get(url);
  }

  getAllClassroomsWithFlagPreJourney(prejourney_id) {
    const url = `school/teacher/classrooms/pre/${prejourney_id}`;
    return this._globalService.get(url);
  }
  getMaxStudentsAndClasses() {
    const url = `payment/subscription/features`;
    return this._globalService.get(url);
  }
}
