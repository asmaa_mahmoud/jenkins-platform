import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { DashboardService } from '../../dashboard/dashboard.service';
import { JourneysService } from '../journeys.service';

@Component({
  selector: 'ngx-assign-modal',
  templateUrl: './assign-modal.component.html',
  styleUrls: ['./assign-modal.component.scss'],
})
export class AssignModalComponent implements OnInit {
  @Input() journeyId: string;
  @Input() type: string;

  classrooms: any[] = [];
  selectedClassrooms = [];
  nonSelectedClassrooms = [];

  constructor(protected ref: NbDialogRef<AssignModalComponent>,
    private _dashboardService: DashboardService,
    private _journeysService: JourneysService) {
  }

  ngOnInit() {
    if (this.type === 'preJourney') {
      this.getAllClassroomsPreJourney();
    } else if (this.type === 'journey') {
      this.getAllClassroomsJourney();
    }
  }

  getAllClassroomsJourney() {
    this._dashboardService.getAllClassroomsWithFlagJourney(this.journeyId).subscribe((data: any) => {
      this.classrooms = data.Object;
    });
  }
  getAllClassroomsPreJourney() {
    this._dashboardService.getAllClassroomsWithFlagPreJourney(this.journeyId).subscribe((data: any) => {
      this.classrooms = data.Object;
    });
  }

  toggle(checked, index) {
    this.classrooms[index].assigned = checked ? 1 : 0;
  }

  close() {
    this.ref.close();
  }

  submit() {
    if (this.type === 'preJourney') {
      this.assignPreJourneyToClassrooms();
    } else if (this.type === 'journey') {
      this.assignJourneyToClassrooms();
    }

  }

  assignJourneyToClassrooms() {
    for (const classroom of this.classrooms) {
      if (classroom.assigned === 1) {
        this.selectedClassrooms.push(classroom.id);
      } else {
        this.nonSelectedClassrooms.push(classroom.id);
      }
    }
    this._journeysService.assignJourneyToClassrooms(this.journeyId, this.selectedClassrooms, this.nonSelectedClassrooms)
      .subscribe((data: any) => {
        this.close();
      });

    this.selectedClassrooms = [];
    this.nonSelectedClassrooms = [];
  }

  assignPreJourneyToClassrooms() {
    for (const classroom of this.classrooms) {
      if (classroom.assigned === 1) {
        this.selectedClassrooms.push(classroom.id);
      } else {
        this.nonSelectedClassrooms.push(classroom.id);
      }
    }
    this._journeysService.assignPreJourneyToClassrooms(this.journeyId, this.selectedClassrooms, this.nonSelectedClassrooms)
      .subscribe((data: any) => {
        this.close();
      });
    this.selectedClassrooms = [];
    this.nonSelectedClassrooms = [];
  }

}
