import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { Journey } from '../../../../../@core/data/journey';
import { Router } from '@angular/router';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { AssignModalComponent } from '../assign-modal/assign-modal.component';
import { JourneysService } from '../journeys.service';
import { Breadcrumb } from '../../../../../@core/data/breadcrumb';

@Component({
  selector: 'ngx-journey-tab',
  templateUrl: './journey-tab.component.html',
  styleUrls: ['./journey-tab.component.scss'],
})
export class JourneyTabComponent implements OnInit {

  journeys: Journey[];
  journeysCount: number;
  journeysLoaded: boolean = false;
  assignDialogRef: NbDialogRef<AssignModalComponent>;


  cardPerPage = 6;
  currentPage = 1;
  start: number = 0;
  noOfCardsPerPage = [6, 9, 12];
  searchValue: string = '';


  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Journey',
      link: '',
    },
  ];


  constructor(
    private _journeyService: JourneysService,
    private _globalService: GlobalService,
    private dialogService: NbDialogService,
    private router: Router,
  ) {
    this._globalService.breadcrumbsService.getBreadcrumbs(this.breadcrumbs);
  }

  ngOnInit() {
    this.countJourneys();
  }

  countJourneys() {
    this._journeyService.countJourneys(this.searchValue).subscribe((data: any) => {
      this.journeysCount = data.Object.count;
      this.getJounreys();
    }, (error: string) => {
      console.log(error);
    },
    );
  }

  getJounreys() {
    this._journeyService.getAllJourneys(
      this.start.toString(), this.cardPerPage.toString(), this.searchValue)
      .subscribe((data: any) => {
        this.journeys = data.Object;
        this.journeysLoaded = true;
      }, (error: string) => {
        console.log(error);
      },
      );
  }

  changePagination(num: number) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.countJourneys();
  }

  changePageLimit() {
    this.start = 0;
    this.getJounreys();
  }

  search(value: string) {
    this.searchValue = value;
    this.start = 0;
    this.countJourneys();
  }

  openJourneyMissions(journey) {
    this.router.navigate([`teacher/journeys/${journey.id}/adventure`]);
  }

  openAssignJourney(event) {
    this.assignDialogRef = this.dialogService.open(AssignModalComponent, {
      context: {
        journeyId: event,
        type: 'journey',
      },
    });
  }

}
