import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalService } from '../../../../@core/utils/global.service';
import { HttpParams, HttpUrlEncodingCodec, HttpParameterCodec } from '@angular/common/http';
import { CustomEncoder } from '../../../../@core/utils/custom-encoder';

@Injectable({
  providedIn: 'root',
})
export class JourneysService {

  constructor(private _globalService: GlobalService) { }

  countJourneys(search) {
    const url = 'coding-ville/journeys/count';
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllJourneys(start, limit, search) {
    const url: string = `coding-ville/journeys/all`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('start', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  assignJourneyToClassroom(journeyId, classroomIds) {
    const url: string = `coding-ville/teacher/classroom/journeys/assign`;
    const params = new HttpParams().set('classroom_id', classroomIds).append('journey_id', journeyId);
    return this._globalService.get(url, params).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'assigned-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  assignJourneyToClassrooms(journeyId, classroomIds, unassignedClassroomIds) {
    const url: string = `coding-ville/teacher/journeys/assign`;
    return this._globalService.post(url,
      {
        journey_id: journeyId,
        classroom_ids: classroomIds,
        unassigned_classroom_ids: unassignedClassroomIds,
      }).map(
        (data: any) => {
          this._globalService.showTranslatedToast('success', null, 'assigned-successfully');
          return data;
        },
      ).catch(
        (error: any) => {
          console.log(error.message);
          this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
          return Observable.throw('Something went wrong!');
        },
      );
  }

  assignPreJourneyToClassrooms(journeyId, classroomIds, unassignedClassroomIds) {
    const url: string = `coding-ville/teacher/activities/assign`;
    return this._globalService.post(url,
      {
        activity_id: journeyId,
        classroom_ids: classroomIds,
        unassigned_classroom_ids: unassignedClassroomIds,
      }).map(
        (data: any) => {
          this._globalService.showTranslatedToast('success', null, 'assigned-successfully');
          return data;
        },
      ).catch(
        (error: any) => {
          console.log(error.message);
          this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
          return Observable.throw('Something went wrong!');
        },
      );
  }

  countPreJourneys(search) {
    const url = 'coding-ville/activities/count';
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('search', search);
    return this._globalService.get(url, params);
  }

  getAllPreJourneys(start, limit, search) {
    const url: string = `coding-ville/activities/all`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('start', start).append('limit', limit).set('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  assignPreJourneyToClassroom(journeyId, classroomIds) {
    const url: string = `coding-ville/teacher/activities/assign`;
    const params = new HttpParams().set('classroom_id', classroomIds).append('activity_id', journeyId);
    return this._globalService.get(url, params).map(
      (data: any) => {
        this._globalService.showTranslatedToast('success', null, 'assigned-successfully');
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

}
