import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { Journey } from '../../../../../@core/data/journey';
import { Router } from '@angular/router';
import { JourneysService } from '../journeys.service';
import { Breadcrumb } from '../../../../../@core/data/breadcrumb';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { AssignModalComponent } from '../assign-modal/assign-modal.component';

@Component({
  selector: 'ngx-pre-journey-tab',
  templateUrl: './pre-journey-tab.component.html',
  styleUrls: ['./pre-journey-tab.component.scss'],
})
export class PreJourneyTabComponent implements OnInit {

  preJourneys: Journey[];
  preJourneyCount;
  preJourneysLoaded: boolean = false;
  searchValue: string = '';
  assignDialogRef: NbDialogRef<AssignModalComponent>;
  cardPerPage = 6;
  start: number = 0;
  currentPage = 1;
  noOfCardsPerPage = [6, 9, 12];
  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Journey',
      link: '',
    },
  ];

  constructor(
    private _globalService: GlobalService,
    private _journeyService: JourneysService,
    private dialogService: NbDialogService,
    private router: Router,
  ) {
    this._globalService.breadcrumbsService.getBreadcrumbs(this.breadcrumbs);
  }

  ngOnInit() {
    this.countPreJourneys();
  }


  countPreJourneys() {
    this._journeyService.countPreJourneys(this.searchValue).subscribe((data: any) => {
      this.preJourneyCount = data.Object.count;
      this.getPreJounreys();
    });
  }

  getPreJounreys() {
    this._journeyService.getAllPreJourneys(
      this.start.toString(), this.cardPerPage.toString(), this.searchValue)
      .subscribe((data: any) => {
        this.preJourneys = data.Object;
        this.preJourneysLoaded = true;
      }, (error: string) => {
        console.log(error);
      },
      );
  }

  openPreJourneyMissions(journey) {
    this.router.navigate([`teacher/journeys/${journey.id}/missions`]);
  }

  changePagination(num) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.countPreJourneys();
  }

  changePageLimit() {
    this.start = 0;
    this.countPreJourneys();
  }

  search(event) {
    this.searchValue = event;
    this.start = 0;
    this.countPreJourneys();
  }

  openAssignPreJourney(event) {
    this.assignDialogRef = this.dialogService.open(AssignModalComponent, {
      context: {
        journeyId: event,
        type: 'preJourney',
      },
    });
  }

}
