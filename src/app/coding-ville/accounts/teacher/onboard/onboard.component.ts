import { Component, OnInit, ViewChild } from '@angular/core';
import { Breadcrumb } from '../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../@core/utils/global.service';
import { NbStepperComponent, NbDialogService } from '@nebular/theme';
import { SurveyModalComponent } from '../../shared/survey-modal/survey-modal.component';
import { AuthenticationService } from '../../../../@core/utils/authentication.service';
import { OnboardService } from './onboard.service';
import { Router } from '@angular/router';
import { env } from '../../../../@core/env/env';

@Component({
  selector: 'ngx-onboard',
  templateUrl: './onboard.component.html',
  styleUrls: ['./onboard.component.scss'],
})
export class OnboardComponent implements OnInit {
  step_one_label;
  roleForm;
  onBoardData: any;
  isOrg = env.isOrg;

  @ViewChild('stepper', { static: true }) stepper: NbStepperComponent;
  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Get Started',
      link: '',
    },
  ];

  constructor(
    private globalService: GlobalService,
    private _dialogService: NbDialogService,
    private _authService: AuthenticationService,
    private _onBoardService: OnboardService,
    private router: Router,

  ) { }

  ngOnInit() {
    this.globalService.getBreadcrumbs(this.breadcrumbs);
    this.getOnBoardingData();
    // this.showSurvey();
  }


  changeHasLogged() {
    this._onBoardService.setHasLogged().subscribe(data => { });
  }


  getOnBoardingData() {
    this._onBoardService.getOnBoardingData().subscribe(data => {
      this.onBoardData = data.Object;
    });
  }

  noExperience() {
    this._onBoardService.submitAnswer(2, 2).subscribe(data => {
      this.stepper.next();
    });
  }

  hasExperience() {
    this._onBoardService.submitAnswer(2, 1).subscribe(data => {
      this.stepper.next();
    });
  }

  goToTraining() {
    this._onBoardService.submitAnswer(1, 3).subscribe(data => {
      this.router.navigate(['/teacher/training']);
    });
  }

  goToClassroom() {
    this._onBoardService.submitAnswer(1, 4).subscribe(data => {
      this.router.navigate(['/teacher/classroom']);
    });
  }


  showSurvey() {
    let data;
    const status = this._authService.isActivated();
    this._authService.currentUser.subscribe(x => data = x);
    if (data.hasLogged === 0 && status && data.terms_agreement) {
      // this._dialogService.open(SurveyModalComponent, {
      //   context: {
      //     scriptKey: 'initial',
      //   },
      // });
      data.hasLogged = 1;
      this.changeHasLogged();
      localStorage.setItem('AuthorizationData', JSON.stringify(data));
    }
  }
}
