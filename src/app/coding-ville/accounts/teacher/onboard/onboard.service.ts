import { Injectable } from '@angular/core';
import { GlobalService } from '../../../../@core/utils/global.service';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class OnboardService {

  constructor(private _globalService: GlobalService) { }

  getOnBoardingData() {
    const url: string = `coding-ville/survey/data`;
    return this._globalService.get(url);
  }

  submitAnswer(questionID, answerID) {
    const url: string = `coding-ville/survey/save`;
    const params = new HttpParams().set('question_id', questionID).append('answer_id', answerID);
    return this._globalService.post(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  setHasLogged() {
    const url: string = `coding-ville/login/logged`;
    return this._globalService.post(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

}
