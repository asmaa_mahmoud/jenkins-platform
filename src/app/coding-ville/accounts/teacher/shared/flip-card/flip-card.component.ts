import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-flip-card',
  templateUrl: './flip-card.component.html',
  styleUrls: ['./flip-card.component.scss'],
})
export class FlipCardComponent implements OnInit {

  @Output() assign = new EventEmitter<number>();
  @Output() tryClicked = new EventEmitter();
  @Input() cardData: any;
  flipped: boolean = false;
  @Input() isLoading: boolean = false;

  constructor() { }

  ngOnInit() {
    this.detectBrowser();

  }



  flip() {
    this.flipped = !this.flipped;
  }

  emitAssign() {
    if (!this.isLoading) {
      this.assign.emit(this.cardData.id);
    }
  }

  try() {
    this.tryClicked.emit();
  }

  detectBrowser() {
    const userAgentString = navigator.userAgent;
    const IExplorerAgent = userAgentString.indexOf('MSIE') > -1 || userAgentString.indexOf('rv:') > -1;
    const edgeAgent = window.navigator.userAgent.indexOf('Edge') > -1;
    if (IExplorerAgent || edgeAgent) {
      const elem = document.getElementsByClassName('flipcard-body');
      for (let i = 0; i < elem.length; i++) {
        elem[i].setAttribute('style', 'transform-style:inherit; ');
      }
    }
  }

}
