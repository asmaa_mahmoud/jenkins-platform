import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS_DISABLED: NbMenuItem[] = [
    {
        title: 'dashboard',
        icon: 'bar-chart-outline',
        home: true,
    },
    {
        title: 'classrooms',
        icon: 'briefcase',
    },
    {
        title: 'journeys',
        icon: 'map',
    },
    {
        title: 'projects',
        icon: 'flag',
    },
    {
        title: 'discover',
        icon: 'compass',
    },
    {
        title: 'training',
        icon: 'book-open',
    },
];
