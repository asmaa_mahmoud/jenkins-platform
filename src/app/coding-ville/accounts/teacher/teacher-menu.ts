import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
    {
        title: 'dashboard',
        icon: { icon: 'dashboard', pack: 'custom-icons' },
        link: '/teacher/dashboard',
        home: true,
    },
    {
        title: 'classrooms',
        icon: { icon: 'classroom', pack: 'custom-icons' },
        link: '/teacher/classroom',
        pathMatch: 'prefix',
    },
    {
        title: 'journeys',
        icon: { icon: 'journeys', pack: 'custom-icons' },
        link: '/teacher/journeys',
        pathMatch: 'prefix',
    },
    {
        title: 'projects',
        icon: { icon: 'projects', pack: 'custom-icons' },
        link: '/teacher/projects',
        pathMatch: 'prefix',
    },
    {
        title: 'discover',
        icon: 'compass',
        link: '/teacher/discover',
        pathMatch: 'prefix',
    },
    {
        title: 'training',
        icon: { icon: 'training', pack: 'custom-icons' },
        link: '/teacher/training',
        pathMatch: 'prefix',
    },
];
