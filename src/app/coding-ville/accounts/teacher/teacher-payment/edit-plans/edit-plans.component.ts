import { Router } from '@angular/router';
import { DashboardService } from './../../dashboard/dashboard.service';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { Component, OnInit } from '@angular/core';
import { TeacherPaymentService } from '../teacher-payment.service';

@Component({
  selector: 'ngx-edit-plans',
  templateUrl: './edit-plans.component.html',
  styleUrls: ['./edit-plans.component.scss'],
})
export class EditPlansComponent implements OnInit {

  selectedPlan = 0;

  plans = [];

  numberOfStudents;
  maxNumberOfStudents;
  addedStudents = 1;
  pricePerStudent;

  constructor(
    private _globalService: GlobalService,
    private _teacherPayment: TeacherPaymentService,
    private _dashboardService: DashboardService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.setBreadcrumbs();
    this.getPlans();
    this.getMaxStudentsAndClasses();
    this.getGeneralData();
  }
  // SECTION  Initial Data

  getMaxStudentsAndClasses() {
    this._dashboardService.getMaxStudentsAndClasses().subscribe(
      res => {
        const students = res.Object.find(x => x.code && x.code === 'students');
        this.maxNumberOfStudents = students.value;

        const price = res.Object.find(x => x.code && x.code === 'student_price');
        this.pricePerStudent = price.value;
      });
  }

  getGeneralData() {
    this._dashboardService.getGeneralData().subscribe(
      res => {
        this.numberOfStudents = res.Object.noOfStudents;
      });
  }


  setBreadcrumbs() {
    const breadcrumbs = [
      {
        name: 'Edit Profile',
        link: '',
      },
      {
        name: 'Change Plans',
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(breadcrumbs);
  }

  // SECTION  Plans

  getPlans() {
    this._teacherPayment.getPlans().subscribe(
      res => {
        this.plans = res.Object;
      });
  }




  choosePlan(plan) {
    this.selectedPlan = plan.id;

    this._teacherPayment.checkout_items = plan;
    this.router.navigate(['/teacher/edit-profile/change-plans/payment']);

  }

  purchaseStudents() {
    if (Number(this.numberOfStudents) !== Number(this.maxNumberOfStudents)) {
      return;
    }

    const items = {
      addedStudents: this.addedStudents,
      amount: this.pricePerStudent,
    };
    this._teacherPayment.checkout_items = items;
    this.router.navigate(['/teacher/edit-profile/change-plans/payment']);
  }
}
