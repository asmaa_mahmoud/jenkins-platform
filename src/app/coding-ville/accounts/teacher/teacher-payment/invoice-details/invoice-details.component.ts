import { GlobalService } from '../../../../../@core/utils/global.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TeacherPaymentService } from '../teacher-payment.service';

@Component({
  selector: 'ngx-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.scss'],
})
export class InvoiceDetailsComponent implements OnInit {

  invoiceId;
  invoiceDetails;


  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _globalService: GlobalService,
    private _teacherPaymentService: TeacherPaymentService,
  ) { }

  ngOnInit() {
    this.getInvoiceId();
  }
  setBreadcrumbs() {
    const breadcrumbs = [
      {
        name: 'Edit Profile',
        link: '/teacher/edit-profile/purchase-history',
      },
      {
        name: 'Purchase details',
        link: '/teacher/edit-profile/purchase-history',
      },
      {
        name: this.invoiceId,
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(breadcrumbs);
  }

  getInvoiceId() {
    this._route.params.subscribe(
      res => {
        this.invoiceId = res['id'];
        if (this.invoiceId) {
          this.setBreadcrumbs();
          this.getInvoiceDetails();
        }
      },
      err => {
        this._router.navigate(['teacher/purchase-history']);
      },
    );
  }

  getInvoiceDetails() {
    this._teacherPaymentService.getInvoiceDetails(this.invoiceId).subscribe(
      res => {
        this.invoiceDetails = res.Object;
      });
  }


}
