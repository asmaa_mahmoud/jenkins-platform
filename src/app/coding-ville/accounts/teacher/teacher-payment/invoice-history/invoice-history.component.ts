import { GlobalService } from '../../../../../@core/utils/global.service';
import { Component, OnInit } from '@angular/core';
import { TeacherPaymentService } from '../teacher-payment.service';

@Component({
  selector: 'ngx-invoice-history',
  templateUrl: './invoice-history.component.html',
  styleUrls: ['./invoice-history.component.scss'],
})
export class InvoiceHistoryComponent implements OnInit {

  invoicesHistory = [];

  totalInvoicesCount = 10;
  currentPage = 1;


  start: number;
  rowPerPage = 10;


  constructor(
    private _globalService: GlobalService,
    private _teacherPaymentService: TeacherPaymentService,
  ) { }

  ngOnInit() {
    this.setBreadcrumbs();
    this.getInvoices();
  }


  setBreadcrumbs() {
    const breadcrumbs = [
      {
        name: 'Edit Profile',
        link: '',
      },
      {
        name: 'Purchase History',
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(breadcrumbs);
  }

  // --------------- Data SECTION ------------------

  getInvoices() {
    this._teacherPaymentService.getInvoices().subscribe(
      res => {
        this.invoicesHistory = res.Object;
      },
    );
  }



  changePagination(num) {
    this.start = num * this.rowPerPage - this.rowPerPage;
    // this.getInvoiceHistory();
  }
}
