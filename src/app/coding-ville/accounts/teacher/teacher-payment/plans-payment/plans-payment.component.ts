import { Router } from '@angular/router';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { StripeService, Elements, Element as StripeElement } from 'ngx-stripe';
import { TeacherPaymentService } from '../teacher-payment.service';

@Component({
  selector: 'ngx-plans-payment',
  templateUrl: './plans-payment.component.html',
  styleUrls: ['./plans-payment.component.scss'],
})
export class PlansPaymentComponent implements OnInit {


  elements: Elements;
  cardData;
  cards = [];

  isPurchaseDone = false;

  newStudents;
  newPlan;


  genratedToken;
  paymentForm: FormGroup;
  isLoading = false;
  selectedCard;

  checkout_item;

  isNoCards: boolean;
  isStudents: boolean;

  constructor(
    private _globalService: GlobalService,
    private fb: FormBuilder,
    private stripeService: StripeService,
    private _teacherPaymentService: TeacherPaymentService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.setBreadcrumbs();
    this.getUserCards();
    this.getData();
  }


  setBreadcrumbs() {
    const breadcrumbs = [
      {
        name: 'Edit Profile',
        link: 'edit-profile',
      },
      {
        name: 'Plans',
        link: 'edit-profile/plans',
      },
      {
        name: 'Payment',
        link: '',
      },
    ];
    this._globalService.getBreadcrumbs(breadcrumbs);
  }

  // -------------------- SECTION  get Data --------------------

  getData() {
    if (!this._teacherPaymentService.checkout_items) {
      this.router.navigate(['/teacher/edit-profile/plans']);
      return;
    }

    this.checkout_item = this._teacherPaymentService.checkout_items;
    this.isStudents = this.checkout_item.addedStudents ? true : false;
  }


  getUserCards() {
    this._teacherPaymentService.getUserCards().subscribe(
      res => {
        if (res.Object.length === 0) {
          this.initStripe();
          this.buildForm();
          this.isNoCards = true;

        } else {
          this.isNoCards = false;
          this.cards = res.Object;
        }
      });
  }


  // -------------------- SECTION  No Cards --------------------

  initStripe() {

    this.stripeService.elements().subscribe(elements => {
      this.elements = elements;
      // Only mount the element the first time
      if (!this.cardData) {
        let cardNumber: StripeElement;
        let cardExpiration: StripeElement;
        let cardCVV: StripeElement;



        cardNumber = this.elements.create('cardNumber', {
          style: {
            base: {
              iconColor: '#666EE8',
            },
          },
        });
        cardExpiration = this.elements.create('cardExpiry', {
          style: {
            base: {
              iconColor: '#666EE8',
            },
          },
        });

        cardCVV = this.elements.create('cardCvc', {
          style: {
            base: {
              iconColor: '#666EE8',
            },
          },
        });




        cardNumber.mount('#card-element');
        cardExpiration.mount('#cardExpiration');
        cardCVV.mount('#cardCVV');


        this.cardData = {
          cardNumber: cardNumber,
          cardExpiration: cardExpiration,
          cardCVV: cardCVV,
        };
      }
    });
  }


  buildForm() {
    const group = {
      card_input: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      cvv_input: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      expiration_input: new FormControl(null, [Validators.required, Validators.minLength(5), Validators.maxLength(5)]),
    };
    this.paymentForm = this.fb.group(group);

  }

  // -------------------- SECTION  Submit --------------------

  submit() {
    if (this.isNoCards) {
      this.getStripToken();
    } else {
      this.buy();
    }
  }

  // -------------------- SECTION  Stirpe Token --------------------

  getStripToken() {
    this.stripeService
      .createToken(this.cardData.cardNumber, undefined)
      .subscribe(result => {
        if (result.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          this.genratedToken = result.token;
          this.buy();
        } else if (result.error) {
          // Error creating the token
          this._globalService.showTranslatedToast('danger', null, 'something-went-wrong');

        }
      });
  }

  // -------------------- SECTION  Buy --------------------

  buy() {
    const data = {};
    if (this.isNoCards) {
      data['stripe_token'] = this.genratedToken.id;
    } else {
      data['payment_method_id'] = this.selectedCard;
    }


    if (this.isStudents) {
      this.buyStudents(data);
    } else {
      this.changePlan(data);
    }
  }

  // -------------------- SECTION  Students or plan --------------------


  buyStudents(data) {

    if (this.isLoading === true) {
      return;
    }
    this.isLoading = true;

    data['students'] = this.checkout_item.addedStudents;

    this._teacherPaymentService.addstudents(data).subscribe(
      res => {
        this._globalService.showTranslatedToast('success', null, 'students-are-added-to-the-current-plan');

        this.router.navigate(['/teacher/edit-profile/plans']);

      }, error => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        this.isLoading = false;

      });
  }

  changePlan(data) {

    if (this.isLoading === true) {
      return;
    }
    this.isLoading = true;

    data['plan_id'] = this.checkout_item.id;


    this._teacherPaymentService.changePlan(data).subscribe(
      res => {
        this._globalService.showTranslatedToast('success', null, 'your-plan-was-changed');

        this.router.navigate(['/teacher/edit-profile/plans']);

      }, error => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        this.isLoading = false;
      });
  }




}
