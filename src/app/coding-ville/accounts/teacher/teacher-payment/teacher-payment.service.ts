import { GlobalService } from './../../../../@core/utils/global.service';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class TeacherPaymentService {

    checkout_items;

    constructor(
        private _globalService: GlobalService,
    ) { }


    getPlans() {
        const url = `payment/plans`;
        return this._globalService.get(url);
    }

    getInvoices() {
        const url = `payment/invoices`;
        return this._globalService.get(url);
    }

    getInvoiceDetails(id) {
        const url = `payment/invoice?invoice_id=${id}`;
        return this._globalService.get(url);
    }
    getMaxStudentsAndClassrooms() {
        const url = ``;
        return this._globalService.get(url);
    }

    getUserCards() {
        const url = 'payment/cards';
        return this._globalService.get(url);
    }

    addstudents(data) {
        const url = 'payment/subscription/addstudents';
        return this._globalService.post(url, data);
    }

    changePlan(data) {
        const url = 'payment/subscription/change';
        return this._globalService.post(url, data);
    }
}
