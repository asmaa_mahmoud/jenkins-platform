import { EditPlansComponent } from './teacher-payment/edit-plans/edit-plans.component';
import { ChangePasswordComponent } from './../shared/edit-profile/change-password/change-password.component';
import { PersonalInfoComponent } from './../shared/edit-profile/personal-info/personal-info.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { TeacherComponent } from './teacher.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClassroomComponent } from './classroom/classroom.component';
import { DiscoverComponent } from '../shared/discover/discover.component';
import { EditProfileComponent } from '../shared/edit-profile/edit-profile.component';
import { ProjectPreviewComponent } from '../shared/project-preview/project-preview.component';
import { StudentProfileComponent } from '../shared/profile/student-profile.component';
import { ClassroomTabsComponent } from './classroom/classroom-tabs/classroom-tabs.component';
import { ClassroomJourneysComponent } from './classroom/classroom-journeys/classroom-journeys.component';
import { ClassroomMiniJourneysComponent } from './classroom/classroom-mini-journeys/classroom-mini-journeys.component';
import { ClassroomProjectsComponent } from './classroom/classroom-projects/classroom-projects.component';
import { ClassroomStudentsComponent } from './classroom/classroom-students/classroom-students.component';
import { StudentProgressComponent } from './classroom/student-progress/student-progress.component';
import { FlipCardComponent } from './shared/flip-card/flip-card.component';
import { AddJourneyComponent } from './classroom/add-journey/add-journey.component';
import { AddPreJourneyComponent } from './classroom/add-pre-journey/add-pre-journey.component';
import { TrainingComponent } from './training/training.component';
import { TrainingMissionsComponent } from './training/training-missions/training-missions.component';
import { StudentModalCardsComponent } from './classroom/classroom-students/student-modal/student-modal-cards/student-modal-cards.component';
import { StudentModalCSVComponent } from './classroom/classroom-students/student-modal/student-modal-csv/student-modal-csv.component';
import { StudentModalAddComponent } from './classroom/classroom-students/student-modal/student-modal-add/student-modal-add.component';
import { JourneysComponent } from './journeys/journeys.component';
import { PreJourneyTabComponent } from './journeys/pre-journey-tab/pre-journey-tab.component';
import { JourneyTabComponent } from './journeys/journey-tab/journey-tab.component';
import { CreateClassroomComponent } from './classroom/create-classroom/create-classroom.component';
import { EchartsBarComponent } from './dashboard/charts/echarts-bar.component';
import { EchartsMultipleXaxisComponent } from './dashboard/charts/echarts-multiple-xaxis.component';
import { StudentModalComponent } from './classroom/classroom-students/student-modal/student-modal.component';
import { AssignModalComponent } from './journeys/assign-modal/assign-modal.component';
import { AdventureComponent } from '../shared/adventure/adventure.component';
import { ProjectsComponent } from '../shared/projects/projects.component';
import { OnboardComponent } from './onboard/onboard.component';
import { InvoiceHistoryComponent } from './teacher-payment/invoice-history/invoice-history.component';
import { PlansPaymentComponent } from './teacher-payment/plans-payment/plans-payment.component';
import { InvoiceDetailsComponent } from './teacher-payment/invoice-details/invoice-details.component';


const routes: Routes = [{
  path: '',
  component: TeacherComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
      data: { title: 'dashboard' },
    },
    {
      path: 'journeys',
      component: JourneysComponent,
      data: { title: 'journey' },
    },
    {
      path: 'classroom/:classroom_id/journey/:journey_id/course/:course_id/:type',
      component: AdventureComponent,
      data: { title: 'journey' },
    },
    {
      path: 'classroom/:classroom_id/journey/:journey_id/:type',
      component: AdventureComponent,
      data: { title: 'journey' },
    },
    {
      path: 'journeys/:journey_id/course/:course_id/:type',
      component: AdventureComponent,
      data: { title: 'journey' },
    },
    {
      path: 'journeys/:journey_id/course/:course_id/:type',
      component: AdventureComponent,
      data: { title: 'journey' },
    },
    {
      path: 'journeys/:journey_id/:type',
      component: AdventureComponent,
      data: { title: 'journey' },
    },
    {
      path: 'journeys/:journey_id/:type',
      component: AdventureComponent,
      data: { title: 'journey' },
    },
    {
      path: 'classroom',
      data: { title: 'classroom' },
      component: ClassroomComponent,
    },
    {
      path: 'classroom/:classroom_id/course/:course_id/:journey_type/:journey_id/progress',
      data: { title: 'classroom' },
      component: StudentProgressComponent,
    },
    {
      path: 'classroom/:classroom_id',
      data: { title: 'classroom' },
      component: ClassroomTabsComponent,
    },
    {
      path: 'classroom/add/:classroom_id',
      component: AddJourneyComponent,
    },
    {
      path: 'classroom/add-pre/:classroom_id',
      component: AddPreJourneyComponent,
    },
    {
      path: 'projects',
      component: ProjectsComponent,
      data: { title: 'Projects' },
    },
    {
      path: 'training',
      data: { title: 'training' },
      component: TrainingComponent,
    },
    {
      path: 'training/:id/training-missions',
      data: { title: 'training' },
      component: TrainingMissionsComponent,
    },
    {
      path: 'discover',
      component: DiscoverComponent,
      data: { title: 'discover' },
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: 'edit-profile',
      component: EditProfileComponent,
      data: { title: 'edit-profile' },
      children: [
        {
          path: '',
          redirectTo: 'profile-details',
          pathMatch: 'full',
        },
        {
          path: 'profile-details',
          component: PersonalInfoComponent,
        },
        {
          path: 'change-password',
          component: ChangePasswordComponent,
        },
        {
          path: 'plans',
          component: EditPlansComponent,
        },
        {
          path: 'purchase-history',
          component: InvoiceHistoryComponent,
        },
      ],
    },
    {
      path: 'edit-profile/purchase-history/:id',
      component: InvoiceDetailsComponent,
    },
    {
      path: 'edit-profile/change-plans/payment',
      component: PlansPaymentComponent,
    },
    {
      path: ':type/:project_id/:discover-project',
      component: ProjectPreviewComponent,
    },
    // ****** preview default projects of journey ******//
    {
      path: ':type/:project_id/:journey-type/adventure/:adventureID/course/:courseID',
      component: ProjectPreviewComponent,
      data: { title: 'project' },
    },
    {
      path: ':type/:project_id/:journey-type/:journeyID/adventure/:adventureID',
      data: { title: 'project' },
      component: ProjectPreviewComponent,
    },
    // ****** preview default projects of pre-journey ******//
    {
      path: ':type/:project_id/:journey-type/:activityID/course/:courseID',
      component: ProjectPreviewComponent,
      data: { title: 'project' },
    },
    {
      path: ':type/:project_id/:journey-type/:activityID',
      component: ProjectPreviewComponent,
      data: { title: 'project' },
    },
    {
      path: 'profile/:student_id',
      component: StudentProfileComponent,
    },
    {
      path: 'on-board',
      component: OnboardComponent,
      data: { title: 'on-board' },
    },

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class TeacherRoutingModule {
}


export const Components =
  [
    TeacherComponent,
    DashboardComponent,
    ClassroomComponent,
    CreateClassroomComponent,
    EchartsBarComponent,
    EchartsMultipleXaxisComponent,
    ClassroomTabsComponent,
    ClassroomJourneysComponent,
    ClassroomMiniJourneysComponent,
    ClassroomProjectsComponent,
    ClassroomStudentsComponent,
    StudentProgressComponent,
    StudentModalComponent,
    StudentModalCardsComponent,
    StudentModalCSVComponent,
    StudentModalAddComponent,
    FlipCardComponent,
    AddJourneyComponent,
    AddPreJourneyComponent,
    AssignModalComponent,
    TrainingComponent,
    TrainingMissionsComponent,
    JourneysComponent,
    JourneyTabComponent,
    PreJourneyTabComponent,
    OnboardComponent,
    InvoiceDetailsComponent,
    InvoiceHistoryComponent,
    EditPlansComponent,
    PlansPaymentComponent,
  ];
