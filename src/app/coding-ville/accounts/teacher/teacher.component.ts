import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../../@core/utils/authentication.service';
import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './teacher-menu';
import { MENU_ITEMS_DISABLED } from './teacher-menu-disabled';
import { NbIconLibraries } from '@nebular/theme';

@Component({
  selector: 'ngx-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.scss'],
})


export class TeacherComponent implements OnInit {

  isActivated: boolean;
  menu: any;

  constructor(
    private iconLibraries: NbIconLibraries,
    private authService: AuthenticationService,
    private router: Router,
    private translateService: TranslateService,
  ) {
    this.iconLibraries.registerSvgPack('custom-icons', {
      'home': '<svg xmlns="http://www.w3.org/2000/svg" width="18.4" height="16.655" viewBox="0 0 18.4 16.655" fill="currentColor"><g transform="translate(-2.802 -3.345)"><path d="M19,9.3V5a1,1,0,0,0-1-1H17a1,1,0,0,0-1,1V6.6l-3.33-3a1.008,1.008,0,0,0-1.34,0L2.97,11.13A.5.5,0,0,0,3.3,12H5v7a1,1,0,0,0,1,1H9a1,1,0,0,0,1-1V14h4v5a1,1,0,0,0,1,1h3a1,1,0,0,0,1-1V12h1.7a.5.5,0,0,0,.33-.87ZM10,10a2,2,0,0,1,4,0Z"/></g></svg>',
    });
    this.isActivated = this.authService.isActivated();
  }

  ngOnInit() {
    if (!this.isActivated) this.router.navigate(['/teacher/dashboard']);
    this.getMenu();
  }

  getMenu() {
    this.menu = this.isActivated ? MENU_ITEMS : MENU_ITEMS_DISABLED;
    this.menu.map(item => {
      this.translateService.get(item.title).subscribe(res => {
        item.title = res;
      });
      // if (!this.isActivated) item.link = '';
    });
  }

}
