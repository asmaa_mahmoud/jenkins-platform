import { TranslatorModule } from './../../../@shared/translator/translator.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-chartjs';
import {
  NbMenuModule,
  NbCardModule,
  NbDialogModule,
  NbButtonModule,
  NbIconModule,
  NbSelectModule,
  NbInputModule,
  NbTabsetModule,
  NbPopoverModule,
  NbCheckboxModule,
  NbStepperModule,
} from '@nebular/theme';
import { ThemeModule } from '../../../@theme/theme.module';


import { TeacherRoutingModule, Components } from './teacher-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CreateClassroomComponent } from './classroom/create-classroom/create-classroom.component';
import { ClassRoomService } from './classroom/class-room.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StudentModalComponent } from './classroom/classroom-students/student-modal/student-modal.component';
import { AssignModalComponent } from './journeys/assign-modal/assign-modal.component';
import { TrainingService } from './training/training.service';
import { NgxPrintModule } from 'ngx-print';
import { TeacherPaymentService } from './teacher-payment/teacher-payment.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ThemeModule,
    FormsModule,
    NbMenuModule,
    NbButtonModule,
    NbCheckboxModule,
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbTabsetModule,
    NbStepperModule,
    NbDialogModule.forChild(),
    NbPopoverModule,
    NgxEchartsModule,
    NgxChartsModule,
    ReactiveFormsModule,
    ChartModule,
    TeacherRoutingModule,
    TranslatorModule,
    NgxPrintModule,
  ],
  declarations: [
    ...Components,
  ],
  entryComponents: [
    CreateClassroomComponent,
    StudentModalComponent,
    AssignModalComponent,
  ],
  providers: [ClassRoomService, TrainingService, TeacherPaymentService],
})
export class TeacherModule { }
