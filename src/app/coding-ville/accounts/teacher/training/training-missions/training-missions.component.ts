import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { Breadcrumb } from './../../../../../@core/data/breadcrumb';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { TrainingService } from '../training.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'ngx-training-missions',
  templateUrl: './training-missions.component.html',
  styleUrls: ['./training-missions.component.scss'],
})
export class TrainingMissionsComponent implements OnInit {
  activityId: any;
  missions: any[];
  adventureName: string;
  currentURL;

  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Training',
      link: 'training',
    },
    {
      name: this.adventureName,
      link: '',
    },
  ];

  constructor(
    private _globalService: GlobalService,
    private _trainingService: TrainingService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
  ) {
    this.router.events.subscribe(x => {
      this.currentURL = this.location.path();
    });
  }

  ngOnInit() {
    this._globalService.breadcrumbsService.getBreadcrumbs(this.breadcrumbs);
    this.getActivityId();
    this.getMissions();
  }

  getMissions() {
    this._trainingService.getActivityMission(this.activityId).subscribe((data: any) => {
      this.convertMissionTypesNames(data.Object.missions);
      this.adventureName = data.Object.name;
      this.breadcrumbs = [
        {
          name: 'Training',
          link: 'training',
        },
        {
          name: this.adventureName,
          link: '',
        },
      ];
      this._globalService.getBreadcrumbs(this.breadcrumbs);

    });
  }

  getActivityId() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.activityId = params['id'];
    });
  }

  setBackInfoForWorkspace() {
    localStorage.setItem('backURL', this.currentURL);
  }

  convertMissionTypesNames(missions) {
    missions.forEach(element => {
      if (element.type === 'mission') element.type = 'blocks';
      else if (element.type === 'html_mission') element.type = 'lesson';
      else if (element.type === 'mission_editor') element.type = 'webeditor';
    });
    this.missions = missions;
  }

  goToMission(mission) {
    if (!mission.isLocked) {
      this.setBackInfoForWorkspace();
      window.location.href = '/workspaces/training/' + mission.type + '/' + this.activityId + '/' + mission.id;
    }
  }



}
