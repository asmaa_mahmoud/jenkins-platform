import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Training } from '../../../../@core/data/training';
import { GlobalService } from './../../../../@core/utils/global.service';
import { Breadcrumb } from './../../../../@core/data/breadcrumb';
import { TrainingService } from './training.service';

@Component({
  selector: 'ngx-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss'],
})
export class TrainingComponent implements OnInit {

  trainingsLength: number = 6;
  cardPerPage = 6;
  currentPage = 1;
  start: number = 0;
  noOfCardsPerPage = [6, 9, 12];
  searchValue: string = '';
  dataLoaded: boolean = false;
  trainings: Training[];
  private breadcrumbs: Breadcrumb[] = [
    {
      name: 'Training',
      link: '',
    },
  ];

  constructor(
    private _globalService: GlobalService,
    private _trainingService: TrainingService,
    private router: Router,
  ) { }

  ngOnInit() {
    this._globalService.breadcrumbsService.getBreadcrumbs(this.breadcrumbs);
    this.countTrainings();
  }

  countTrainings() {
    this._trainingService.getTrainingCount().subscribe(res => {
      this.trainingsLength = res.Object.count;
      this.getAllTrainingActivities();
    }, error => {
      console.log(error);
    });
  }

  getAllTrainingActivities() {
    this._trainingService.getAllTrainingActivities(
      this.start.toString(),
      this.cardPerPage.toString(),
      this.searchValue)
      .subscribe(res => {
        this.trainings = res.Object;
        this.trainings = this.trainings.filter((el) => {
          delete el.fromGrade;
          delete el.toGrade;
          delete el.difficulty;
          delete el.totalXps;
          delete el.totalCoins;
          return el;
        });
        this.dataLoaded = true;
      }, error => {
        console.log(error);
      });
  }

  openMissions(id) {
    this.router.navigate([`teacher/training/${id}/training-missions`]);
  }

  changePagination(num: number) {
    this.start = num * this.cardPerPage - this.cardPerPage;
    this.countTrainings();
  }

  changePageLimit() {
    this.start = 0;
    this.countTrainings();
  }

  search(value: string) {
    this.searchValue = value;
    this.start = 0;
    this.countTrainings();
  }
}
