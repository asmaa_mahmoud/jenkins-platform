import { Injectable } from '@angular/core';
import { GlobalService } from '../../../../@core/utils/global.service';
import 'rxjs/Rx';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { CustomEncoder } from '../../../../@core/utils/custom-encoder';

@Injectable({
  providedIn: 'root',
})
export class TrainingService {

  constructor(private _globalService: GlobalService) { }

  getTrainingCount() {
    const url = 'coding-ville/teacher/training/count';
    return this._globalService.get(url).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getAllTrainingActivities(start, limit, search) {
    const url: string = `coding-ville/teacher/training/all`;
    const params = new HttpParams({ encoder: new CustomEncoder() }).set('start', start).append('limit', limit).append('search', search);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }

  getActivityMission(activity_id) {
    const url: string = `coding-ville/teacher/training/missions`;
    const params = new HttpParams().set('activity_id', activity_id);
    return this._globalService.get(url, params).map(
      (data: any) => {
        return data;
      },
    ).catch(
      (error: any) => {
        console.log(error.message);
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
        return Observable.throw('Something went wrong!');
      },
    );
  }
}
