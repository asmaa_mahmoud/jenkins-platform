import { AuthenticationService } from './../../@core/utils/authentication.service';
import { GlobalService } from '../../@core/utils/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Role } from '../../@core/data/role';

@Component({
  selector: 'ngx-activate-user',
  templateUrl: './activate-user.component.html',
  styleUrls: ['./activate-user.component.scss'],
})
export class ActivateUserComponent implements OnInit {

  hash: string;
  userData: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private globalService: GlobalService,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    this.hash = this.route.snapshot.paramMap.get('hash') ? this.route.snapshot.paramMap.get('hash') : '';
    this.globalService.get(`user/activation/${this.hash}`).subscribe(res => {
      if (this.authenticationService.currentUserValue) {
        this.authenticationService.userStatusValue = 'active';
        this.authenticationService.userStatusSubject.next('active');
        this.authenticationService.currentUser.subscribe(x => this.userData = x);
        this.userData['hasLogged'] = 0;
        if (Role['Student'].includes(this.userData.role) || Role['individualStudent'].includes(this.userData.role))
          this.router.navigate(['student/on-board']);
        else if (Role['Teacher'].includes(this.userData.role))
          this.router.navigate(['teacher/on-board']);
      } else this.router.navigate(['/auth/login']);
    }, err => {
      if (this.authenticationService.currentUserValue) this.router.navigate(['']);
      else this.router.navigate(['/auth/login']);
    });
  }

}
