import { ResetPasswordComponent } from './reset-password/reset-password/reset-password.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthWizardComponent } from './register/wizard/wizard.component';
import { LtiComponent } from './lti/lti.component';
import { ResetPasswordEmailComponent } from './reset-password/email/reset-password-email.component';

const routes: Routes = [{
    path: '',
    component: AuthComponent,
    children: [


        {
            path: '',
            redirectTo: 'registration',
            pathMatch: 'full',
        },

        {
            path: 'login',
            component: LoginComponent,
        },

        {
            path: 'lti',
            component: LtiComponent,
        },

        {
            path: 'registration',
            component: RegisterComponent,
            children: [
                {
                    path: '',
                    component: AuthWizardComponent,
                },
                {
                    path: ':type',
                    component: AuthWizardComponent,
                },
            ],
        },

        {
            path: 'reset-password/email',
            component: ResetPasswordEmailComponent,
        },

        {
            path: 'resetpassword',
            component: ResetPasswordComponent,
        },

    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class LoginRoutingModule {
}

