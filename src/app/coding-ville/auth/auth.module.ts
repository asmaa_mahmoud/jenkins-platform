import { environment } from './../../../environments/environment';
import { PaymentComponent } from './register/wizard/payment/payment.component';
import { PlansComponent } from './register/wizard/plans/plans.component';
import { ResetPasswordComponent } from './reset-password/reset-password/reset-password.component';
import { AuthThanksStepComponent } from './register/wizard/auth-thanks-step/auth-thanks-step.component';
import { AuthTeacherFormComponent } from './register/wizard/profile-details-step/auth-teacher-form/auth-teacher-form.component';
import { AuthStudentFormComponent } from './register/wizard/profile-details-step/auth-student-form/auth-student-form.component';
import { ProfileDetailsStepComponent } from './register/wizard/profile-details-step/profile-details-step.component';
import { SelectRoleStepComponent } from './register/wizard/select-role-step/select-role-step.component';
import { AuthWizardComponent } from './register/wizard/wizard.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbButtonModule, NbIconModule, NbStepperModule, NbCardModule, NbSelectModule, NbCheckboxModule } from '@nebular/theme';
import { AuthComponent } from './auth.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginRoutingModule } from './auth-routing.module';
import { ThemeModule } from '../../@theme/theme.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LtiComponent } from './lti/lti.component';
import { ClassroomCodeStepComponent } from './register/wizard/classroom-code-step/classroom-code-step.component';
import { ResetPasswordEmailComponent } from './reset-password/email/reset-password-email.component';
import { TranslatorModule } from '../../@shared/translator/translator.module';

import { NgxStripeModule } from 'ngx-stripe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ThemeModule,
    ReactiveFormsModule,
    LoginRoutingModule,
    NbButtonModule,
    NbIconModule,
    NbStepperModule,
    NbCardModule,
    NbSelectModule,
    NbCheckboxModule,
    TranslatorModule,
    NgxStripeModule.forChild(environment.stripeKey),

  ],
  declarations: [
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    AuthWizardComponent,
    LtiComponent,
    SelectRoleStepComponent,
    ClassroomCodeStepComponent,
    ProfileDetailsStepComponent,
    AuthStudentFormComponent,
    AuthTeacherFormComponent,
    AuthThanksStepComponent,
    ResetPasswordEmailComponent,
    ResetPasswordComponent,
    PlansComponent,
    PaymentComponent,
  ],

})
export class AuthModule { }
