import { Subject, BehaviorSubject } from 'rxjs';
import { GlobalService } from './../../@core/utils/global.service';
import { OnInit, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements OnInit {

  classroomCodeChanges = new Subject<{ hasCode: boolean, code: string }>();
  stepBackFromForms = new Subject<void>();

  orgTeacherData = new BehaviorSubject<OrgTeacherData>(null);

  constructor(
    private _globalService: GlobalService) { }

  ngOnInit() {

  }

  validateClassroomCode(code: string) {
    const url = `coding-ville/register/validateclassroom`;
    return this._globalService.post(url, { classroom_code: code });
  }

  registerStudentWithCode(body: { classroom_code, full_name, username, password }) {
    const url = `coding-ville/register/school/student`;
    return this._globalService.post(url, body);
  }

  registerStudentNoCode(body: { full_name, username, password, age, email }) {
    const url = `coding-ville/register/individiual`;
    return this._globalService.post(url, body);
  }

  registerTeacher(body: { full_name, username, email, password, school_name, province_id, city }) {
    const url = `coding-ville/register/school/teacher`;
    return this._globalService.post(url, body);
  }

  getCountries() {
    const url = `countries`;
    return this._globalService.get(url);
  }

  getProvinces(countryID) {
    const url = `states?country_id=${countryID}`;
    return this._globalService.get(url);
  }

  getProvinceCities(provinceID) {
    const url = `cities?state_id=${provinceID}`;
    return this._globalService.get(url);
  }

  resetPassword(email: string, token: string, password: string) {
    const url = 'user/confirmreset/password';
    return this._globalService.post(url, { email: email, token: token, password: password });
  }

  resetPasswordEmail(email: string) {
    const url = 'user/reset/password';
    return this._globalService.post(url, { email: email });
  }

  registerOrgTeacher(data) {
    const url = 'coding-ville/register/school/teacher/payment';
    return this._globalService.post(url, data);
  }

  validateUserData(userData) {
    const url = 'coding-ville/register/user/validate';
    return this._globalService.post(url, userData);
  }

}

interface OrgTeacherData {
  plan_id: any;
  data: any;
}
