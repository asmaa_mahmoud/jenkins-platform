import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../../@core/utils/authentication.service';
import { Role } from '../../../@core/data/role';
import { GlobalService } from '../../../@core/utils/global.service';


@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private _globalService: GlobalService,
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          const user = data;
          const userRole = data.role;
          if (Role['Student'].includes(userRole))
            this._globalService.eventEmitter('landing_page', 'login', null, null, 'student');
          else if (Role['individualStudent'].includes(userRole))
            this._globalService.eventEmitter('landing_page', 'login', null, null, 'individual');
          if (Role['Teacher'].includes(userRole)) {
            this._globalService.eventEmitter('landing_page', 'login', null, null, 'teacher');
            if (user.hasLogged)
              this.router.navigate(['teacher/dashboard']);
            else
              this.router.navigate(['teacher/on-board']);
          } else if (Role['Student'].includes(userRole) || Role['individualStudent'].includes(userRole)) {
            // if (user.hasLogged)
            this.router.navigate(['student/dashboard']);
            // else
            //   this.router.navigate(['student/on-board']);
          } else if (Role['Admin'].includes(userRole)) {
            this.router.navigate(['admin/dashboard']);
          } else {
            this.router.navigate(['auth']);
          }
          // this.router.navigate([this.returnUrl]);
        },
        error => {
          console.log(error);
          this.error = error.error.errorMessage;
          this.loading = false;
        });
  }

  removeError() {
    this.error = null;
  }

}
