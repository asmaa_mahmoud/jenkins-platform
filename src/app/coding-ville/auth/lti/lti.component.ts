import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../@core/utils/global.service';
import { Role } from '../../../@core/data/role';
import { AuthenticationService } from '../../../@core/utils/authentication.service';

@Component({
  selector: 'ngx-lti',
  templateUrl: './lti.component.html',
  styleUrls: ['./lti.component.scss'],
})
export class LtiComponent implements OnInit {
  error;
  message;
  loginData;
  role;

  constructor(private route: ActivatedRoute, private router: Router,
    private _globalService: GlobalService,
    private _authService: AuthenticationService) {

  }

  ngOnInit() {
    try {
      this.route.queryParams.subscribe(params => {
        this.error = params['error'];
        this.message = params['message'];
        this.loginData = params['login_data'];
      });
      this.error = this.error === 'true' ? true : false;
      this.loginData = JSON.parse(decodeURIComponent(this.loginData)).Object;
    } catch (error) {
      localStorage.clear();
      this.router.navigate(['auth/login']);
    }
    this.checkAuthData();
  }

  checkAuthData() {
    if (this.error) {
      this._globalService.showTranslatedToast('danger', this.message, 'something-went-wrong');
      this.router.navigate(['auth/login']);
    } else {
      this._authService.currentUserSubject.next(this.loginData.user);
      this._authService.studentClassroomIDSubject.next(this.loginData.user.classroomId);
      this._authService.userStatusSubject.next(this.loginData.status);
      this.role = this.loginData.user.role;
      localStorage.setItem('AuthorizationData', JSON.stringify(this.loginData.user));
      localStorage.setItem('satellizer_token', this.loginData.token.token);
      localStorage.setItem('userStatus', JSON.stringify(this.loginData.status));
      if (Role['Student'].includes(this.role))
        localStorage.setItem('classroomID', JSON.stringify(this.loginData.user.classroomId));
      if (Role['Student'].includes(this.role) || Role['individualStudent'].includes(this.role)) {
        this.router.navigate(['student/dashboard']);
      } else if (Role['Teacher'].includes(this.role)) {
        this.router.navigate(['teacher/dashboard']);
      }
    }
  }

}
