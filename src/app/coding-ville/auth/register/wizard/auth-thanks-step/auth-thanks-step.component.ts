import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'ngx-auth-thanks-step',
  templateUrl: './auth-thanks-step.component.html',
  styleUrls: ['./auth-thanks-step.component.scss'],
})
export class AuthThanksStepComponent implements OnInit {

  @Input('userType') userType: string;
  @Input('userEmail') userEmail: string;

  @Output() removeCompleted = new EventEmitter<void>();

  ngOnInit() {

  }

  removeCompletedClicked() {
    this.removeCompleted.emit();
  }

}
