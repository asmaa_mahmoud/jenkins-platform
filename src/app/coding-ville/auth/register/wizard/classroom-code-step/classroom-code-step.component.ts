import { AuthService } from './../../../auth.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'ngx-classroom-code-step',
  templateUrl: './classroom-code-step.component.html',
  styleUrls: ['./classroom-code-step.component.scss'],
})
export class ClassroomCodeStepComponent implements OnInit {

  @Output() removeCompleted = new EventEmitter<void>();
  @Output() classroomCodeValidated = new EventEmitter<string>();
  @Output() noClassroomCode = new EventEmitter<string>();

  codeForm: FormGroup;

  classroom_code = new FormControl(null, [Validators.required]);

  codeError: string;

  constructor(private fb: FormBuilder, private _authService: AuthService) {}

  ngOnInit() {
    this.codeForm = this.fb.group({
      classroom_code: this.classroom_code,
    });
  }

  onCodeSubmit() {
    this.codeError = null;
    this._authService.validateClassroomCode(this.classroom_code.value).subscribe(
      (data: any) => {
        if (data.Object.isValid) {
          this.classroomCodeValidated.emit(this.classroom_code.value);
        } else {
          this.codeError = 'Incorrect classroom code';
        }
      }, (error: any) => {
        console.log(error);
        this.codeError = 'Something went wrong!';
      },
    );
  }

  onNoClassroomCode() {
    this.classroom_code.setValue(null);
    this.noClassroomCode.emit(this.classroom_code.value);
  }

  removeCompletedClicked() {
    this.codeError = null;
    this.removeCompleted.emit();
  }

}
