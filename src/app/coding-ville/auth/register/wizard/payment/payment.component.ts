import { GlobalService } from './../../../../../@core/utils/global.service';
import { AuthService } from './../../../auth.service';
import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from 'ngx-stripe';
@Component({
  selector: 'ngx-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit, AfterViewInit {

  @Input() selectedPlan;
  @Output() stepToSuccess = new EventEmitter<{ email: string, type: string }>();

  elements: Elements;
  cardData;

  genratedToken;
  paymentForm: FormGroup;
  loading = false;

  @Output() removeCompleted = new EventEmitter<void>();

  constructor(
    private fb: FormBuilder,
    private stripeService: StripeService,
    private _authService: AuthService,
    private _globalService: GlobalService) { }

  ngOnInit() {
    this.buildForm();
    console.log('data', this._authService.orgTeacherData.value);

  }

  buildForm() {
    const group = {
      card_input: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      cvv_input: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      expiration_input: new FormControl(null, [Validators.required, Validators.minLength(5), Validators.maxLength(5)]),
    };
    this.paymentForm = this.fb.group(group);

  }


  ngAfterViewInit() {
    this.initStripe();
  }

  initStripe() {
    this.stripeService.elements().subscribe(elements => {
      this.elements = elements;
      // Only mount the element the first time
      if (!this.cardData) {
        let cardNumber: StripeElement;
        let cardExpiration: StripeElement;
        let cardCVV: StripeElement;



        cardNumber = this.elements.create('cardNumber', {
          style: {
            base: {
              iconColor: '#666EE8',
            },
          },
        });
        cardExpiration = this.elements.create('cardExpiry', {
          style: {
            base: {
              iconColor: '#666EE8',
            },
          },
        });

        cardCVV = this.elements.create('cardCvc', {
          style: {
            base: {
              iconColor: '#666EE8',
            },
          },
        });




        cardNumber.mount('#card-element');
        cardExpiration.mount('#cardExpiration');
        cardCVV.mount('#cardCVV');


        this.cardData = {
          cardNumber: cardNumber,
          cardExpiration: cardExpiration,
          cardCVV: cardCVV,
        };
      }
    });
  }



  buy() {
    this.loading = true;
    this.stripeService
      .createToken(this.cardData.cardNumber, undefined)
      .subscribe(result => {
        if (result.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          this.genratedToken = result.token;

          this.submitData();

        } else if (result.error) {
          // Error creating the token
          this._globalService.showTranslatedToast('danger', null, 'something-went-wrong');

          this.loading = false;

        }
      }, error => {
        this.loading = false;
      });

  }

  submitData() {
    const savedData = this._authService.orgTeacherData.value;
    const userData = {
      ...savedData.data,
      plan_id: savedData.plan_id,
      stripe_token: this.genratedToken.id,
    };


    this._authService.registerOrgTeacher(userData).subscribe(
      res => {
        const userEmail = this._authService.orgTeacherData.value.data.email;
        const emmittedData = { email: userEmail, type: 'teacher' };
        this.stepToSuccess.emit(emmittedData);
        this.loading = false;
      },
      error => {
        this.loading = false;
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');

      });
  }

  onSubmitHandler() {

  }

  removeCompletedClicked() {
    this.removeCompleted.emit();
  }



}
