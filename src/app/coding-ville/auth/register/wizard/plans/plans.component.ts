import { GlobalService } from './../../../../../@core/utils/global.service';
import { AuthService } from './../../../auth.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TeacherPaymentService } from '../../../../accounts/teacher/teacher-payment/teacher-payment.service';

@Component({
  selector: 'ngx-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss'],
})
export class PlansComponent implements OnInit {

  @Output() selectedPlan = new EventEmitter<void>();
  @Output() removeCompleted = new EventEmitter<void>();

  plans = [];
  constructor(
    private _teacherPayment: TeacherPaymentService,
    private _authService: AuthService,
    private _globalService: GlobalService,
  ) { }

  ngOnInit() {
    this.getPlans();
  }

  getPlans() {
    this._teacherPayment.getPlans().subscribe(
      res => {
        this.plans = res.Object;
      },
      error => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
      });
  }


  choosePlan(plan) {

    const savedUserData = this._authService.orgTeacherData.value;
    const userData = {
      data: savedUserData ? savedUserData.data : null,
      plan_id: plan.id,
    };

    this._authService.orgTeacherData.next(userData);

    this.selectedPlan.next(plan);

  }

  removeCompletedClicked() {
    this.removeCompleted.emit();
  }
}
