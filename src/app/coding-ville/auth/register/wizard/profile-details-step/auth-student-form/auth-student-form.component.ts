import { AuthService } from './../../../../auth.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { GlobalService } from '../../../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-auth-student-form',
  templateUrl: './auth-student-form.component.html',
})
export class AuthStudentFormComponent implements OnInit, AfterContentInit {

  @Input('classroom_code') classroom_code: string;

  @Output() stepToSuccess = new EventEmitter<{ email: string, type: string }>();

  loading: boolean = false;

  backError: string;

  ages = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];

  selectedAge: number;

  username_pattern = '^[a-zA-Z0-9\u0620-\u064A_\\$@\\-]*$';
  fullname_pattern = '[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ ]+';

  studentRegisterForm: FormGroup;

  student_fullname = new FormControl(null, [Validators.required, Validators.pattern(this.fullname_pattern), Validators.minLength(2)]);
  student_nickname = new FormControl(null, [Validators.required, Validators.pattern(this.username_pattern), Validators.minLength(2)]);
  student_password = new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(24)]);
  student_age = new FormControl(null, [Validators.required]);
  student_email = new FormControl(null, [Validators.required, Validators.email]);
  roboGarden = new FormControl(null, [Validators.required, Validators.pattern('true')]);

  validationErrors = {
    email: false,
    username: false,
  };

  constructor(
    private _globalService: GlobalService,
    private fb: FormBuilder,
    private _authService: AuthService,
  ) { }

  ngOnInit() {
    this.studentRegisterForm = this.fb.group({
      student_fullname: this.student_fullname,
      student_nickname: this.student_nickname,
      student_password: this.student_password,
      student_age: this.student_age,
      student_email: this.student_email,
      roboGarden: this.roboGarden,
    });
    this._authService.stepBackFromForms.subscribe(() => {
      this.backError = null;
      this.selectedAge = null;
      this.student_age.setValue(this.selectedAge);
    });
  }

  ngAfterContentInit() {
    this._authService.classroomCodeChanges.subscribe(data => {
      if (data.hasCode) {
        this.student_age.setValidators([]);
        this.student_email.setValidators([]);
        this.student_age.updateValueAndValidity();
        this.student_email.updateValueAndValidity();
      } else {
        this.student_age.setValidators([Validators.required]);
        this.student_email.setValidators([Validators.required, Validators.email]);
        this.student_age.updateValueAndValidity();
        this.student_email.updateValueAndValidity();
      }
    });
  }

  onChangeStudentAge() {
    this.student_age.setValue(this.selectedAge);
  }

  onSubmitHandler() {
    if (this.studentRegisterForm.invalid) {
      return;
    }
    this.loading = true;
    this.backError = null;
    if (this.classroom_code) {
      const body = {
        classroom_code: this.classroom_code,
        full_name: this.student_fullname.value,
        username: this.student_nickname.value,
        password: this.student_password.value,
      };
      this._authService.registerStudentWithCode(body).subscribe(
        (data: any) => {
          this.loading = false;
          this.stepToSuccess.emit({ email: this.student_email.value, type: null });
          this._globalService.eventEmitter('landing_page', 'sign_up', null, null, 'student');
        }, (error: any) => {
          this.loading = false;
          console.log(error);
          this.backError = error.error['errorMessage'];
        },
      );
    } else {
      const body = {
        full_name: this.student_fullname.value,
        username: this.student_nickname.value,
        password: this.student_password.value,
        age: this.student_age.value,
        email: this.student_email.value,
      };
      this._authService.registerStudentNoCode(body).subscribe(
        (data: any) => {
          this.loading = false;
          const type = this.selectedAge > 13 ? 'oldStudent' : 'youngStudent';
          this.stepToSuccess.emit({ email: this.student_email.value, type: type });
          this._globalService.eventEmitter('landing_page', 'sign_up', null, null, 'individual');
        }, (error: any) => {
          this.loading = false;
          console.log(error);
          this.backError = error.error.errorMessage;
        },
      );
    }
  }

  validateUserData(type, value) {

    if (!value || (type === 'username' && !this.student_nickname.valid) || (type === 'email' && !this.student_email.valid)) {
      return;
    }

    const userData = type === 'username' ? { username: value } : { email: value };

    this._authService.validateUserData(userData).subscribe(
      res => {
        if (res.Object.valid === 0) {
          this.validationErrors[type] = false;
        } else {
          this.validationErrors[type] = true;
        }
      },
      error => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
      });
  }

}
