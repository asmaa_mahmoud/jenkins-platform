import { env } from './../../../../../../@core/env/env';
import { AuthService } from './../../../../auth.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GlobalService } from '../../../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-auth-teacher-form',
  templateUrl: './auth-teacher-form.component.html',
  styleUrls: ['./auth-teacher-form.component.scss'],
})
export class AuthTeacherFormComponent implements OnInit {

  @Output() stepToSuccess = new EventEmitter<{ email: string, type: string }>();

  loading: boolean = false;

  backError: string;

  username_pattern = '^[a-zA-Z0-9\u0620-\u064A_\\$@\\-]*$';
  isOrg = env.isOrg;
  fullname_pattern = '[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ ]+';

  teacherRegisterForm: FormGroup;

  teacher_fullname = new FormControl(null, [Validators.required, Validators.pattern(this.fullname_pattern), Validators.minLength(2)]);
  teacher_nickname = new FormControl(null, [Validators.required, Validators.pattern(this.username_pattern), Validators.minLength(2)]);
  teacher_email = new FormControl(null, [Validators.required, Validators.email]);
  teacher_password = new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(24)]);
  teacher_school = new FormControl(null, [Validators.required, Validators.minLength(2)]);
  roboGarden = new FormControl(null, [Validators.required, Validators.pattern('true')]);
  teacher_province = new FormControl(null, [Validators.required]);
  teacher_city = new FormControl(null, [Validators.required]);
  teacher_country = new FormControl(null, [Validators.required]);
  provinces: any[];
  cities: any[] = [];
  countryID: any;
  provinceID: any;
  countries: any[] = [];


  validationErrors = {
    email: null,
    username: null,
  };

  constructor(
    private _globalService: GlobalService,
    private fb: FormBuilder, private _authService: AuthService) { }

  ngOnInit() {
    this.buildForm();
    this.checkenv();
    this._authService.stepBackFromForms.subscribe(() => this.backError = null);
  }

  buildForm() {
    const group = {
      teacher_fullname: this.teacher_fullname,
      teacher_nickname: this.teacher_nickname,
      teacher_email: this.teacher_email,
      teacher_password: this.teacher_password,
      teacher_school: this.teacher_school,
      teacher_province: this.teacher_province,
      teacher_city: this.teacher_city,
      roboGarden: this.roboGarden,
    };

    if (this.isOrg) {
      this._authService.getCountries().subscribe(data => {
        this.countries = data.Object;
      });
      group['teacher_country'] = this.teacher_country;
    }
    this.teacherRegisterForm = this.fb.group(group);
  }

  checkenv() {
    if (this.isOrg) {
      this._authService.getCountries().subscribe(data => {
        this.countries = data.Object;
        this.getProvinces(this.countries[0].id);
      });
    } else {
      this.getProvinces(39);
    }
  }

  getProvinces(countryID) {
    this._authService.getProvinces(countryID).subscribe(
      res => {
        if (res) {
          this.provinces = res.Object;
          this.getCities(this.provinces[0].id);
        }
      });
  }

  getCities(provinceID) {
    this._authService.getProvinceCities(provinceID).subscribe(
      res => {
        if (res) {
          this.cities = res.Object;
        }
      });
  }


  changeCountry() {
    this.getProvinces(this.countryID);
    this.provinceID = null;
    this.teacherRegisterForm.controls['teacher_province'].setErrors({ 'required': true });
  }

  changeProvince() {
    this.getCities(this.provinceID);
  }

  onSubmitHandler() {
    if (this.teacherRegisterForm.invalid || !this.validationErrors.email || !this.validationErrors.username) {
      return;
    }
    this.backError = null;


    const body = {
      full_name: this.teacher_fullname.value,
      username: this.teacher_nickname.value,
      email: this.teacher_email.value,
      password: this.teacher_password.value,
      school_name: this.teacher_school.value,
      province_id: this.teacher_province.value,
      province: this.provinces.find(x => x.id === this.teacher_province.value).name,
      country_id: this.countryID,
      country: this.countries.find(x => x.id === this.countryID).name,
      city: this.teacher_city.value,
      terms_agreement: true,
    };

    if (!this.isOrg) {
      this.register(body);


    } else {

      body['terms_agreement'] = true;

      const savedUserData = this._authService.orgTeacherData.value;
      const userData = {
        data: body,
        plan_id: savedUserData ? savedUserData.plan_id : null,
      };


      this._authService.orgTeacherData.next(userData);

      this.stepToSuccess.emit({ email: this.teacher_email.value, type: 'teacher' });
    }

  }

  register(body) {
    this.loading = true;

    this._authService.registerTeacher(body).subscribe(
      (data: any) => {
        this.loading = false;
        this.stepToSuccess.emit({ email: this.teacher_email.value, type: 'teacher' });
        this._globalService.eventEmitter('landing_page', 'sign_up', null, null, 'teacher');
      }, (error: any) => {
        this.loading = false;
        this.backError = error.error['errorMessage'];
      },
    );
  }

  validateUserData(type, value) {

    if (!value || (type === 'username' && !this.teacher_nickname.valid) || (type === 'email' && !this.teacher_email.valid)) {
      return;
    }

    const userData = type === 'username' ? { username: value } : { email: value };

    this._authService.validateUserData(userData).subscribe(
      res => {
        if (res.Object.valid === 0) {
          this.validationErrors[type] = false;
        } else {
          this.validationErrors[type] = true;
        }
      },
      error => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
      });
  }

}
