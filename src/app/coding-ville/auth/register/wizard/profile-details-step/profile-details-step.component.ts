import { AuthService } from './../../../auth.service';
import { Subject, Observable } from 'rxjs';
import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'ngx-profile-details-step',
  templateUrl: './profile-details-step.component.html',
  styleUrls: ['./profile-details-step.component.scss'],
})
export class ProfileDetailsStepComponent implements OnInit {

  @Input('role') role: string;
  @Input('classroom_code') classroom_code: string;

  @Output() removeCompleted = new EventEmitter<void>();
  @Output() stepToSuccess = new EventEmitter<{email: string, type: string}>();

  emitClassroomCodeChange = new Subject<{hasCode: boolean, code: string}>();

  constructor(private _authService: AuthService) {}

  ngOnInit() {
  }

  removeCompletedClicked() {
    this._authService.stepBackFromForms.next();
    this.removeCompleted.emit();
  }

  emitSuccess(data: {email: string, type: string}) {
    this.stepToSuccess.emit(data);
  }

}
