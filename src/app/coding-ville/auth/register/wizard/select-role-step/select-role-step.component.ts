import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'ngx-select-role-step',
  templateUrl: './select-role-step.component.html',
  styleUrls: ['./select-role-step.component.scss'],
})
export class SelectRoleStepComponent implements OnInit {

  @Input('role') role: string;

  @Output() removeCompleted = new EventEmitter<void>();
  @Output() roleSelected = new EventEmitter<string>();

  ngOnInit() {

  }

  removeCompletedClicked() {
    this.removeCompleted.emit();
  }

  onRoleSelected(selectedRole: string) {
    this.roleSelected.emit(selectedRole);
  }
}
