import { env } from './../../../../@core/env/env';
import { AuthService } from './../../auth.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbStepperComponent, NbStepComponent } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-auth-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class AuthWizardComponent implements OnInit, AfterViewInit {

  @ViewChild('stepper', { static: true }) stepper: NbStepperComponent;

  userType: string;

  userEmail: string;

  showClassroomCode: boolean = true;

  classroom_code: string;

  roleForm: FormGroup;

  profileControlForm: any;

  step_one_label: string = 'Student or Teacher';

  isOrg = env.isOrg;
  clientName = env.clientName;
  selectedPlan: any;

  constructor(
    private fb: FormBuilder,
    private _authService: AuthService,
    private _route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.setUpForm();
  }

  ngAfterViewInit() {
    this.checkRoute();
  }
  checkRoute() {
    this._route.params.subscribe(
      res => {
        console.log('res', res);

        if ((res.type === 'student' || res.type === 'teacher') && !this.isOrg) {
          this.onRoleSelected(res.type);
        } else {
          this.router.navigate(['auth/registration']);
        }
      });

  }

  setUpForm() {
    this.roleForm = this.fb.group({
      selectedRole: ['', Validators.required],
    });
  }

  get accessRole() {
    return this.roleForm.controls.selectedRole;
  }

  onRoleSelected(role: string) {
    this.accessRole.setValue(role);
    this.step_one_label = role;
    if (role === 'student') {
      this.showClassroomCode = true;
      this.profileControlForm = 'student';
      this.stepper.next();
    } else if (role === 'teacher') {
      this.showClassroomCode = false;
      this.profileControlForm = 'teacher';
      console.log('----------');

      this.stepper.next();
    }
  }

  onClassroomCodeValidated(code: string) {
    this.classroom_code = code;
    this._authService.classroomCodeChanges.next({ hasCode: true, code: this.classroom_code });
    this.stepper.next();
  }

  onNoClassroomCode(code: string) {
    this.classroom_code = code;
    this._authService.classroomCodeChanges.next({ hasCode: false, code: this.classroom_code });
    this.stepper.next();
  }

  onSubmitSuccessfully(data: { email: string, type: string }) {
    console.log('submitted', data);

    this.userType = data.type;
    this.userEmail = data.email;
    this.stepper.next();
  }

  removeCompleted(step: NbStepComponent, event) {
    step.reset();
  }

  getSelectedPlan(plan) {
    this.selectedPlan = plan;
    this.stepper.next();
  }

}
