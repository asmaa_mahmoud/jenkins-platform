import { AuthService } from '../../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';


@Component({
  selector: 'ngx-reset-password-email',
  templateUrl: './reset-password-email.component.html',
  styleUrls: ['./reset-password-email.component.scss'],
})
export class ResetPasswordEmailComponent implements OnInit {

  resetPasswordEmailForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  sentSuccessfully = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService) {}

  ngOnInit() {
    this.resetPasswordEmailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.resetPasswordEmailForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.resetPasswordEmailForm.invalid) {
      return;
    }

    this.loading = true;
    this.authService.resetPasswordEmail(this.f.email.value)
      .pipe(first())
      .subscribe(
        res => {
          this.sentSuccessfully = true;
          this.loading = false;
        },
        error => {
          console.log(error);
          this.error = error.error.errorMessage;
          this.loading = false;
        });
  }

}
