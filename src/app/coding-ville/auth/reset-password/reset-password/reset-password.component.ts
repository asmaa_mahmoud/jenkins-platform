import { AuthService } from './../../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';


@Component({
  selector: 'ngx-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  password: any;
  retype_password: any;
  passwordsMatch: boolean;
  retypePasswordMsg: string;
  email: string;
  token: string;
  resetSuccessfully = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(24)]],
      retype_password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(24)]],
    });
    this.email = this.route.snapshot.queryParams['email'] || '';
    this.token = this.route.snapshot.queryParams['token'] || '';
  }

  // convenience getter for easy access to form fields
  get f() { return this.resetPasswordForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.resetPasswordForm.invalid || this.f.password.value !== this.f.retype_password.value) {
      return;
    }

    this.loading = true;
    this.authService.resetPassword(this.email, this.token, this.f.password.value)
      .pipe(first())
      .subscribe(
        res => {
          this.loading = false;
          this.resetSuccessfully = true;
        },
        error => {
          console.log(error);
          this.error = error.error.errorMessage;
          this.loading = false;
        });
  }

  validateRetype() {
    if (!this.f.retype_password.value || !this.f.password.value) {
      this.retypePasswordMsg = '';
    } else if (this.f.retype_password.value !== this.f.password.value) {
      this.retypePasswordMsg = 'passwords must match';
      this.passwordsMatch = false;
    } else {
      this.retypePasswordMsg = 'passwords match';
      this.passwordsMatch = true;
    }
  }

}
