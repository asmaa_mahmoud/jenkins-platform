import { RobogardenPrivacyPolicyComponent } from './robograden/robogarden-privacy-policy/robogarden-privacy-policy.component';
import { RobogradenPolicyUsageComponent } from './robograden/robograden-policy-usage/robograden-policy-usage.component';
import { MindfuelPrivacyPolicyComponent } from './mindfuel/mindfuel-privacy-policy/mindfuel-privacy-policy.component';
import { MindfuelUsageTermsComponent } from './mindfuel/mindfuel-usage-terms/mindfuel-usage-terms.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PoliciesComponent } from './policies.component';

const routes: Routes = [
  {
    path: '',
    component: PoliciesComponent,

    children: [
      {
        path: 'mindfuel/privacy-policy',
        component: MindfuelPrivacyPolicyComponent,
      },
      {
        path: 'mindfuel/terms-of-use',
        component: MindfuelUsageTermsComponent,
      },
      {
        path: 'robogarden/privacy-policy',
        component: RobogardenPrivacyPolicyComponent,
      },
      {
        path: 'robogarden/terms-of-use',
        component: RobogradenPolicyUsageComponent,
      },
    ],
  }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class PoliciesRoutingModule { }

export const components = [
  MindfuelPrivacyPolicyComponent,
  MindfuelUsageTermsComponent,
  RobogardenPrivacyPolicyComponent,
  RobogradenPolicyUsageComponent,
  PoliciesComponent,
];


