import { PoliciesRoutingModule, components } from './policies-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
  imports: [
    CommonModule,
    PoliciesRoutingModule,
   ThemeModule,
  ],
  declarations: [
    ...components,
  ],
})
export class PoliciesModule { }
