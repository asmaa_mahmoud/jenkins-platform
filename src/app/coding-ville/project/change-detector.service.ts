import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ChangeDetectorService {

  projectHasChangesForReload: BehaviorSubject<boolean> = new BehaviorSubject(false);
  projectHasChangesForSave: BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor() { }

  setProjectAsChanged() {
    this.projectHasChangesForSave.next(true);
    this.projectHasChangesForReload.next(true);
  }

  setNoChangesForReload() {
    this.projectHasChangesForReload.next(false);
  }

  setNoChangesForSave() {
    this.projectHasChangesForSave.next(false);
  }
}
