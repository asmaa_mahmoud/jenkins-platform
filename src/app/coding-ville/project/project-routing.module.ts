import { AuthorizationGuard } from './../../@core/utils/authorization.guard';
import { AuthGuard } from './../../@core/utils/auth.guard';
import { Role } from './../../@core/data/role';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ProjectComponent } from './project.component';



const routes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            redirectTo: 'create',
            pathMatch: 'full',
        },
        // project-game/create
        {
            path: 'create',
            component: ProjectComponent,
            canActivate: [AuthGuard],
        },
        // project-game/:id
        {
            path: ':id',
            component: ProjectComponent,
            canActivate: [AuthGuard],
        },
        // project-game/remix/:id
        {
            path: 'remix/:id',
            component: ProjectComponent,
            canActivate: [AuthGuard],
        },



        // project-game/journeys/project-game/:adventureId/:missionId/:jourenyId
        {
            path: 'journey/:adventureId/:missionId/:jourenyId',
            component: ProjectComponent,
            canActivate: [AuthGuard],
        },
        {
            path: 'journey/:adventureId/:missionId/:jourenyId/:courseId',
            component: ProjectComponent,
            canActivate: [AuthGuard],
        },
        {
            path: 'journey/:adventureId/:missionId/:jourenyId/:courseId/:studentId',
            component: ProjectComponent,
            canActivate: [AuthGuard],
        },
        {
            path: 'pre-journey/:missionId/:activityId',
            component: ProjectComponent,
            canActivate: [AuthGuard],
        },
        {
            path: 'pre-journey/:missionId/:activityId/:courseId',
            component: ProjectComponent,
            canActivate: [AuthGuard],
        },
        {
            path: 'pre-journey/:missionId/:activityId/:courseId/:studentId',
            component: ProjectComponent,
            canActivate: [AuthGuard],
        },

    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class ProjectRoutingModule {
}
