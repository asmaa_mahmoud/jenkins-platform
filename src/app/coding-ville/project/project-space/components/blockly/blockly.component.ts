import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { BlocklyProvider } from './../../../../../@core/utils/blockly.service';
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { GlobalService } from '../../../../../@core/utils/global.service';
import { WindowRef } from '../../../../../@core/utils/window.service';

declare var Blockly: any;
Blockly.Msg['DELETE_VARIABLE'] = 'Delete variable %1';
declare var tippy: any;

@Component({
  selector: 'ngx-blockly-editor',
  templateUrl: './blockly.component.html',
  styleUrls: ['./blockly.component.scss'],
})

export class BlocklyComponent implements OnInit, AfterViewInit, OnDestroy {

  toolbox_xml: any;
  categories: any;
  toolbox_categories = [
    { name: 'events' },
    { name: 'actions' },
    { name: 'looks' },
    { name: 'sensors' },
    { name: 'values' },
    { name: 'gameplay' },
    { name: 'controls' },
    { name: 'math' },
    { name: 'variables' },
  ];
  translatedCategory: string;
  categoryDescription: string;

  subscriptions = new Subscription();

  // To Track changes in blockly code
  jsCode: string;

  constructor(
    private _Window: WindowRef,
    private _globalService: GlobalService,
    private blocklyProvider: BlocklyProvider,
    private _translateService: TranslateService,
  ) {

  }

  ngOnInit() {
    this.getBlocks();
  }

  getBlocks() {
    const url: string = 'coding-ville/projects/toolbox';
    this._globalService.get(url).subscribe((res) => {
      this.categories = res.Object;
      this.categories = this.toolbox_categories;
      this.createBlocks();
    });
  }
  ngAfterViewInit() {
  }

  createBlocks() {

    this.toolbox_xml = '<xml id="toolbox">';
    this.toolbox_xml += `
      <category name='events'>
        <block type="on_start"></block>
        <block type="control_button_clicked"></block>
        <block type="joystick_pressed"></block>
        <block type="every_millis_event">
          <value name="millis">
            <block type="math_number">
              <field name="NUM">0</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">0</field>
            </shadow>
          </value>
        </block>
        <block type="after_millis_event">
          <value name="millis">
            <block type="math_number">
              <field name="NUM">0</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">0</field>
            </shadow>
          </value>
        </block>
        <block type="robo_collide_with">
          <value name="obj_type">
          <shadow type="object_type">
          </shadow>
          <block type="object_type"></block>
          </value>
        </block>
        <block type="robo_collects">
          <value name="obj_type">
          <shadow type="collectables_list">
          </shadow>
          <block type="collectables_list"></block>
          </value>
        </block>
      </category>
      <category name='actions'>
        <block type='move_forward'></block>
        <block type='move_backward'></block>
        <block type='swap_right'></block>
        <block type='swap_left'></block>
        <block type="say">
          <value name="message">
            <shadow type="text">
              <field name="TEXT">message</field>
            </shadow>
            <block type="text">
              <field name="TEXT">message</field>
            </block>
          </value>
        </block>
        <block type="move_to_xy">
          <value name="X">
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
          <value name="Y">
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
        </block>
      </category>
      <category name="Looks">
        <block type='set_obj_at'>
          <value name="X">
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
          <value name="obj_type">
            <shadow type="object_type">
            </shadow>
            <block type="object_type"></block>
          </value>
          <value name="Y">
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
        </block>
        <block type='set_obj_at_with_name'>
        <value name="X">
          <block type="math_number">
            <field name="NUM">1</field>
          </block>
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="obj_type">
          <shadow type="object_type">
          </shadow>
          <block type="object_type"></block>
        </value>
        <value name="Y">
          <block type="math_number">
            <field name="NUM">1</field>
          </block>
          <shadow type="math_number">
            <field name="NUM">1</field>
          </shadow>
        </value>
        <value name="obj_name">
          <shadow type="text">
            <field name="TEXT">name</field>
          </shadow>
          <block type="text">
            <field name="TEXT">name</field>
          </block>
        </value>
      </block>
        <block type="remove_all_with_type">
          <value name="type">
            <shadow type="object_type"></shadow>
            <block type="object_type"></block>
          </value>
        </block>
        <block type="remove_touched_obj"></block>
        <block type='set_cell_text'>
          <value name="x">
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
          <value name="text">
            <shadow type="text">
              <field name="TEXT">R</field>
            </shadow>
            <block type="text">
              <field name="TEXT">R</field>
            </block>
          </value>
          <value name="y">
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
        </block>
        <block type="move_obj">
          <value name="obj_type">
            <shadow type="object_type">
            </shadow>
            <block type="object_type"></block>
          </value>
          <value name="obj_name">
            <shadow type="text">
              <field name="TEXT">name</field>
            </shadow>
            <block type="text">
              <field name="TEXT">name</field>
            </block>
          </value>
        </block>
        <block type="set_color">
          <value name="color">
            <shadow type="cell_colors">
            </shadow>
            <block type="cell_colors"></block>
          </value>
          <value name="x">
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
          <value name="y">
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
        </block>
        <block type="hide_button"></block>
      </category>
      <category name="Sensors">
        <block type="get_cell_color"></block>
        <block type="get_robo_xy"></block>
        <block type="get_cell_text"></block>
      </category>
      <category name="Values">
        <block type="cell_colors"></block>
        <block type="object_type"></block>
        <block type="collectables_list"></block>
        <block type="text"></block>
      </category>
      <category name="Gameplay">
        <block type="get_score"></block>
        <block type="get_left_lives"></block>
        <block type="set_score">
          <value name="score">
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
          </value>
        </block>
        <block type="set_left_lives">
          <value name="lives">
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
          </value>
        </block>
        <block type="change_player_score">
          <value name="amount">
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
          </value>
        </block>
        <block type="change_left_lives">
          <value name="amount">
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
          </value>
        </block>
        <block type="go_to_level"></block>
        <block type="get_current_level"></block>
        <block type="make_winner"></block>
        <block type="game_over"></block>
      </category>
      <category name="Control">
        <block type="logic_boolean"></block>
        <block type="logic_compare"></block>
        <block type="controls_if"></block>
        <block type="controls_if">
          <mutation else="1"></mutation>
        </block>
        <block type="controls_repeat_ext">
        <value name="TIMES">
          <shadow type="math_number">
            <field name="NUM">10</field>
          </shadow>
          <block type="math_number">
            <field name="NUM">10</field>
          </block>
        </value>
      </block>
      <block type="controls_whileUntil"></block>

      <block type="controls_forEach"></block>

      <block type="controls_flow_statements"></block>
      </category>
      <category name="math">
        <block type="math_number"></block>
        <block type="math_arithmetic">
          <value name="A">
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
          <value name="B">
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
          </value>
        </block>
        <block type="math_random_int">
          <value name="FROM">
            <shadow type="math_number">
              <field name="NUM">1</field>
            </shadow>
            <block type="math_number">
              <field name="NUM">1</field>
            </block>
          </value>
          <value name="TO">
            <shadow type="math_number">
              <field name="NUM">100</field>
            </shadow>
            <block type="math_number">
              <field name="NUM">100</field>
            </block>
          </value>
        </block>
        <block type="math_number_property">
        </block>
      </category>
      <category name="Variables" custom="VARIABLE"></category>
      `;

    this.toolbox_xml += '</xml>';
    const options = {
      path: './assets/js/google-blockly/',
      toolbox: this.toolbox_xml,
      scrollbars: true,
      readOnly: false,
      zoom: {
        controls: false,
        wheel: false,
        startScale: 1,
        maxScale: 3,
        minScale: 0.3,
        scaleSpeed: 1.2,
      },
      grid: {
        spacing: 20,
        length: 2,
        colour: '#888',
        snap: true,
      },
    };
    const workspacePlayground = Blockly.inject('blocklyDiv', options);
    // ===========================================
    if (this.blocklyProvider.instructions) {
      this.blocklyProvider.setWorkspace();
    }
    // ====================================
    if (!this.blocklyProvider.initialized.getValue()) {
      this.blocklyProvider.initialized.next(true);
    }
    const loop = setInterval(() => {
      if (this.blocklyProvider.getJSCode()) {
        this.jsCode = this.blocklyProvider.getJSCode();
        workspacePlayground.addChangeListener(this.workSpaceChangeDetector.bind(this));
        clearInterval(loop);
      }
    }, 100);


    const icons = document.getElementsByClassName('blocklyTreeIconNone');
    for (let i = 0; i < icons.length; i++) {
      this.getTextTranslation(this.categories[i].name);
      const parent = document.getElementsByClassName('blocklyTreeIconNone')[i].parentElement;
      parent.children[2].textContent = this.translatedCategory;
      parent.parentElement.classList.add('tree_' + this.translatedCategory.toLowerCase());
      tippy(parent, {
        content: this.categoryDescription,
        boundary: 'window',
        placement: 'right',
        theme: 'nebularGreen',
      });
      icons[i].classList.add(this.translatedCategory.toLowerCase());
    }
  }

  compareTwoCodes(code1, code2) {
    // removing commented lines
    code1 = code1.replace(/^\/\/.*$/gm, '');
    code2 = code2.replace(/^\/\/.*$/gm, '');
    // removing white spaces
    code1 = code1.replace(/ |\n/gm, '');
    code2 = code2.replace(/ |\n/gm, '');
    return code1 === code2;
  }


  workSpaceChangeDetector(event) {
    if (!this.compareTwoCodes(this.blocklyProvider.getJSCode(), this.jsCode)) {
      this.jsCode = this.blocklyProvider.getJSCode();
      this.blocklyProvider.setWorkSpaceAsChanged();
    }
  }

  onWorkspaceChange(event) {
    const workspace = Blockly.Workspace.getById(event.workspaceId);
    const block = workspace.getBlockById(event.blockId);
  }

  getTextTranslation(text) {
    const sub = this._translateService.get(text).subscribe(
      res => {
        this.translatedCategory = res;
      });
    this.subscriptions.add(sub);

    const sub2 = this._translateService.get(text + '_category').subscribe(
      res => {
        this.categoryDescription = res;
      });
    this.subscriptions.add(sub2);
  }
  ngOnDestroy() {
    this.blocklyProvider.setInstructions(null);
    if (Blockly.getMainWorkspace()) {
      Blockly.getMainWorkspace().clear();
    }

    this.subscriptions.unsubscribe();
  }

}
