import { tiles } from './../constansts';
import { filter, map } from 'rxjs/operators';
import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, Input, OnChanges } from '@angular/core';
import * as createjs from 'createjs-module';
import { Level, Item, Dimensions, Color } from './models/interfaces';
import * as pathesJson from './pathes.json';
import { BoardService } from './board.service';

const pathes: any = (pathesJson as any).default;

@Component({
  selector: 'ngx-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})

export class BoardComponent implements OnChanges, OnInit, AfterViewInit {
  stage;
  cell = new Image();
  cellSize;
  @Input() level: Level;
  cols: number;
  rows: number;
  selectedBrush: string;
  @ViewChild('board', { static: false }) board: ElementRef;

  constructor(private boardService: BoardService) { }

  ngOnInit() {
    this.boardService.selectedBrush.asObservable().subscribe(data => {
      this.selectedBrush = data;
    });
  }

  ngAfterViewInit() {
    this.init();
  }

  ngOnChanges(changes) {
    this.stage && this.stage.removeAllChildren();
    this.init();
  }

  init() {
    this.cols = this.level.dimensions.cols;
    this.rows = this.level.dimensions.rows;
    this.cell.src = pathes[this.cols]['cell'];
    this.cell.onload = () => {
      this.cellSize = this.cell.width;
      this.board.nativeElement.width = this.cols * this.cellSize;
      this.board.nativeElement.height = this.rows * this.cellSize;
      this.stage = new createjs.Stage('board');
      this.stage.enableMouseOver();
      createjs.Touch.enable(this.stage);
      createjs.Ticker.on('tick', this.stage);
      this.drawMap();
      this.drawColors();
      this.drawItems();
      this.drawHiddens();
    };
  }

  drawMap() {
    for (let x = 0; x < this.cols; x++) {
      for (let y = 0; y < this.rows; y++) {
        this.drawCell(x * this.cellSize, y * this.cellSize, this.cell);
      }
    }
  }

  drawItems() {
    this.level.items.forEach(item => {
      this.drawItem((item.x - 1) * this.cellSize, (item.y - 1) * this.cellSize, pathes[this.cols][item.type]);
    });
  }

  // For Color Picker
  // drawColors() {
  //   const hiddenCells = [];
  //   this.level.colors.forEach(color => {
  //     if (color.color === 'hidden') {
  //       hiddenCells.push(color);
  //     } else {
  //       const cell = this.stage.getObjectUnderPoint((color.x - 1) * this.cellSize, (color.y - 1) * this.cellSize, 2);
  //       cell.name = 'coloredCell';
  //       const colored = this.drawColor((color.x - 1) * this.cellSize, (color.y - 1) * this.cellSize,
  //         this.cellSize, this.cellSize, color.color, 'color');
  //       colored.compositeOperation = 'color-dodge';
  //     }
  //   });
  //   hiddenCells.forEach(cell => {
  //     const layers = this.stage.getObjectsUnderPoint((cell.x - 1) * this.cellSize, (cell.y - 1) * this.cellSize, 1);
  //     layers.forEach(layer => {
  //       layer.alpha = 0.01;
  //     });
  //   });
  // }

  drawColors() {
    this.level.colors.forEach(color => {
      if (color.color !== 'hidden') {
        const cell = this.stage.getObjectUnderPoint((color.x - 1) * this.cellSize, (color.y - 1) * this.cellSize, 2);
        this.stage.removeChild(cell);
        this.drawCell((color.x - 1) * this.cellSize, (color.y - 1) * this.cellSize, pathes[this.cols][color.color]);
      }
    });
  }

  drawHiddens() {
    this.level.colors.forEach(color => {
      if (color.color === 'hidden') {
        const cell = this.findElementUnderPoint(this.stage, (color.x - 1) * this.cellSize, (color.y - 1) * this.cellSize, 'cell')[0];
        if (cell) cell.alpha = 0.01;
      }
    });
  }

  drawCell(x, y, image) {
    const bitmap = new createjs.Bitmap(image);
    bitmap.x = x;
    bitmap.y = y;
    bitmap.name = 'cell';
    bitmap.on('mouseover', this.overCell);
    bitmap.on('mouseout', this.outCell);
    bitmap.on('click', this.clickCell);
    this.stage.addChild(bitmap);
    return bitmap;
  }

  drawItem(x, y, image, name = 'crystal') {
    const bitmap = new createjs.Bitmap(image);
    bitmap.x = x;
    bitmap.y = y;
    bitmap.name = name;
    bitmap.on('mouseover', this.eraseIn);
    bitmap.on('mouseout', this.eraseOut);
    bitmap.on('click', this.erase);
    this.stage.addChild(bitmap);
  }

  setAlphaOfColorLayerOfCell(x, y, alpha) {
    const layers = this.stage.getObjectsUnderPoint(x, y, 1);
    const colorLayer = layers.find(layer => layer.name === 'color');
    colorLayer.alpha = alpha;
  }

  overCell = (event) => {
    const cell = event.target;
    if (cell.alpha === 1) {
      if (this.selectedBrush && this.selectedBrush !== 'show' && this.selectedBrush !== 'erase') {
        this.drawColor(cell.x, cell.y, this.cellSize, this.cellSize, 'DeepSkyBlue', 'buildHover');
      }
    } else if (cell.alpha === 0.01) {
      if (this.selectedBrush === 'show') {
        this.drawColor(cell.x, cell.y, this.cellSize, this.cellSize, 'DeepSkyBlue', 'buildHover');
      }
    }
  }

  outCell = (event) => {
    if (this.selectedBrush && this.selectedBrush !== 'erase') {
      this.stage.removeChild(this.stage.getChildByName('buildHover'));
    }
  }

  clickCell = (event) => {
    const cell = event.target;
    if (this.selectedBrush && cell.alpha === 1) {
      if (this.selectedBrush === 'hide') {
        this.stage.removeChild(cell);
        const newCell = this.drawCell(cell.x, cell.y, pathes[this.cols]['default']);
        newCell.alpha = 0.01;
        this.level.colors = this.deleteColor(cell.x / this.cellSize + 1, cell.y / this.cellSize + 1, this.level.colors);
        if (cell.name === 'coloredCell') {
          this.setAlphaOfColorLayerOfCell(cell.x, cell.y, 0.01);
        }
        this.level.colors.push({ x: cell.x / this.cellSize + 1, y: cell.y / this.cellSize + 1, color: 'hidden' });
      } else if (this.selectedBrush.includes('color') && cell.name !== 'coloredCell') {
        const color = this.selectedBrush.split(':')[1];
        const colored = this.drawColor(cell.x, cell.y, this.cellSize, this.cellSize, color, 'color');
        cell.name = 'coloredCell';
        colored.compositeOperation = 'color-dodge';
        this.level.colors.push({ x: cell.x / this.cellSize + 1, y: cell.y / this.cellSize + 1, color });
      } else if (tiles.includes(this.selectedBrush)) {
        this.stage.removeChild(cell);
        this.drawCell(cell.x, cell.y, pathes[this.cols][this.selectedBrush]);
        this.level.colors = this.deleteColor(cell.x / this.cellSize + 1, cell.y / this.cellSize + 1, this.level.colors);
        this.level.colors.push({ x: cell.x / this.cellSize + 1, y: cell.y / this.cellSize + 1, color: this.selectedBrush });
      } else if (this.selectedBrush && this.selectedBrush !== 'show' && this.selectedBrush !== 'erase') {
        this.drawItem(cell.x, cell.y, pathes[this.cols][this.selectedBrush], this.selectedBrush);
        this.level.items.push({ x: cell.x / this.cellSize + 1, y: cell.y / this.cellSize + 1, type: this.selectedBrush });
      }
      this.boardService.setLevelsAsChanged();
    } else if (cell.alpha === 0.01) {
      if (this.selectedBrush === 'show') {
        cell.alpha = 1;
        this.level.colors = this.deleteColor(cell.x / this.cellSize + 1, cell.y / this.cellSize + 1, this.level.colors, 'hidden');
        if (cell.name === 'coloredCell') {
          this.setAlphaOfColorLayerOfCell(cell.x, cell.y, 1);
        }
        this.boardService.setLevelsAsChanged();
      }
    }
    this.stage.removeChild(this.stage.getChildByName('buildHover'));
  }

  erase = (event) => {
    if (this.selectedBrush === 'erase') {
      const cell = event.target;
      this.stage.removeChild(cell);
      this.stage.removeChild(this.stage.getChildByName('eraseHover'));
      const item: Item = { x: cell.x / this.cellSize + 1, y: cell.y / this.cellSize + 1, type: cell.name };
      this.level.items = this.deleteItem(item, this.level.items);
      this.boardService.setLevelsAsChanged();
    }
  }

  eraseIn = (event) => {
    if (this.selectedBrush === 'erase') {
      const cell = event.target;
      this.drawColor(cell.x, cell.y, this.cellSize, this.cellSize, 'red', 'eraseHover');
    }
  }

  eraseOut = (event) => {
    if (this.selectedBrush === 'erase') {
      this.stage.removeChild(this.stage.getChildByName('eraseHover'));
    }
  }

  drawColor(x, y, width, height, color, name) {
    const shape = new createjs.Shape();
    shape.graphics.beginFill(color).drawRect(x, y, width, height);
    shape.name = name;
    this.stage.addChild(shape);
    return shape;
  }

  build() {
    this.boardService.save();
    // const rows = this.rows;
    // const cols = this.cols;
    // const cellSize = this.cellSize;
    // console.log(this.stage.children);
    // let objects = this.stage.children.filter(child => child.name !== 'cell');
    // console.log(objects);
    // objects = objects.map(child => {
    //   const x = child.x / cellSize + 1;
    //   const y = child.y / cellSize + 1;
    //   const type = child.name;
    //   return { x, y, type };
    // });
    // console.log(objects);
    // // =======================
    // const cell_size = this.cellSize === 50 ? 86 : 65;
    // const builder = { cell_size, width: cols, height: rows };
    // const levels = [{ objects, colors: [] }];
    // const result = { builder, levels };
    // console.log('++++++++++++++++++++++++result');
    // console.log(result);
    // console.log(this.level.items);
    // console.log(objects);
    // console.log(this.boardService.levels);
    // console.log(this.boardService.levels);
    // this.boardService.config = result;
    // console.log(this.boardService.config);
  }

  deleteItem(item: Item, items: Item[]) {
    return items.filter(i => !(i.x === item.x && i.y === item.y));
  }

  deleteColor(x, y, colors: Color[], color = null) {
    if (color) {
      return colors.filter(i => !(i.x === x && i.y === y && i.color === color));
    } else {
      return colors.filter(i => !(i.x === x && i.y === y));
    }
  }

  findElementUnderPoint(stage, x, y, name) {
    const res = stage.children.filter(ele => {
      return (ele.x === x && ele.y === y && ele.name === name);
    });
    return res;
  }

}
