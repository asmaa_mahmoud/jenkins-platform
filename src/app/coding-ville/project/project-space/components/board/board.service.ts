import { ChangeDetectorService } from './../../../change-detector.service';
import { Injectable } from '@angular/core';
import { Level, Dimensions } from './models/interfaces';
import { BehaviorSubject } from 'rxjs';

const defaultLevel: Level = { id: 1, dimensions: { rows: 7, cols: 4 }, items: [], colors: [] };

@Injectable({
  providedIn: 'root',
})
export class BoardService {
  lastId: number;
  selectedLevelId: BehaviorSubject<number> = new BehaviorSubject(null);
  levels: Level[];
  level: BehaviorSubject<Level> = new BehaviorSubject(null);
  config: any = {
    builder: {
      cell_size: 86,
      width: 4,
      height: 7,
    },
    levels: [
      {
        objects: [
          {
            x: 1,
            y: 1,
            type: 'crystal',
          },
          {
            x: 4,
            y: 5,
            type: 'ring',
          },
        ],
        colors: [
          {
            x: 1,
            y: 2,
            color: 'red',
          },
          {
            x: 2,
            y: 2,
            color: 'hidden',
          },
          {
            x: 1,
            y: 4,
            color: 'red',
          },
          {
            x: 1,
            y: 5,
            color: 'red',
          },
          {
            x: 1,
            y: 3,
            color: 'red',
          },
          {
            x: 2,
            y: 5,
            color: 'red',
          },
          {
            x: 4,
            y: 2,
            color: 'red',
          },
          {
            x: 4,
            y: 4,
            color: 'red',
          },
          {
            x: 4,
            y: 5,
            color: 'red',
          },
          {
            x: 3,
            y: 1,
            color: 'red',
          },
          {
            x: 1,
            y: 2,
            color: 'red',
          },
        ],
      },
      {
        objects: [
          {
            x: 2,
            y: 3,
            type: 'coin',
          },
        ],
        colors: [
          {
            x: 1,
            y: 1,
            color: 'red',
          },
          {
            x: 1,
            y: 3,
            color: 'red',
          },
          {
            x: 1,
            y: 5,
            color: 'red',
          },
          {
            x: 1,
            y: 4,
            color: 'red',
          },
          {
            x: 2,
            y: 3,
            color: 'red',
          },
          {
            x: 2,
            y: 5,
            color: 'red',
          },
          {
            x: 3,
            y: 3,
            color: 'red',
          },
          {
            x: 3,
            y: 5,
            color: 'red',
          },
          {
            x: 4,
            y: 4,
            color: 'red',
          },
          {
            x: 3,
            y: 2,
            color: 'red',
          },
        ],
      },
    ],
  };
  selectedBrush = new BehaviorSubject<string>('');
  constructor(private changeDetectorService: ChangeDetectorService) { }

  init(levels: Level[]) {
    this.levels = levels;
    this.lastId = levels[levels.length - 1] && levels[levels.length - 1].id;
    this.level.next(levels[0]);
    this.selectedLevelId.next(levels[0].id);
  }

  addLevel() {
    this.levels.push({ id: ++this.lastId, dimensions: this.level.getValue().dimensions, items: [], colors: [] });
    this.setLevelsAsChanged();
  }

  deleteLevel(id) {
    this.levels = this.levels.filter(level => level.id !== id);
    if (this.selectedLevelId.getValue() === id) {
      const firstLevelId = this.levels[0].id;
      this.selectedLevelId.next(firstLevelId);
      this.level.next(this.getLevel(firstLevelId));
    }
    this.setLevelsAsChanged();
  }

  changeLevel(id) {
    // saving current level state in levels array
    this.syncLevelsWithCurrentLevel();
    // =========================================
    this.level.next(this.getLevel(id));
    this.selectedLevelId.next(id);
  }

  changeBrush(brush) {
    this.selectedBrush.next(brush);
  }

  clearLevel() {
    const level = this.level.getValue();
    if (level.items.length > 0) {
      level.items = [];
      this.level.next(JSON.parse(JSON.stringify(level)));
      this.syncLevelsWithCurrentLevel();
    }
    this.setLevelsAsChanged();
  }

  updateSize(dimensions: Dimensions) {
    // Temporeary Solution
    this.syncLevelsWithCurrentLevel();
    this.levels.forEach(lev => {
      if (lev.dimensions.cols !== dimensions.cols) {
        lev.dimensions = { ...dimensions };
        lev.items = lev.items.filter(item => item.x <= dimensions.cols && item.y <= dimensions.rows);
        lev.colors = lev.colors.filter(color => color.x <= dimensions.cols && color.y <= dimensions.rows);
      }
    });
    const level = this.getLevel(this.selectedLevelId.getValue());
    this.level.next(JSON.parse(JSON.stringify(level)));
    this.setLevelsAsChanged();
    // ========================================================
    // const level = this.level.getValue();
    // level.dimensions = { ...dimensions };
    // level.items = level.items.filter(item => item.x <= dimensions.cols && item.y <= dimensions.rows);
    // level.colors = level.colors.filter(color => color.x <= dimensions.cols && color.y <= dimensions.rows);
    // this.level.next(JSON.parse(JSON.stringify(level)));
  }

  getLevel(id) {
    return this.levels.find(level => level.id === id);
  }

  syncLevelsWithCurrentLevel() {
    const levelIndex = this.levels.findIndex(level => level.id === this.level.value.id);
    this.levels[levelIndex] = this.level.getValue();
  }

  save() {
    const result = { builder: {}, levels: [], userName: '', gameName: '' };
    this.syncLevelsWithCurrentLevel();
    this.levels.forEach(level => {
      result.levels.push({ objects: level.items, colors: level.colors, dimensions: level.dimensions });
    });
    result.builder = {
      cell_size: 86,
      width: this.level.getValue().dimensions.cols,
      height: this.level.getValue().dimensions.rows,
    };
    return result;
  }

  setLevelsAsChanged() {
    this.changeDetectorService.setProjectAsChanged();
  }

}
