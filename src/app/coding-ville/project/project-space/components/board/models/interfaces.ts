export interface Dimensions { rows: number; cols: number; }
export interface Item { type: string; x: number; y: number; }
export interface Color { color: string; x: number; y: number; alpha?: number; }
export interface Level { id: number; dimensions: Dimensions; items: Item[]; colors: Color[]; }
