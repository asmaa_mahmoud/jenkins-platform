export const tiles = [
    'ice',
    'yellow',
    'green',
    'blue',
    'default',
    'lava',
    'sand',
    'brick',
    'magic',
    'wood',
    'crack',
    'electronic',
    'grass',
    'oil',
];

export const checkPoints = [
    'crown',
    'flag',
    'trophy',
];

export const damages = [
    'mountain',
    'flammable',
    'barrier',
    'trap',
    'ufo',
];

const chars = Array(16);

for (let i = 0; i < chars.length; i++) {
    chars[i] = 'char_' + (i + 1);
}
export { chars };
