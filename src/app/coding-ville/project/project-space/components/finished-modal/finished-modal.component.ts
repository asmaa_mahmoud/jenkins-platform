import { ProjectSpaceService } from './../../project-space.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-finished-modal',
  templateUrl: './finished-modal.component.html',
  styleUrls: ['./finished-modal.component.scss'],
})
export class FinishedModalComponent implements OnInit {
  data;
  @Input() ref;
  @Input() isTeacher;

  constructor(
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _ProjectSpace: ProjectSpaceService,
  ) { }

  ngOnInit() {
    this.getModalData();
  }

  getModalData() {
    this.data = this._ProjectSpace.finishModalStatus.value;
    this.data.progress_after = this.data.progress_after ? this.data.progress_after.toString() : null;
  }
  close() {
    this.ref.close();
    this._ProjectSpace.toggleFinishedModal(false);
  }

  buildApk() {
    let adventureId;
    let missionId;
    let jourenyId;
    let courseId;

    this._activeRoute.params.subscribe(
      res => {
        adventureId = res['adventureId'];
        missionId = res['missionId'];
        jourenyId = res['jourenyId'];
        courseId = res['courseId'];
      });
    this.close();

    let url = '';
    if (this.isTeacher) {
      if (courseId) {
        url = `/teacher/journeys/${missionId}/journey/adventure/${adventureId}/course/${courseId}`;

      } else {
        url = `/teacher/journeys/${missionId}/journey/${jourenyId}/adventure/${adventureId}`;
      }

    } else {
      if (courseId) {
        url = `/student/journeys/${missionId}/journey/adventure/${adventureId}/course/${courseId}`;
      } else {
        url = `/student/journeys/${missionId}/journey/${jourenyId}/adventure/${adventureId}`;
      }
    }
    this._router.navigate([url]);
  }

}
