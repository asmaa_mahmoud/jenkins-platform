import { BehaviorSubject } from 'rxjs';
import { BoardService } from '../board/board.service';
import { Component, OnInit, Output, Input } from '@angular/core';
import { tiles, checkPoints, damages, chars } from '../constansts';
@Component({
  selector: 'ngx-level-editor-toggle-menu',
  templateUrl: './level-editor-toggle-menu.component.html',
  styleUrls: ['./level-editor-toggle-menu.component.scss'],
})
export class LevelEditorToggleMenuComponent implements OnInit {

  @Output() isSideTabToggled = new BehaviorSubject(false);

  @Input() set isNbTabChanged(isNbTabChanged) {
    if (isNbTabChanged) {
      this.activeTab = null;
      this.lastActiveTab = null;
    }
  }

  lastActiveTab: string;
  activeTab;

  selectedBrush;
  colorSelected;
  cols: number;
  rows: number;
  level;
  tiles = tiles;
  checkPoints = checkPoints;
  damages = damages;
  chars = chars;
  collectablesList = [
    'crystal',
    'coin',
    'ring',
  ];

  constructor(private boardService: BoardService) { }

  ngOnInit() {
    this.getLevelData();
    this.getBrush();
  }

  getLevelData() {
    this.boardService.level.asObservable().subscribe((level) => {
      this.level = level;
      this.cols = this.level.dimensions.cols;
      this.rows = this.level.dimensions.rows;
    });
  }

  getBrush() {
    this.boardService.selectedBrush.subscribe((data) => {
      if (data.includes('color')) {
        this.colorSelected = data.split(':')[1];
        this.selectedBrush = '';
      } else {
        this.selectedBrush = data;
        this.colorSelected = '';
      }
    });
  }

  updateBrush(name) {
    if (this.selectedBrush === name) {
      this.boardService.changeBrush('');
    } else {
      this.boardService.changeBrush(name);
    }
  }

  clearLevel() {
    this.boardService.clearLevel();
  }

  changeTab(tab) {
    this.lastActiveTab = this.activeTab;
    this.activeTab = this.activeTab === tab ? 'empty' : tab;

    if (this.activeTab === 'empty' || this.lastActiveTab === 'empty' || !this.lastActiveTab) {
      this.isSideTabToggled.next(true);

      setTimeout(() => {
        this.isSideTabToggled.next(false);
      }, 500);
    }



  }

  sizeUp() {
    if (this.cols < 6) {
      const cols = this.cols + 1;
      const rows = this.rows + 2;
      this.boardService.updateSize({ cols, rows });
    }
  }

  sizeDown() {
    if (this.cols > 4) {
      const cols = this.cols - 1;
      const rows = this.rows - 2;
      this.boardService.updateSize({ cols, rows });
    }
  }

  selectColor(color) {
    const brushColor = this.colorSelected === color ? '' : 'color:' + color;
    this.boardService.changeBrush(brushColor);
  }
}
