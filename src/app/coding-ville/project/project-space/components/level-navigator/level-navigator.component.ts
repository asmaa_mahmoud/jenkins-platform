import { BoardService } from '../board/board.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'ngx-level-navigator',
  templateUrl: './level-navigator.component.html',
  styleUrls: ['./level-navigator.component.scss'],
})
export class LevelNavigatorComponent implements OnInit {

  @ViewChild('lvlContainer', { static: false }) lvlContainer: ElementRef;

  constructor(public boardService: BoardService) { }
  selectedLevelId;
  ngOnInit() {
    this.boardService.selectedLevelId.asObservable().subscribe((id) => {
      this.selectedLevelId = id;
    });
  }

  changeLevel(id) {
    this.boardService.changeLevel(id);
  }

  addLevel() {
    this.boardService.addLevel();
  }

  scroll(dir) {
    if (dir === 'back') {
      this.lvlContainer.nativeElement.scrollLeft -= 65;

    } else if (dir === 'forward') {
      this.lvlContainer.nativeElement.scrollLeft += 65;
    }
  }

  deleteLevel(id) {
    this.boardService.deleteLevel(id);
  }


}
