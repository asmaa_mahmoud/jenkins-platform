import { GlobalService } from './../../../../../@core/utils/global.service';
import { PublishService } from './../../../publish.service';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-publish-modal',
  templateUrl: './publish-modal.component.html',
  styleUrls: ['./publish-modal.component.scss'],
})
export class PublishModalComponent implements OnInit {
  @Input() isTeacher;
  @Input() ref;
  @Input() isProjectPublished;
  @Input() projectId;


  constructor(
    private _router: Router,
    private _publishService: PublishService,
    private _globalService: GlobalService,
  ) { }

  ngOnInit() {
    this.getPublicStatus();
  }

  getPublicStatus() {
    this.isProjectPublished = this._publishService.isProjectPublished.value;
  }

  closeModal() {
    this.ref.close();
    this._publishService.isProjectPublished.next(null);
  }

  publish() {

    this._publishService.togglePublicState(this.projectId).subscribe(
      res => {
        this._globalService.showTranslatedToast('success', null, 'public-state-changed');
        if (res.Object.public === 1) {

          if (this.isTeacher) {
            this._router.navigate([`/teacher/projects/${this.projectId}/preview`]);
          } else {
            this._router.navigate([`/student/projects/${this.projectId}/preview`]);
          }

        } else {
          this._publishService.isProjectPublished.next(res.Object.public);
        }
        this.closeModal();
      }, (error: any) => {
        this._globalService.showTranslatedToast('danger', error.error.errorMessage, 'something-went-wrong');
      },
    );
  }

}
