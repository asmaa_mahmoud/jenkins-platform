import { ProjectService } from './../../../project.service';
import { PublishService } from './../../../publish.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.scss'],
})
export class PublishComponent implements OnInit {

  currentUser;

  projectName;
  projectDescription;
  projectId: number;


  constructor(
    private _projectService: ProjectService,
    private _publishService: PublishService,
    private activatedRoute: ActivatedRoute,

  ) { }

  ngOnInit() {
    this.getData();
    this._publishService.makeConnection(this.currentUser.id);
    this.activatedRoute.params.subscribe(params => {
      this.projectId = params.id;
    });
  }
  getData() {
    this.currentUser = JSON.parse(localStorage.getItem('AuthorizationData'));
    this._projectService.projectName.subscribe(
      name => {
        this.projectName = name;
      });

  }

  updateName() {
    this._projectService.updateSelectedProject(this.projectName);
  }

  closePublish() {
    this._publishService.togglePublishModal(false);
  }


}
