import { Subscription } from 'rxjs';
import { ProjectService } from './../../../project.service';
import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
declare var lottie: any;

@Component({
    selector: 'ngx-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.scss'],
})

export class SpinnerComponent implements OnInit, OnDestroy, AfterViewInit {
    spinner;

    subscription = new Subscription();

    constructor(private _projectService: ProjectService) { }

    ngOnInit() {
        this.listenToSpinner();
    }

    ngAfterViewInit() {
        lottie.loadAnimation({
            container: document.getElementById('loading-project-gif'), // the dom element that will contain the animation
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: './assets/loading.json', // the path to the animation json
        });
    }

    listenToSpinner() {
        this.spinner = 0; // the spinner will be off if the it equals 0 (show +1 and  hide -1)
        const sub = this._projectService.spinner.subscribe(
            res => {
                this.spinner += res;
                if (this.spinner < 0) {
                    throw new Error('spinner was Hidden before shown');
                }
            });

        this.subscription.add(sub);
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
        this._projectService.spinner.next(0);

    }
}
