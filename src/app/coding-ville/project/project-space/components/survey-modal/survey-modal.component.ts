import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { GlobalService } from '../../../../../@core/utils/global.service';

@Component({
  selector: 'ngx-survey-modal',
  templateUrl: './survey-modal.component.html',
  styleUrls: ['./survey-modal.component.scss'],
})
export class SurveyModalComponent implements OnInit, AfterViewInit {


  @Input() scriptKey: string;
  @ViewChild('iframe', { static: false }) iframe: ElementRef;
  htmlCode;
  constructor(protected ref: NbDialogRef<SurveyModalComponent>, private globalService: GlobalService) { }

  ngOnInit() {
  }

  close() {
    this.ref.close();
  }

  ngAfterViewInit(): void {
    const script = this.globalService.getSurveyScript(this.scriptKey);
    console.log(script);
    this.iframe.nativeElement.srcdoc = `
      <body>
        <script> ${script} </script>
      </body>
    `;
  }

}
