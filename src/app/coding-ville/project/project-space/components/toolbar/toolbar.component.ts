import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ProjectSpaceService } from './../../project-space.service';
import { ProjectService } from './../../../project.service';
import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ngx-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  @Output() zoomOutput = new BehaviorSubject(null);
  @Output() centerOutput = new BehaviorSubject(null);

  isTutorial;
  isJourney;
  isTeacher: boolean;
  constructor(
    private _ProjectSpace: ProjectSpaceService,
    private _router: Router,
    private _projectService: ProjectService,
  ) { }

  ngOnInit() {
    this.getTutorialStatus();
    this.checkUrl();
    this.getUserData();
  }

  getUserData() {
    const currentUser = JSON.parse(localStorage.getItem('AuthorizationData'));
    this.isTeacher = currentUser.role.includes('teacher');
  }

  getTutorialStatus() {
    this._ProjectSpace.tutorialStatus.subscribe(
      res => {
        this.isTutorial = res;
      });
  }

  toggleTutorial() {
    this._ProjectSpace.toggleTutorial();

  }

  zoom(isZoomIn) {
    this.zoomOutput.next(isZoomIn);
  }
  center() {
    this.centerOutput.next(true);
  }

  reset(value) {
    this._projectService.resetAnswer.next(value);
  }


  checkUrl() {
    if (this._router.url.includes('journey')) {
      this.isJourney = true;
    }
  }
}
