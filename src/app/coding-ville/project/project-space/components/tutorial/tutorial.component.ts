import { ProjectSpaceService } from './../../project-space.service';
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ProjectService } from '../../../project.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ngx-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss'],
})
export class TutorialComponent implements OnInit, AfterViewInit, OnDestroy {

  status;

  activepage = 0;
  totalPages;
  pagesArray = [];
  htmlString;
  projectName: string;
  private subscriptions = new Subscription();

  constructor(private _ProjectSpace: ProjectSpaceService, private projectService: ProjectService) { }

  ngOnInit() {
    this.getProjectName();
    this.getTutorialStatus();
    this.getTutorialData();
    const el = document.createElement('html');
    el.innerHTML = this.htmlString;
    for (let i = 0; i < el.getElementsByClassName('desc_step').length; i++) {
      this.pagesArray.push(i);
    }
    this.totalPages = el.getElementsByClassName('desc_step').length;
  }

  ngAfterViewInit() {
    for (let i = 0; i < this.pagesArray.length; i++) {
      if (i !== this.activepage) document.getElementsByClassName('desc_step')[i]['style'].display = 'none';
    }
  }

  getTutorialStatus() {
    this._ProjectSpace.tutorialStatus.subscribe(
      res => {
        this.status = res;
      });
  }

  getTutorialData() {
    this.htmlString = this._ProjectSpace.tutorialDescription;
  }

  toggleTutorial() {
    this._ProjectSpace.toggleTutorial();

  }
  renderContent() {
    for (let i = 0; i < this.pagesArray.length; i++) {
      if (i !== this.activepage) {
        document.getElementsByClassName('desc_step')[i]['style'].display = 'none';

      } else {
        const newDiv = document.getElementsByClassName('desc_step');
        newDiv[i]['style'].display = 'block';
        const container = document.getElementById('tut-container');
        container.scrollTop = 0;
      }
    }
  }

  changePage(num) {
    const newPage = this.activepage + num;
    if (newPage < 0 || newPage > this.totalPages - 1) {
      return;
    } else {
      this.activepage = newPage;
    }
    this.renderContent();
  }

  goToPage(index) {
    this.activepage = index;
    this.renderContent();
  }

  getProjectName() {
    const sub = this.projectService.projectName.subscribe(name => this.projectName = name);
    this.subscriptions.add(sub);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
