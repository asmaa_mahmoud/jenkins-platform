import { Router } from '@angular/router';
import { ProjectService } from './../../project.service';
import { GlobalService } from './../../../../@core/utils/global.service';
import { BoardService } from '../components/board/board.service';
import { BlocklyProvider } from '../../../../@core/utils/blockly.service';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Input, OnDestroy } from '@angular/core';
import panzoom, { PanZoom, PanZoomOptions } from 'panzoom';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
declare var Blockly: any;

@Component({
  selector: 'ngx-project-builder',
  templateUrl: './project-builder.component.html',
  styleUrls: ['./project-builder.component.scss'],
})
export class ProjectBuilderComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() isTutorial: boolean;
  @ViewChild('lvlScene', { static: false }) lvlScene: ElementRef;
  lvlZoomElement: PanZoom;
  private subscriptions = new Subscription();
  selectedTab = 'logic-editor';
  isSideTabOpened = false;

  isNbTabChanged = false;

  isLg: boolean;

  tabObserver: BehaviorSubject<string>;
  modelAnswer: boolean;
  resetAnswer: boolean;
  isAnimate: boolean;

  constructor(
    private boardService: BoardService,
    private blockly: BlocklyProvider,
    private _globalService: GlobalService,
    private _projectService: ProjectService,
    private _router: Router,
    private _blocklyProvider: BlocklyProvider,
  ) { }
  level;
  ngOnInit() {
    this.getLevel();
    this.getWidthType();
    this.modelAndResetAnswerLogicInjection();
  }

  getLevel() {
    const sub = this.boardService.level.asObservable().subscribe((level) => {
      this.level = level;
    });
    this.subscriptions.add(sub);
  }

  getWidthType() {
    const sub = this._globalService.widthBreakPoint.subscribe(res => {
      this.isLg = res === 'lg' || res === 'xlg';
    });
    this.subscriptions.add(sub);
  }

  ngAfterViewInit(): void {
    const opts: PanZoomOptions = {
      maxZoom: 2,
      minZoom: 0.5,
      transformOrigin: { x: 0.5, y: 0.5 },
      bounds: true,
      boundsPadding: 0.1,
      smoothScroll: false,
    };
    this.lvlZoomElement = panzoom(this.lvlScene.nativeElement, opts);
  }

  zoom(isZoomIn) {
    if (isZoomIn != null) {

      if (this.selectedTab === 'level-editor') {
        let scale = this.lvlZoomElement.getTransform().scale;
        scale = isZoomIn === true ? scale + .2 : scale - .2;
        if (scale < .1) {
          this.lvlZoomElement.smoothZoomAbs(0.5, 0.5, .1);
          return;
        }
        if (scale > 2) {
          this.lvlZoomElement.smoothZoomAbs(0.5, 0.5, 2);
          return;
        }
        this.lvlZoomElement.smoothZoomAbs(0.5, 0.5, scale);
      } else {
        const scale = isZoomIn === true ? 0.2 : - 0.2;
        this.blockly.zoom(scale);
      }

    }

  }

  center(ev) {
    if (ev != null) {
      if (this.selectedTab === 'level-editor') {
        this.lvlZoomElement.zoomAbs(0.5, 0.5, 1);
        this.lvlZoomElement.moveTo(0.5, 0.5);
      } else {
        this.blockly.center();
      }
    }
  }

  changeTab(ev) {

    this.isNbTabChanged = true;

    setTimeout(() => this.isNbTabChanged = false, 300);
    this.isAnimate = false;
    this.isSideTabOpened = false;
    this.selectedTab = ev.tabId;
    if (this.selectedTab === 'level-editor') {
      Blockly.ContextMenu.hide();
      Blockly.getMainWorkspace().toolbox_.clearSelection();
    }
    // this.tabHasChanged = this.selectedTab === 'level-editor' ? true : false;

    if (this.tabObserver) {
      this.tabObserver.next(this.selectedTab);
    }
  }

  changeSideTabStatus(value: boolean) {
    if (value) {
      this.isSideTabOpened = !this.isSideTabOpened;
      this.isAnimate = true;
      setTimeout(() => this.isAnimate = false, 300);

    }
  }

  modelAndResetAnswerLogicInjection() {
    const currentUser = JSON.parse(localStorage.getItem('AuthorizationData'));
    if (currentUser) {

      const isTeacher = currentUser.role.includes('teacher');
      if (this._router.url.includes('journey') && isTeacher) {
        this.tabObserver = new BehaviorSubject(null);
        const sub = this._projectService.toggleModelAnswer.subscribe(
          res => {
            if (res === 'model') {

              if (this.selectedTab === 'logic-editor') {

                this.setModelAnswer();
              } else {
                this.modelAnswer = true;
                this.tabObserver.subscribe(
                  tab => {

                    if ((tab === 'logic-editor' || this.selectedTab === 'logic-editor') && this.modelAnswer) {
                      setTimeout(() => {
                        this.setModelAnswer();
                      }, 10);
                      this.modelAnswer = false;
                    }
                  });

              }
            }
          });
          const sub2 = this._projectService.resetAnswer.subscribe(
            res => {
              if (res === 'model') {

                if (this.selectedTab === 'logic-editor') {

                  this.reset();
                } else {
                  this.resetAnswer = true;
                  this.tabObserver.subscribe(
                    tab => {

                      if ((tab === 'logic-editor' || this.selectedTab === 'logic-editor') && this.resetAnswer) {
                        setTimeout(() => {
                          this.reset();
                        }, 10);
                        this.resetAnswer = false;
                      }
                    });

                }
              }
            });
        this.subscriptions.add(sub);
        this.subscriptions.add(sub2);
      }
    }
  }

  setModelAnswer() {
    Blockly.getMainWorkspace().clear();
    this._blocklyProvider.setInstructions(this._projectService.modelAnswer.logic[0].code);
    this._projectService.toggleModelAnswer.next(null);
  }

  reset() {
    Blockly.getMainWorkspace().clear();
    this._blocklyProvider.setInstructions(this._projectService.initialAnswer.logic[0].code);
    this._projectService.resetAnswer.next(null);
  }
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
