import { ChangeDetectorService } from './../../change-detector.service';
import { env } from './../../../../@core/env/env';
import { Subscription } from 'rxjs';
import { User } from './../../../../@core/data/users';
import { ProjectService } from './../../project.service';
import { BlocklyProvider } from './../../../../@core/utils/blockly.service';
import { BoardService } from '../components/board/board.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import gamebuilder from './../../../../../assets/js/gamebuilder/Game.js';
import { GlobalService } from '../../../../@core/utils/global.service';
import { NbDialogService } from '@nebular/theme';
import { SurveyModalComponent } from '../components/survey-modal/survey-modal.component';

@Component({
  selector: 'ngx-project-preview',
  templateUrl: './project-preview.component.html',
  styleUrls: ['./project-preview.component.scss'],
})
export class ProjectPreviewComponent implements OnInit, OnDestroy {
  isOrg = env.isOrg;
  game: any = null;
  isRunning: boolean = false;
  userCode: string = ``;
  blocklyInitialized: boolean;
  levelsInitialized: boolean;
  currentUser: string;
  projectName: string;
  subscriptions = new Subscription();
  firstTime: boolean;
  projectChanged: boolean;

  warningShowedBefore: boolean = false;
  constructor(
    private boardService: BoardService,
    private blocklyProvider: BlocklyProvider,
    private projectService: ProjectService,
    private changeDetectorService: ChangeDetectorService,
    private globalServise: GlobalService,
    private _dialogService: NbDialogService,
  ) {
  }

  compareTwoCodes(code1, code2) {
    return code2.replace(/ |\n/gm, '') === code1.replace(/ |\n/gm, '');
  }

  removeVarFromCode(code: string) {
    const regex = /var.*;/;
    const match = code.match(regex);
    if (match) {
      const variablesLine = match[0];
      const start = match.index;
      const end = start + variablesLine.length;
      const newCode = code.slice(end);
      return { variablesLine, newCode };
    }
    return false;
  }

  removeFloatedBlocks(code: string) {
    let tempCode = code;
    const startRegex = /\/\/\d+_block_start/;
    let result = '';
    const variableLineRemovingResult = this.removeVarFromCode(tempCode);
    if (variableLineRemovingResult) {
      result += variableLineRemovingResult.variablesLine;
      tempCode = variableLineRemovingResult.newCode;
    }

    let firstMatch = tempCode.match(startRegex);
    while (firstMatch) {
      const num = firstMatch[0].match(/\d+/);
      const index = firstMatch.index;
      const endRegex = new RegExp(`//${num}_block_end`);
      const endMatch = tempCode.match(endRegex);
      const end = endMatch[0].length + endMatch.index;
      result += ('\n' + tempCode.substring(index, end));
      tempCode = tempCode.slice(end);
      firstMatch = tempCode.match(startRegex);
    }
    if (!this.firstTime && !this.warningShowedBefore && !this.compareTwoCodes(code, result)) {
      this.warningShowedBefore = true;
      this.globalServise.showTranslatedToast('warning', null, 'floated-blocks-msg');
    }
    return result;
  }

  runProject(event) {
    this.isRunning = true;
    // this.showSurvey();
    const config = this.boardService.save();
    if (config.builder['width'] === 4) {
      this.userCode = `
      let i=0;
      this.robo.moveTo(1, 7);
    `;
    } else if (config.builder['width'] === 5) {
      this.userCode = `
      let i=0;
      this.robo.moveTo(1, 9);
    `;
    } else {
      this.userCode = `
      let i=0;
      this.robo.moveTo(1, 11);
    `;
    }
    const code = this.removeFloatedBlocks(this.blocklyProvider.getJSCode());
    this.userCode += code;
    config.userName = this.currentUser;
    config.gameName = this.projectName;
    config['isOrg'] = this.isOrg;
    if (this.game) {
      this.game.destroy(true);
      this.game = null;
    }
    this.changeDetectorService.setNoChangesForReload();
    setTimeout(() => {
      this.game = new gamebuilder(this.userCode, config, 'main');
      if (this.firstTime) {
        this.firstTimeGameDisable();
      }
    }, 500);
  }

  firstTimeGameDisable() {
    const timer = setInterval(() => {
      if (this.game && this.game.scene.isActive('StartScene')) {
        this.game.scene.pause('StartScene');
        clearInterval(timer);
      }
    }, 100);
  }
  playAction(event) {
    this.firstTime = false;
    this.runProject(event);
  }

  stopProject(event) {
    this.isRunning = false;
    // Array of scenes keys as string
    this.pauseActiveScene();
  }

  pauseActiveScene() {
    const scenesKeys = Object.keys(this.game.scene.keys);
    scenesKeys.some(key => {
      if (this.game.scene.isActive(key)) {
        this.game.scene.pause(key);
        return true;
      }
    });
  }

  ngOnInit() {
    this.firstTime = true;
    this.getData();
    this.bindChangedProps();
    const sub1 = this.blocklyProvider.initialized.asObservable().subscribe(data => {
      this.blocklyInitialized = data;
      if (this.blocklyInitialized && this.levelsInitialized && !this.isRunning) {
        this.runProject(null);
      }
    });
    const sub2 = this.blocklyProvider.levelsInitialized.asObservable().subscribe(data => {
      this.levelsInitialized = data;
      if (this.blocklyInitialized && this.levelsInitialized && !this.isRunning) {
        this.runProject(null);
      }
    });
    this.subscriptions.add(sub1);
    this.subscriptions.add(sub2);
  }

  bindChangedProps() {
    const sub1 = this.changeDetectorService.projectHasChangesForReload.subscribe(data => {
      this.projectChanged = data;
    });
    this.subscriptions.add(sub1);
  }

  getData() {

    this.currentUser = JSON.parse(localStorage.getItem('AuthorizationData')).username;
    this.projectService.projectName.subscribe(name => {
      this.projectName = name;
      if (!name) {
        this.projectName = 'My Awesome Game';
      }
      this.game && this.game.scene.getScene('StartScene').setGameNameText(this.projectName);
    });
  }

  ngOnDestroy() {
    if (this.game) {
      this.game.destroy(true);
      this.game = null;
    }

    this.subscriptions.unsubscribe();
  }

  showSurvey() {
    const data = JSON.parse(localStorage.getItem('AuthorizationData'));
    console.log(data);
    if (!this.firstTime && data.game_survey === 0 && data.role.includes('student')) {
      this._dialogService.open(SurveyModalComponent, {
        context: {
          scriptKey: 'game_builder',
        },
      });
      this.globalServise.post('coding-ville/user/survey/status', { type: 'game' }).subscribe();
      data.game_survey = 1;
      localStorage.setItem('AuthorizationData', JSON.stringify(data));
    }
  }

}
