import { Subscription } from 'rxjs';
import { GlobalService } from './../../../@core/utils/global.service';
import { PublishService } from './../publish.service';
import { NbDialogService } from '@nebular/theme';
import { BoardService } from './components/board/board.service';
import { ProjectSpaceService } from './project-space.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import {
  BreakpointObserver,
  BreakpointState,
} from '@angular/cdk/layout';
@Component({
  selector: 'ngx-project-space',
  templateUrl: './project-space.component.html',
  styleUrls: ['./project-space.component.scss'],
})
export class ProjectSpaceComponent implements OnInit, OnDestroy {
  @ViewChild('publishModal', { static: false }) publishModal;

  isTutorial;
  level;
  isProjectPublished;
  isTeacher: boolean;
  isLg: boolean;

  private subscriptions = new Subscription();
  projectId: boolean;
  constructor(
    private _ProjectSpace: ProjectSpaceService,
    private boardService: BoardService,
    private _publishService: PublishService,
    private dialogService: NbDialogService,
    private _globalService: GlobalService,
    public breakpointObserver: BreakpointObserver,
  ) { }

  ngOnInit() {
    this.init();
    this.getWidthType();
  }

  init() {
    this.getTutorialStatus();
    this.listenToLevels();
    this.getPublishStatus();
    this.getUserData();
  }

  getWidthType() {
    const sub = this._globalService.widthBreakPoint.subscribe(res => {
      this.isLg = res === 'lg' || res === 'xlg';
    });
    this.subscriptions.add(sub);
  }

  getUserData() {
    const currentUser = JSON.parse(localStorage.getItem('AuthorizationData'));
    this.isTeacher = currentUser.role.includes('teacher');
  }

  getTutorialStatus() {
    const sub = this._ProjectSpace.tutorialStatus.subscribe(
      res => {
        this.isTutorial = res;
      });

    this.subscriptions.add(sub);
  }

  listenToLevels() {
    this.boardService.level.asObservable().subscribe((level) => {
      this.level = level;
    });
  }

  getPublishStatus() {
    const sub = this._publishService.isPublishModal.subscribe(
      projectId => {

        if (projectId === undefined) {
          this._globalService.showTranslatedToast('danger', null, 'project-should-be-saved-first');
        } else if (projectId) {
          this.projectId = projectId;
          this.dialogService.open(this.publishModal);
        }
      });

    this.subscriptions.add(sub);

  }


  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
    this._publishService.togglePublishModal(null);
  }


}
