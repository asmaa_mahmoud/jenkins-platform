import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ProjectSpaceService {

  tutorialStatus = new BehaviorSubject(null);
  finishModalStatus = new BehaviorSubject(null);
  hasChanges: boolean = false;
  tutorialDescription;
  expandNavBar = new BehaviorSubject(null);

  constructor() { }

  toggleTutorial() {
    const state = !this.tutorialStatus.value ? true : false;

    this.tutorialStatus.next(state);
    if (!state) {
      setTimeout(
        () => {
          this.tutorialStatus.next(null);
        }, 700);
    }
  }

  toggleFinishedModal(value) {
    this.finishModalStatus.next(value);
  }

}
