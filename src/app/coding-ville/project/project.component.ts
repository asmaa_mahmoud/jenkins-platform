import { AuthenticationService } from './../../@core/utils/authentication.service';
import { NbDialogService } from '@nebular/theme';
import { Subscription } from 'rxjs';
import { PublishService } from './publish.service';
import { BlocklyProvider } from './../../@core/utils/blockly.service';
import { ProjectService } from './project.service';
import { ProjectSpaceService } from './project-space/project-space.service';
import { Component, OnInit, HostListener, ViewChild, OnDestroy } from '@angular/core';
import { BoardService } from './project-space/components/board/board.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
})

export class ProjectComponent implements OnInit, OnDestroy {
  @ViewChild('finishedModal', { static: false }) finishedModal;
  @ViewChild('wrongDataModal', { static: false }) wrongDataModal;

  isTeacher: any;


  @HostListener('window:beforeunload', ['$event'])
  stopClose($event) {
    const hasChanges = this._ProjectSpace.hasChanges;
    if (hasChanges) $event.returnValue = 'Your data will be lost!';
  }
  id: number;
  errorMessage: any;
  isActivated: boolean;

  journeysProject;
  private subscriptions = new Subscription();

  constructor(
    private _ProjectSpace: ProjectSpaceService,
    private boardService: BoardService,
    private activatedRoute: ActivatedRoute,
    private _router: Router,
    private projectService: ProjectService,
    private location: Location,
    private blocklyProvider: BlocklyProvider,
    private _publishService: PublishService,
    private dialogService: NbDialogService,
    private authService: AuthenticationService,
    private router: Router,
  ) {
    this.isActivated = this.authService.isActivated();
  }

  ngOnInit() {
    if (!this.isActivated) this.router.navigate(['/teacher/dashboard']);
    this.getUrlStatus();
    this.getUserData();
  }

  getUrlStatus() {
    if (this._router.url.includes('journey')) {
      this.checkProjectAuth();
    } else {
      this.getNormalProjectData();
    }
  }

  getNormalProjectData() {

    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    });

    if (this.id) {

      this.projectService.showSpinner();

      this.projectService.getProject(this.id).subscribe(
        data => {
          if (data.Object) {
            const levels = data.Object.levels.map(level => JSON.parse(level));
            this.boardService.init(levels);
            this.projectService.updateSelectedProject(data.Object.name);
            this.blocklyProvider.setInstructions(data.Object.logic[0].code);
            this.blocklyProvider.levelsInitialized.next(true);

            this.checkProjectData(data.Object.has_project, data.Object.isPublic);
            this.projectService.hideSpinner();

          }
        }, error => {
          this.errorMessage = error.error.errorMessage;
          this.projectService.hideSpinner();
          this.dialogService.open(this.wrongDataModal, { closeOnBackdropClick: false });
        });
    } else {
      this.projectService.updateSelectedProject(null);
      this.boardService.init([{ id: 1, dimensions: { rows: 7, cols: 4 }, items: [], colors: [] }]);
      this.blocklyProvider.setInstructions(`
      <xml xmlns="http://www.w3.org/1999/xhtml"><block type="on_start" id="T%|Pk6vHpWVHtXdY/0)0" x="150" y="90"></block></xml>`);
      this.blocklyProvider.levelsInitialized.next(true);
    }
  }

  gotoProjectsPage() {
    if (this.isTeacher) {
      this.router.navigate(['/teacher/projects/']);
    } else {
      this.router.navigate(['/student/projects/']);
    }
  }

  checkProjectData(has_project, isPublic) {

    if (!has_project) {
      this.location.go('/project-game/remix/' + this.id);
      this._publishService.isProjectPublished.next(3); // Not the users project so he can't (un)publish
    } else {
      this._publishService.isProjectPublished.next(isPublic);
    }
  }
  // --------------------- SECTION Journy Project ---------------------




  checkProjectAuth() {
    this.activatedRoute.params.subscribe(params => {
      this.journeysProject = {
        adventure_id: params.adventureId,
        journey_id: params.jourenyId,
        mission_id: params.missionId,
      };
      let reqData, reqProjectData;
      if (params.courseId) {
        this.journeysProject['course_id'] = params.courseId;
      }

      if (params.studentId) {
        this.journeysProject['student_id'] = params.studentId;
      }
      if (params.courseId) {
        reqData = {
          adventure_id: this.journeysProject.adventure_id,
          journey_id: this.journeysProject.journey_id,
          mission_id: this.journeysProject.mission_id,
          course_id: this.journeysProject.course_id,
          type: 'project',
        };
        reqProjectData = {
          adventure_id: this.journeysProject.adventure_id,
          journey_id: this.journeysProject.journey_id,
          project_id: this.journeysProject.mission_id,
          course_id: this.journeysProject.course_id,
          type: 'project',
        };
      } else {
        reqData = {
          adventure_id: this.journeysProject.adventure_id,
          journey_id: this.journeysProject.journey_id,
          mission_id: this.journeysProject.mission_id,
          type: 'project',
        };
        reqProjectData = {
          adventure_id: this.journeysProject.adventure_id,
          journey_id: this.journeysProject.journey_id,
          project_id: this.journeysProject.mission_id,
          type: 'project',
        };
      }

      this.projectService.showSpinner();

      this.projectService.getProjectAuthorization(reqData).subscribe(
        res => {
          this.projectService.hideSpinner();

          if (this.journeysProject['student_id']) {
            reqData['student_id'] = this.journeysProject['student_id'];
            this.getStudentAnswers(reqData);
          } else {
            this.getJournyProjectData(reqProjectData);
          }
        }, error => {
          this.projectService.hideSpinner();
        });
    });

  }

  getJournyProjectData(reqData) {

    this.projectService.showSpinner();
    this.projectService.getMissionData(reqData).subscribe(
      data => {
        if (data.Object) {
          const obj = data.Object;
          this._ProjectSpace.tutorialDescription = obj.description;
          this.projectService.updateSelectedProject(obj.name);
          if (this.isTeacher) {
            this.initModelAnswer(obj.modelAnswerLevels, obj.modelAnswerLogic);
            this.initInitialAnswer(obj.initialLevels, obj.initialLogic);
          }
          if (obj.userLogic && obj.userLogic.length) {
            this.blocklyProvider.setInstructions(obj.userLogic[0].code);

            const levels = obj.userLevels.map(level => JSON.parse(level));
            this.boardService.init(levels);

          } else {
            this.blocklyProvider.setInstructions(obj.initialLogic[0].code);

            const levels = obj.initialLevels.map(level => JSON.parse(level.level));
            this.boardService.init(levels);
          }

          this.blocklyProvider.levelsInitialized.next(true);
          this.getFinishModalStatus();
        }
        this.projectService.hideSpinner();
        setTimeout(() => {
          this._ProjectSpace.toggleTutorial();
        }, 500);

      }, error => {
        this.projectService.hideSpinner();
      });
  }

  getStudentAnswers(reqData) {

    this.projectService.getStudentAnswer(reqData).subscribe(
      res => {
        if (res.Object) {
          const obj = res.Object;
          this.projectService.updateSelectedProject(obj.project_data.name);
          this.blocklyProvider.setInstructions(obj.Student_solution.logic[0].code);
          const levels = obj.Student_solution.levels.map(level => JSON.parse(level));
          this.boardService.init(levels);
          this.blocklyProvider.levelsInitialized.next(true);

        }
      });
  }

  initModelAnswer(lvls, logic) {
    this.projectService.modelAnswer = {
      logic: logic,
      levels: lvls,
    };
    const sub = this.projectService.toggleModelAnswer.subscribe(
      res => {
        if (res === 'model') {
          const levels = this.projectService.modelAnswer.levels.map(level => JSON.parse(level.level));
          this.boardService.init(levels);
        }
      });
    this.subscriptions.add(sub);
  }

  initInitialAnswer(lvls, logic) {
    this.projectService.initialAnswer = {
      logic: logic,
      levels: lvls,
    };
    const sub = this.projectService.resetAnswer.subscribe(
      res => {
        if (res === 'model') {
          const levels = this.projectService.initialAnswer.levels.map(level => JSON.parse(level.level));
          this.boardService.init(levels);
        }
      });
    this.subscriptions.add(sub);
  }

  getFinishModalStatus() {
    const sub = this._ProjectSpace.finishModalStatus.subscribe(
      res => {
        if (res) {
          this.dialogService.open(this.finishedModal, { closeOnBackdropClick: false });
        }
      });
    this.subscriptions.add(sub);
  }


  getUserData() {
    const currentUser = JSON.parse(localStorage.getItem('AuthorizationData'));
    this.isTeacher = currentUser.role.includes('teacher');
  }


  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
    this.projectService.toggleModelAnswer.next(null);
    this._ProjectSpace.tutorialStatus.next(null);
    this.blocklyProvider.initialized.next(false);
    this.blocklyProvider.levelsInitialized.next(false);
  }
}
