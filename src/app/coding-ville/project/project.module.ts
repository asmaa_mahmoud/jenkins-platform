import { TranslatorModule } from './../../@shared/translator/translator.module';
import { FormsModule } from '@angular/forms';
import { PublishComponent } from './project-space/components/publish/publish.component';
import { ProjectService } from './project.service';
import { ProjectSpaceService } from './project-space/project-space.service';
import { TutorialComponent } from './project-space/components/tutorial/tutorial.component';
import { ToolbarComponent } from './project-space/components/toolbar/toolbar.component';
import { NgModule } from '@angular/core';
import { NbMenuModule, NbTabsetModule, NbCardModule, NbButtonModule, NbIconModule,
  NbProgressBarModule, NbTooltipModule, NbDialogModule } from '@nebular/theme';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from './project.component';
import { ThemeModule } from '../../@theme/theme.module';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectSpaceComponent } from './project-space/project-space.component';
import { ProjectBuilderComponent } from './project-space/project-builder/project-builder.component';
import { ProjectPreviewComponent } from './project-space/project-preview/project-preview.component';
import { BoardComponent } from './project-space/components/board/board.component';
import { LevelEditorToggleMenuComponent } from './project-space/components/level-editor-toggle-menu/level-editor-toggle-menu.component';
import { LevelNavigatorComponent } from './project-space/components/level-navigator/level-navigator.component';
import { BlocklyComponent } from './project-space/components/blockly/blockly.component';
import { SpinnerComponent } from './project-space/components/spinner/spinner.component';
import { FinishedModalComponent } from './project-space/components/finished-modal/finished-modal.component';
import { PublishModalComponent } from './project-space/components/publish-modal/publish-modal.component';
import { SurveyModalComponent } from './project-space/components/survey-modal/survey-modal.component';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbMenuModule,
    ProjectRoutingModule,
    NbTabsetModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    FormsModule,
    NbProgressBarModule,
    NbTooltipModule,
    TranslatorModule,
    NbDialogModule.forRoot(),
  ],
  declarations: [
    ProjectComponent,
    ProjectSpaceComponent,
    ProjectBuilderComponent,
    ProjectPreviewComponent,
    BoardComponent,
    LevelEditorToggleMenuComponent,
    LevelNavigatorComponent,
    BlocklyComponent,
    ToolbarComponent,
    TutorialComponent,
    PublishComponent,
    SpinnerComponent,
    FinishedModalComponent,
    PublishModalComponent,
    SurveyModalComponent,
  ],
  providers: [ProjectSpaceService, ProjectService],
  entryComponents: [
    SurveyModalComponent,
  ],
})
export class ProjectModule { }
