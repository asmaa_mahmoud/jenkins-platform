import { BlocklyProvider } from './../../@core/utils/blockly.service';
import { BoardService } from './project-space/components/board/board.service';
import { GlobalService } from './../../@core/utils/global.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

declare var Blockly: any;

@Injectable()
export class ProjectService {
  constructor(private _globalService: GlobalService, private boardService: BoardService, private blocklyProvider: BlocklyProvider) {
  }

  prefix = 'coding-ville/projects/';
  defaultPrefix = 'coding-ville/default_projects/';
  projectName = new BehaviorSubject(null);
  toggleModelAnswer = new BehaviorSubject(null);
  resetAnswer = new BehaviorSubject(null);
  modelAnswer: { logic: any; levels: any; };
  initialAnswer: { logic: any; levels: any; };

  spinner = new BehaviorSubject(null);

  getUserScore() {
    const url = 'school/student/score';
    return this._globalService.get(url);
  }

  updateSelectedProject(name) {
    this.projectName.next(name);
  }

  getProject(projectId) {
    const url = this.prefix + projectId;
    return this._globalService.get(url);
  }

  saveProject(name, id) {

    const data = this.prepereDataForSave(name, id);
    const url = this.prefix + 'save';
    return this._globalService.post(url, data);
  }


  getToolBox() {
    const url = this.prefix + 'toolbox';
    return this._globalService.get(url);
  }

  // helper
  prepereDataForSave(name, id) {
    const canvas: any = document.getElementById('main').lastChild;

    const img = canvas && canvas.toDataURL('image/png');
    this.boardService.syncLevelsWithCurrentLevel();
    const stringifiedLevels = this.boardService.levels.map(level => JSON.stringify(level));
    const xml_instructions = Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.getMainWorkspace()));
    const js_code = Blockly.JavaScript.workspaceToCode(Blockly.getMainWorkspace());
    const data = {
      name: name || 'My Project',
      levels: stringifiedLevels,
      type_id: 1,
      project_id: id,
      logic: [{ language: 'xml', code: xml_instructions, js_code: js_code }],
      icon: img,
    };
    return data;
  }

  // ----------- SECTION Journy Project -----------

  getProjectAuthorization(data) {
    const url = this.defaultPrefix + 'auth';
    return this._globalService.post(url, data);
  }

  getNextMission(data) {
    const url = this.defaultPrefix + 'next';
    return this._globalService.post(url, data);
  }
  getMissionData(data) {
    const url = this.defaultPrefix + 'data';
    return this._globalService.post(url, data);
  }


  submitProjectAnswer(data) {
    const reqData = { ...data, ...this.getLevelAndLogic() };
    const url = this.defaultPrefix + 'progress';
    return this._globalService.post(url, reqData);
  }

  getLevelAndLogic() {
    const stringifiedLevels = this.boardService.levels.map(level => JSON.stringify(level));

    const xml_instructions = Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(Blockly.getMainWorkspace()));
    const js_code = Blockly.JavaScript.workspaceToCode(Blockly.getMainWorkspace());

    const data = {
      levels: stringifiedLevels,
      logic: [{ language: 'xml', code: xml_instructions, js_code: js_code }],
    };

    return data;
  }


  getStudentAnswer(data) {
    const url = this.prefix + `answer?project_id=${data.mission_id}&course_id=${data.course_id}&student_id=${data.student_id}&adventure_id=${data.adventure_id}`;
    return this._globalService.get(url);
  }
  // ----------- SECTION Spinner -----------

  showSpinner() {
    this.spinner.next(1);
  }
  hideSpinner() {
    this.spinner.next(-1);
  }

}
