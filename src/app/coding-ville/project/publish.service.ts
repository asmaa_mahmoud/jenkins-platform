import { environment } from './../../../environments/environment';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import Pusher from 'pusher-js';
import { GlobalService } from '../../@core/utils/global.service';
@Injectable({
  providedIn: 'root',
})

export class PublishService {
  private pusherClient: Pusher;
  private notification: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  isPublishModal = new BehaviorSubject(false);
  isProjectPublished = new BehaviorSubject(null);
  public isPublishSubject = new BehaviorSubject(false);
  private apiUrlPrefix = environment.apiUrlPrefix;
  private pusherKey: string = environment.pusherKey;
  private cluster: string = environment.cluster;
  private channelPrefix: string = environment.channelPrefix;
  channel;


  constructor(private _globalService: GlobalService) { }

  makeConnection(user_id: number) {
    this.pusherClient = new Pusher(this.pusherKey, { cluster: this.cluster, authEndpoint: this.apiUrlPrefix + 'pusher/auth' });
    this.channel = this.pusherClient.subscribe(this.channelPrefix + user_id);
    this.channel.bind(
      'App\\Events\\BuildApkEvent',
      (data) => {
        if (data.success === 1) {
          // this._globalService.showToast('success', data.message, 'Project build succeded');
          this._globalService.showNgxToast(
            `<a href="${data.apk_link}" class="btn btn-light btn-sm mt-3" download>Download</a>`,
            data.message, 'toast-success',
            {
              timeOut: 5000,
            });
          this.notification.next(data);
        } else if (data.success === 0) {
          this._globalService.showTranslatedToast('danger', data.message, 'project-build-failed');
        }
      }, err => {
        console.log(err);
        this._globalService.showTranslatedToast('danger', err.message, 'project-build-failed');
      },
    );
  }

  cutConnection() {
    this.pusherClient = null;
    this.channel.unsubscribe();
    this.channel.unbind();
  }

  getNotification(): Observable<any> {
    return this.notification;
  }

  prepareDataForSave(levels, logic, userName, gameName) {
    const parsedLevels = levels.map(level => {
      const parsed = JSON.parse(level);
      return { ...parsed, objects: parsed.items };
    });
    const dimensions = parsedLevels[0].dimensions;
    const config = { builder: {}, levels: [], userName, gameName };
    config.builder = {
      cell_size: 86,
      width: dimensions.cols,
      height: dimensions.rows,
    };
    config.levels = parsedLevels;
    const gameLogic = logic[0].js_code;
    const result = [{ 'config.js': [config], 'gameLogic.js': [{ code: gameLogic }] }];
    return result;
  }

  publish(project_id, user_id, type, data) {

    this._globalService.post(`coding-ville/projects/${project_id}/buildApk?user_id=${user_id}&type=${type}`, data).subscribe(res => {
    }, error => {
      console.log(error);
    });
  }

  publishFromPreview(project_id, user_id, type, data) {
    return this._globalService.post(`coding-ville/projects/${project_id}/buildApk?user_id=${user_id}&type=${type}`, data);
  }

  publishDefault(type, data) {
    return this._globalService.post(`coding-ville/default_projects/buildApk?type=${type}`, data);
  }

  togglePublicState(projectId) {
    const url: string = `coding-ville/projects/${projectId}/togglePublicState`;
    return this._globalService.put(url, {});
  }

  togglePublishModal(value) {

    this.isPublishModal.next(value);
  }

}
