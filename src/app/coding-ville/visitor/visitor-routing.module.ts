import { ProjectPreviewComponent } from './../accounts/shared/project-preview/project-preview.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { VisitorComponent } from './visitor.component';
import { VisitorTestComponent } from './test/test.component';

const routes: Routes = [{
  path: '',
  component: VisitorComponent,
  children: [
    {
      path: '',
      redirectTo: 'discover/:project_id/discover-project',
      pathMatch: 'full',
    },
    {
      path: 'discover/:project_id/discover-project',
      component: ProjectPreviewComponent,
    },
    {
      path: 'test',
      component: VisitorTestComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class VisitorRoutingModule {
}

