import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Role } from '../.././@core/data/role';
import { AuthenticationService } from './../../@core/utils/authentication.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'ngx-visitor',
  templateUrl: './visitor.component.html',
})
export class VisitorComponent implements OnInit {

  projectID: number;

  currentURL: string;

  currentUserID: string;
  currentRole: string;

  checksCompleted = false;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private location: Location,
  ) {

    if (this.authService.currentUserValue)
      this.authService.currentUser.subscribe(x => { this.currentUserID = x.id, this.currentRole = x.role; });
    else {
      this.currentUserID = null;
      this.currentRole = null;
    }


    this.route.firstChild.params.subscribe((params: Params) => {
      this.projectID = params['project_id'];
    });
  }

  ngOnInit() {
    this.router.events.subscribe(x => {
      this.currentURL = this.location.path();
    });

    this.checkUser();
  }

  checkUser() {
    if (this.authService.currentUserValue) {
      if (Role['Teacher'].includes(this.currentRole)) {
        this.router.navigate([`teacher/discover/${this.projectID}/discover-project`]);
      } else if (Role['Student'].includes(this.currentRole) || Role['individualStudent'].includes(this.currentRole)) {
        this.router.navigate([`student/discover/${this.projectID}/discover-project`]);
      }
    } else {
      this.checksCompleted = true;
    }
  }


}
