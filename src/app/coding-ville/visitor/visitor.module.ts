import { SharedModule } from './../accounts/shared/shared.module';
import { VisitorRoutingModule } from './visitor-routing.module';
import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { CommonModule } from '@angular/common';
import { NbButtonModule, NbIconModule, NbStepperModule, NbCardModule, NbSelectModule, NbDialogModule } from '@nebular/theme';
import { VisitorComponent } from './visitor.component';
import { VisitorTestComponent } from './test/test.component';



@NgModule({
  declarations: [VisitorComponent, VisitorTestComponent],
  imports: [
    CommonModule,
    ThemeModule,
    SharedModule,
    NbButtonModule,
    NbIconModule,
    NbStepperModule,
    NbCardModule,
    NbSelectModule,
    VisitorRoutingModule,
    NbDialogModule.forChild(),
  ],
})
export class VisitorModule { }
