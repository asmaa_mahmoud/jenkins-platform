import Phaser from 'phaser'
import GameScene from './scenes/Game'
import dimensionHelper from './dimensionHelper';
import StartScene from './scenes/Start';
import WinScene from './scenes/win';
import LoseScene from './scenes/lose';

export default class Game extends Phaser.Game {
  constructor(code, config, container) {
    config.builder.cell_size = 67;
    let builder_config = config.builder;
    window.dimensionHelper = new dimensionHelper({ config });
    const gameConfig = Object.assign(
      {
        type: Phaser.CANVAS,
        parent: container,
        width: window.dimensionHelper.canvas_width,
        height: window.dimensionHelper.canvas_height,
        physics: {
          default: 'arcade',
          arcade: { debug: false }
        },
        localStorageName: 'phaseres6webpack'
      },
      {
        scene: [new StartScene(config.userName, config.gameName), new GameScene(code, config), new LoseScene(), new WinScene()]
      }
    )

    super(gameConfig)
  }
}