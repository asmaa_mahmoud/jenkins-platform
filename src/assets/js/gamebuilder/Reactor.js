class CustomEvent {
  constructor(name) {
    this.name = name;
    this.callbacks = [];
  }

  registerCallback(callback) {
    this.callbacks.push(callback);
  }
}

export default class Reactor {
  constructor() {
    this.events = {};
  }

  registerEvent(eventName) {
    let event = new CustomEvent(eventName);
    this.events[eventName] = event;
  }

  dispatchEvent(eventName) {
    this.events[eventName].callbacks.forEach((callback) => callback());
  }

  addEventListener(eventName, callback) {
    //console.log(eventName);
    this.events[eventName].registerCallback(callback);
  }
}