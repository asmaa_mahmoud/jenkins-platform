import Phaser from 'phaser'
export default class extends Phaser.GameObjects.Rectangle {
  constructor ({ scene, x, y,color,alpha=1}) {
    super(scene, window.dimensionHelper.X(x), window.dimensionHelper.Y(y),window.dimensionHelper.cell_size,window.dimensionHelper.cell_size,color,alpha);
    this.depth=-1;
    this.setBlendMode(Phaser.BlendModes.COLOR_BURN);
    scene.add.existing(this);
  }
}
