import Cell from '../sprites/Cell';
import ColoredCell from '../api/ColoredCell';

export default class GameBuilder {
  constructor({ scene, objectManger, config, reactor, robo }) {
    this.scene = scene;
    this.reactor = reactor;
    this.objectManger = objectManger;
    this.robo = robo;
    this.levels = config.levels;
    this.board = this._create2DBoard(window.dimensionHelper.width, window.dimensionHelper.height);

    this.hash = 'c2d9d824b1cb450fe40bb8bdaec9bfdd'
    this.objectNameCounter = 1;
    this.current_level = 0;
  }

  getCellColor() {
    let xy = this.getXYBoard(this.robo.robo.x, this.robo.robo.y);
    return this.board[xy[0] - 1][xy[1] - 1].config.color;
  }

  getXYBoard(x, y) {
    return window.dimensionHelper.getXYBoard(x, y);
  }

  setCellText(x, y, text) {
    let cell = this.board[x - 1][y - 1];
    cell.text = text;
    if (cell.textObj) cell.textObj.destroy();
    cell.textObj = this.scene.add.text(window.dimensionHelper.XCellText(x), window.dimensionHelper.YCellText(y), text, { fontSize: '48px', fontWeight: 'bold', fill: '#ffffff' });
    cell.textObj.depth = -1;
  }

  setCellColor(x, y, color) {
    let cell = this.board[x - 1][y - 1];
    cell.sprite.setTexture(`cells[${color}]`);
  }

  getCellText() {
    let xy = this.getXYBoard(this.robo.robo.x, this.robo.robo.y);
    let cell = this.board[xy[0] - 1][xy[1] - 1];
    return cell.text ? cell.text : '';
  }

  create() {
    this.loadLevel(0);
  }


  getCurrentLevel() {
    return this.current_level + 1;
  }

  next() {
    this.destroy();
    this.loadLevel(this._nextLevel());
  }

  goToLevel(it) {
    this.destroy();
    this.loadLevel(this._getLevel(it));
  }

  loadLevel(level) {
    this.loadLevelConfig(level);
    this.draw();
  }

  loadLevelConfig(level) {
    let levelConfigs = this.levels[level];
    levelConfigs.colors.forEach((color) => {
      this.board[color.x - 1][color.y - 1].config.asset = `cells[${color.color}]`;
      this.board[color.x - 1][color.y - 1].config.color = color.color;
    });
    levelConfigs.objects.forEach((object) => {
      this.board[object.x - 1][object.y - 1].config.objects.push({ type: object.type, name: this._newName(), x: object.x, y: object.y });
    });
  }

  draw() {
    this.board.forEach((col) => {
      col.forEach((cell, index) => {
        if (cell.config.asset != 'cells[hidden]') this._drawCell(cell)
      })
    })
  }

  destroy() {
    for (let i = 0; i < window.dimensionHelper.width; i++) {
      for (let j = 0; j < window.dimensionHelper.height; j++) {
        let cell = this.board[i][j];
        if (cell.sprite) cell.sprite.destroy();
        if (cell.colored) cell.colored.destroy();
        cell.objects.forEach((object) => {
          this.objectManger.removeObject(object.name);
        });

        this.board[i][j] = this._freshCell(cell.config.x, cell.config.y);
      }
    }
  }

  _drawCell(cell) {

    cell.sprite = new Cell({ scene: this.scene, x: cell.config.x, y: cell.config.y, asset: cell.config.asset });
    cell.config.objects.forEach((object) => {
      cell.objects.push(this.objectManger.addObjectAt(object.name, object.type, object.x, object.y));
    });
  }

  _create2DBoard(width, height) {
    let array = new Array(width);
    for (let i = 0; i < width; i++) {
      array[i] = new Array(height);
      for (let j = 0; j < height; j++) {
        array[i][j] = this._freshCell(i + 1, j + 1);
      }
    }
    return array;
  }

  _freshCell(x, y) {
    return {
      config: {
        x: x,
        y: y,
        asset: 'cells[default]',
        objects: [],
        color: 'default'
      },
      text: null,
      textObj: null,
      sprite: null,
      objects: []
    }
  }

  _nextLevel() {
    this.current_level++;
    if (this.current_level >= this.levels.length) {
      this.current_level = 0;
    }
    return this.current_level;
  }
  _getLevel(it) {
    if (it == 'previous') {
      if (this.current_level > 0) {
        this.current_level--;
      }
    } else if (it == 'next') {
      if (this.current_level < this.levels.length - 1) {
        this.current_level++;
      }
    } else if (it == 'last') {
      this.current_level = this.levels.length - 1;
    } else {
      this.current_level = 0;
    }
    return this.current_level;
  }
  _newName() {
    return `${this.hash}_${this.objectNameCounter++}`;
  }

}