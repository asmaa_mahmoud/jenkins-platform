import GameControl from '../sprites/GameControl';

export default class GamePad {
  constructor({ scene, reactor }) {
    this.scene = scene;
    this.reactor = reactor
    this.controls = {};
  }

  create() {
    const width = window.dimensionHelper.canvas_width;
    const height = window.dimensionHelper.canvas_height;
    const ratio_x = width / 270;
    const ratio_y = height / 480;
    const img = this.scene.add.image(167 * ratio_x, 370 * ratio_y, 'gamepad[base]');
    img.alpha = 0.6;
    img.scaleX = ratio_x;
    img.scaleY = ratio_y;
    img.setOrigin(0);
    this.controls.up = this._createControl('up', 187 * ratio_x, 355 * ratio_y);
    this.controls.down = this._createControl('down', 187 * ratio_x, 425 * ratio_y);
    this.controls.left = this._createControl('left', 152 * ratio_x, 390 * ratio_y);
    this.controls.right = this._createControl('right', 222 * ratio_x, 390 * ratio_y);
    this.controls.w = this._createControl('w', 48 * ratio_x, 361 * ratio_y);
    this.controls.a = this._createControl('a', 26 * ratio_x, 405 * ratio_y);
    this.controls.d = this._createControl('d', 71 * ratio_x, 406 * ratio_y);
    window.x = this.controls.x;
    //
    this.reactor.registerEvent('joystick-up-pressed');
    this.reactor.registerEvent('joystick-down-pressed');
    this.reactor.registerEvent('joystick-left-pressed');
    this.reactor.registerEvent('joystick-right-pressed');
    this.reactor.registerEvent('up-pressed');
    this.reactor.registerEvent('left-pressed');
    this.reactor.registerEvent('right-pressed');

    this.scene.input.on('gameobjectdown', (pointer, gameControl) => {
      if (gameControl.name == 'up') {
        this.reactor.dispatchEvent('joystick-up-pressed');
      }
      if (gameControl.name == 'down') {
        this.reactor.dispatchEvent('joystick-down-pressed');
      }
      if (gameControl.name == 'left') {
        this.reactor.dispatchEvent('joystick-left-pressed');
      }
      if (gameControl.name == 'right') {
        this.reactor.dispatchEvent('joystick-right-pressed');
      }
      if (gameControl.name == 'w') {
        this.reactor.dispatchEvent('up-pressed');
      }
      if (gameControl.name == 'd') {
        this.reactor.dispatchEvent('right-pressed');
      }
      if (gameControl.name == 'a') {
        this.reactor.dispatchEvent('left-pressed');
      }
    });
  }

  hide(name) {
    this.controls[name].setVisible(false);
  }
  show(name) {
    this.controls[name].setVisible(true);
  }
  _createControl(name, x, y) {
    let gameControl = new GameControl({
      scene: this.scene,
      name: name,
      x: x,
      y: y,
      asset: `gamepad[${name}]`
    });
    // FIXME Remove me
    // gameControl.displayWidth = 40
    // gameControl.scaleY = gameControl.scaleX = 0.8;
    ////////////////////////////////
    gameControl.setOrigin(0);
    gameControl.setInteractive();
    gameControl.on('pointerdown', () => {
      gameControl.setTexture(`gamepad[${name}Clicked]`);
    });
    gameControl.on('pointerup', () => {
      gameControl.setTexture(`gamepad[${name}]`);
    });
    this.scene.add.existing(gameControl);
    return gameControl;
  }
}