export default class Lives {
  constructor({ scene }) {
    this.scene = scene;
    this.lives = 0;
  }

  create() {
    let graphics = this.scene.add.graphics();
    const width = window.dimensionHelper.canvas_width;
    const height = window.dimensionHelper.canvas_height;
    const ratio_x = width / 270;
    const ratio_y = height / 480;
    // graphics.fillStyle(0xF3F3F3, 0.53);
    // graphics.fillRoundedRect(160 * ratio_x, 12 * ratio_y, 98 * ratio_x, 42 * ratio_y, 20 * ratio_x);
    // graphics.fillStyle(0xF3F3F3, 1);
    // graphics.fillRoundedRect(210 * ratio_x, 18 * ratio_y, 40 * ratio_x, 30 * ratio_y, 15 * ratio_x);
    this.scoreText = this.scene.add.text(177 * ratio_x, 8 * ratio_y, '', { fontWeight:"bold" ,fontFamily: 'Dosis',fontSize: 18 * ratio_x, fill: '#FF623A' });
    this.scoreText.depth = 2;
    const bg = this.scene.add.image(151 * ratio_x, 6 * ratio_y, 'lives').setOrigin(0, 0);
    bg.scaleX = ratio_x;
    bg.scaleY = ratio_y;
    this.draw();
  }

  set(lives) {
    this.lives = lives>=0?lives:0;
    this.draw();
  }

  get() {
    return this.lives;
  }

  draw() {
    this.scoreText.setText(this.lives);
  }

}