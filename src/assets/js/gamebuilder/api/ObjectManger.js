export default class ObjectManger {
  constructor({ scene, config }) {
    this.scene = scene;
    this.objects = [];
    this.objectsEvents = [];
    this.robo = null;
    this.objectsCounter = 0;
  }

  addObjectAt(name, type, x, y) {
    let newObject = this.scene.physics.add.sprite(window.dimensionHelper.X(x), window.dimensionHelper.Y(y), `objects[${type}]`);
    if (name === "object_name") {
      name += this.objectsCounter++;
    }
    newObject.name = name;
    newObject.type = type;
    newObject.cb = null;
    newObject.position = { x: x, y: y };
    this.objects.push(newObject);
    this.scene.add.existing(newObject);
    this.addEvents(newObject);
    return newObject;
  }

  addEvents(obj) {
    if (this.objectsEvents.length > 0) {
      for (var i = 0; i < this.objectsEvents.length; i++) {
        if (this.objectsEvents[i][0] == obj.type) {
          obj.cb = true;
          this.scene.physics.add.collider(this.robo.robo, obj, this.objectsEvents[i][1], null, this.scene);
        }
      }
    }
  }

  addMovableObjectAt(name, type, x, y, speed) {
    newObject = this.addObjectAt(name, type, x, y);
    newObject.setVelocityY(speed);
    return newObject;
  }

  moveObjectTo(name, x, y) {
    let object = this.getObjectByName(name);
    object.x = window.dimensionHelper.X(x);
    object.y = window.dimensionHelper.X(y);
  }

  getObjectByName(name) {
    for (let i = 0; i < this.objects.length; i++) {
      if (this.objects[i].name == name) {
        return this.objects[i];
      }
    }
  }

  getAllObjectsWithNameAndType(name, type) {
    return this.objects.filter(obj => obj.name === name && obj.type === type);
  }

  // move all objects with certain name and type in direction 
  moveAllObjectsInDirection(direction, name, type) {
    const objects = this.getAllObjectsWithNameAndType(name, type);
    objects.forEach(obj => {
      this.moveObjectInDirection(direction, obj);
    })
  }

  moveObjectInDirection(direction, object) {
    let x = window.dimensionHelper.getXYBoard(object.x, object.y)[0];
    let y = window.dimensionHelper.getXYBoard(object.x, object.y)[1];
    if (direction === 'up') {
      y === 1 ? y : y--;
    }
    else if (direction === 'right') {
      x === window.dimensionHelper.width ? x : x++;
    }
    else if (direction === 'down') {
      y === window.dimensionHelper.height ? y : y++;
    }
    else if (direction === 'left') {
      x === 1 ? x : x--;
    }
    object.x = window.dimensionHelper.X(x);
    object.y = window.dimensionHelper.X(y);
  }

  incrementObjectPositionBy(name, x, y) {
    let object = this.getObjectByName(name);
    object.x += (x * window.dimensionHelper.Step());
    object.y += (-1 * y * window.dimensionHelper.Step());
  }

  removeObject(name) {
    // let object = this.getObjectByName(name);
    // if (object) object.destroy();
    for (let i = 0; i < this.objects.length; i++) {
      if (this.objects[i].name == name) {
        this.objects[i].destroy();
      }
    }
  }

  removeObjectsWithType(type) {
    for (let i = 0; i < this.objects.length; i++) {
      if (this.objects[i].type == type) this.objects[i].destroy();
    }
  }
  getObjectsByType(type) {
    let objects = this.objects.filter((object) => {
      return object.type == type;
    });
    return objects;
  }

  getObject(name) {
    return this.getObjectByName(name);
  }

}