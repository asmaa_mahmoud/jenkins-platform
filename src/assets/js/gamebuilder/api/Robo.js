
export default class Robo {
  constructor({ scene, x, y, objectManger }) {
    this.scene = scene;
    this.objectManger = objectManger;
    this.x = x;
    this.y = y;
    this.robo = {};
    this.message = null;
  }

  create() {
    this._createRobo(this.x, this.y);
  }

  getRoboXY() {
    return window.dimensionHelper.getXYBoard(this.robo.x, this.robo.y);
  }

  getRoboPosition(which) {
    let pos = this.getRoboXY();
    return which == 'x' ? pos[0] : pos[1];
  }

  swapLeft() {
    let xy = this.getRoboXY();
    if (xy[0] > 1) {
      this.robo.x -= window.dimensionHelper.Step() * window.dimensionHelper.ratioX;
    }
  }

  swapRight() {
    let xy = this.getRoboXY();
    if (xy[0] < window.dimensionHelper.width) {
      this.robo.x += window.dimensionHelper.Step() * window.dimensionHelper.ratioX;
    }
  }

  rotateRight() {
    this.robo.angle += 90;
  }

  rotateLeft() {
    this.robo.angle -= 90;
  }

  stepForward() {
    let xy = this.getRoboXY();
    if (xy[1] > 1) {
      this.robo.x -= Math.sin(this.robo.angle * Math.PI / 180) * window.dimensionHelper.Step() * window.dimensionHelper.ratioX;
      this.robo.y -= Math.cos(this.robo.angle * Math.PI / 180) * window.dimensionHelper.Step() * window.dimensionHelper.ratioY;
    }

  }

  stepBackward() {
    let xy = this.getRoboXY();
    if (xy[1] < window.dimensionHelper.height) {
      this.robo.x += Math.sin(this.robo.angle * Math.PI / 180) * window.dimensionHelper.Step() * window.dimensionHelper.ratioX;
      this.robo.y += Math.cos(this.robo.angle * Math.PI / 180) * window.dimensionHelper.Step() * window.dimensionHelper.ratioY;
    }
  }

  moveTo(x, y) {
    this.robo.x = window.dimensionHelper.X(x);
    this.robo.y = window.dimensionHelper.Y(y);
  }

  onTouch(name, callback) {
    this.scene.physics.add.collider(this.robo, this.objectManger.getObject(name), callback, null, this.scene);
  }

  onTouchWithType(type, callback) {
    if (!this.objectManger.robo) this.objectManger.robo = this;
    this.objectManger.objectsEvents.push([type, callback]);
    let objects = this.objectManger.getObjectsByType(type);
    for (let i = 0; i < objects.length; i++) {
      objects[i].cb = true;
      this.scene.physics.add.collider(this.robo, objects[i], callback, null, this.scene);
    }
  }
  say(quote, height = 100, width = 100, fontSize = 15) {
    if (this.message) {
      this.message.bubble.destroy();
      this.message.content.destroy();
      this.message = null;
    }
    quote = quote.toString();
    console.log(quote);
    const x = this.robo.x;
    const y = this.robo.y;
    const roboHeight = this.robo.height;
    const roboWidth = this.robo.width;
    let position = 'upRight';
    if ((y - (.5 * roboHeight) - height) > 0) {
      if (x + width > 270) {
        position = 'upLeft';
        this._createSpeechBubble(x - width, y - (.5 * roboHeight) - height, width, height, quote, fontSize, position);
      }
      else {
        this._createSpeechBubble(x, y - (.5 * roboHeight) - height, width, height, quote, fontSize, position);
      }
    }
    else {
      if (x + width > 270) {
        position = 'left';
        this._createSpeechBubble(x - (.5 * roboWidth) - width, y - (.5 * roboHeight) + (height / 10), width, height, quote, fontSize, position);
      }
      else {
        position = 'right';
        this._createSpeechBubble(x + (.5 * roboWidth), y - (.5 * roboHeight) + (height / 10), width, height, quote, fontSize, position);
      }
    }
  }

  _createRobo(x, y) {
    this.robo = this.scene.physics.add.sprite(window.dimensionHelper.X(x), window.dimensionHelper.Y(y), 'robo');
    //this.robo.angle -= 90;
    const ratio_x = window.dimensionHelper.canvas_width / 270;
    const ratio_y = window.dimensionHelper.canvas_height / 480;
    this.robo.scale = window.dimensionHelper.width == 4 ? 0.7 * ratio_x : window.dimensionHelper.width == 5 ? 0.5 * ratio_x : 0.45 * ratio_x;
    this.robo.setCollideWorldBounds(true);
  }

  _createSpeechBubble(x, y, width, height, quote, fontSize, position) {
    var bubbleWidth = width;
    var bubbleHeight = height;
    var bubblePadding = 10;
    var arrowHeight = bubbleHeight / 4;
    var bubble = this.scene.add.graphics({ x: x, y: y });
    bubble.depth = 3;
    //  Bubble shadow
    // bubble.fillStyle(0x222222, 0.5);
    // bubble.fillRoundedRect(6, 6, bubbleWidth, bubbleHeight, 16);

    //  Bubble color
    bubble.fillStyle(0xffffff, 1);

    //  Bubble outline line style
    bubble.lineStyle(4, 0x565656, 1);

    //  Bubble shape and outline
    bubble.strokeRoundedRect(0, 0, bubbleWidth, bubbleHeight, 16);
    bubble.fillRoundedRect(0, 0, bubbleWidth, bubbleHeight, 16);

    //  Calculate arrow coordinates
    // *******************
    if (position === 'upRight') {
      var point1X = Math.floor(bubbleWidth / 7);
      var point1Y = bubbleHeight;
      var point2X = Math.floor((bubbleWidth / 7) * 2);
      var point2Y = bubbleHeight;
      var point3X = Math.floor(bubbleWidth / 7);
      var point3Y = Math.floor(bubbleHeight + arrowHeight);
    }
    else if (position === 'upLeft') {
      var point1X = Math.floor((bubbleWidth / 7) * 5);
      var point1Y = bubbleHeight;
      var point2X = Math.floor((bubbleWidth / 7) * 6);
      var point2Y = bubbleHeight;
      var point3X = Math.floor((bubbleWidth / 7) * 6);
      var point3Y = Math.floor(bubbleHeight + arrowHeight);
    }
    else if (position === 'right') {
      var point1X = 0;
      var point1Y = Math.floor(bubbleWidth / 7);
      var point2X = 0;
      var point2Y = Math.floor((bubbleWidth / 7) * 2);
      var point3X = Math.floor(-arrowHeight);
      var point3Y = Math.floor(bubbleWidth / 7);
    }
    else if (position === 'left') {
      var point1X = bubbleWidth;
      var point1Y = Math.floor(bubbleWidth / 7);
      var point2X = bubbleWidth;
      var point2Y = Math.floor((bubbleWidth / 7) * 2);
      var point3X = Math.floor(bubbleWidth + arrowHeight);
      var point3Y = Math.floor(bubbleWidth / 7);
    }

    //  Bubble arrow shadow
    // bubble.lineStyle(4, 0x222222, 0.5);
    // bubble.lineBetween(point2X - 1, point2Y + 6, point3X + 2, point3Y);

    //  Bubble arrow fill
    bubble.fillTriangle(point1X, point1Y, point2X, point2Y, point3X, point3Y);
    bubble.lineStyle(2, 0x565656, 1);
    bubble.lineBetween(point2X, point2Y, point3X, point3Y);
    bubble.lineBetween(point1X, point1Y, point3X, point3Y);

    var content = this.scene.add.text(0, 0, '', { fontFamily: 'Arial', fontSize, color: '#000000', align: 'center', wordWrap: { width: bubbleWidth - (bubblePadding * 2) } });
    content.depth = 3;
    let i = 0;
    let speed = 100;
    this.robo.depth = 2.5;
    const typeWriter = () => {
      if (i < quote.length) {
        content.setText(content.text + quote.charAt(i));
        var b = content.getBounds();
        content.setPosition(bubble.x + (bubbleWidth / 2) - (b.width / 2), bubble.y + (bubbleHeight / 2) - (b.height / 2));
        i++;
        setTimeout(typeWriter, speed);
      }
      else {
        setTimeout(() => {
          bubble.destroy();
          content.destroy();
          this.robo.depth = -1;
        }, 2500)
      }
    }
    typeWriter();
    this.message = { bubble, content };
  }
}