export default class Score {
  constructor({ scene }) {
    this.scene = scene;
    this.score = 0;
  }

  create() {
    let graphics = this.scene.add.graphics();
    const width = window.dimensionHelper.canvas_width;
    const height = window.dimensionHelper.canvas_height;
    const ratio_x = width / 270;
    const ratio_y = height / 480;
    // graphics.fillStyle(0xF3F3F3, 0.53);
    // graphics.fillRoundedRect(19 * ratio_x, 12 * ratio_y, 131 * ratio_x, 42 * ratio_y, 20 * ratio_x);
    // graphics.fillStyle(0xF3F3F3, 1);
    // graphics.fillRoundedRect(64 * ratio_x, 18 * ratio_y, 75 * ratio_x, 30 * ratio_y, 15 * ratio_y);
    this.scoreText = this.scene.add.text(95 * ratio_x, 8 * ratio_y, '', { fontWeight:"bold" ,fontFamily: 'Dosis',fontSize: ratio_x * 18 + 'px', fill: '#FFD557' });
    this.scoreText.depth = 2;
    const bg = this.scene.add.image(67 * ratio_x, 6 * ratio_y, 'score').setOrigin(0, 0);
    bg.scaleX = ratio_x;
    bg.scaleY = ratio_y;
    this.draw();
  }

  set(score) {
    this.score = score>=0?score:0;
    this.draw();
  }

  get() {
    return this.score;
  }

  draw() {
    this.scoreText.setText(this.score);
  }


}