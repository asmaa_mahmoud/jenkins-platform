export default class Timer{
  constructor ({ scene }) {
    this.scene = scene;
  }

  every(ms, callback){
    this.scene.time.addEvent({ delay: ms, callback: function(){
        callback();
    }, callbackScope: this.scene, loop: true });
  }
}