// import GameControl from '../sprites/GameControl';

// export default class GamePad {
//   constructor({ scene, reactor }) {
//     this.scene = scene;
//     this.reactor = reactor
//     this.controls = {};
//   }

//   create() {
//     const width = window.dimensionHelper.canvas_width;
//     const height = window.dimensionHelper.canvas_height;
//     const ratio_x = width / 270;
//     const ratio_y = height / 480;
//     const img = this.scene.add.image(204 * ratio_x, 407.5 * ratio_y, 'gamepad[base]');
//     img.alpha = 0.6;
//     img.scaleX = ratio_x;
//     img.scaleY = ratio_y;
//     this.controls.up = this._createControl('up', 204.5 * ratio_x, 372.5 * ratio_y);
//     this.controls.down = this._createControl('down', 204.5 * ratio_x, 442.5 * ratio_y);
//     this.controls.left = this._createControl('left', 169.5 * ratio_x, 407.5 * ratio_y);
//     this.controls.right = this._createControl('right', 240.5 * ratio_x, 407.5 * ratio_y);
//     //
//     this.reactor.registerEvent('joystick-up-pressed');
//     this.reactor.registerEvent('joystick-down-pressed');
//     this.reactor.registerEvent('joystick-left-pressed');
//     this.reactor.registerEvent('joystick-right-pressed');


//     this.scene.input.on('gameobjectdown', (pointer, gameControl) => {
//       if (gameControl.name == 'up') {
//         this.reactor.dispatchEvent('joystick-up-pressed');
//       }
//       if (gameControl.name == 'down') {
//         this.reactor.dispatchEvent('joystick-down-pressed');
//       }
//       if (gameControl.name == 'left') {
//         this.reactor.dispatchEvent('joystick-left-pressed');
//       }
//       if (gameControl.name == 'right') {
//         this.reactor.dispatchEvent('joystick-right-pressed');
//       }
//     });
//   }

//   hide(name) {
//     this.controls[name].setVisible(false);
//   }
//   show(name) {
//     this.controls[name].setVisible(true);
//   }
//   _createControl(name, x, y) {
//     let gameControl = new GameControl({
//       scene: this.scene,
//       name: name,
//       x: x,
//       y: y,
//       asset: `gamepad[${name}]`
//     });
//     // FIXME Remove me
//     // gameControl.displayWidth = 40
//     // gameControl.scaleY = gameControl.scaleX = 0.8;
//     ////////////////////////////////
//     gameControl.setInteractive();
//     gameControl.on('pointerdown', () => {
//       gameControl.setTexture(`gamepad[${name}Clicked]`);
//     });
//     gameControl.on('pointerup', () => {
//       gameControl.setTexture(`gamepad[${name}]`);
//     });
//     this.scene.add.existing(gameControl);
//     return gameControl;
//   }
// }