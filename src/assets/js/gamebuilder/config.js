export default {
  builder: {
    cell_size: 80,
    width: 7,
    height: 10
  },
  levels: [
    {
      objects:[
        {
          x: 1,
          y: 1,
          type: 'rock'
        },
        {
          x: 7,
          y: 10,
          type: 'rock'
        }
      ],
      colors:[
        {
          x: 1,
          y: 2,
          color: 'red'
        },
        {
          x: 1,
          y: 4,
          color: 'red'
        },
        {
          x: 1,
          y: 6,
          color: 'red'
        },
        {
          x: 1,
          y: 8,
          color: 'red'
        },
        {
          x: 1,
          y: 10,
          color: 'red'
        },
        {
          x: 7,
          y: 2,
          color: 'red'
        },
        {
          x: 7,
          y: 4,
          color: 'red'
        },
        {
          x: 7,
          y: 6,
          color: 'red'
        },
        {
          x: 7,
          y: 8,
          color: 'red'
        },
        {
          x: 7,
          y: 10,
          color: 'red'
        }
      ]
    },
    {
      objects:[
        {
          x: 7,
          y: 9,
          type: 'rock'
        }
      ],
      colors:[
        {
          x: 1,
          y: 1,
          color: 'red'
        },
        {
          x: 1,
          y: 3,
          color: 'red'
        },
        {
          x: 1,
          y: 5,
          color: 'red'
        },
        {
          x: 1,
          y: 7,
          color: 'red'
        },
        {
          x: 1,
          y: 9,
          color: 'red'
        },
        {
          x: 7,
          y: 1,
          color: 'red'
        },
        {
          x: 7,
          y: 3,
          color: 'red'
        },
        {
          x: 7,
          y: 5,
          color: 'red'
        },
        {
          x: 7,
          y: 7,
          color: 'red'
        },
        {
          x: 7,
          y: 9,
          color: 'red'
        }
      ]
    }
  ]
}