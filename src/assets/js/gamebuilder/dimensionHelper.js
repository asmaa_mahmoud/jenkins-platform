export default class DimensionHelper {
  constructor({ config }) {
    this.isOrg=config.isOrg;
    this.is_mobile = config.mobile ? true : false;
    this.cell_size = config.builder.cell_size;
    this.width = config.builder.width;
    this.height = config.builder.height;
    if (this.is_mobile) {
      this.ratioY = window.innerHeight / (this.height * this.cell_size);
      this.ratioX = window.innerWidth / (this.width * this.cell_size);
      this.canvas_width = window.innerWidth;
      this.canvas_height = window.innerHeight;
    } else {
      this.ratioY = 480 / (this.height * this.cell_size);
      this.ratioX = 270 / (this.width * this.cell_size);
      this.canvas_width = 270;
      this.canvas_height = 480;
    }
  }

  X(x) {
    return (((x - 1) * this.cell_size) + (this.cell_size / 2)) * this.ratioX;
  }

  Y(y) {
    return (((y - 1) * this.cell_size) + (this.cell_size / 2)) * this.ratioY;
  }

  XCellText(x){
    return this.X(x)-15;
  }

  YCellText(y){
    return this.width==4?this.Y(y)-15:this.width==5?this.Y(y)-20:this.Y(y)-25;
  }

  getXYBoard(x, y) {
    return [Math.ceil(x / (67 * this.ratioX)), Math.ceil(y / (67 * this.ratioY))];
  }


  Step() {
    return this.cell_size;
  }
}  