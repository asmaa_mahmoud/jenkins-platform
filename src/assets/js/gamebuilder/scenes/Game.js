/* globals __DEV__ */
import Phaser from 'phaser'

import images from '../images';
import Robo from '../api/Robo';
import Reactor from '../Reactor';
import GamePad from '../api/GamePad';
import GameBuilder from '../api/GameBuilder';
import ObjectManger from '../api/ObjectManger';
import Timer from '../api/Timer';
import Score from '../api/Score';
import Lives from '../api/Lives';
import JoyStick from '../api/joystick';

export default class extends Phaser.Scene {
  constructor(code, config) {
    super({ key: 'GameScene' })
    this.reactor = new Reactor();
    this.config = config;
    this.code = code;
    // this.joystick = new JoyStick({ scene: this, reactor: this.reactor });
    this.timer = new Timer({ scene: this });
    this.gamePad = new GamePad({ scene: this, reactor: this.reactor });
    this.objectManger = new ObjectManger({ scene: this });
    this.robo = new Robo({ scene: this, x: 1, y: 7, objectManger: this.objectManger });
    this.gameBuilder = new GameBuilder({ scene: this, objectManger: this.objectManger, config: config, reactor: this.reactor, robo: this.robo });
    window.gameBuilder = this.gameBuilder;
    window.gamePad = this.gamePad;
    this.score = new Score({ scene: this });
    this.lives = new Lives({ scene: this });
  }

  preload() {
    // load robo
    this.load.image('robo', images.robo);
    // load lives
    this.load.image('lives', images.lives);
    // load score
    this.load.image('score', images.score);

    this.load.image('score_lives_bg',images.score_lives_bg);
    // load Gamepad 
    for (var property in images.gamepad) {
      this.load.image(`gamepad[${property}]`, images.gamepad[property]);
    }
    // load cells
    for (var property in images.cells) {
      this.load.image(`cells[${property}]`, images.cells[property]);
    }
    // load objects
    for (var property in images.objects[this.config.builder.width]) {
      this.load.image(`objects[${property}]`, images.objects[this.config.builder.width][property]);
    }
  }

  goToScene(name) {
    this.scene.switch(name);
  }

  createScoreLivesBG(){
    const width = window.dimensionHelper.canvas_width;
    const height = window.dimensionHelper.canvas_height;
    const ratio_x = width / 270;
    const ratio_y = height / 480;
    const background = this.add.image(8 * ratio_x, 0, 'score_lives_bg').setOrigin(0, 0);
    background.scaleX = ratio_x;
    background.scaleY = ratio_y;
  }
  create() {
    this.reactor.registerEvent('on_start');
    this.robo.create();
    this.gamePad.create();
    // this.joystick.create();
    this.gameBuilder.create();
    this.createScoreLivesBG();
    this.score.create();
    this.lives.create();

    //  user code 
    //////////////////////////////////
    eval(this.code);
    this.reactor.dispatchEvent('on_start');
  }
  update() {

  }

}
