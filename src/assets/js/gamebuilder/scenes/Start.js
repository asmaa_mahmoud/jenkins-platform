/* globals __DEV__ */
import Phaser from 'phaser'

export default class extends Phaser.Scene {
    constructor(userName, gameName) {
        super({ key: 'StartScene' })
        this.userName = userName;
        this.gameName = gameName;
        this.gameNameText = '';
    }

    preload() {
        this.load.image('bg', './assets/images/scenes/start/bg.jpg');
        this.load.image('play', './assets/images/scenes/start/play_button.png');
        this.load.image('bg_codegoat','./assets/images/scenes/start/bg_codegoat.jpg');
    }

    create() {
        const ratio_x = window.dimensionHelper.canvas_width / 270;
        const ratio_y = window.dimensionHelper.canvas_height / 480;
        if(window.dimensionHelper.isOrg){
            const bg_codegoat = this.add.image(0, 0, 'bg_codegoat').setOrigin(0, 0);
            bg_codegoat.scaleX = ratio_x;
            bg_codegoat.scaleY = ratio_y;
        }else{
            const bg = this.add.image(0, 0, 'bg').setOrigin(0, 0);
            bg.scaleX = ratio_x;
            bg.scaleY = ratio_y;
        }
       
        this.addGameNameText();
        this.add.text(65 * ratio_x, 185 * ratio_y, 'By: ' + this.userName, { fontFamily: 'Dosis', fontSize: 20 * ratio_x, fontStyle: 'bold', wordWrap: { width: 145 * ratio_x, useAdvancedWrap: true } });
        const playButton = this.add.image(135 * ratio_x, 340 * ratio_y, 'play');
        playButton.setInteractive();
        playButton.on('pointerover', () => { });
        playButton.on('pointerdown', () => {
            this.scene.switch('GameScene');

        });
    }

    addGameNameText() {
        const ratio_x = window.dimensionHelper.canvas_width / 270;
        const ratio_y = window.dimensionHelper.canvas_height / 480;
        const gameName = this.add.text(0, 30 * ratio_y, this.gameName, { fontFamily: 'Bungee', align: 'center', fontSize: 30 * ratio_x, wordWrap: { width: 230 * ratio_x, useAdvancedWrap: true } });
        // coordinates calculation
        this.centerText(gameName);
        this.gameNameText = gameName;
        window.x = this.gameNameText;
    }

    centerText(text) {
        let content = text.text;
        if(content.length>30){
            content = content.slice(0, 27);
            content += '...';   
        }
        text.setText(content);
        const bounds = text.getBounds();
        const x = (270 - bounds.width) * 0.5;
        text.setX(x);
        return text;
    }

    setGameNameText(name) {
        this.gameNameText.setText(name);
        this.centerText(this.gameNameText);
    }

    update() {

    }

}
