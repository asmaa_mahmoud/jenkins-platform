/* globals __DEV__ */
import Phaser from 'phaser'
import GameScene from './Game';
export default class extends Phaser.Scene {
    constructor() {
        super({ key: 'WinScene' })
    }

    preload() {
        this.load.image('bg_win', './assets/images/scenes/win/bg.jpg');
        this.load.image('play_win', './assets/images/scenes/win/play_again.png');
    }

    create() {
        const ratio_x = window.dimensionHelper.canvas_width / 270;
        const ratio_y = window.dimensionHelper.canvas_height / 480;
        const bg = this.add.image(0, 0, 'bg_win').setOrigin(0, 0);
        bg.scaleX = ratio_x;
        bg.scaleY = ratio_y;
        const playButton = this.add.image(135 * ratio_x, 365 * ratio_y, 'play_win');
        playButton.setInteractive();
        playButton.on('pointerover', () => {});
        playButton.on('pointerdown', () => {
            const gameScene = this.scene.get('GameScene');
            this.scene.remove('GameScene');
            const game = new GameScene(gameScene.code, gameScene.config)
            this.scene.add('GameScene', game);
            this.scene.switch('GameScene');
        });
    }

    update() {

    }

}
