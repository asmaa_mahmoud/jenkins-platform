import Phaser from 'phaser'
export default class extends Phaser.GameObjects.Sprite {
  constructor ({ scene, x, y, asset}) {
    super(scene, window.dimensionHelper.X(x), window.dimensionHelper.Y(y), asset);
    this.depth = -1;
    this.scaleX=window.dimensionHelper.ratioX;
    this.scaleY=window.dimensionHelper.ratioY;
    scene.add.existing(this);
  }
}
