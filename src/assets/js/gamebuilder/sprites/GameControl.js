import Phaser from 'phaser'

export default class extends Phaser.GameObjects.Sprite {
  constructor({ scene, name, x, y, asset }) {
    super(scene, x, y, asset);
    this.name = name;
    this.depth = 1;
    this.setInteractive();
    this.scaleX = window.dimensionHelper.canvas_width / 270;
    this.scaleY = window.dimensionHelper.canvas_height / 480;

  }
}
