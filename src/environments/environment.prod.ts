/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  apiUrlPrefix: 'https://backend.robogarden.ca/en/',
  pusherKey: '8156f9a1755222c08f0e',
  cluster: 'mt1',
  channelPrefix: 'private-build-apk-channel-',
  stripeKey: 'pk_test_2siQNqmyIG3t7fddTPbZHoyf00ySZFd8Yb',

};
